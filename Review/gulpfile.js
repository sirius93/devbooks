var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var sourcemaps = require('gulp-sourcemaps');
var browserify = require("browserify");
var babelify = require("babelify");
var browserSync = require("browser-sync");

gulp.task('browserify', function () {
    return gulp.src('src/es6/index.js')
        .pipe(sourcemaps.init())
        .pipe($.browserify({transform:babelify}))
        .pipe(sourcemaps.write('../maps'))
        .pipe(gulp.dest('dist'))
});

gulp.task('watch', ['browserify'], function () {
    gulp.watch('src/es6/*.js', ['browserify']);
});

gulp.task('js-watch', ['browserify'], browserSync.reload);

gulp.task('serve', ['browserify'], function () {
    browserSync({
        server: {
            baseDir: "./"
        }
    });

    gulp.watch("src/es6/*.js", ['js-watch']);
});