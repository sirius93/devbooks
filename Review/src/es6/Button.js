import {attach,identity} from './utils';

class Button {
    constructor(props){
        this.props=props;
    }
    render(){
        let {id,label,icon='',opt='',classnames='easyui-linkbutton',styles='',disabled=false}=this.props||{};
        if(disabled){
            opt+='disabled:true,';
            styles+='pointer-events: none;';
        }
        const identitis=identity(this.props);
        return (
            `<a ${identitis}
                href="#"
                icon="${icon}"
                class="${classnames}"
                style="${styles}"
                data-options="${opt||''}">
                ${label||''}
            </a>`
        );
    }
    bindEvent(self,props){
        const {id, ref, click, submit}=props;
        if(click && submit){
            console.error("警告，一个按钮上同时设置了单击事件和提交事件");
        }
        if(submit){
            let {target, url, validate=true, success=()=>{}}= submit;
            target = target || id || ref;
            $(document).delegate(id,'click',()=>{
                let self = $('#' + target);
                $(`#${target}`).form("submit", {
                    url,
                    onSubmit:()=> {
                        if(!validate){
                            return true;
                        }
                        return self.form('validate');
                    },
                    onLoadError:()=>{
                        debugger
                    },
                    success
                });
            });
        }else{
            self.click(click);
        }
    }
}

Button.load=(pid,props)=>{
    const cmp=new Button(props);
    attach(cmp,pid,props);
    return cmp;
}

module.exports=Button;