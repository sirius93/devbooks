module.exports=(type)=>(props)=>{
    let {title, message, callback}=props;
    switch(type){
        case 'alert':
            $.messager.alert(title || '提示', message, 'info', callback);
            break;
        case 'error':
            $.messager.alert(title || '错误', message, 'error', callback);
            break;
        case 'warn':
            $.messager.alert(title || '警告', message, 'warning', callback);
            break;
        case 'confirm':
            $.messager.confirm(title || '确认', message,  callback);
            break;
    }
};