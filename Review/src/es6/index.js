import Button from './Button';
import Select from './Select';
import Form from './Form';
import Message from './Message';

global.ky=module.exports={};
ky.button=Button.load;
ky.form=Form.load;
ky.select=Select.load;
ky.alert=Message('alert');
ky.warn=Message('warn');
ky.error=Message('error');
ky.confirm=Message('confirm');
 