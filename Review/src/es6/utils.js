const attach=(obj,pid,props)=>{
    const html=obj.render();

    if(pid){
        const parent=$(`#${pid}`);
        parent.html(html);
    }

    const {id}=props;
    const self=$(`#${id}`);
    if(obj.bindEvent){
        obj.bindEvent(self,props);
    }

    if(obj.afterAttach){
        obj.afterAttach(self);
    }

    if(pid){
        $.parser.parse(`#${pid}`);
    }
}

const identity=({id,name,ref})=>{
    let _id=id || ref || '';
    let _name=name || ref || '';
    return `id="${_id}" name="${_name}"`;
}



module.exports={
    attach,
    identity
}