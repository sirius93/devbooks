import {attach,identity} from './utils';

class Select {
    constructor(props){
        this.props=props;
    }
    render(){
        let {id,label,icon='',value, option='',datasource, classnames='easyui-combobox',styles='width:220px;',disabled=false}=this.props||{};
        const identitis=identity(this.props);
        let content = '';
        if(datasource){
            content=datasource.map(it=>{
                let selected = !!it.selected ? "selected" : "";
                if(value){
                    selected = it.value==value ? "selected" : "";
                }
                return `<option value="${it.value}" ${selected}>${it.label}</option>`;
            }).reduce((result, current)=>{
                return result + current;
            });
        }
        return (
            `<select ${identitis} class=${classnames} label="State:" labelPosition="left" style="${styles}">
                ${content}
            </select>
            `
        );
    }
    bindEvent(self,props){
        const {click}=props;
        self.click(click);
    }
    afterAttach(self){

    }
}

Select.load=(pid,props)=>{
    const cmp=new Select(props);
    attach(cmp,pid,props);
    return cmp;
}

module.exports=Select;