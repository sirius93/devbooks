import {attach, identity} from './utils';
import FormText from './FormText';
import Button from './Button';

class Form {
    constructor(props){
        this.props=props;
    }
    renderTd(it){
        let colspan='';
        if(it.colspan){
            colspan=`colspan="${it.colspan}"`
        }
        let content = new FormText(it).render();
        return (
            `<td ${colspan}>
                ${content}
            </td>`
        );
    }
    renderTr(columnsPerRow,ds,tds){
        let trs='';
        for(let i=0;i<ds.length;i++){
            const colspan=ds[i].colspan || 1;
            const col=i % columnsPerRow;
            if(col==9){
                trs+='<tr>';
            }
            trs+=tds[i];
            if(col+colspan>=columnsPerRow){
                trs+='</tr>';
            }
        }
        return trs;
    }
    renderFoot(buttons=[]){
        if(buttons.length==0){
            return '';
        }
        let content=buttons.map(it=>{
            const btn = new Button(it);
            const result = btn.render();
            const self=$(`#${it.id || it.ref}`);
            btn.bindEvent(self, it);
            return result;
        }).reduce((result,current)=>{
            return result + current;
        });
        return (`<tr><td>${content}</td></tr>`);
    }
    render(){
        let {id,value,ref, name, method, icon,opt,datasource,buttons, columnsPerRow=2}=this.props||{};
        const identitis=identity(this.props);
        const tds=datasource.map(it=>{
            return this.renderTd(it);
        })
        let tbody=this.renderTr(columnsPerRow, datasource, tds);
        let tfoot=this.renderFoot(buttons);
        return (
            `<form ${identitis} method="${method}">
                <table>
                    <tbody>
                        ${tbody}
                    </tbody>
                    <tfoot>
                        ${tfoot}
                    </tfoot>
                </table>
            </forM>`
        );
    }
}

Form.load=(pid,props)=>{
    const form=new Form(props);
    attach(form,pid,props)
}
  
module.exports=Form;