import {attach} from './utils';

class FormText{
    constructor(props){
        this.props=props;
    }
    render(){
        let {id,name,label,ref,value,icon,opt,rules}=this.props||{};
        id=id||ref||'';
        name=name || ref||'';
        let classnames='';
        if(rules){
            classnames='easyui-validatebox';
        }
        let labelTxt='';
        if(label){
            labelTxt=`<label for="${name}">${label || ''}</label>`;
        }
        return `<div>
                ${labelTxt}
                <input id="${id}"
                    name="${name}"
                    class="${classnames}"
                    data-options="required:true">
            </div>`;
    }
}

FormText.load=(pid,props)=>{
    const cmp=new FormText(props);
    attach(cmp, pid, props);
}
module.exports=FormText;