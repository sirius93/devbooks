require.config({
    shim: {
        'easyui': ['jquery'],
        'easyui_cn': ['easyui']
    },
    paths: {
        jquery: './plugins/jquery.min',
        easyui: './plugins/jquery.easyui.min',
        easyui_cn: './plugins/easyui-lang-zh_CN'
    }
});
