陆金所配置

lujinsuo config
#database
database.loginTimeout=3000
.checkoutTimeout=3000
.preferredTestQuery=select 1 from dual
.idleConnectionTestPeriod=10000
.testConnectionOnCheckout=true
.minPoolSize=5
.maxPoolSize=15
.initialPoolSize=1
.acquireIncrement=1
.acquireRetryAttempts=30
.acquireRetryDelay=1000
.maxIdleTime=25000
