[TOC]

# Oracle


## 使用

### 基本语法

#### 清屏

```
clear scr;
```

#### null

只要表达式中包含 null 值其结果就是 null

判断是否为空值

```
is null
is not null
```

#### 操作符

- `<>` 不等于

#### 字符串连接

字符串连接使用 `||`

#### 显示字符串

select 默认跟的为列名，如果需要指定字符串需要使用 `''` 包裹，如果在字符串中需要使用 `'`，则需要使用 `''''` 包裹。

```
select ename||'ename' from emp;
select ename||'ename ''as'' string' from emp;
```

#### 结果集去重

结果集去重使用 `distinct`。`distinct` 在 `select` 后使用，且仅执行一次。
如果 `select` 的列为多个时，则去重的是多个列的组合。

### Sql 函数

#### 单行函数

- `lower(colName)`，例：select lower(ename) from emp;
- `upper(colName)`
- `substr(colName, fromIndex, toIndex)`，例：james,2,4-->ames
- `chr(ascii)`
- `ascii(chr)`
- `round(n)`
- `round(n, 精度)`
- `to_char(colName, format)`
  - Format 格式
    - 9 a number
    - L locate money sign
    - 0 a number ,not exist display zero
    - YYYY year
    - MM
    - DD
    - HH
    - HH24
    - MI
    - SS
- `to_date()`
- `to_date(str, format)`
- `to_number(num, format)`
- `nvl(colName, val)` 用以处理空值

#### 组函数

特点：多个输入，一个输出

- `max(c)`
- `min(c)`
- `avg(c)`
- `sum(c)`

组函数可以嵌套一层，也最多一层，例：`max(avg(sal))`

### CRUD

#### count

- `count(*)` 获得总记录数
- `count(column)` 获得不为空的记录数

#### group by

在 select 后执行，使用 group by 后 select 语句只能出现 group by 使用的字段或者使用组函数的字段

#### having

出现在 where 中，过滤单条数据


#### 连接符

- `between and` 左右都是闭区间
- `in(...)` 相当于 or

#### join

sql1999 table connection

sql1992 连接条件放在 where 中

例

```sql
where e.deptno = d.deptno   //conn cond
and e.sal = d.sal     //conn cond
and job <>'CLERK'     //filter cond
```

sql1999 进行了分离，放在 statement 中

cross join

语法：`join <tableName> on <cond>`

```sql
select * from emp e join dept d on ..
join salgrade s on ...
where ...
```

外链接

left join 或 left outer join
left table 显示全部

full outer join

左表，右表都显示全部

#### 查询效率

比较效率

```sql
select * from emp where deptno=10 and ename like '%A%';
select * from emp where ename like '%A%' and deptno=10;
```

第一个快，数字不同就不用比较了

#### rollback

```sql
rollback;
```

#### alter

```sql
alter table <tableName> add(colName colType)
alert table <tableName> drop (colName)
alter table <tableName> modify(colName colType)
alter table <tableName> drop constraint conName
```

### 事务

事务指一系列行为要么同时发生，要么同时不发生
事务起始于 DML 语句，终止于 commit 或 DDL 和 DCL 语句或用户正常断开连接 (exit)
非正常时为回滚

### 表

#### 基本操作

显示表结构

```
desc <tableName>
```

#### 列别名

```
select sal * 12 annual_sal from emp;
```

如果别名中需要包含空格符，需要使用 `""` 进行包裹。

#### 创建表即删除表

```sql
create table tableName (colName colType...);
```

```sql
drop table tableName;
```

列格式

```
类型 [default 默认值]
```

number(长度[,精度]
varchar2(length)

如：age number(2) default 1


#### dual

`dual` 是 Oracle 中的特殊表，可以用于进行简单的计算

```
use table dual
select 2 * 3 from dual;
```

如果使用其它表的话，结果会被显示多次（表中的记录数）

`dual` 也可以用于显示日期

```
use column sysdate
select sysdate from dual
```

#### Sequence

```sql
create sequence seqName [start with n increment by step]
```

when insert,using seq.nextval.
select seq.nextval from dual;

#### 系统表

user_tables
user_views
user_constraints
user_indexes

例

```sql
select table_name from user_tables;
```

if you forget the system table name,you can
search in it other table dictionary.
dictionary

#### 备份表

```sql
create table target as select * from src;
insert data from other table(the structure of two talbe must be same);
insert into target select * from src;
```

#### rownum

oracle 专用，每个表中隐藏有这个字段

求前5行
select * from emp where rownum>=5;
rownum不能直接取第几行到第几行,也不能直接用=
如：rownum>=5 and rownum<=10
rownum=10
注：rownum发生在order by之前

### Indexes

```sql
create index indexName on tableName(colName...);
drop index indexName;
```

The speed of reading is faster after creating an index
The speed of writing is slower after creating an index


### View

```sql
create view name as select 语句
```

### 用户及权限

授权创建表

```sql
grant create table, create view to scott;
```

Unlock User

sqlplus sys/psw as sysdba
alter user scott account unlock;

创建用户

create new user and insert
oracle为每个用户分配自己的表空间，在里面有表的拷贝，
所以多用户时不会互相影响，但是不同机器用同一用户使用的是相同的表空间
先备份一下原来的表
cmd在需要将备份的文件放在哪里
-->exp
-->username/psw
-->buffersize
-->file
-->user or table
-->-->-->
创建用户及权限
create user name identified by psw default tablespace 表空间 quota 10M on 表空间
grant create session,create table,create view to name
引入
cmd在备份文件所在目录
-->imp
-->username/psw
--->--->-->...
-->the username that creates the import file
