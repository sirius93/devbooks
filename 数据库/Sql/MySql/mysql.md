目录

[TOC]

## 数据类型

DATETIME YYYY-MM-DD HH:MM:SS 格式时间
TIMESTAMP 从1973年开始的毫秒数

## 数据库操作

### 显示所有数据库

```sql
show databases;
```

### 创建数据库

```sql
create database <dbName>;
```

### 选择数据库

```sql
use <dbName>;
```

### 显示当前数据库内所有表名

```sql
show tables;
```

## 表操作

### 显示表结构

```sql
SHOW COLUMNS FROM <dbName>;
```

### 创建表

```sql
DROP TABLE IF EXISTS `t_toy`;
CREATE TABLE `t_toy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `price` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
```

- ENGINE=InnoDB 是 MySQL 的数据库引擎的设置
- AUTO_INCREMENT=2 是自动递增列的初始数值设为2

### 查询表

```sql
SELECT <distinct> <column_list>
FROM <table>
ORDER BY <column_list>
LIMIT <num|offset,num>;
```

limit 可以指定结果集条数，也可以指定偏移量表示从第几条开始一共显示几条

as 可以用于指定列的别名

### 表连接

表连接实际相对于创建临时表。

INNER JOIN

内连接取表之间的交集

```sql
FROM table1 JOIN table2
```

LEFT OUTER JOIN

左外连接返回左表中的所有数据

其中 `OUTER` 可以省略

```sql
FROM table1 LEFT OUTER JOIN table2
```

union

合并两个表的结果，返回不重复的数据

```sql
SELECT column_name(s) FROM table1
UNION
SELECT column_name(s) FROM table2;
```

union all

合并两个表

union 要求两个表的列数相同，如果列数不相同时可以使用 `null` 替代

```sql
SELECT FirstName, LastName, Company FROM businessContacts
UNION
SELECT FirstName, LastName, NULL FROM otherContacts;
```

## 函数

concat

```sql
SELECT CONCAT(FirstName, ', ' , City) FROM customers;
```

- upper(text)
- lower(text)
- sqrt(number)
- avg(number)
- sum(number)
- min(number)


## 索引

### 索引分类

- 普通索引
- 唯一索引
- 主键索引
- 组合索引
- 全文索引

### 建立索引

#### 主键索引

```sql
ALTER TABLE `table_name` ADD PRIMARY KEY ( `column` )
```

#### 唯一索引

```sql
CREATE UNIQUE INDEX index_name on `table_name`(`column`);
ALTER TABLE `table_name` ADD UNIQUE ( `column` )
```

#### 普通索引

```sql
ALTER TABLE `table_name` ADD INDEX index_name ( `column` )
```

全文索引

```sql
ALTER TABLE `table_name` ADD FULLTEXT ( `column`)
```

#### 复合索引

```sql
ALTER TABLE `table_name` ADD INDEX index_name ( `column1`, `column2`, `column3` )
```

### 显示索引

```sql
SHOW INDEX FROM `table_name`
```

### 建立索引的时机

- 一般来说，在 WHERE 和 JOIN 中出现的列需要建立索引。
- 索引适用于大表
- 如果需要访问表中大部分行时，那么顺序读取要比索引快得多。

### 索引的优缺点

#### 优点

- 提高查询速度。
不使用索引时，查找数据时必须一条条依次检索。
使用索引时，则可以根据索引跳转到指定位置，从该位置开始检索。

#### 缺点

- 降低表的更新速度
- 更多的磁盘空间占用

### 如何检查是否使用了索引

使用关键字 `explain`

```sql
explain SELECT
	*
FROM
	t_toy
WHERE
	price> 9999
```

结果集的 type 显示了查询类型，如果为 all 的话就为全表扫描。

### 索引无效或低效的情况

- 查询只会使用一次索引，所以 `where` 中使用了索引，则 `orderBy` 中就不会使用索引
- 当建立索引的列为非空时，则使用索引会花费更多时间在优化上
- 在 `where` 语句中使用函数时不会使用索引
- 在 `where` 语句中使用了否定形式不会使用索引
- 在 `where` 中使用使用了前匹配时不会使用索引
- 复合索引时，列的顺序非常重要，只有最左边的列包含在 `where` 中才会使用索引

### 使用索引的注意事项

* 避免全表扫描
* 避免或者简化排序
* 避免对大表进行全表扫描
* 避免使用通配符进行前匹配
* 避免在 `where` 中使用函数
* 避免在 `where` 中使用否定形式
* 避免在常更新的列建立索引
* 数据量很大时考虑使用 `union` 分成几个查询来代替 `or` 和 `in`
* 避免使用相关子查询

## 视图

视图用于缓存查询结果

创建视图

```sql
CREATE VIEW <name> AS
<SELECT QUERY>
```

更新视图

```sql
CREATE OR REPLACE VIEW <name> AS
<SELECT QUERY>
```

## 临时表

用户会话结束时临时表就会消除，使用时和普通表一样，只是创建时需要加 `temporary` 关键字，此外 `show tables` 也无法显示临时表。

例

```sql
create temporary table tmp( title varchar(20));
```

## 用户

### 创建用户

注意原来用户表的 `password` 已经变为 `authentication_string` 了。

```sql
CREATE USER 'username'@'host' IDENTIFIED BY 'password';
```

例

```sql
CREATE USER 'dog'@'localhost' IDENTIFIED BY '123456';
CREATE USER 'pig'@'%' IDENTIFIED BY '123456';
```

### 授权

```sql
GRANT privileges ON databasename.tablename TO 'username'@'host'
```

例

```sql
GRANT SELECT, INSERT ON test.user TO 'pig'@'%';
```

## 数据库配置

lower_case_table_names = 1  表名存储在磁盘是小写的，但是比较的时候是不区分大小写
