目录

[TOC]

摘要

>a

## 安装与运行

### 概述

[官网](http://www.postgresql.org/)

- 默认端口 `5432`
- 命令行工具 `psql`

### 数据类型

- `text`	任意长度字符串
- `varchar`	指定长度的可变字符串
- `char`	指定长度的固定字符串

## CRUD

### 数据库

创建数据库

```sql
CREATE DATABASE book
  WITH ENCODING='UTF8'
       CONNECTION LIMIT=-1;
```

### 表

####  创建表

```sql
 CREATE TABLE countries (
   country_code char(2) PRIMARY KEY,
   country_name text UNIQUE
 );
```

#### Insert

```sql
INSERT INTO countries (country_code, country_name)
VALUES ('us', 'United States'), ('mx', 'Mexico'), ('au', 'Australia');
```

#### Delete

```sql
DELETE FROM countries
WHERE country_code = 'll';
```

### 外键

```sql
CREATE TABLE cities (
  name text NOT NULL,
  country_code char(2) REFERENCES COUNTRIES,
  PRIMARY KEY (country_code)
);
```

### 联接

####  内联接

```sql
SELECT cities.*
FROM cities INNER JOIN countries
ON cities.country_code = countries.country_code;
```



