[TOC]

# ORM

## 实现

- [MyBatis](http://www.mybatis.org/mybatis-3/zh/index.html)
- [JOOQ](http://www.jooq.org/)

## 参考

- [Running with Maven](http://mbg.cndocs.tk/running/runningWithMaven.html)
- [MyBatis-Generator最佳实践](http://arccode.net/2015/02/07/MyBatis-Generator%E6%9C%80%E4%BD%B3%E5%AE%9E%E8%B7%B5/)
- [MyBatis Tutorial: Part1 – CRUD Operations](http://sivalabs.in/mybatis-tutorial-part1-crud-operations/)
- [MyBatis学习总结](http://www.cnblogs.com/xdp-gacl/category/655890.html)
- [JOOQ 3.8.2 使用 教程](https://amao12580.github.io/post/2016/04/JOOQ-from-entry-to-improve/)




---

可运行实例代码位于 `spring/first_db/` 目录下
