# MyBatis

MyBatis 原名为 iBatis

## 安装

```xml
<dependency>
  <groupId>org.mybatis</groupId>
  <artifactId>mybatis</artifactId>
  <version>3.3.0</version>
</dependency>
```

## 使用

### 创建 MyBatis 配置文件

mybatis-conf.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    <!--指定属性文件位置-->
    <properties resource="db.properties"/>
    <settings>
        <setting name="logImpl" value="STDOUT_LOGGING"/>
        <setting name="mapUnderscoreToCamelCase" value="true" />
    </settings>
    <plugins>
        <plugin interceptor="com.github.pagehelper.PageHelper">
            <property name="dialect" value="mysql" />
        </plugin>
    </plugins>
    <!-- development : 开发模式
9         work : 工作模式 -->
    <environments default="development">
        <environment id="development">
            <transactionManager type="JDBC"/>
            <dataSource type="POOLED">
                <property name="driver" value="${driver}"/>
                <property name="url" value="${url}"/>
                <property name="username" value="${username}"/>
                <property name="password" value="${password}"/>
            </dataSource>
        </environment>
    </environments>
    <mappers>
        <!--基于 XML-->
        <mapper resource="blog-mapper.xml"/>
        <!--基于注解-->
        <mapper class="com.mrseasons.first.db.mysql.CommentMapper"/>
    </mappers>
</configuration>
```

属性文件可以定义在任何位置，比如说以上的 `db.properties`。

```
driver=com.mysql.jdbc.Driver
url=jdbc:mysql://192.168.1.67:3306/test
username=root
password=tiger
```

### 基于 XML 配置 Mapper

定义 mapper 文件，需要同时定义在 MyBatis 配置文件的 `<mapper resource...>`

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<!--namespace的值习惯上设置成包名+sql映射文件名-->
<mapper namespace="com.mrseasons.first.db.mysql.blogMapper">
    <select id="getBlogById" parameterType="int" resultMap="blogResultMap">
        SELECT *
        FROM blog
        WHERE id = #{id}
    </select>
    <!---字符串插值中指定的是类的属性名-->
    <insert id="insertBlog" parameterType="com.mrseasons.first.db.mysql.Blog">
        INSERT INTO blog (title, description, create_time, update_time)
        VALUES (#{title},#{description}, #{createTime}, #{updateTime})
    </insert>
    <select id="findAll" resultType="com.mrseasons.first.db.mysql.Blog">
        SELECT *
        FROM blog
    </select>
    <!-- 也可以使用parameterMap 指定映射关系-->
    <parameterMap id="blogParamMap" type="com.mrseasons.first.db.mysql.Blog">
        <parameter property="title" resultMap="title"/>
        <parameter property="description" resultMap="description"/>
        <parameter property="createTime" resultMap="create_time"/>
    </parameterMap>
    <resultMap id="blogResultMap" type="com.mrseasons.first.db.mysql.Blog">
        <result column="create_time" property="createTime"/>
        <result column="update_time" property="updateTime"/>
    </resultMap>
</mapper>
```

使用

```java
String resource = "mybatis-config.xml";
InputStream inputStream = Resources.getResourceAsStream(resource);
SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
SqlSession sqlSession = sqlSessionFactory.openSession();
String nameSpace = "com.mrseasons.first.db.mysql.blogMapper";
//find one
Blog blog = sqlSession.selectOne(nameSpace + ".getBlogById", 2);
//insert
blog = new Blog();
blog.setTitle("Temp Blog");
blog.setDescription("Desc...");
blog.setCreateTime(new Date());
blog.setUpdateTime(blog.getCreateTime());
int count = sqlSession.insert(nameSpace + ".insertBlog", blog);
sqlSession.commit();
sqlSession.close();
```

### 基于注解配置 Mapper

创建 Mapper 类，需要同时定义在 MyBatis 配置文件的 `<mapper class=...>`。

```java
public interface CommentMapper {

    @Select("select * from comment where id=#{id}")
    public Comment getById(int id);

    @Insert("insert into comment(blog_id, author, content, create_time, update_time) " +
            "values(#{blogId},#{author},#{content},#{createTime},#{updateTime})")
    public int insertComment(Comment comment);
}
```

使用

```java
String resource = "mybatis-config.xml";
InputStream inputStream = Resources.getResourceAsStream(resource);
SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
SqlSession sqlSession = sqlSessionFactory.openSession();
CommentMapper commentMapper = sqlSession.getMapper(CommentMapper.class);
//find one
Comment comment = commentMapper.getById(2);
//insert
comment = new Comment();
comment.setBlogId(2);
comment.setAuthor("no name");
comment.setContent("=====");
comment.setCreateTime(new Date());
comment.setUpdateTime(comment.getCreateTime());
int count = commentMapper.insertComment(comment);
sqlSession.commit();
sqlSession.close();
```

#### 一对多配置

在一的一方保存另一方的 List

```xml
<select id="getWithComments" parameterType="int" resultMap="blogResultMap2">
   SELECT *
   FROM blog b
       JOIN comment c
           ON b.id = c.blog_id
   WHERE b.id = #{id}
</select>
<resultMap id="blogResultMap2" type="com.mrseasons.first.db.mysql.Blog">
   <result column="id" property="id"/>
   <result column="title" property="title"/>
   <result column="description" property="description"/>
   <result column="create_time" property="createTime"/>
   <result column="update_time" property="updateTime"/>
   <collection property="comments" ofType="com.mrseasons.first.db.mysql.Comment">
       <result column="create_time" property="createTime"/>
       <result column="update_time" property="updateTime"/>
   </collection>
</resultMap>
```

#### 一对一配置

```xml
<association property="teacher" column="teacher_id" javaType="me.gacl.domain.Teacher">
   <id property="id" column="t_id"/>
   <result property="name" column="t_name"/>
</association>
```

### MyBatis Generator

#### 添加依赖

```xml
 <dependency>
    <groupId>org.mybatis.generator</groupId>
    <artifactId>mybatis-generator-core</artifactId>
</dependency>

<build>
    <plugins>
        <plugin>
            <groupId>org.mybatis.generator</groupId>
            <artifactId>mybatis-generator-maven-plugin</artifactId>
            <version>1.3.2</version>
            <configuration>
                <configurationFile>${project.basedir}/src/test/resources/config/mybatis/generatorConfig.xml
                </configurationFile>
                <verbose>true</verbose>
                <overwrite>true</overwrite>
            </configuration>
            <dependencies>
                <dependency>
                    <groupId>mysql</groupId>
                    <artifactId>mysql-connector-java</artifactId>
                    <version>8.0.11</version>
                </dependency>
            </dependencies>
        </plugin>
    </plugins>
</build>
```


### 使用注解




#### generatorConfig.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE generatorConfiguration
        PUBLIC "-//mybatis.org//DTD MyBatis Generator Configuration 1.0//EN"
        "http://mybatis.org/dtd/mybatis-generator-config_1_0.dtd">

<generatorConfiguration>
    <!--<classPathEntry location="/Program Files/IBM/SQLLIB/java/db2java.zip" />-->

    <context id="DB2Tables" targetRuntime="MyBatis3">

        <!--<plugin type="com.bookislife.calpis.sso.persistence.MySQLLimitPlugin"></plugin>-->

        <commentGenerator>
            <property name="suppressAllComments" value="true"/>
        </commentGenerator>

        <jdbcConnection driverClass="com.mysql.cj.jdbc.Driver"
                        connectionURL="jdbc:mysql://aliyun:13306/testDb"
                        userId="orcl"
                        password="orcl">
        </jdbcConnection>

        <javaTypeResolver>
            <property name="forceBigDecimals" value="false"/>
        </javaTypeResolver>

        <javaModelGenerator targetPackage="com.bookislife.boot.data.mybatis.entity"
                            targetProject="src/test/java">
            <property name="enableSubPackages" value="false"/>
            <property name="trimStrings" value="true"/>

            <!-- 给Model添加一个父类 -->
            <!--<property name="rootClass" value="com.bookislife.calpis.sso.shared.Entity"/>-->
        </javaModelGenerator>

        <sqlMapGenerator targetPackage="mapper"
                         targetProject="src/test/resources/config/mybatis">
            <property name="enableSubPackages" value="false"/>
        </sqlMapGenerator>

        <!--type=ANNOTATEDMAPPER,XMLMAPPER-->
        <javaClientGenerator type="XMLMAPPER"
                             targetPackage="com.bookislife.boot.data.mybatis.mapper"
                             targetProject="src/test/java">
            <property name="enableSubPackages" value="false"/>
        </javaClientGenerator>

        <table schema="testDb" tableName="Foo" domainObjectName="Foo">
            <generatedKey column="ID" sqlStatement="MySql" identity="true"/>
            <!--<generatedKey column="tracking_id" sqlStatement="select SEQ.ne"/>-->
            <!--<columnOverride column="ID" jdbcType="NUMERIC" javaType="Long"/>-->

            <!--以下 column 来自父类继承-->
            <!--<ignoreColumn column="id"/>-->
            <!--<ignoreColumn column="created_at"/>-->
            <!--<ignoreColumn column="created_by"/>-->
            <!--<ignoreColumn column="updated_at"/>-->
            <!--<ignoreColumn column="updated_by"/>-->
            <!--<ignoreColumn column="lock_version"/>-->
            <!--<ignoreColumn column="is_deleted"/>-->
            <!--<ignoreColumn column="tracking_id"/>-->
        </table>
        <!--<table tableName="test" schema="UMR"/>-->

    </context>
</generatorConfiguration>
```

#### 执行生成命令

```shell
mvn mybatis-generator:generate
```

## 概念

### 缓存

#### 一级缓存

使用场景：
需要在一次数据库会话中，执行多次查询条件完全相同的SQL的场景

原理：
每个 SqlSession 中都持有一个 Executor，每个 Executor 中有一个 LocalCache。
当用户发起查询时，MyBatis 根据当前执行的语句生成对应 MappedStatement，在 Local Cache 进行查询，如果缓存命中的话，直接返回结果给用户，如果缓存没有命中的话，查询数据库，结果写入 Local Cache，最后返回结果给用户。

使用：

```xml
<setting name="localCacheScope" value="SESSION"/>
```

共有两个选项，SESSION 或者 STATEMENT，默认是 SESSION 级别，即在一个 MyBatis 会话中执行的所有语句，都会共享这一个缓存。一种是STATEMENT级别，可以理解为缓存只对当前执行的这一个Statement有效。


#### 二级缓存

原理：
如果多个 SqlSession之 间需要共享缓存，则需要使用到二级缓存。开启二级缓存后，会使用 CachingExecutor 装饰 Executor，进入一级缓存的查询流程前，先在 CachingExecutor 进行二级缓存的查询
二级缓存开启后，同一个 namespace下的所有操作语句，都影响着同一个 Cache，即二级缓存被多个 SqlSession共享，是一个全局的变量
当开启缓存后，数据的查询执行的流程就是 二级缓存 -> 一级缓存 -> 数据库。

使用：
开启缓存
1.MyBatis 的配置文件中开启二级缓存。<setting name="cacheEnabled" value="true"/>
2.在MyBatis 的映射 XML 中配置cache 或者 cache-ref 。
cache标签用于声明这个namespace使用二级缓存，并且可以自定义配置。
<cache/>
cache-ref代表引用别的命名空间的Cache配置，两个命名空间的操作使用的是同一个Cache。
<cache-ref namespace="mapper.StudentMapper"/>
