## JOQQ

JOOQ 可以实现 DSL 查询语句，并且可以通过 Codegen 扫描数据库自动创建对应的表的 META 对象和记录对象。

### 安装

基本配置

```xml
<dependency>
    <groupId>org.jooq</groupId>
    <artifactId>jooq</artifactId>
    <version>3.7.2</version>
</dependency>
<dependency>
    <groupId>org.jooq</groupId>
    <artifactId>jooq-meta</artifactId>
    <version>3.7.2</version>
</dependency>
```

Codegen 配置

```xml
<dependencies>
   <dependency>
        <groupId>org.jooq</groupId>
        <artifactId>jooq-codegen</artifactId>
        <version>3.7.2</version>
    </dependency>
</dependencies>

<build>
  <plugins>
  <plugin>
    <groupId>org.jooq</groupId>
    <artifactId>jooq-codegen-maven</artifactId>
    <version>3.7.2</version>
    <executions>
        <execution>
            <goals>
                <goal>generate</goal>
            </goals>
        </execution>
    </executions>
    <configuration>
      <jdbc>
          <driver>com.mysql.jdbc.Driver</driver>
          <url>jdbc:mysql://192.168.1.67/test</url>
          <user>root</user>
          <password>tiger</password>
      </jdbc>
      <generator>
          <database>
              <name>org.jooq.util.mysql.MySQLDatabase</name>
              <includes>.*</includes>
              <!--<excludes></excludes>-->
              <!--扫描的数据库-->
              <inputSchema>test</inputSchema>
          </database>
          <target>
              <!--生成的对象的位置和波包名-->
              <packageName>com.mrseasons.first.db.mysql.jooqschema</packageName>
              <directory>target/generated-sources/jooq</directory>
          </target>
      </generator>
    </configuration>       
    </plugin>
  </plugins>
</build>
```

### 使用

#### 使用 Codegen

```bash
mvn jooq-codegen:generate
```

#### 查询

查询一条记录

```java
try (Connection conn = DriverManager.getConnection(url, userName, password)) {
    DSLContext dslContext = DSL.using(conn, SQLDialect.MYSQL);
    Record record = dslContext.select()
            .from("blog")
            .where("id=2")
            .fetchOne();
    Integer id = (Integer) record.getValue("id");
    String title = (String) record.getValue("title");
    System.out.println(id + "," + title);
}catch (Exception e) {
    e.printStackTrace();
}
```

查询记录自动转换为特定的 Bean

```java
dslContext.select()
      .from("blog")
      .where("id=2")
      .fetchInto(Blog.class);
```

获得具体的执行语句

```java
String sql = dslContext.select()
                .from("blog")
                .where("id=2")
                .getSQL();
```

#### 插入操作

第一种方式

```java
int count = dslContext.insertInto(Blog.BLOG,
                Blog.BLOG.TITLE,
                Blog.BLOG.DESCRIPTION,
                Blog.BLOG.CREATE_TIME,
                Blog.BLOG.UPDATE_TIME)
                .values(blog.getTitle(),
                        blog.getDescription(),
                        new Timestamp(blog.getCreateTime().getTime()),
                        new Timestamp(blog.getUpdateTime().getTime()))
                .execute();
```

第二种方式

```java
dslContext.insertInto(Blog.BLOG)
                .set(Blog.BLOG.TITLE, "foobar")
                .set(Blog.BLOG.DESCRIPTION, "foobar")
                .set(Blog.BLOG.UPDATE_TIME, new Timestamp(0))
                .execute();
```

批量插入

```java
int[] numOfBatch =
      create.batch(
              create.insertInto(ACTOR, ACTOR.ID, ACTOR.NAME).values(17,"穂積隆信"),
              create.insertInto(ACTOR, ACTOR.ID, ACTOR.NAME).values(18,"信欣三"),
              create.insertInto(ACTOR, ACTOR.ID, ACTOR.NAME).values(19,"浜村純")
      )
              .execute();
```

#### 使用事务

```java
dslContext.transaction(configuration -> {
            DSLContext context = DSL.using(configuration);
            int count = context.insertInto(Blog.BLOG,
                    Blog.BLOG.TITLE,
                    Blog.BLOG.DESCRIPTION,
                    Blog.BLOG.CREATE_TIME,
                    Blog.BLOG.UPDATE_TIME)
                    .values("100",
                            blog.getDescription(),
                            new Timestamp(blog.getCreateTime().getTime()),
                            new Timestamp(blog.getUpdateTime().getTime()))
                    .execute();

            context.insertInto(Blog.BLOG,
                    Blog.BLOG.TITLE,
                    Blog.BLOG.DESCRIPTION)
                    .values("110",
                            blog.getDescription())
                    .execute();
        });
```
