目录

[TOC]

摘要

>哈希表（单条与多条数据存取，获取所有键或值，删除指定键），列表（尾部追加与获取，弹出与压入，阻塞命令），集合（添加与获取），有序集合（添加与获取，删除，score）

## 复杂数据类型

### 哈希表

#### 概述

- 哈希表用于实现类似对象的功能，从而避免使用不自然的前缀来存储数据。
- 哈希表的命令通常以字母 "H" 开头

#### 使用

##### 创建哈希表

格式

`<HSET | HMSET> key field value`

以上即为创建一个以 "key" 命名的哈希表，值为 "field-value" 的键值对。

例

```shell
HMSET user:peter name "Peter Pan" password zxcf
HSET user:peter no 1
```

##### 获取所有键

`HKEYS key`

```shell
HKEYS user:peter
```

##### 获取所有值

`HVALS key`

```shell
HVALS user:peter
```

##### 获得指定键值

```shell
HGET user:peter password
```

##### 获得键值数

```shell
HLEN user:peter
```

##### 删除指定键

```shell
HDEL user:peter no
```

##### 递增指定键值

`HINCRBY key field increment`

例

```shell
HINCRBY user:peter no 10
```

### 列表 List

#### 概述

列表的大部分命令以 "L" 开头。

#### 普通列表

##### 从尾部追加

```shell
RPUSH peter:wishlist tv 3ds psp
```

##### 获得长度

```shell
HLEN peter:wishlist
```

##### 获取指定范围的值

`LRANGE key from to`

例

```shell
LRANGE peter:wishlist 0 -1
```

索引从 "0" 开始，负数表示从尾部开始算起，都是闭区间

##### 删除指定值

删除指定数目的匹配值

`LREM key count value`

`count` 为 "0" 的话表示删除所有匹配的，为负数表示从尾部开始算起

例

```shell
LREM peter:wishlist 0 3ds
```

##### 获取并弹出头部

```shell
LPOP peter:wishlist
```

##### 弹出并压入

从一个列表中的尾部弹出，追加到另一个列表的头部

`RPOPLPUSH from to`

例

```shell
RPOPLPUSH peter:wishlist peter:giveuplist
```
#### 阻塞列表

##### BRPOP

监听阻塞队列

`BRPOP key timeout`

`timeout` 单位为 "秒"，直到接收到消息或超时该命令会一直进行阻塞

例

```shell
BRPOP comments 300
```

这时在其它控制台向 "comments" 压入消息后，监听者会显示收到的消息键值对和耗时

```shell
LPUSH comments "hello"
```

##### 其它命令

除了 `BRPOP` 还有 `BLPOP` 和 `BRPOPLPUSH` 版本。


### 集合 Set

#### 概述

列表的大部分命令以 "S" 开头。无序且不重复。

#### 普通集合

##### 添加到集合

`SADD key member [member ...]`

例

```shell
SADD news times.com blog.com
```

##### 获取集合的值

```shell
SMEMBERS news
```

##### 获取交集

```shell
 SINTER news tech
```

获取并存储到新的集合中

```shell
SINTERSTORE websites news tech
```

#####  获取差集


获取的是前一个和后一个不同的值

```shell
SDIFF news tech
```

获取并存储到新的集合中

```shell
SDIFFSTORE websites news tech
```

##### 获取并集

```shell
SUNION news tech
```

获取并存储到新的集合中

```shell
SUNIONSTORE websites news tech
```

##### 其它命令

- `SMOVE` 将值从一个集合移到另一个集合
- `SCARD` 获取集合长度
- `SPOP` 弹出一个随机值
- `SREM` 删除指定的值

#### 有序集合

#####  概述

有序集合像列表一样有序，像集合一样没有重复，像哈希表一样拥有键，只是键只能为整型，代表值的位置

##### 添加到列表

`ZADD key score member [score member ...]`

例

```shell
ZADD words 1 a 26 z 9 i 3 c
```

##### 递增

```shell
ZINCRBY words 10 a
```

##### 获取指定范围的值

```shell
ZRANGE words 0 -1
```

反向为 `ZRANGE words 0 -1`
`WITHSCORES` 可以同时返回 "score" 的值

```shell
ZREVRANGE words 0 -1 WITHSCORES
```

##### 获取指定 Score 的值

```shell
ZRANGEBYSCORE words 1 20
```

返回的为 score 值在 "1-20" 的值，为闭区间。如果想使用开区间，需要在值前面加上 "("。

```shell
ZRANGEBYSCORE words (1 20

ZRANGEBYSCORE words 1 (20
```

反向为 `ZREVRANGEBYSCORE`

也可以使用 `-inf` 和 `inf` 表示负无穷和正无穷

##### 删除指定范围的值

按位置删除

```shell
ZREMRANGEBYRANK words 2 3
```

按 Score 删除为 `ZREMRANGEBYRANK`

##### 并集

`  ZUNIONSTORE destination numkeys key [key ...] [WEIGHTS weight] [AGGREGATE SUM|
MIN|MAX]`

- `destination` 要存入的键
- `numkeys` 做并集的键的个数
- `key`	需要做并集的键
- `weight` 可选，用来乘以相应键的 "score"
- `aggregate` 可选，处理每个加权得分的可选规则，默认为总和

例

```shell
 ZUNIONSTORE importance 2 visits votes WEIGHTS 1 2 AGGREGATE SUM
```

以上表示建立 key 为 importance 的集合，其值为 visits 和 votes 两个集合的并集，且后者加权为 2

该命令也可以用于修改自身集合的 `score`

```shell
ZUNIONSTORE votes 1 votes WEIGHTS 2
```

##### 交集

ZINTERSTORE，使用同并集













