目录

[TOC]

摘要

>set，get，mset，mget，incr，decr，incrby，decrby

## 设值与取值

### set & get

set 需要两个参数：键和值

```shell
set foo bar
```

get 需要一个参数：键

```shell
get foo
```

### mset & mget

mset 与 mget 可以用于设置或取得多个值来减少通信的开销

mset

```shell
mset foo1 bar1 foo2 bar2
```

mget

```shell
mget foo1 foo2
```

mget 取得的是一个有序列表

### count

Redis 存储的是字符串，但是它也可以用于处理数字，进行计数

```shell
set count 2
```

递增与递减

```shell
incr count
decr count
```

增加与减少指定值

```shell
incrby count 10
decrby count 5
```





