[TOC]

# Redis

[官网](http://redis.io/)

## 使用场景

- 会话缓存(Session Cache)
- 全页缓存（FPC）
- 队列
- 排行榜/计数器，访问次数限制
- 发布/订阅

## Redis vs Memcached

Redis 支持持久化操作以及更多的数据类型支持，而不是简单的键值对。
Redis 通过 Multi / Watch /Exec 等命令支持事务操作。
Redis 有虚拟内存的概念，可以让 Redis 数据容量突破了物理内存的限制。并实现了数据冷热分离。

## 最佳实践

- Redis 使用最佳方式是全部数据 in-memory
- 设置 key 值的存活时间
- 选择合适的回收策略（建议使用 volatile-lru 策略）
- Redis 是一个单线程进程，所以一台机器只需启动一个实例
- 使用 Redis Sentinel 实现高可用集群

## 注意事项

- 作为缓存时，需要提高数据命中率，防止大量访问直接穿透到DB，使数据库无法支撑

## 坑

## 参考

- [计数场景的优化](http://www.xdata.me/?p=262)
- [国内外三个不同领域巨头分享的Redis实战经验及使用场景](http://www.csdn.net/article/1970-01-01/2817107)
- [Redis Sentinelで自動フェイルオーバー](http://qiita.com/wellflat/items/8935016fdee25d4866d9)
- [谈谈陌陌争霸在数据库方面踩过的坑( Redis 篇)](http://blog.codingnow.com/2014/03/mmzb_redis.html)
- [redis cluster使用经验](http://yangzhe1991.org/blog/2015/04/redis-cluster/)
- [美团在Redis上踩过的一些坑](http://carlosfu.iteye.com/blog/2254154)
- [redis持久化和常见故障](https://segmentfault.com/a/1190000004135982)
