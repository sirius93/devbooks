目录

[TOC]

摘要

>发布，订阅，取消订阅

## 发布者与订阅者

### 订阅

`SUBSCRIBE channel [channel ...]`

例

```shell
SUBSCRIBE comments
```

可以开启多个客户端进行订阅

### 发布

`PUBLISH channel message`

例

```shell
PUBLISH comments "this is a comments"
```

执行发布后，所有订阅该频道的客户端都能接收到消息

### 取消订阅

取消所有

```shell
UNSUBSCRIBE
```

取消指定频道

```shell
UNSUBSCRIBE comments
```