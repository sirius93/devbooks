目录

[TOC]

摘要

>

## 安装与运行

### 特点

- Redis 是键-值对存储库
- 支持基于集合的查询操作，但不支持关系数据库同样的粒度和类型

### 安装

Windows

官方不支持 Windows 平台，需要下载 MSOpenTech 的版本

[下载地址](https://github.com/MSOpenTech/redis)

Mac

下载 [Redis](http://www.redis.io/download)

解压，执行 `make`

### 运行

#### 运行服务端

**Windows**

`redis-server.exe redis.windows.conf`

Windows 平台如果没有启动成功的话，可能需要改变 `heapdir` 和 `maxheap` 的值来减少占用空间。

**Mac**

`redis-server`

#### 运行客户端

**Windows**

格式

`redis-cli.exe <-h ip> <-p port>`

例

`redis-cli.exe -h localhost -p 6379`

其中 `-h` 为服务器地址，`-p` 为端口， `6379` 为 Redis 的默认端口。`cli` 指的是命令行界面（Command-Line Interface）。

**Mac**

`redis-cli`

####  后台运行

Redis 默认非后台运行，但是可以在命令后加上 `&` 来实现后台运行。

### 使用

设置值

`set <key> <value>`

```shell
set foo bar
```

取值

`get <key>`

```shell
get foo
```

### 帮助

- 输入 `help` 可显示所有帮助
- `help 任意命令` 可显示对应命令的帮助
- `help` 后跟空格，点击 `Tab` 按钮可以循环显示所有命令

### 应用

Redis 可以用于构建短域名服务，将请求的短网址重定向到映射前的 URL








