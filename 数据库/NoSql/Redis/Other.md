
docker run -d -h redis_master --name redis_master sidneyxu/redis_master
docker run -d -h redis_replica1 --name redis_replica1 --link redis_master sidneyxu/redis_master

docker run -ti --rm --volumes-from redis_replica1 centos:7
vi /opt/redis/redis.conf
slaveof redis_master 6379
docker restart redis_replica1

docker run -d -h redis_replica2 --name redis_replica2 --link redis_master sidneyxu/redis_master
docker run -ti --rm --volumes-from redis_replica2 centos:7
slaveof redis_master 6379
exit
docker restart redis_replica2
docker logs redis_replica2

1:S 05 Dec 12:46:59.734 * MASTER <-> SLAVE sync: Flushing old data
1:S 05 Dec 12:46:59.734 * MASTER <-> SLAVE sync: Loading DB in memory
1:S 05 Dec 12:46:59.734 * MASTER <-> SLAVE sync: Finished with success



http://hot66hot.iteye.com/blog/2050676


logstash.net

或者 Add 到一个特别的地方


docker run -ti --rm --link redis_master --link redis_replica1 --link redis_replica2 sidneyxu/base-redis /bin/bash
 /opt/redis/src/redis-cli -h redis_master -p $REDIS_MASTER_PORT_6379_TCP_PORT


### 客户端操作

查看节点状态

```bash
cluster nodes
```

查看插槽状态

```bash
cluster slots
```

1)2)表示处理的插槽的范围，3)4)表示主从节点信息

查看键所在插槽信息

```bash
cluster keyslot <key>
```

### 插槽指派与分片

Redis集群通过分片的方法保存数据库中的键值对，集群的数据库被分为16384个槽。
每个键都属于这些槽中的一个，集群中的每个节点可以处理0个到16384个槽。
槽(slots)数据结构为二进制数组，数组长度为2048字节，16384位.

当某个槽中的某个索引的值为1时，表示该节点处理该索引


没有被分配过
cluster addslots slot

#### 自动重新分片

连接任意节点

```bash
/opt/redis/src/redis-trib.rb reshard 127.0.0.1:7001
```

按照提示输入对应信息

```bash
# 输入要移动的插槽数量
How many slots do you want to move (from 1 to 16384)? 1
# 目标节点的 ID
What is the receiving node ID? 5718aa50f99b239d6c9c23ce5065140cbb28cff5
# 源节点ID，可输入多个
Source node ce4f0a14e52a1363281afc4817464643e325606c
Source node done
```

最后输入 `yes` 完成分片。

#### 手工重新分片

当插槽中没有键时

```bash
cluster setslot <slot> node <node_id>
```

迁移带键插槽

在目标节点

```bash
cluster setslot <slot> importing <source_node_id>
```

cluster setslot 12182 importing 401bb10399a914586fa96767c07108bd27c168c7

在源节点

```bash
cluster setslot <slot> migrating <target_node_id>
```

cluster setslot 12182 migrating ce4f0a14e52a1363281afc4817464643e325606c

获取键列表

```bash
cluster getkeysinslot <slot> <count>
```

迁移键，对每个键执行

集群模式只能使用0号数据库

```bash
migrate <target_host> <target_port> <key> <target_database> 15999 replace
```

迁移槽
cluster setslot <slot> node <target_node_id>

#### 转向与故障恢复

Moved错误
当节点发现键所在的槽不是由自己处理就会返回Moved错误
集群模式的redis-cli客户端接收到错误后会自动转向到新的节点

Ask错误
客户端指定的key不存在于源节点中，但是key存在，只是源节点正在迁移槽

asking
get foo


集群中的节点操作

添加节点
1. 启动新节点
2. 执行
redis-trib.rb add-node <host>:<port> 任意节点
3. 设置主从
主
移动插槽
redis-trib.rb reshared
从
redis-cli -c -p 节点
cluster replicate <master_node_id>

删除节点
删除从节点
redis-trib.rb del-node 节点host 节点id
删除主节点
删除插槽
redis-trib.rb reshard 节点host
redis-trib.rb del-node
