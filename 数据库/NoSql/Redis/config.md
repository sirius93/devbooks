[TOC]

## Redis 配置

### 运行设置

- daemonize
配置redis实例是否以后台进程运行，默认为：no，如果设置为：yes，redis实例将以后台进程模式运行，并在配置项 pidfile 指定的文件中写入进程Id。
配置范例：daemonize yes

- pidfile
当redis实例以后台进程模式运行时，即配置项：daemonize 为 yes时，redis实例进程Id写入的文件路径
配置范例：pidfile /var/run/redis.pid

- port
配置redis实例接收TCP请求端口，默认配置：6379
配置范例：port 6379

- bind
配置redis实例接收指定的TCP请求IP，在产品环境应该配置具体IP，保证安全。默认配置：无，即所有代表redis实例所安装在的机器的IP都可以访问，例如：127.0.0.1、192.168.1.11
配置范例：bind 127.0.0.1

- timeout
配置redis请求连接多久以后自动关闭，以秒为单位。默认配置：0，即表示永远不会关闭，使用连接池以后此项无多大意义。
配置范例：timeout 0

### 存储快照相关设置

- save
配置建立快照的间隔，无配置即表示不使用该功能
配置范例：
save “”  表示不使用该功能
save 900 1          在900秒之内，redis至少发生1次修改则redis抓快照到磁盘
save 300 100        在300秒之内，redis至少发生100次修改则redis抓快照到磁盘
save 60 10000       在60秒之内，redis至少发生10000次修改则redis抓快照到磁盘

### 限制相关

- maxclients
客户端请求最大连接数
默认配置：无限制，即和按操作系统可以打开文件数相同
配置范例：maxclients 10000

- maxmemory
最大内存，到达限制时会按照maxmemory-policy的设置进行回收，实际生产环境中应该设置以防止内存增长超过物理内存，该内存除了 redis实例之外还包括用于主从同步的缓冲区
默认配置：无限制
配置范例：maxmemory 4GB

- maxmemory-policy
最大内存达到时，redis自动清理keys策略，有如下策略可以选择，如果你不确定使用哪种策略，那么推荐使用allkeys-lru
# volatile-lru -> remove the key with an expire set using an LRU algorithm
# allkeys-lru -> remove any key accordingly to the LRU algorithm
# volatile-random -> remove a random key with an expire set
# allkeys-random -> remove a random key, any key
# volatile-ttl -> remove the key with the nearest expire time (minor TTL)
# noeviction -> don't expire at all, just return an error on write operations
默认配置：volatile-lru
配置范例：maxmemory-policy allkeys-lru

- maxmemory-samples
每次进行淘汰的时候 会随机抽取N个key 从里面淘汰最不经常使用的
默认配置：3
配置范例：maxmemory-samples 3
