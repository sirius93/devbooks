[TOC]

## Jedis

### 概述

Jedis 为 redis 的 Java 客户端。


```xml
<dependency>
    <groupId>redis.clients</groupId>
    <artifactId>jedis</artifactId>
    <version>2.7.2</version>
    <type>jar</type>
    <scope>compile</scope>
</dependency>
```

### 基本使用

#### Jedis

Jeids 对象代表一个 redis 的客户端连接，非线程安全。

创建一个 Jedis 对象

```java
Jedis jedis = new Jedis("192.168.99.100", 6379);
jedis.close();
```

#### Jedis Pool

Jedis Pool 是 Jedis 池，线程安全。

```java
JedisPool jedisPool = new JedisPool(new JedisPoolConfig(), "localhost");
jedisPool.setMaxTotal(20);
jedisPool.setMaxIdle(20);
jedisPool.setTimeBetweenEvictionRunsMillis(600 * 1000);

Jedis jedis = null;
try {
    jedis = jedisPool.getResource();
    jedis.set("foo", "bar");
    assertEquals("bar", jedis.get("foo"));

    jedis.zadd("sose", 0, "car");
    jedis.zadd("sose", 0, "bike");

    Set<String> sose = jedis.zrange("sose", 0, -1);
    assertTrue(sose.contains("car"));
    assertTrue(sose.contains("bike"));
    assertEquals(2, sose.size());
} finally {
    if (jedis != null) {
        jedis.close();
    }
}

jedisPool.destroy();
```

#### Jedis 集群

手动指定 Redis 的主从集群

```java
//主节点
jedis = new Jedis(SERVER, 6380);
//从节点
jedis.slaveof("localhost", 6379);
```

如果系统主节点挂掉了需要手动下线主节点后再将一个从节点提升为主节点

```java
jedis.slaveofNoOne();
jedis.slaveOf("192.168.1.36", 6379);
```


#### Jedis Cluster

Jedis Cluster 用于连接 Redis 集群，其辉自动连接到一个节点上

```java
Set<HostAndPort> jedisClusterNodes = new HashSet<>();
jedisClusterNodes.add(new HostAndPort("127.0.0.1", 6380));
jedisClusterNodes.add(new HostAndPort("127.0.0.1", 6379));

JedisCluster jedisCluster = new JedisCluster(jedisClusterNodes);

jedisCluster.close();
```

接下来可以像普通 Jedis 对象一样进行各种操作


#### Jedis Shared Pool

```java
JedisPoolConfig config = new JedisPoolConfig();
config.setMaxTotal(20);
config.setMaxIdle(5);
config.setTestOnBorrow(false);

List<JedisShardInfo> shards = new ArrayList<JedisShardInfo>();
shards.add(new JedisShardInfo("127.0.0.1", 6379, "master"));

ShardedJedisPool shardedJedisPool = new ShardedJedisPool(config, shards);

shardedJedisPool.destroy();
```

### 常用操作

#### 基本操作


```java
//set
jedis.set("foo", "bar");

//get
String result = jedis.get("foo");

//exists
boolean exist = jedis.exists("foo");

//clear db
jedis.flushDB();

//close
jedis.close();
```

#### Map

```java
//put
jedis.hset("person", "name", "Peter");
jedis.hset("person", "age", "18");
jedis.hset("person", "sex", "mail");

//get keys
Set<String> keys = jedis.hkeys("person");

//get values
List<String> values = jedis.hvals("person");

//exists
assertTrue(jedis.hexists("person", "sex"));

//delete
jedis.hdel("person", "sex");
```

#### List

```java
jedis.rpush("list", "a");
jedis.rpush("list", "b");
jedis.rpush("list", "c");
jedis.rpush("list", "c");

List<String> list = jedis.lrange("list", 0, -1);
assertEquals(4, list.size());
assertEquals("b", list.get(1));

jedis.lrem("list", 2, "c");
list = jedis.lrange("list", 0, -1);
assertEquals(2, list.size());
assertEquals("b", list.get(1));

SortingParams sortingParams = new SortingParams();
sortingParams.desc();
sortingParams.alpha();
sortingParams.limit(0, 2);
list = jedis.sort("list", sortingParams);
list.forEach(System.out::println);
```

#### Set

```java
//put
jedis.sadd("set", "ele1");
jedis.sadd("set", "ele2");
jedis.sadd("set", "ele3");
jedis.sadd("set", "ele3");

//get values
Set<String> set = jedis.smembers("set");
assertEquals(3, set.size());

//delete
jedis.srem("set", "ele2");
set = jedis.smembers("set");
assertEquals(2, set.size());
assertFalse(set.contains("ele2"));

//contains
assertFalse(jedis.sismember("set", "ele2"));
assertTrue(jedis.sismember("set", "ele1"));
```

#### SortedSet

```java
//put
jedis.zadd("zset", 7.0, "ele1");
jedis.zadd("zset", 8.0, "ele2");
jedis.zadd("zset", 2.0, "ele3");
jedis.zadd("zset", 3.0, "ele4");

//sort by score
Set<String> set = jedis.zrange("zset", 0, -1);

//delete
jedis.zrem("zset", "ele3");
long size = jedis.zcard("zset");
assert 3 == size;
assert 2 == jedis.zcount("zset", 1.0, 7.0);
assert 3.0 == jedis.zscore("zset", "ele4");

set = jedis.zrangeByScore("zset", 7.1, 8.0);
set.forEach(System.out::println);
```

#### 指定多个键

```java
//mset
jedis.mset("foo", "bar", "foo1", "bar1", "foo2", "bar2");

//loop keys
jedis.keys("*").forEach(System.out::println);

//mget
List<String> keys = jedis.mget("foo", "foo2");
assertEquals("bar2", keys.get(1));

//delete
jedis.del("foo", "abc");
```

#### 过期时间

```java
jedis.set("foo", "bar");
jedis.expire("foo", 3);
Thread.sleep(5000);
assertFalse(jedis.exists("foo"));

//ttl
assert -2 == jedis.ttl("foo");
assert -1 == jedis.ttl("foo1");

//type
assertEquals("none", jedis.type("foo"));
assertEquals("string", jedis.type("foo1"));
```

#### 切换数据库

```java
jedis.set("foo", "bar");
jedis.move("foo", 1);
assertNull(jedis.get("foo"));

jedis.select(1);
assertEquals("bar", jedis.get("foo"));
```
