
# MongoDB 

## 安装

```xml
<dependency>
        <groupId>org.mongodb</groupId>
        <artifactId>mongo-java-driver</artifactId>
        <version>3.2.2</version>
</dependency>
```

## MongoClient

### 连接到单个 Server

```java
MongoClient mongoClient = new MongoClient(SERVER, PORT);
``` 

### 连接多个 Server

```java
MongoClient mongoClient = new MongoClient(Arrays.asList(new ServerAddress("localhost", 27017), new ServerAddress("localhost", 27018)));
```

### 授权验证

```java
String userName = "";
String database = "";
String password = "";
MongoCredential credential = MongoCredential.createMongoCRCredential(userName, database, password.toCharArray());
MongoClient mongoClient = new MongoClient(new ServerAddress(), Arrays.asList(credential));
```

## DB

### 获得所有数据库名

```java
List<String> databaseNames = mongoClient.getDatabaseNames();
databaseNames.forEach(database -> {
    println(database);
});
```

### 创建 DB 对象

```java
MongoDatabase db = mongoClient.getDatabase(DATABASE);
```

### 创建集合

```java
db.createCollection(COLLECTION,
        new BasicDBObject("capped", true)
                .append("size", 1048576));
```


##  DBCollection

### 获得所有集合名

```java
Set<String> collectionNames = db.getCollectionNames();
```

### 创建 Collection 对象

```java
DBCollection coll = db.getCollection(COLLECTION);
```

### 插入操作

```java
BasicDBObject doc = new BasicDBObject("name", "MongoDB")
        .append("type", "database")
        .append("count", 1)
        .append("info", new BasicDBObject("x", 203).append("y", 102));

coll.insert(doc);
```

### 删除操作

删除 Collection 中所有数据

```java
coll.drop();
```

### 批量操作

按顺序执行批量操作

```java
BulkWriteOperation builder = coll.initializeOrderedBulkOperation();
builder.insert(new BasicDBObject("_id", 1));
builder.insert(new BasicDBObject("_id", 2));
builder.insert(new BasicDBObject("_id", 3));

builder.find(new BasicDBObject("_id", 1)).updateOne(new BasicDBObject("$set", new BasicDBObject("x", 2)));
builder.find(new BasicDBObject("_id", 2)).removeOne();
builder.find(new BasicDBObject("_id", 3)).replaceOne(new BasicDBObject("_id", 3).append("x", 4));

BulkWriteResult result = builder.execute();
assertEquals(3, result.getInsertedCount());
assertEquals(1, result.getRemovedCount());
assertEquals(2, result.getModifiedCount());
println("Ordered bulk write result : " + result);

BasicDBObject query = new BasicDBObject("_id", 1);
DBObject dbo = coll.findOne(query);
assertEquals(1, dbo.get("_id"));
assertEquals(2, dbo.get("x"));

query = new BasicDBObject("_id", 2);
dbo = coll.findOne(query);
assertNull(dbo);

query = new BasicDBObject("_id", 3);
dbo = coll.findOne(query);
assertEquals(3, dbo.get("_id"));
assertEquals(4, dbo.get("x"));
```

无序执行批量操作

```java
BulkWriteOperation builder = coll.initializeUnorderedBulkOperation();
builder.insert(new BasicDBObject("_id", 1));
builder.insert(new BasicDBObject("_id", 2));
builder.insert(new BasicDBObject("_id", 3));

builder.find(new BasicDBObject("_id", 1)).removeOne();
builder.find(new BasicDBObject("_id", 2)).removeOne();

BulkWriteResult result = builder.execute();
println("Ordered bulk write result : " + result);
```

## Index

### 获取所有索引

```java
List<DBObject> list = coll.getIndexInfo();
list.forEach(System.out::println);
```

### 创建索引

```java
coll.createIndex(new BasicDBObject("i", 1),
        new BasicDBObject("name", "i_inx")
                .append("unique", 1)
                .append("dropDups", 1));
```

### 删除所有索引

```java
coll.dropIndexes();
```

### 全文索引

```java
// create a text index on the "content" field
coll.createIndex(new BasicDBObject("content", "text"));

coll.insert(new BasicDBObject("_id", 0).append("content", "textual content"));
coll.insert(new BasicDBObject("_id", 1).append("content", "additional content"));
coll.insert(new BasicDBObject("_id", 2).append("content", "irrelevant content"));

// Find using the text index
//with textual or content, without irrelevant
BasicDBObject search = new BasicDBObject("$search", "textual content -irrelevant");
BasicDBObject textSearch = new BasicDBObject("$text", search);
int matchCount = coll.find(textSearch).count();
System.out.println("Text search matches: " + matchCount);

// Find using the $language operator
textSearch = new BasicDBObject("$text", search.append("$language", "english"));
matchCount = coll.find(textSearch).count();
System.out.println("Text search matches (english): " + matchCount);

// Find the highest scoring match
BasicDBObject projection = new BasicDBObject("score", new BasicDBObject("$meta", "textScore"));
DBObject myDoc = coll.findOne(textSearch, projection);
System.out.println("Highest scoring document: " + myDoc);
```




