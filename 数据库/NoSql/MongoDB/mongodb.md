# 目录

# 基本概念
MongoDB是面向文档的数据库。
MongoDB没有模式的概念，这意味着文档的键无需事先创建，也不好固定不变。
MongoDB中的文档(Document)类似关系数据库的行(Row)，集合(Collection)类似关系数据库的表(Table)。

# 运行
## 启动MongoDB

```shell
mongod --dbpath <path> --port <port> --httpinterface --rest
```

启动前必须先确定数据库路径已经被创建且拥有读写权限。
默认的数据库路径位/data/db，端口为27017

参数

- ==dbpath== - 指定db路径
- ==port== - 指定端口号
- ==httpinterface== - 是否开启http服务，http服务默认端口位28017
- ==rest== - 是否允许通过rest接口进行访问

## 运行Javascript Shell

```shell
mongo
```

MongoDB Javascript shell是完备的Javascript解释器，可以运行任何Javascript代码。

### 帮助
输入以下命令均可以显示对应帮助
- help
- db.help()
- db.dbName.help()

### 显示源代码
调用方法时不加==()==会显示Javascript源代码

### 请求和连接
MongoDB为每一个数据库连接都建立了一个队列，存放这个连接的各种请求。这意味着两个shell是两个连接，一个shell中执行插入操作后，在另一个shell中并不能总是返回结果。但是同一个shell是一定会的。

### `_id`
每个文档都有一个_id。默认类型为ObjectId，不同于其它数据库的自动增长的主键，ObjectId在不同机器上也是唯一的，所以更适合作为分布式数据库。

# 语法
## 数据类型

null
boolean
32bit int
64bit int
64bit float
string
ObjectId
...

## 数值类型
MongoDB支持3种类型：32位整数，64位整数和64位浮点数。

### 规律
- 默认有小数点的数字会被保存为64位double。
- 在32位机器上
	- 没有小数点的数字会被保存为32位int，使用bigint会被保存为64位int
	- 超过64位int的数字会被自动解析为double
- 在64位机器上
	- 没有小数点的数字会被保存为64位int，且无法保存32位int

所以如果32位机器和64位机器共同使用的话，会发生各种奇怪的现象。

### Shell中的数值
Shell只有一种数字类型，8-byte float，这意味着它并不是总能显示正确的8-byte integer。在shell显示64位int时，它可能会被显示为近似值。

例如：
```javscript
{
     “x”:{
          “floatApprox”:xxx,
          “top”:yyy,
          “bottom”:zzz
     }
}
```
这并不表示x是浮点型，而是表示x并不精确，top是高32位，bottom是低32位

但是Javascript shell只支持64位浮点数，这意味着即使从数据库原封不动取得一个32位整数的记录再重新更新，数据也会被转换为64位浮点数。

## 数组类型

### 访问数组中的元素

#### 使用下标

```shell
db.<dbName>.<collectionName>.<index>
```

例如：
`db.blog.comments.0.author`

#### 使用"$"

很多时候必须先进行查询才能获得下标。此时可以使用"$"，其总会返回匹配条件的第一条。

```shell
<collectionName>.$
```

例如：
`db.blog.update({“comments.author”:”John”}, {“$set”:{“comments.$.author”:”Jim"}})`


# 基本使用
## Database

### 显示所有DB
```shell
show dbs
```

### 选择DB
```shell
use <dbName>
```

### 查看当前DB名
```shell
db
```

## Collection
### 显示所有collections
```shell
show collections
```

### Query
查找多条记录
```shell
db.<dbName>.find([cond])
```

查找单条记录
```shell
db.<dbName>.findOne([cond])
```

Shell每次最多返回20条记录，可以输入'==it=='来获得更多记录。


### Issue
#### 迷惑的集合名
Javascript只有在db的属性中中找不到与dbName不符的属性时，才会将其作为集合返回。比如说version 是 db 的一个属性，如果 db name 也为 version 的话，db.version 是不会正确返回的。此外"foo-bar"之类的不合法名也不会正确返回。

解决办法
```shell
db.getCollection(dbName)
```

## Document

### 插入操作
```shell
db.<dbName>.insert(doc)
```

### 批量插入
执行插入时，数据会被转为BSON格式，数据库会检查是否包含_id键且文档大小是否大于4M。除此之外，不做其他数据验证，也不执行任何代码。所以其副作用是允许插入无效的数据，但同时也避免了插入这块的SQL注入问题。

### 删除文档
```shell
db.<dbName>.remove([cond])
```

删除集合中的所有文档，但不会删除集合本身，也不会删除索引。

不写条件的话会删除所有记录，需要特别小心。

如果希望更快的删除所有数据，可以直接删除集合并重建索引，缺点是不能有任何限制条件。

### 更新文档

更新操作是原子性的，先到服务器的命令会先执行。

#### 文档替换

```shell
db.<dbName>.update(cond, nDoc)
```

此操作会完全替换原来的文档内容，可以修改_id

#### 修改器

```shell
db.<dbName>.update(cond, modifierSequence)
```

使用修改器时无法替换_id
modifierSequence见下列所述

##### inc

增加或减少一个键的值，如果不存在此键就创建它

```shell
{"$inc":{key:value}}
```

##### set

指定一个键的值，如果不存在此键就创建它

```shell
{“$set":{key:value}}
```

##### unset

```shell
{“$unset":{key:1}}
```
##### push
向数组末尾添加数据，不存在此键就创建它

```shell
{“$push”:{key:value}}
```
当值不重复时就添加

```shell
db.<dbName>.update({key:{“$ne”:value}},{“$push”:{key:value}})
```

或者

```shell
{“$addToSet”:{key:value}}
```

##### addToSet
添加不重复的数据，不存在此键的话就创建它
一次添加单条数据
```shell
{“$addToSet”:{key:value}}
```

一次添加多条数据
```shell
{“$addToSet”:{key:{“$each”:[values...]}}}
```

##### pop
从数组的一端删除数据

从末尾删除
```shell
{“$pop”:{key:1}}
```
从头部删除
```shell
{“$pop”:{key:-1}}
```

##### pull
根据条件从数组中删除数据

```shell
{“$pull”:{key:value}}
```

#### update 参数

##### upsert

没有符合条件的情况下，就以这个条件和更新内容为基础构建新文档。如果有符合条件，则正常更新。
整个操作是原子的。

```shell
db.<dbName>.update(cond, nDoc，isUpsert)
```

##### update all

默认更新操作只会更新匹配到的第一个文档。
要更新所有匹配到得文档，需要修改update的第四个参数为true。
由于服务器可能会修改默认更新的策略，所以最好所有的操作都显示指明此参数。

```shell
db.<dbName>.update(cond, nDoc，isUpsert, isUpdateAll)
```

### Other

#### 安全操作

以上的更新操作均是非安全的，不会等待数据库的响应，但也不是异步操作，只是不关心数据库的执行结果

### Save Shell

save是个shell函数，可以执行不存在时插入或存在时更新

```shell
db.yourDBName.save(doc)
var d = db.test.findOne()
d.x = 5
db.test.save(d)
```

### 查询操作

#### find & findOne

```shell
db.<dbName>.find([cond，slice])
db.<dbName>.findOne([cond, slice])
```

查询的值必须是常量，不能是变量或者引用其它文档

#### select keys
find或findOne的第二个参数，1表示包含，0表示排除

```shell
db.<dbName>.find(cond, selectKeys)
db.<dbName>.findOne(cond, selectKeys)
```

无论是否指定, `_id`总是会被返回

例如：
`db.yourDBName.find({}, {key1:1, key2:1})`

#### 查询条件

##### $ne, $lt, $gt, $lte, $gte

```shell
{key: {“$lt”: value}}
```

日期类型可以直接使用这些条件

例如：

```shell
start = new Date(“01/01/2007")
db.user.find({“registerDate”:{“$lt”: start}})
```

##### $in, $nin

```shell
{“$in”: [value1...]}
```

##### $or

```shell
{“$or”: [{cond1}, {cond2}...]}
```

##### $not
可以用在任何条件之前

```shell
{“$not”:{otherOps}}
```

##### 注意

修改器在外层文档中，条件在内层文档

#### 特定类型查询

##### null
null作为value可以匹配值为null或者不存在的文档

##### 正则表达式
MongoDB使用Perl表达式

```perl
/xxx/i
```

MongoDB可以为前缀正则(/^xxx/)建立索引，所以这种查询会比较快

#### 数组中的查询

##### $all

```shell
{key:{“$all”:[value1…]}}
```

##### $size

匹配拥有指定长度的数组

```shell
{key:{“$size”: value}}
```

##### $slice

作为find的第二个参数
选别数组的指定长度的子集合，正数从前开始取，负数从后开始取，也可以自己指定范围

```shell
{key:{“$slice”:size}}
{key:{“$slice”:[from, to]}}
```

与其它操作符不同，未提及的键都会一并返回

例如：
有如下记录

```shell
{ "_id" : ObjectId("5487c2cc15f90600b28eca10"), "name" : "joe", "age" : 49, "x" : [ 3, 4, 6, 7, 8 ] }
{ "_id" : ObjectId("5487e6b2aa8340c80df67bd6"), "age" : 59, "x" : 99 }
```

执行

```shell
db.people.find({},{"x":{"$slice":2}})
```

返回

```shell
{ "_id" : ObjectId("5487c2cc15f90600b28eca10"), "name" : "joe", "age" : 49, "x" : [ 3, 4 ] }
{ "_id" : ObjectId("5487e6b2aa8340c80df67bd6"), "age" : 59, "x" : 99 }
```

#### 查询内嵌文档

内嵌文档的查询要求完全匹配，指定每一个键
由于”.”在查询文档时表示深入内嵌文档，所以如果保存url等数据时应在插入之前先将”.”替换成其它不会混淆的特殊字符（如url的非法字符）

##### $elemMatch

内嵌文档的模糊匹配，指定一组条件时不用指定每一个键，用于对一个内嵌文档的多个键进行匹配时使用

```shell
{“$elemMatch”:cond}
```

#### $where

执行任意Javascript代码进行查询，效率比常规查询低很多

```shell
db.yourDBName.find({“$where”:”this.x + this.y==10"})
db.yourDBName.find({“$where”:function(){
     if.. return true;
     else…return false;
}})
```

函数返回true的结果会被作为文档返回
执行过程中每个BSON文档都会被转为Javascript对象，然后运行代码，此外也不能使用索引。

#### 游标

游标用于控制find的结果

```shell
var cursor = db.yourDBName.find()
cursor.hasNext()
cursor.next()
```

游标还实现了forEach接口

```shell
cursor.forEach(function(d){
     print(d.x);
});
```

游标的find()方法不会马上返回结果，所以可以加上各种限制条件
此外大部分游标对象上的方法都会返回游标本身

```shell
var cursor = db.foo.find().sort({“x”:1}).limit(1).skip(1);
```

#### limit, skip 和 sort

##### sort
sort中1表示升序，-1表示降序

##### skip
skip略过大量结果集时会很慢，应尽量避免

##### 不使用skip实现分页

1. 利用sort再limit限制第一页
2. 之后根据前一次的最后一条记录加上之前的sort查询其余页

##### 随机选取文档

低效做法是随机产生一个0到所有文档记录数量的数，然后skip掉之前的所有文档

好的做法是每次插入文档时加上一个随机字段，查询时产生一个随机数然后去find小于这个随机数的文档，这种情况下可以使用索引

#### 获取一致结果

将数据查询出来后经过转换后再存回去，由于某些存回去的文档可能变大，原来的位置无法容纳，导致这些结果被查到了集合的末尾，而游标也跟着到了末尾，游标指向的文档集合也发生了变化。快照可以解决这个问题。

##### $snapshot快照选项
对查询结果进行快照，所有返回一组结果的查询都进行了快照，但是使用游标时不会

### 索引

#### 特点

优化查找，插入会略慢

#### 何时使用索引

- 当查询条件总是带有某一个键时

- 当查询仅使用一个键时

#### 创建索引

```shell
db.yourDBName.ensureIndex({“name”:1})
```

1或-1表示索引方向，多个键上建立索引时需要考虑索引方向

例如

```shell
{user:1,date:-1}
```

索引建立时按用户名升序，再按日期降序，如果是大热站点，单用户的操作可能会特别多，会导致索引无法完全载入内存。
此时如果颠倒顺序的话，那么获得用户的最新状态时效率要高得多

##### 注意事项

默认每个集合的最大索引数为64个
如果常常需要查询一半以上的集合，那么表扫描可能比索引要快

#### 删除索引

```shell
db.yourDBName.dropIndex(...)
```

#### 扩展索引

索引建立时也可以使用内嵌文档(.)
