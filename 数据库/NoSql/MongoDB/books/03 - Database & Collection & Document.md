目录

[TOC]

摘要

>显示所有 DB，选择 DB，查看当前 DB，显示所有 Collection，_id，删除当前 DB

## DataBase, Collection与 Document

### DataBase

#### 显示所有 DB

```javascript
show dbs
```

#### 选择 DB

```javascript
use <dbName>
```

#### 查看当前 DB 名

```javascript
db
```

此 db 是一个 JavaScript 对象，包含有当前数据库的有关信息。

#### 删除当前 DB

```javascript
db.dropDatabase()
```

### Collection

#### 显示所有 collections

```javascript
show collections
```

### Document

#### _id

每个文档都有一个 `_id`。默认类型为 `ObjectId`，不同于其它数据库的自动增长的主键，`ObjectId` 在不同机器上也是唯一的，所以更适合作为分布式数据库。

