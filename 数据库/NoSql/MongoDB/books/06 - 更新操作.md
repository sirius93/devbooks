目录

[TOC]

摘要

>文档替换，修改器，inc，set，unset，push，addToSet，pop，pull，upsert，update all

## 更新操作

### 概述

- 更新操作是原子性的，先到服务器的命令会先执行。
- 更新操作接收两个参数，第一个参数为条件，第二个参数要么是新文档的内容，要么是修改器。

### 文档替换

```javascript
db.<collectionName>.update(cond, nDoc)
```

- `cond` 更新条件
- `nDoc` 新文档内容

此操作会完全替换原来的文档内容，同时可以修改 "_id"

例

```javascript
db.test.update( {"x" : 1}, {"hello":"hello world"} )
```

### 修改器

#### 语法

```javascript
db.<collectionName>.update(cond, modifierSequence)
```

使用修改器（modifierSequence）时可以修改指定部分内容，而不是完全替换，但是此操作无法替换 "_id"

#### ModifierSequence

##### inc

增加或减少一个键的值，如果不存在此键就创建它

```javascript
db.towns.update(
	{ _id : ObjectId("55227ee3a78613d8b7786b62") },
	{ $inc : {"population" : 100 }}
)
```

##### set

指定一个键的值，如果不存在此键就创建它

```javascript
db.towns.update(
	{ _id : ObjectId("55227ee3a78613d8b7786b62") },
	{ $set : {"population" : 1200 }}
)
```

##### unset

删除一个键，值可以是任何数

```javascript
db.towns.update(
	{ _id : ObjectId("553081a72783e2e8a0bdf19e") },
	{ $unset : {"population" : 1 }}
)
```

##### push

向数组末尾添加数据，不存在此键就创建它

```javascript
db.towns.update(
	{ _id : ObjectId("553081a72783e2e8a0bdf19e") },
	{ $push : {"arr" : 1 }}
)
```

##### addToSet

添加不重复的数据，不存在此键的话就创建它

一次添加单条数据

```javascript
db.towns.update(
	{ _id : ObjectId("553081a72783e2e8a0bdf19e") },
	{ $addToSet : {"arr" : 10 }}
)
```

一次添加多条数据

```javascript
db.towns.update(
	{ _id : ObjectId("553081a72783e2e8a0bdf19e") },
	{
      $addToSet : { "arr" : { $each : [20, 30, 40, 50] } }
    }
)
```

##### pop

从数组的一端删除数据

从末尾删除

```javascript
db.towns.update(
	{ _id : ObjectId("553081a72783e2e8a0bdf19e") },
	{ $pop : {"arr" : 1 }}
)
```

从头部删除

```javascript
db.towns.update(
	{ _id : ObjectId("553081a72783e2e8a0bdf19e") },
	{ $pop : {"arr" : -1 }}
)
```

##### pull

根据条件从数组中删除数据

```javascript
db.towns.update(
	{ _id : ObjectId("553081a72783e2e8a0bdf19e") },
	{ $pull : {"arr" : 10 }}
)
```

### update 参数

#### 概述

update 操作时可以指定一些参数来完成特定操作。

#### upsert

没有符合条件的情况下，就以这个条件和更新内容为基础构建新文档。如果有符合条件，则正常更新。
整个操作是原子的。

```javascript
db.<collectionName>.update(cond, nDoc，isUpsert)
```

例

```javascript
db.towns.update(
	{ _id : "abc" },
	{ $push : {"arr" : 10 } },
    true
)
```

#### update all

- 默认更新操作只会更新匹配到的第一个文档。
- 要更新所有匹配到得文档，需要修改 `update` 的第四个参数为 `true`。
- 由于服务器可能会修改默认更新的策略，所以最好所有的操作都显示指明此参数。

```javascript
db.<collectionName>.update(cond, nDoc，isUpsert, isUpdateAll)
```

例

```javascript
db.towns.update(
	{ },
	{ $push : {"arr" : 99 } },
    false,
    true
)
```

### Other

#### 安全操作

以上的更新操作均是非安全的，不会等待数据库的响应，但也不是异步操作，只是不关心数据库的执行结果。
