目录

[TOC]

摘要

>eval，runCommand，top，listCommands

## 服务端命令

### eval

```javascript
update_area = function() {
	db.phone.find().forEach({
		function(phone){
			phone.components.area++;
			phone.display = "+" +
				phone.components.country + " " +
				phone.components.area + "-" +
				phone.components.number;
			db.phones.update( { _id : phone._id }, phone, false );
		}
	})
}
```

执行如上代码时，客户端会依次将每条数据取出到本地然后再执行更新命令。

而 `eval` 可以将函数直接传递给服务器，让代码在服务器端执行，从而减少服务器和客户端的对话次数。

```sql
db.eval(update_area)
```

注意 `eval` 会阻塞 mongoDB，所以主要用于测试，不常见于产品环境。

### 预设命令

#### 概述

Mongo 预设了几个在服务器端执行的命令

#### runCommand

`runCommand` 可以运行大部分命令

```javascript
db.runCommand({"count" : "phones"})
```

通过 `db.runCommand` 可以看到其本身也是一种辅助函数，封装了对集合 `$cmd` 的调用，因此上述命令也可以转化为

```javascript
db.$cmd.findOne({'count' : 'phones'})
```

#### top

只能运行在 admin 数据库上

```javascript
db.runCommand("top")
```

输出服务器上所有集合的访问细节

#### listCommands

```sql
db.runCommand("listCommands")
```

输出所有命令

