目录

[TOC]

摘要

>GridFS，查询，上传，chunks，files

## GridFS

### 概述

分布式系统通常缺乏一个一致的文件系统。如果在几个不同节点上运行几个服务器，则必须手动上传图片复制到每个服务器上。

而 MongoDB 则采用了自己的分布式文件系统，名为 `GridFS`。

### 查询

```javascript
mongofiles -h localhost:27020 list
```

如果没有任何文件，则会得到一个空列表

### 上传

```javascript
mongofiles -h localhost:27020 -d blog put FavoriteList.txt
```

- `-h` 指定 host
- `-d` 指定 database

上传成功后会在 mongodb 中生成对应的数据集合`fs.chunks`，`fs.files`，所以也可以像普通集合一样使用它们。

其中

- `fs.chunks` 存储文件的数据
- `fs.files` 存储文件信息

例

fs.files

```javascript
{
	"_id" : ObjectId("5546f35c1bedfe2842b35eeb"),
	"filename" : "aa.html",
	"chunkSize" : 261120,
	"uploadDate" : ISODate("2015-05-04T04:19:40.644Z"),
	"md5" : "db42e2fecf6c09e9c67945381f8c5352",
	"length" : 1612
}
```