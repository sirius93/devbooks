function insertCity(name, population, last_census, famous_for, mayor_info){
	db.towns.insert({
		name:				name,
		population:		population,
		last_census:		last_census,
		famous_for:		famous_for,
		mayor_info:		mayor_info
	});
}

insertCity("NewYork", 6000,'2008-31-01',["hotdog"],{name:"Jim"})
insertCity("Shanghai", 586000,'2007-20-09',["beer","food"],{name:"Tony"})

db.towns.find(
  { famous_for : { $exists : true }}
)

db.countries.insert({
  _id : "us",
  name : "United States",
  exports : {
	foods : [
	  {name : "bacon", tasty : true}	,
	  {name : "burgers"}	
	]
  }
})
db.countries.insert({
  _id : "ca",
  name : "Canada",
  exports : {
	foods : [
	  {name : "bacon", tasty : false}	,
	  {name : "syrup", tasty : true}	
	]
  }
})
db.countries.insert({
  _id : "mx",
  name : "Mexico",
  exports : {
	foods : [
	  {name : "salsa" , tasty : true, condiment : true}
	]
  }
})

找出一个国家，不但出口 bacon ，而且 bacon 味道不错。
错误
db.countries.find(
	{'exports.foods.name':"bacon", 'exports.foods.tasty':true}
)
正确
db.countries.find(
	{
		'exports.foods' : {
			$elemMatch : {
				name : 'bacon',
				tasty : true
			}
		}
	}
)

查询一个国家，id 为 mx 或 name 为 United States

db.countries.find(
	{
		$or : [
			{ _id : "mx" },
			{ name : "United States"}
		]
	}
)

引用
创建引用
db.towns.update(
	{ _id : ObjectId("55227ee3a78613d8b7786b62") },
	{ $set : { country : { $ref : "countries", $id : "us" }}}
)

查找引用
var newyork = db.towns.findOne( { _id : ObjectId("55227ee3a78613d8b7786b62") });
方式一
db.countries.findOne( { _id : newyork.country.$id } )
方式二
db[ newyork.country.$ref ].findOne( { _id : newyork.country.$id })


索引
测试数据
populatePhones = function(area, start, stop){
	for(var i = start; i < stop; i++){
		var country = 1 + ((Math.random() * 8) << 0);
		var num = (country * 1e10) + (area * 1e7) + i;
		db.phones.insert({
			_id : num,
			components: {
				country : country,
				area : area,
				prefix : (i * 1e-4) << 0,
				number : i
			},
			display : "+" + country + " " + area + "-" + i
		});
	}
}

populatePhones(800, 555000, 565000)
db.phones.find().limit(2)

db.phones.find({display : "+1 800-5650001"}).explain()

创建索引

db.phones.ensureIndex(
	{ display : 1 },
	{ unique : true, dropDups : true}
)

db.phones.count({ 'components.number' : { $gt : 559999 }})
db.phones.distinct( 'components.number' , { 'components.number' : { $lt : 555005 }})
db.phones.group({
	initial : { prefixes : {} },
	reduce : function(phone, output) { output.prefixes[phone.components.prefix] = 1; },
	cond : { 'components.number' : { $gt : 559999 } },
	key : { 'components.area' : true },
	finalize : function(out){
		var arr = [];
		for (var p in out.prefixes){
			arr.push(  parseInt(p) );
		}
		out.prefixes = arr;
	}
})[0].prefixes

db.phones.group({
	initial : { prefixes : {} },
	reduce : function(phone, output) { output.prefixes[phone.components.prefix] = 1; },
	cond : { 'components.number' : { $gt : 559999 } },
	key : { 'components.area' : true }
})


服务端命令

update_area = function() {
	db.phone.find().forEach({
		function(phone){
			phone.components.area++;
			phone.display = "+" +
				phone.components.country + " " +
				phone.components.area + "-" +
				phone.components.number;
			db.phones.update( { _id : phone._id }, phone, false );
		}
	})
}

mapreduce

distinct

数据
distinctDigits = function(phone){
  var
    number = phone.components.number + '',
    seen = [],
    result = [],
    i = number.length;
  while(i--) {
    seen[+number[i]] = 1;
  }
  for (i=0; i<10; i++) {
    if (seen[i]) {
      result[result.length] = i;
    }
  }
  return result;
}
db.system.js.save({_id: 'distinctDigits', value: distinctDigits})

执行
db.eval("distinctDigits(db.phones.findOne({ 'components.number' : 555121 }))")

map
map = function() {
  var digits = distinctDigits(this);
  emit({digits : digits, country : this.components.country}, {count : 1});
}
reduce
reduce = function(key, values) {
  var total = 0;
  for(var i=0; i<values.length; i++) {
    total += values[i].count;
  }
  return { count : total };
}

results = db.runCommand({
	mapReduce : 'phones',
	map : map,
	reduce : reduce,
	out : 'phones.report'
})

db.phones.report.find({ '_id.country' : 8})



一般来说，在 WHERE 和 JOIN 中出现的列需要建立索引，但也不完全如此。因为 MySQL 只对 "<，<=，=，>，>=，BETWEEN，IN"，以及某些时候的 "LIKE" 才会使用索引。


mongod --shardsvr --dbpath data/mongo4 --port 27014
mongod --shardsvr --dbpath data/mongo5 --port 27015

mongod --configsvr --dbpath data/mongoconfig --port 27016

mongos --configdb localhost:27016 --chunkSize 1 --port 27020

mongo localhost:27020/admin
db.runCommand( { addshard : "localhost:27014" })
db.runCommand( { addshard : "localhost:27015" })

db.runCommand( { enablesharding : "test"})

db.runCommand( { shardcollection : "test.cities", key : {name : 1}})

mongoimport -h localhost:27020 --db test --collection cities \ --type json mongo_cities1000.json

db.runCommand({ geoNear : 'cities', near : [45.52, -122.67], num : 5, maxDistance :1 })