# ElasticSearch

## 安装

```xml
<dependency>
    <groupId>org.elasticsearch</groupId>
    <artifactId>elasticsearch</artifactId>
    <version>2.2.0</version>
</dependency>
```

## 使用

### Node

```java
Node node = NodeBuilder.nodeBuilder()
        .settings(Settings.builder()
            .put("path.home", "/Users/sidneyy/devtools/elasticsearch-2.2.0"))
        .node();
Client client = node.client();

String json = "{" +
        "\"user\":\"kimchy\"," +
        "\"postDate\":\"2013-01-30\"," +
        "\"message\":\"trying out Elasticsearch\"" +
        "}";
IndexResponse response = client.prepareIndex("twitter_index", "tweet_type")
        .setSource(json)
        .execute()
        .actionGet();
System.out.println(response);

node.close();
```

### TransportClient

Get 

```java
Client client = TransportClient.builder().build()
        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"), 9300));

GetResponse response = client.prepareGet("twitter_index", "tweet_type", "AVMrFW16VLjNgvbNGGSE_id")
        .setOperationThreaded(false)
        .get();
System.out.println(response.isExists());
System.out.println(response.getId());
System.out.println(response.getIndex());
System.out.println(response.getSourceAsString());
```

### 插入 Document

```java
Client client = TransportClient.builder().build()
        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"), 9300));

//use string
String json = "{" +
        "\"user\":\"use string\"," +
        "\"postDate\":\"2013-01-30\"," +
        "\"message\":\"trying out Elasticsearch\"" +
        "}";
IndexResponse response = client.prepareIndex("twitter", "tweet")
        .setSource(json)
        .execute()
        .actionGet();
System.out.println(response);

//use map
Map<String, Object> map = new HashMap<String, Object>();
map.put("user", "use map");
map.put("postDate", new Date());
map.put("message", "trying out Elasticsearch");
response = client.prepareIndex("twitter", "tweet")
        .setSource(map)
        .execute()
        .actionGet();
System.out.println(response);

//use jackson
User user = new User();
user.setUser("use jackson");
user.setPostDate(new Date());
user.setMessage("foobar");
ObjectMapper mapper = new ObjectMapper();
byte[] jsonBytes = mapper.writeValueAsBytes(user);
response = client.prepareIndex("twitter", "tweet")
        .setSource(jsonBytes)
        .execute()
        .actionGet();
System.out.println(response);

//user es helper
XContentBuilder builder = jsonBuilder()
        .startObject()
        .field("user", "kimchy")
        .field("postDate", new Date())
        .field("message", "trying out Elasticsearch")
        .endObject();
json = builder.string();
response = client.prepareIndex("twitter", "tweet")
        .setSource(jsonBytes)
        .execute()
        .actionGet();
System.out.println(response);


client.close();
```