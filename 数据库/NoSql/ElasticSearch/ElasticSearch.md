[TOC]

# ElasticSearch

## 安装

1. 下载 [压缩包](https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.4.0.tar.gz)
2. 解压
3. 运行 `bin/elasticsearch`
4. 控制台内执行 `curl http://localhost:9200/` 查看执行结果

## 原理

每个全文索引都是一个词元的倒排索引

### Lucene 如何做到实时更新，准实时搜索

新收到的数据写入到新的索引文件中
Lucene 把每次生成的倒排索引，叫做一个段(segment)。然后另外使用一个 commit 文件，记录索引内所有的 segment。而生成 segment 的数据来源，则是内存中的 buffer。
即
commit 文件记录索引内所有 segment 文件
新的数据写入内存 buffer 中，同时也写入 translog，每隔一段时间（默认 1s）写入新的 segment 文件，并更新 commit 文件
translog 用于解决故障内存 buffer 丢失的场景，translog 默认每 30 分钟或文件大于 512 MB 时进行一次 flush，即文件清空
刚写入的数据可以通过 translog 获取，但是需要等到 refresh 成为一个 segment 后才能被搜索到

一个索引对于多个 segment 文件，每 1s 产生新的 segment 会严重影响 io 效率，所以 es 会在后台起任务对小的 segment 进行合并操作，默认大于 5GB 的 segment 文件不会进行合并。该操作可以通过 `indices.store.throttle.max_bytes_per_sec` 进行限速，默认为 20MB，通常可以调高到 100MB。
对于日志系统，无需如此高的实时性，可以通过 `_settings` 接口加大 `refresh_interval` 参数
例：
```bash
curl -XPOST http://localhost:9200/logstash-2015.06.21/_settings -d'
{"refresh_interval": "10s"}'
```

### 分片路由

ElasticSearch 路由处理非常简洁
公式：shard = hash(routing) % number_of_prmiary_shards
每个数据有一个 routing 参数，默认情况下就是 `_id` 的值。由于路由完全依赖于分母，所以 ElasticSearch 索引的主分片数不能随便修改。

## 使用

### 写入

```bash
curl -XPOST http://localhost:9200/logstash-2015.06.21/testlog -d'
{"user": "Peter"}'
```
返回结果

```
{
    "_index": "logstash-2015.06.21",
    "_type": "testlog",
    "_id": "AVvhz0NKzki9Go9lI3sN",
    "_version": 1,
    "result": "created",
    "_shards": {
        "total": 2,
        "successful": 1,
        "failed": 0
    },
    "created": true
}
```

### 获取

```bash
curl -XGET http://localhost:9200/logstash-2015.06.21/testlog/AVvhz0NKzki9Go9lI3sN
```

### 全文搜索

#### QueryString

```bash
curl -XGET http://localhost:9200/logstash-2015.06.21/testlog/_search?q=Jane
```

输出

```
{
    "took": 11,
    "timed_out": false,
    "_shards": {
        "total": 5,
        "successful": 5,
        "failed": 0
    },
    "hits": {
        "total": 1,
        "max_score": 0.2876821,
        "hits": [
            {
                "_index": "logstash-2015.06.21",
                "_type": "testlog",
                "_id": "AVvh0opnzki9Go9lI3sQ",
                "_score": 0.2876821,
                "_source": {
                    "user": "Jane"
                }
            }
        ]
    }
}
```

基本语法
?q=<query_string>

其中，<query_string> 的值有以下几种：

- string 全文检索
- column:string 单字段的全文检索
- column:"string" 单字段的精确搜索
- column:(string1 OR string2) NOT,AND,OR 组合搜索
- _exists_:column 字段存在
- _missing_:column 字段不存在
- ?, * 通配符检索
- 正则表达式，ElasticSearch 正则很慢，一般不要使用
- string~ 近似搜索，如使用 `frist~` 查 `first`
- 数值和时间的范围搜索，date:["now-6h" TO "now"}，[ 表示闭区间， { 表示开区间


### 重建索引

ElasticSearch 本身不支持对索引进行 rename，mapping 等操作，所以需要使用 Logstash 等重新导出再导入

### 与 Spark Streaming 交互

可以通过 Spark Streaming 接收 Kafka 消息队列，然后写入 ElasticSearch

## 坑

### 启动时报 can not run elasticsearch as root

ElasticSearch 不允许 root 用户执行应用，可以新建一个用户

```bash
groupadd elsearch
useradd elsearch -g elsearch -p elasticsearch
chown -R elsearch:elsearch  elasticsearch
su elsearch
```
