# ELK

## Logstash

### 安装

安装指南 [https://www.elastic.co/downloads/logstash](https://www.elastic.co/downloads/logstash)

1. 下载 Logstash
  ```bash
  wget https://artifacts.elastic.co/downloads/logstash/logstash-5.3.2.tar.gz
  ```

2. 验证安装
  ```bash
  bin/logstash -e 'input { stdin { } } output { stdout {} }'
  ```
  等控制台显示 `Pipeline main started`，输入任意字符看是否有对应的输出显示

### 使用配置文件

1. 创建文件 `logstash.conf`
2. 修改文件内容
```
input { stdin { } }
output {
  elasticsearch { hosts => ["localhost:9200"] }
  stdout { codec => rubydebug }
}
```
3. 使用配置文件启动 Logstash
```bash
bin/logstash -f logstash.conf
```
4. 如果希望后台运行，可以使用 nohup 的方式或者使用 daemontools 工具

### 语法

#### 数据类型

布尔 debug => true
字符串 host => "host"
数值 port => 1234
数组 array => [1, 2, 3]
哈希 opts => {k1=>"a", k2=>"b"}

#### 字段引用

值中可以通过 `[]` 引用其它字段，对于数组类型可以使用索引，例 `[geoip][loc][0]`

#### 字符串插值

可以使用 `%{}` 在值中使用字符串插值

#### 条件语句

相等性 ==, !=, <, >, <=, >=
正则 =~, !~
包含 in, not in
布尔 and, or, nand, xor
一元 !()

### 命令行参数

-e 运行时配置
-f 使用配置文件，如果是目录的话会读取所有文本文件
-t 测试配置文件是否正确
-l 输出日志，生产环境下可以通过 `-l foo.log` 来输出到指定文件
-w 配置过滤插件的工作线程，如 `-w 5`，输入插件不支持多线程，输出插件在配置中设置
-P 加载本地插件，如 `-P /path/own/plugins`



### 配置

#### 基本配置

默认输出

{
    "@timestamp" => 2017-05-04T13:00:01.520Z,
      "@version" => "1",
          "host" => "iZwz959junxm4tmayk8sh8Z",
       "message" => "hello"
}

自定义配置

input {
  stdin {
    add_field => {"key" => "value"}
    codec => "plain"
    tags => ["add"]
    type => "std"
  }
}
output {
  stdout { codec => rubydebug }
}

新的输出结果

{
    "@timestamp" => 2017-05-04T13:01:13.827Z,
      "@version" => "1",
          "host" => "iZwz959junxm4tmayk8sh8Z",
       "message" => "hello",
          "type" => "std",
           "key" => "value",
          "tags" => [
        [0] "add"
    ]
}

其中 `type` 和 `tags` 为内置的字段， `type` 用于表示类型， `tags` 通常用于插件中，`add_field` 为自定义字段


#### 安装插件

查看本地已安装插件

bin/logstash-plugin list

安装插件，所有插件可以在 [https://github.com/logstash-plugins](https://github.com/logstash-plugins) 找到

bin/plugin install <plugin_name>

更新插件

bin/plugin update <plugin_name>

### 插件

#### 输入插件

##### logstash-input-stdin

用于配置输入项

```
stdin {
  add_field => {"key" => "value"}
  codec => "plain"
  tags => ["add"]
  type => "std"
}
```

##### logstash-input-file

用于监控文件变化

```
file {
  path => ["/var/log/*.log"]
  type => "system"
  start_position => "begining"
}
```

默认不会递归子目录，需要使用 `**` 这样的通配符来表示
start_position 只有从未监视过的文件才有用，sincedb 文件会记录具体位置，如果希望每次都使用该属性，需要手动删除 sincedb 文件

##### logstash-input-tcp

用于处理 tcp 请求，通常只做为临时任务，Logstash 只能缓存 20 个事件，所有生产环境下都会使用其它消息队列来做 Broker

#### Codec 插件

用以进行 encode 和 decode 事件

##### logstash-filter-json

用以进行 JSON 编码

例：

读取 Nginx 配置文件

修改 `nginx.conf`

```
log_format json     '{"@timestamp":"$time_iso8601",'
                    '"@version":"1",'
                    '"host":"$server_addr",'
                    '"client":"$remote_addr",'
                    '"responsetime":$request_time,'
                    '"domain":"$host",'
                    '"url":"$uri",'
                    '"status":"$status"}';
access_log  /var/log/nginx/access.log_json json;
```

修改 Logstash 配置文件

```
input {
  file {
    path => "/var/log/nginx/access.log_json"
    codec => "json"
  }
}
```

然后访问 Nginx 页面就可以了

##### logstash-codec-multiline

用以处理多行文本,否则输入时一行后回车就认为是一个事件

```
stdin {
  codec => multiline {
    pattern => "^\["
    negate => true
    what => "previous"
  }
}
```

以上就是匹配到 `^\[` 才认为是一个事件的结束

#### filter 插件

每个过滤插件都有 `add_tag`, `remove_tag`, `add_field`, `remove_field` 四个方法用以在匹配成功时进行处理

##### logstash-filter-date

用以进行时间处理，支持五种时间： ISO8601 （2011-01-11T02:33:31.212Z), UNIX(1970秒数), UNIX_MS(1970毫秒), TAI64N, Joda-Time（Java 的 Joda 时间库）

```
filter {
  date {
    match => ["logdate", "dd/MMM/yyyy:HH:mm:ss Z"]
  }
}
```

##### logstash-filter-grok

用以进行正则匹配

```
filter {
  grok {
    match => {
      "message" => "\s+(?<request_time>\d+(?:\.\d+)?)\s+"
    }
  }
}
```

以上输入 `begin 123.456 end` 会在输出上新增字段 `request_time`，值为 `123.456`

实际使用中通常将所有 grok 表达式写在一个文件中，然后通过 `patterns_dir` 指明文件

```
grok {
  patterns_dir => "/path/your/patterns"
}
```

如果需要匹配多行的话，需要在表达式开头加上 `(?m)`

##### logstash-filter-geoip

用以通过 GeoIP 服务根据 ip 查询地址

```
filter {
  geoip {
    source => "message"
    fields => ["city_name"]
  }
}
```

fields 用以指定需要输出的字段，不写的话会输出所有地址信息

##### logstash-filter-json

和 codec 不同，用以处理结果只有一部分是 JSON 格式的情况

```
filter {
  json {
    source => "message"
    target => "jsoncontent"
  }
}
```

不使用 target 的话会直接将结果嵌入在父层

##### logstash-filter-mutate

用以进行数据类型的转换，处理

```
filter {
  mutate {
    convert => ["request_time", "float"]
    split => ["message", "|"]
    join => ["message", ","]
    merge => ["message", "message"]
    strip => ["log"]  //去除前后空格
    rename => ["log", "log_json"]
  }
}
```

##### logstash-filter-ruby

用以执行 Ruby 代码，相比较使用各种插件进行数据转换和处理可以大幅提高 CPU 效率

```
filter {
  ruby {
    init => "@kname=['client', 'servername']"
    code => "event.append(Hash[@kname.zip(event['message'].split('|'))])"
  }
}
```

其中 `init` 定义由新字段名组成的数组，`code` 指定 Ruby 代码

##### logstash-filter-split

用以拆分多个事件

```
filter {
  split {
    field => "message"
    terminator => "#"
  }
}
```

以上表示以 `#` 作为分隔符


#### 输出插件

##### logstash-output-elasticsearch

输出到 ElasticSearch 中

```
output {
  elasticsearch {
    host => "192.168.1.2"
    protocol => "http"
    index => "logstash-%{type}-%{+YYYY.MM.dd}"
    index_type => "%{type}"
    workers => 5
    template_overwrite => true
  }
}
```

- index 为索引名，其中 `+` 后面的字符串会被认为是时间格式，由于 ES 不支持的原因，索引名不能使用大写字母。
- protocol 协议，有三种：node, http, transport。通常使用 node 是最方便的。

##### logstash-output-file

输出到文件中

```
output {
  file {
    path => "/path/to/%{+yyyy/MM/dd/HH}/%{host}.log.gz"
    message_format => "%{message}"
    gzip => true
  }
}
```

默认输出 event 的 JSON 格式，通过使用 message_format 可以输出原始格式，但是需要注意之前没有使用 remove_field 等删除原始数据。

##### logstash-output-stdout

输出到控制台

```
stdout {
  codec => rubydebug
  workers => 2
}
```

##### 输出到 HDFS

数据写入到 HDFS 通常是日志收集系统的最终目的，但 Logstash 官方不支持，可以使用社区的 logstash-webhdfs 或 logstash-hdfs 插件，前面一个使用了 Http 接口，后面一个使用了 Java 接口。

### 实战

#### Nginx 访问日志

Nginx 配置

```
log_format json '{"@timestamp":"$time_iso8601",'
                 '"host":"$server_addr",'
                 '"clientip":"$remote_addr",'
                 '"size":$body_bytes_sent,'
                 '"responsetime":$request_time,'
                 '"upstreamtime":"$upstream_response_time",'
                 '"upstreamhost":"$upstream_addr",'
                 '"http_host":"$host",'
                 '"url":"$uri",'
                 '"xff":"$http_x_forwarded_for",'
                 '"referer":"$http_referer",'
                 '"agent":"$http_user_agent",'
                 '"status":"$status"}';
```

Logstash 配置

```
input {
    file {
        path => "/var/log/nginx/access.log"
        codec => json
    }
}
filter {
    mutate {
        split => [ "upstreamtime", "," ]
    }
    mutate {
        convert => [ "upstreamtime", "float" ]
    }
}
```

#### Nginx 错误日志

```
filter {
    grok {
        match => { "message" => "(?<datetime>\d\d\d\d/\d\d/\d\d \d\d:\d\d:\d\d) \[(?<errtype>\w+)\] \S+: \*\d+ (?<errmsg>[^,]+), (?<errinfo>.*)$" }
    }
    mutate {
        rename => [ "host", "fromhost" ]
        gsub => [ "errmsg", "too large body: \d+ bytes", "too large body" ]
    }
    if [errinfo]
    {
        ruby {
            code => "
                new_event = LogStash::Event.new(Hash[event.get('errinfo').split(', ').map{|l| l.split(': ')}])
                new_event.remove('@timestamp')
                event.append(new_event)
            "
        }
    }
    grok {
        match => { "request" => '"%{WORD:verb} %{URIPATH:urlpath}(?:\?%{NGX_URIPARAM:urlparam})?(?: HTTP/%{NUMBER:httpversion})"' }
        patterns_dir => ["/etc/logstash/patterns"]
        remove_field => [ "message", "errinfo", "request" ]
    }
}
```

#### Java 日志

基于 Logback，参考 [https://github.com/logstash/logstash-logback-encoder](https://github.com/logstash/logstash-logback-encoder)

#### MySQL 慢查询

```
input {
  file {
    type => "mysql-slow"
    path => "/var/log/mysql/mysql-slow.log"
    codec => multiline {
      pattern => "^# User@Host:"
      negate => true
      what => "previous"
    }
  }
}

filter {
  # drop sleep events
  grok {
    match => { "message" => "SELECT SLEEP" }
    add_tag => [ "sleep_drop" ]
    tag_on_failure => [] # prevent default _grokparsefailure tag on real records
  }
  if "sleep_drop" in [tags] {
    drop {}
  }
  grok {
    match => [ "message", "(?m)^# User@Host: %{USER:user}\[[^\]]+\] @ (?:(?<clienthost>\S*) )?\[(?:%{IP:clientip})?\]\s*# Query_time: %{NUMBER:query_time:float}\s+Lock_time: %{NUMBER:lock_time:float}\s+Rows_sent: %{NUMBER:rows_sent:int}\s+Rows_examined: %{NUMBER:rows_examined:int}\s*(?:use %{DATA:database};\s*)?SET timestamp=%{NUMBER:timestamp};\s*(?<query>(?<action>\w+)\s+.*)\n# Time:.*$" ]
  }
  date {
    match => [ "timestamp", "UNIX" ]
    remove_field => [ "timestamp" ]
  }
}
```


#### Docker 日志

第一种方式，容器日志记录到宿主机上

第二种方式，使用 logspout

### Broker 扩展

#### Redis

官方推荐

##### 读取数据

支持三种数据类型，对应 Redis 的三种操作

list => BLPOP
channel => SUBSCRIBE
pattern_channel => PSUBSCRIBE

配置示例

```
input {
    redis {
        data_type => "pattern_channel"
        key => "logstash-*"
        host => "localhost"
        port => 6379
        threads => 5
    }
}
```

data_type 使用 pattern_channel 后可以使用正则匹配相同前缀的 key，一次性订阅多个频道

可以使用 redis-cli 输入 `publish logstash-demo "hello world"` 验证结果，消息会被消费 5 次，也可以直接 publish json 字符串，Logstash 会自动进行转换。

以上通过频道发布的消息会被广播到所有订阅者上，如果希望只有一个订阅者消费，需要使用 list 类型。

配置示例

```
input {
    redis {
        batch_count => 1
        data_type => "list"
        key => "logstash-list"
        host => "localhost"
        port => 6379
        threads => 5
    }
}
```

batch_count 可以支持批量推送，默认为 50

可以使用 redis-cli 输入 `rpush logstash-list "hello world"` 验证结果，消息会被消费 1 次，注意此时 key 必须完全匹配，测试批量推送可以输入 `rpush logstash-list "hello" "world" "goodbye"`

#### 输出到 Redis

配置示例

```
input { stdin {} }
output {
  redis {
    data_type => "channel"
    key => "logstash-chan-%{+yyyy.MM.dd}"
  }
}
```
可以使用 redis-cli 输入 `subscribe logstash-chan-2014.08.08` 验证结果


#### Kafka

##### Input 配置

input {
    kafka {
        zk_connect => "localhost:2181"
        group_id => "logstash"
        topic_id => "test"
        codec => plain
        reset_beginning => false # boolean (optional)， default: false
        consumer_threads => 5  # number (optional)， default: 1
        decorate_events => true # boolean (optional)， default: false
    }
}

想要使用多个 logstash 端协同消费同一个 topic 的话，那么需要把两个或是多个 logstash 消费端配置成相同的 group_id 和 topic_id， 但是前提是要把相应的 topic 分多个 partitions (区)，多个消费者消费是无法保证消息的消费顺序性的

###### Output 配置

output {
  kafka {
      bootstrap_servers => "localhost:9092"
      topic_id => "test"
      compression_codec => "snappy" # string (optional)， one of ["none"， "gzip"， "snappy"]， default: "none"
  }
}




### 坑

#### Logstash 启动过慢

原因分析见 [http://www.tuicool.com/articles/jEBBZbb](http://www.tuicool.com/articles/jEBBZbb) ，主要发生在虚拟机上，原因是随机数发生器产生随机数的速度过慢。

解决方法

```bash
yum -y install haveged
systemctl start haveged
systemctl enable haveged
```

安装完后再启动 Logstash




---

可以通过 top 查看 Logstash 启动的多个线程
线程名为 <xx 表示输入， |xx 为过滤，>xx 为输出
数据在线程间以事件形式进行传递
每个事件都会有一个 @timestamp 表示发生时间

Logstash 中进程分为 Shipper, Broker, Indexer 三种角色

事件的传递者（shipper）
Redis，就是我们所指定的Broker，用于进行事件流管理，在该角色中，LogStash还支持其他的队列技术：AMPQ和ZeroMQ。搜索/存储的路由工作由LogStash的Indexer实例负责执行。

shipper 读取数据转换格式发送给 broker，indexer 处理后进行存储


https://www.elastic.co/start
