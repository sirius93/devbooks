[TOC]

# ElasticSearch

- [官网](https://www.elastic.co/products/elasticsearch)

## 安装

1. 在 [官网](https://www.elastic.co/downloads/elasticsearch) 下载压缩包。
2. 解压后进入 `bin` 目录。
3. 执行 `./elasticsearch` 开启 es 服务，后台运行时加上 `-d`。
4. 访问 `http://localhost:9200/` 验证安装完成。

## 基本概念

在 Elasticsearch 中，存储数据的行为就叫做 索引(indexing) 。

与关系型数据库比较
关系数据库     ⇒ 数据库 ⇒ 表    ⇒ 行    ⇒ 列(Columns)
Elasticsearch  ⇒ 索引   ⇒ 类型  ⇒ 文档  ⇒ 字段(Fields)

## 使用

### Restful API

#### 基本结构

```bash
curl -X GET 'http://localhost:9200/<path>?pretty' -d '<JSON Body>'
```

- `pretty` 表示优化 JSON 格式

例

```bash
curl -X GET 'localhost:9200/_count?pretty' -d '
{
    "query": {
        "match_all": {}
    }
}'
```

#### 插入数据

```bash
PUT /megacorp/employee/1
{
    "first_name" : "John",
    "last_name" :  "Smith",
    "age" :        25,
    "about" :      "I love to go rock climbing",
    "interests": [ "sports", "music" ]
}
```

#### 检索文档

##### 单个文档

获得文档数据

```bash
GET /megacorp/employee/1
```

检查文档是否存在，存在返回200，不存在返回404

```bash
HEAD /megacorp/employee/1
```

##### 全部文档

默认返回最前的10个数值

```bash
GET /megacorp/employee/_search
```

根据简单条件进行匹配

```bash
GET /megacorp/employee/_search?q=last_name:Smith
```

#### 删除文档

```bash
DELETE /megacorp/employee/1
```

#### Query DSL

match

当匹配的单词是多个时（使用空格分隔），只要有一个匹配到就算匹配到，但是全部匹配到的 score 更高。

```bash
POST /megacorp/employee/_search
{
    "query" : {
        "match" : {
            "last_name" : "Smith"
        }
    }
}
```

match phrase

同 match，但是多个单词作为一个整体的短语进行匹配

```bash
{
    "query" : {
        "match_phrase" : {
            "about" : "rock climbing"
        }
    }
}
```

filter

```js
{
    "query" : {
        "filtered" : {
            "filter" : {
                "range" : {
                    "age" : { "gt" : 30 }
                }
            },
            "query" : {
                "match" : {
                    "last_name" : "Smith"
                }
            }
        }
    }
}
```

highlight

返回的结果会被 html 标签进行高亮标注。

```bash
{
    "query" : {
        "match_phrase" : {
            "about" : "rock climbing"
        }
    },
    "highlight": {
        "fields" : {
            "about" : {}
        }
    }
}
```

#### 统计

aggregation

类似 group by，以下例子中按 `interests` 字段进行分组，返回不同的值及数量

```bash
POST /megacorp/employee/_search
{
  "aggs": {
    "all_interests": {
      "terms": { "field": "interests" }，
      "aggs" : {
            "avg_age" : {
                "avg" : { "field" : "age" }
            }
        }
    }
  }
}
```

## 坑

1. 报 "JarHell" 异常，需要将JDK 中的 `ant-javafx.jar` 移除。

## 参考资料

- [Getting Started](https://www.elastic.co/guide/en/elasticsearch/reference/current/getting-started.html)
- [Elasticsearch 权威指南](http://www.learnes.net/)
- [Elasticsearch 开发指南](elasticsearch-guide)



---

可运行实例代码位于 `spring/first_elasticsearch/` 目录下
