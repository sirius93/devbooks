[TOC]

# Kibana

## 安装

1. 下载 [压缩包](https://artifacts.elastic.co/downloads/kibana/kibana-5.4.0-linux-x86_64.tar.gz)
2. 解压
3. 修改 `config/kibana.yml`，将 `server.host` 改为 `0.0.0.0` 否则只能本地访问，然后修改 `elasticsearch.url` 为 ElasticSearch 的地址。
4. 使用浏览器访问 `http://localhost:5601`
5. 默认 index 为 `logstash-*`，如果没数据的话会无法创建，可以通过 API 导入数据
```bash
curl -XPOST http://localhost:9200/logstash-2017.05.08/testlog -d'
{"user": "Peter"}'
```

## 配置

### 生产环境部署

Kibana 不是静态文件，需要消耗资源，生产环境可能要多点部署，用 Nginx 做代理。

```
upstream kibana5 {
    server 127.0.0.1:5601 fail_timeout=0;
}
server {
    listen               *:80;
    server_name          kibana_server;
    access_log           /var/log/nginx/kibana.srv-log-dev.log;
    error_log            /var/log/nginx/kibana.srv-log-dev.error.log;

    ssl                  on;
    ssl_certificate      /etc/nginx/ssl/all.crt;
    ssl_certificate_key  /etc/nginx/ssl/server.key;

    location / {
        root   /var/www/kibana;
        index  index.html  index.htm;
    }

    location ~ ^/kibana5/.* {
        proxy_pass           http://kibana5;
        rewrite              ^/kibana5/(.*)  /$1 break;
        proxy_set_header     X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header     Host            $host;
        auth_basic           "Restricted";
        auth_basic_user_file /etc/nginx/conf.d/kibana.myhost.org.htpasswd;
    }
}
```
