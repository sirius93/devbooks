# Cassandra

## 安装

```xml
<dependency>
    <groupId>com.datastax.cassandra</groupId>
    <artifactId>cassandra-driver-core</artifactId>
    <version>3.0.0</version>
</dependency>
<dependency>
    <groupId>com.datastax.cassandra</groupId>
    <artifactId>cassandra-driver-mapping</artifactId>
    <version>3.0.0</version>
</dependency>
<dependency>
    <groupId>com.datastax.cassandra</groupId>
    <artifactId>cassandra-driver-extras</artifactId>
    <version>3.0.0</version>
</dependency>
```

## 使用

### Cluser 和 Session 对象

```java
Cluster cluster = Cluster.builder()
                .addContactPoint("127.0.0.1")
                .withRetryPolicy(DowngradingConsistencyRetryPolicy.INSTANCE)
                .withReconnectionPolicy(new ConstantReconnectionPolicy(100))
                .withLoadBalancingPolicy(Policies.defaultLoadBalancingPolicy())
                .build();

//session  支持同步和异步操作
Session session = cluster.connect();
session.execute("create keyspace if not exists test with replication={'class': 'SimpleStrategy', 'replication_factor' : 3}");
session.close();

cluster.close();
```

#### 重试策略

- DefaultRetryPolicy    没有查询到数据，或者写入时超时的情况下进行重新查询
- DowngradingConsistencyRetryPolicy 与 DefaultRetryPolicy 一样，不同点是考虑了最终数据一致性问题
- FallthroughRetryPolicy    这一策略不重试查询，但允许客户端业务逻辑实现重试
- LoggingRetryPolicy    不重试查询，用来记录日志信息，info 级别

#### 重连策略

- ConstantReconnectionPolicy    固定时间间隔进行重连
- ExponentialReconnectionPolicy 指数级别增加重连时间，但不会超过最大重连时间

        

### 获得元数据

```java
Metadata metadata = cluster.getMetadata();
for (Host host : metadata.getAllHosts()) {
    System.out.println("==>" + host.getAddress());
}
for (KeyspaceMetadata keyspaceMetadata : metadata.getKeyspaces()) {
    System.out.println("==>" + keyspaceMetadata.getName());
}
```

### 基本语句

```java
 Session session = cluster.connect("custom keyspace");

// 创建表
 session.execute("create table if not exists blog (\n" +
        " userid int,\n" +
        " title text,\n" +
        " content text,\n" +
        " primary key(userid, title)\n" +
        ") with comment='create a table for test'");

// 新增数据
Insert insert = QueryBuilder
        .insertInto("testkeyspace1", "student")
        .value("name", "lisi")
        .value("age", 40);
session.execute(insert);

// 查询数据
Select.Where select = QueryBuilder
        .select()
        .all()
        .from("testkeyspace1", "student")
        .where(QueryBuilder.eq("name", "lisi"));
// 或者直接自行拼接语句
Result rs = session.execute("SELECT * FROM blog WHERE title='one'");

System.out.println(select);
ResultSet rs = session.execute(select);
for (Row row : rs.all()) {
    System.out.println("=>name: " + row.getString("name"));
    System.out.println("=>age : " + row.getInt("age"));
}

// 更新数据
System.out.println("更新数据...");
Update.Where update = QueryBuilder
        .update("testkeyspace1", "student")
        .with(QueryBuilder.set("age", 45))
        .where(QueryBuilder.eq("name", "lisi"));
System.out.println(update);
session.execute(update);
rs = session.execute(select);
for (Row row : rs.all()) {
    System.out.println("=>name: " + row.getString("name"));
    System.out.println("=>age : " + row.getInt("age"));
}

// 删除数据
System.out.println("删除数据...");
Delete.Where delete = QueryBuilder
        .delete()
        .from("testkeyspace1", "student")
        .where(QueryBuilder.eq("name", "lisi"));
System.out.println(delete);
session.execute(delete);
rs = session.execute(select);
for (Row row : rs.all()) {
    System.out.println("=>name: " + row.getString("name"));
    System.out.println("=>age : " + row.getInt("age"));
}
```

### PreparedStatement

```java
PreparedStatement statement = session.prepare("insert into testkeyspace1.student(name,age) values(?,?)");
session.execute(statement.bind("wangwu", 26));

String queryCQL = "select * from testkeyspace1.student";
ResultSet rs = session.execute(queryCQL);
List<Row> dataList = rs.all();
for (Row row : dataList) {
    System.out.println("=>name: " + row.getString("name"));
    System.out.println("=>age : " + row.getInt("age"));
}
```

### 异步调用 

```js
Session session = cluster.connect("test");
Select.Where select = QueryBuilder
        .select()
        .all()
        .from("blog")
        .where(QueryBuilder.eq("title", "one"));
ResultSetFuture future = session.executeAsync(select);
ResultSet rows = future.getUninterruptibly();
rows.forEach(System.out::println);
```

### 数据一致性

```js
QueryOptions options = new QueryOptions();
options.setConsistencyLevel(ConsistencyLevel.QUORUM);

cluster = Cluster.builder()
        .addContactPoint("127.0.0.1")
        .withQueryOptions(options).build();
Session session = cluster.connect();

PreparedStatement statement = session.prepare("INSERT INTO test.student(name, age) values (?, ?)");
statement.setConsistencyLevel(ConsistencyLevel.ONE);
session.execute(statement.bind("zhangsan", 20));
```