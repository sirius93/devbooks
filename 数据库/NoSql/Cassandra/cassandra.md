[TOC]

# Cassandra

## 基本概念

### 概述

由 Java 语言编写
不支持关联操作和子查询，所以设计时关联操作通常需要通过冗余数据或多次查询来实现
写性能高于读的性能

### 特点

- 高可扩展
- 高可用，无单点故障，P2P 去中心化处理
- NOSQL 列族实现
- *非常高的写入吞吐量和良好的读取吞吐量*
- CQL 查询语言
- 范围查询
- 可调节的一致性
- 灵活的模式

### 应用场景

待处理的数据量很大
数据量超过关系型数据库的承载能力
大量集群

### 各种概念

#### Cassandra 的排序规则

Cassandra 在定义列族时，可以包含一个名为 CompareWith 的元素，这个元素
决定了此列族的排序规则。Cassandra 提供的排序支持以下几种数据类型，包含了
字符、字节、数字和日期时间：AsciiType, BytesType, LexicalUUIDType, Integer
Type, LongType, TimeUUIDType, or UTF8Type

#### Cassandra 数据的设计模式

- 面向行（Row-Oriented）
- 在 Cassandra 中可以使用一个唯一识别号访问行，所以我们可以更好理解为，
Cassandra 是一个带索引的，面向行的存储。
- 无结构（Schema Free）
- 根据你的需求场景，你可以只保存你需要的数据，而不必拘泥于早前定义
的表结构。

### 数据模型

#### 列

一组包含名称=值对的数据叫做行（Row），而每一组名称-值对（Name/Value
Pair）被称之为列（Column）。

列最基本的数据结构是，它是一个 3 元的数据类型，包含：
name，value 和 timestamp。（再加一个客户端提供的时间戳，它常记录了最后一
次变更的时间）。其中 name 和 value 都是 byte[] 类型的。value 是非流式数据，会被全部加载到内存中，所以不适合存放大量数据。

#### 超级列（Super Column）

如果 Column 的 value 值不是单纯的数值，而是 Column，那么这
个 Column 就叫做 Super Column。SuperColumn 本身是不包含 timestamp 的。

#### 列族（Column Families）

多个列的键值对组成列族（Column Family）。
Column Family 的概念，用于逻辑上的分割，将同类的数据联系在一起。类似于关系数据库中的
table。

#### 键空间（Keyspaces）

列族的上一级容器就是键空间（Keyspaces）。键空间是 Cassandra 的数据容器，
可以理解为关系型数据库中的数据库（Database）。

#### 复合键（Composite Keys）

Cassandra 允许你使用 Key1:Key2 的结构来存储一对值作为Key，一个常见的例子是使用
<userid:lastupdate> 这样的结构来存储用户 ID 及最后登录时间。


### 数据一致性

一致性，在分布式系统中的所有数据备份，在同一时刻是否同样的值。数据一致性分为写操作一致性和读操作一致性。
Cassandra 采用最后数据一致性，也就是说数据最后保证一致，但当中的某一时刻并不保证每个节点的数据是一致的。

在 Cassandra 中，可以通过客户端来指定不同的一致性级别。
一般来说使用 QUORUM 级别。

#### 相关概念

- QUORUM：是一个可以计算的数字，对数据一致性重要作用
- Node：主要用来存储数据
- Data Center：数据中心
- 机柜（机架）：机房中放服务器的架子，通常一个机架上会放置多台服务器
- Cluster：一个 Cluster 包括多个数据中心
- CommitLog：主要记录下客户端提交过来的数据以及操作。这个数据将被持久
化到磁盘中，以便数据没有被持久化到磁盘时可以用来恢复。 当持久化完成后
 CommitLog 会被自动清除。
- Memtable：数据在 CommitLog 中记录完成后会写入 Memtable，它是用户写的数
据在内存中的形式。
. SSTable（sorted string table）：Cassandra 会根据规则定期把 Memtable 中的数据持久化到 SSTable 中，新的数据总是追加到文件末尾。一个列族一个 SSTable 文件。SSTable 创建后不能更改，修改的数据会被放到新的 SSTable 中，所以一个列族的数据可能分布在多个 SSTable 中，需要定期将多个 SSTable 合并到一起以增加读取效率。

#### QUORUM 级别

QUORUM 级别确保数据写到指定 quorum 数量的节点，计算公式：

```
quorum = (sum_of_replication_factors / 2) + 1
```

sum_of_replication_factors 指每个数据中心的所有 replication_factor 设置的总和。
假如现在有一个数据中心共 3 个副本节点，如果想确保读写一致性可以使用下面的公式：

```
(nodes_written + nodes_read) > replication_factor
```

### 节点的连接

#### 基本概念

节点的连接是由客户端驱动决定的。

由 Contact points 属性决定候选节点的 IP 地址集合
由 LoadBalancingPolicy 决定负载均衡策略

其中 driver 会顺序连接 Contact points，有一个成功后就会停止其它连接。并且连接后 driver 会从服务器获取所有可用节点的元数据构成连接池。
所以 contact points 不必设置所有节点，只要设置响应最快的节点地址就可以了

#### Gossip

Cassandra 使用 Gossip 协议在集群中进行通信

### 数据分发与复制



## 安装

前往官网 [链接](http://cassandra.apache.org/download/) 下载后直接解压。

## 使用

### 启动服务器

```bash
bin/cassandra [-f]
```

- `-f` 表示前台运行

### 停止服务器

```bash
ps -ef|grep CassandraDaemon
```

找到对应的进程号，kill xxx

### 连接客户端

```bash
bin/cqlsh
```

### 显示帮助

```bash
help [cmd]
```

没有指定命令的话则列出所有命令


## 配置

conf/cassandra.yaml

cassandra.yaml 决定节点属于哪个集群。

常用配置

- cluster_name ：这个节点属于的 Cassandra 集群的名字，集群中的每一个节点的
集群名字都应该是一样的。
- listen_address ：当前节点的 ip 地址或者主机名，使得其他节点能够联系到这个节
点。这个地址应该是公共地址，而不是 localhost。
- seed_provider ：通过逗号分隔的 seed 节点的主机名或 ip 地址。每个节点的此选项
应该是一致的。在包含多个数据中心的集群中，每个数据中心都应该有一个 seed 节
点。默认为 `- seeds: "127.0.0.1"`。
- storage_port ：节点间通信端口（默认为7000）。集群中每个节点的配置应该是
一样的。
- initial_token ：这个参数用于手动指定节点的token（哈希）值，一般不用配置
- num_tokens ：虚拟节点中定义随机分配到该节点的 token 的数量。原来情况下，
一个节点只有一个 token，很容易造成数据都分布在一个节点上的情况，采用虚拟
节点，每个节点有多个 token，可以平均存放数据，可以避免造成某个节点数据量
过大。如果某个节点的机器性能好一些，可以把这个数值设置的大一些，越大存储
的数据越多。如果所有机器配置都一样，建议所有机器节点配置一样的。默认为 256。


## CQL

### 概念

CQL 是 Cassandra Query Language 的简称，CQL 类似 SQL 的语句，
包括修改，查询，保存，变更数据的存储方式等等功能。每一行语句由分号（;）结
束。

在 CQL 里，keyspace，column 和 table 的名称是忽略大小写的，除非
用双引号（"）括起来，才是大小写敏感的。如果不用双引号括起来，即使 CQL 写成
大写，也会被保存为小写。

*数据类型*

ascii,bigint,blob,boolean,counter,decimal,double,int,
list,map,set,text,timestamp,uuid 等


### DDL

#### 概念

CQL 数据定义语句主要是 CREATE、DROP、ALTER，具体包括如下几点：

- CREATE KEYSPACE/TABLE/INDEX/TRIGGER/TYPE
- USE KEYSPACE
- ALTER KEYSPACE/TABLE/INDEX/TRIGGER/TYPE
- DROP KEYSPACE/TABLE/INDEX/TRIGGER/TYPE

#### KeySpace

创建 KeySpace

```
CREATE KEYSPACE (IF NOT EXISTS)? <identifier> WITH <properties>
```

properties

- replication 复制策略，为 Map 类型，是必需属性。有 SimpleStrategy、NetworkTopologyStrategy(推荐使用)、
OldNetworkTopologyStrategy 几种。值至少要包括 class 属性，其他的属
性有 `replication_factor` 等。
- durable_writes  是否使用 commitlog 持久化写入，默认为 true。

例

```sql
CREATE KEYSPACE Excalibur
           WITH replication = {'class': 'NetworkTopologyStrategy', 'DC1' : 1, 'DC2' : 3}
            AND durable_writes = false;
```

切换 KeySpace

```
USE <identifier>
```

修改 KeySpace

```
ALTER KEYSPACE <identifier> WITH <properties>
```

删除 KeySpace

```
DROP KEYSPACE ( IF EXISTS )? <identifier>
```

#### Table

创建 Table

```
<create-table-stmt> ::= CREATE ( TABLE | COLUMNFAMILY ) ( IF NOT EXISTS )? <tablename>
                          '(' <column-definition> ( ',' <column-definition> )* ')'
                          ( WITH <option> ( AND <option>)* )?

<column-definition> ::= <identifier> <type> ( STATIC )? ( PRIMARY KEY )?
                      | PRIMARY KEY '(' <partition-key> ( ',' <identifier> )* ')'

<partition-key> ::= <identifier>
                  | '(' <identifier> (',' <identifier> )* ')'

<option> ::= <property>
           | COMPACT STORAGE
           | CLUSTERING ORDER
```

option

- comment 对列族的描述信息。
- bloom_filter_fp_chance 指定 bloom_filter 算法的容错率，一般写 0.01 或者 0.1。
- caching 设置缓存方案。
- compaction 数据压缩策略。
- compression 数据压缩算法。
- default_time_to_live 存活时间，默认 0（永久存活）。
- memtable_flush_period_in_ms 内存数据刷新时间间隔。
- read_repair_chance 0-1 之间的数值，与数据的一致性有关。

例

```sql
CREATE TABLE timeline (
    userid uuid,
    posted_month int,
    posted_time uuid,
    body text,
    posted_by text,
    PRIMARY KEY (userid, posted_month, posted_time)
) WITH compaction = { 'class' : 'LeveledCompactionStrategy' }
AND comment='Important biological records';
```

修改 Table

```
<alter-table-stmt> ::= ALTER (TABLE | COLUMNFAMILY) <tablename> <instruction>

<instruction> ::= ALTER <identifier> TYPE <type>
                | ADD   <identifier> <type>
                | DROP  <identifier>
                | WITH  <option> ( AND <option> )*
```

删除 Table

```
DROP TABLE ( IF EXISTS )? <tablename>
```

删除 Table 内的数据

```
TRUNCATE ( TABLE | COLUMNFAMILY )? <tablename>
```

#### Index

创建索引

```
<create-index-stmt> ::= CREATE ( CUSTOM )? INDEX ( IF NOT EXISTS )? ( <indexname> )?
                            ON <tablename> '(' <index-identifier> ')'
                            ( USING <string> ( WITH OPTIONS = <map-literal> )? )?

<index-identifier> ::= <identifier>
                     | keys( <identifier> )
```

例

```sql
CREATE INDEX userIndex ON NerdMovies (user);
CREATE INDEX ON users (keys(favs));
CREATE CUSTOM INDEX ON users (email) USING 'path.to.the.IndexClass' WITH OPTIONS = {'storage': '/mnt/ssd/indexes/'};
```

删除索引

```
DROP INDEX ( IF EXISTS )? ( <keyspace> '.' )? <identifier>
```

#### Type  数据类型

自定义数据类型就是一组数据类型的组合

创建 Type

```
<create-type-stmt> ::= CREATE TYPE ( IF NOT EXISTS )? <typename>
                         '(' <field-definition> ( ',' <field-definition> )* ')'

<typename> ::= ( <keyspace-name> '.' )? <identifier>

<field-definition> ::= <identifier> <type>
```

例

```sql
CREATE TYPE address (
    street_name text,
    street_number int,
    city text,
    state text,
    zip int
)
```

修改 Type

```
<alter-type-stmt> ::= ALTER TYPE <typename> <instruction>

<instruction> ::= ALTER <field-name> TYPE <type>
                | ADD <field-name> <type>
                | RENAME <field-name> TO <field-name> ( AND <field-name> TO <field-name> )*
```

删除 Type

```
<drop-type-stmt> ::= DROP TYPE ( IF EXISTS )? <typename>
```

#### 触发器

```
CREATE TRIGGER ( IF NOT EXISTS )? ( <triggername> )?
ON <tablename>
USING <string>
```

具体的处理逻辑需要使用 java 语言编写，然后把 java 代码放到 Cassandra 的
 lib/triggers 目录下。Cassandra 启动的时候每个集群节点都会运行触发器。


### DML

Insert

设置时可以添加数据的过期时间

```
<insertStatement> ::= INSERT INTO <tablename>
                      ( ( <name-list> VALUES <value-list> )
                      | ( JSON <string> ))
                      ( IF NOT EXISTS )?
                      ( USING <option> ( AND <option> )* )?

<names-list> ::= '(' <identifier> ( ',' <identifier> )* ')'

<value-list> ::= '(' <term-or-literal> ( ',' <term-or-literal> )* ')'

<term-or-literal> ::= <term>
                    | <collection-literal>

<option> ::= TIMESTAMP <integer>
           | TTL <integer>
```

例

```sql
INSERT INTO NerdMovies (movie, director, main_actor, year)
                VALUES ('Serenity', 'Joss Whedon', 'Nathan Fillion', 2005)
USING TTL 86400;
```

Update

```
<update-stmt> ::= UPDATE <tablename>
                  ( USING <option> ( AND <option> )* )?
                  SET <assignment> ( ',' <assignment> )*
                  WHERE <where-clause>
                  ( IF <condition> ( AND condition )* )?

<assignment> ::= <identifier> '=' <term>
               | <identifier> '=' <identifier> ('+' | '-') (<int-term> | <set-literal> | <list-literal>)
               | <identifier> '=' <identifier> '+' <map-literal>
               | <identifier> '[' <term> ']' '=' <term>

<condition> ::= <identifier> <op> <term>
              | <identifier> IN (<variable> | '(' ( <term> ( ',' <term> )* )? ')')
              | <identifier> '[' <term> ']' <op> <term>
              | <identifier> '[' <term> ']' IN <term>

<op> ::= '<' | '<=' | '=' | '!=' | '>=' | '>'

<where-clause> ::= <relation> ( AND <relation> )*

<relation> ::= <identifier> '=' <term>
             | '(' <identifier> (',' <identifier>)* ')' '=' <term-tuple>
             | <identifier> IN '(' ( <term> ( ',' <term>)* )? ')'
             | <identifier> IN <variable>
             | '(' <identifier> (',' <identifier>)* ')' IN '(' ( <term-tuple> ( ',' <term-tuple>)* )? ')'
             | '(' <identifier> (',' <identifier>)* ')' IN <variable>

<option> ::= TIMESTAMP <integer>
           | TTL <integer>
```

例

```sql
UPDATE NerdMovies USING TTL 400
SET director = 'Joss Whedon',
    main_actor = 'Nathan Fillion',
    year = 2005
WHERE movie = 'Serenity';
```

Delete

```
<delete-stmt> ::= DELETE ( <selection> ( ',' <selection> )* )?
                  FROM <tablename>
                  ( USING TIMESTAMP <integer>)?
                  WHERE <where-clause>
                  ( IF ( EXISTS | ( <condition> ( AND <condition> )*) ) )?

<selection> ::= <identifier> ( '[' <term> ']' )?

<where-clause> ::= <relation> ( AND <relation> )*

<relation> ::= <identifier> <op> <term>
             | '(' <identifier> (',' <identifier>)* ')' <op> <term-tuple>
             | <identifier> IN '(' ( <term> ( ',' <term>)* )? ')'
             | <identifier> IN <variable>
             | '(' <identifier> (',' <identifier>)* ')' IN '(' ( <term-tuple> ( ',' <term-tuple>)* )? ')'
             | '(' <identifier> (',' <identifier>)* ')' IN <variable>

<op> ::= '=' | '<' | '>' | '<=' | '>='

<condition> ::= <identifier> (<op> | '!=') <term>
              | <identifier> IN (<variable> | '(' ( <term> ( ',' <term> )* )? ')')
              | <identifier> '[' <term> ']' (<op> | '!=') <term>
              | <identifier> '[' <term> ']' IN <term>
```

例

```sql
DELETE FROM NerdMovies USING TIMESTAMP 1240003134 WHERE movie = 'Serenity';
```

Batch

```
<batch-stmt> ::= BEGIN ( UNLOGGED | COUNTER ) BATCH
                 ( USING <option> ( AND <option> )* )?
                    <modification-stmt> ( ';' <modification-stmt> )*
                 APPLY BATCH

<modification-stmt> ::= <insert-stmt>
                      | <update-stmt>
                      | <delete-stmt>

<option> ::= TIMESTAMP <integer>
```

例

```sql
BEGIN BATCH
  INSERT INTO users (userid, password, name) VALUES ('user2', 'ch@ngem3b', 'second user');
  UPDATE users SET password = 'ps22dhds' WHERE userid = 'user3';
  INSERT INTO users (userid, password) VALUES ('user4', 'ch@ngem3c');
  DELETE name FROM users WHERE userid = 'user1';
APPLY BATCH;
```

### DQL

```
<select-stmt> ::= SELECT ( JSON )? <select-clause>
                  FROM <tablename>
                  ( WHERE <where-clause> )?
                  ( ORDER BY <order-by> )?
                  ( LIMIT <integer> )?
                  ( ALLOW FILTERING )?

<select-clause> ::= DISTINCT? <selection-list>
                  | COUNT '(' ( '*' | '1' ) ')' (AS <identifier>)?

<selection-list> ::= <selector> (AS <identifier>)? ( ',' <selector> (AS <identifier>)? )*
                   | '*'

<selector> ::= <identifier>
             | WRITETIME '(' <identifier> ')'
             | TTL '(' <identifier> ')'
             | CAST '(' <selector> AS <type> ')'
             | <function> '(' (<selector> (',' <selector>)*)? ')'

<where-clause> ::= <relation> ( AND <relation> )*

<relation> ::= <identifier> <op> <term>
             | '(' <identifier> (',' <identifier>)* ')' <op> <term-tuple>
             | <identifier> IN '(' ( <term> ( ',' <term>)* )? ')'
             | '(' <identifier> (',' <identifier>)* ')' IN '(' ( <term-tuple> ( ',' <term-tuple>)* )? ')'
             | TOKEN '(' <identifier> ( ',' <identifer>)* ')' <op> <term>

<op> ::= '=' | '<' | '>' | '<=' | '>=' | CONTAINS | CONTAINS KEY
<order-by> ::= <ordering> ( ',' <odering> )*
<ordering> ::= <identifer> ( ASC | DESC )?
<term-tuple> ::= '(' <term> (',' <term>)* ')'
```

注意点

- 可以使用 COUNT 计数函数，这也是 CQL 中唯一一个可以用的函数
- 可以使用 LIMIT 关键字限制数量
- 查询默认依赖索引，所以 WHERE 中指定的列必须出现在索引中，否则的话需要在查询语句最后指定 `ALLOW FILTERING` 关键字。
- 通常 PRIMARY KEY 中指定的第一个列名也叫做 PARTITION KEY，它对数据在节点上的分布起到了重要作用
- WHERE 中对于类型为 set\list 的列可以使用 CONTAINS 关键字，对于类型为 map 的列可以使用 CONTAINS
- KEY 关键字，对于 PRIMARY KEY 中的最后一个列可以使用 IN 关键字
- 集合类型不能用为 PRIMARY KEY，但是可以 CREATE INDEX

例

```sql
SELECT time, value
FROM events
WHERE event_type = 'myEvent'
  AND time > '2011-02-03'
  AND time <= '2012-01-01';

SELECT COUNT(*) AS user_count FROM users;  
```

## 参考资料

- [DataStax Cassandra 教程](http://docs.datastax.com/en/cassandra/3.x/cassandra/cassandraAbout.html#)
- [DataStax Cassandra Client](http://www.planetcassandra.org/apache-cassandra-client-drivers/)
----


例

```sql
create keyspace if not exists test with replication={'class': 'SimpleStrategy', 'replication_factor' : 3};

use test;

create table blog (
 userid int,
 title text,
 content text,
 primary key(userid, title)
) with comment='create a table for test';

create index titleIndex on blog(title);

create type description(
 tag text,
 score int
);

insert into blog(userid, title, content) values (1,'one', 'content one');
insert into blog(userid, title, content) values (2,'two', 'content two');
insert into blog(userid, title, content) values (3,'three', 'content three');


begin batch
  insert into blog(userid, title, content) values (10,'one', 'content ten');
  insert into blog(userid, title, content) values (20,'one', 'content twenty');
apply batch;
```







Cassandra数据分发与复制，包括三部分内容：
• 一致性哈希
• 虚拟节点 （vnode）
• 数据复制

一致性哈希决定数据存储在哪个节点上，基于主键的哈希值。

虚拟节点：每个节点负责多个部分的数据，当一个节点移除后，它负载的token会托管给多个节点，从而解决数据分布不均匀的问题。使用虚拟节点只要设置 num_tokens > 1。

数据复制：在集群的副本总数被称为复制因子，它决定的是一行数据被存储到多少
台机器上面。复制因子为1意味着每一行在一个节点只有一个副本。为2意味着每一
行有两个备份，其中每个副本是在不同的节点。所有的副本都同等重要，没有主副
之分。
• SimpleStrategy：单数据中心使用，SimpleStrategy根据主键的哈希值，决定
第一个副本应该复制到哪个节点，然后不考虑节点之间的拓扑网络结构，其他的
副本放置在环中顺时针方向的下一个节点。
• NetworkTopologyStrategy：当你的集群使用或者计划使用多数据中心时，请
使用NetworkTopologyStrategy。这种策略指定了每个数据中心有多少副本。

配置多个数据中心的集群最常见的两种方法:
• 每个数据中心两个副本：这种配置允许每个备份组出现单节点故障，同时仍然保
证本地读取的一致性水平为ONE。
• 每个数据中心三个副本：这种配置允许每个备份组出现单节点故障，同时保证本
地读取的一致性水平为LOCAL_QUORUM。或者每个备份组出现多节点故障，同
时保证本地读取的一致性水平为ONE。

分区器的概念
我们存储的数据是如何对应到相应的虚拟节点的呢？分区器决定了数据如何在集群
内被分发。简单来说，一个分区器就是一个用来计算partition key哈希值的一个哈
希函数。每一行数据，由partition key的值唯一标识，并且根据partition key的哈
希值决定如何在集群内被分发。包路径org.apache.cassandra.dht
• Murmur3Partitioner：基于MurmurHash哈希算法
• RandomPartitioner：基于MD5哈希算法
• ByteOrderedPartitioner：根据partition key的bytes进行有序分区

Murmur3Partitioner：基于MurmurHash哈希算法
• Murmur3Partitioner和RandomPartitioner都使用哈希值来平均分配一个
column family的数据到集群上的所有节点
• Murmur3Partitioner使用MurmurHash函数，MurmurHash哈希函数为每
一个行键值创建了64比特的哈希值。哈希数值的范围从-263 到2+63
• 当使用Murmur3Partitioner的时候，你可以在CQL 3 查询中使用token
function为所有行分页，在程序中记录上次查询最后一个primarykey的值

告密者（Snitch）
一个snitch（告密者）决定应当从哪个数据数据中心和机架写入和读取数据。
• Dynamic snitching：默认所有snitch都使用了一个动态snitch层，用来监控
读请求，当发现有延迟的时候，把请求转发到其他性能比较好的节点。默认
开启，官方推荐使用。
• SimpleSnitch：SimpleSnitch是默认使用的snitch，主要用于一个数据中心
的情况下。不考虑数据中心和机架的具体信息。通常在定义keyspace的时候，
使用SimpleStrategy。


一个snitch（告密者）决定应当从哪个数据数据中心和机架写入和读取数据。
• RackInferringSnitch：根据IP地址的第二段决定数据中心，第三段决定机架，
如192.168.199.201和192.168.222.20认为是同一个数据中心的节点，
192.168.199.201与192.168.199.202认为是同一个机架的节点。


---


1.Cassandra 运维常用工具

nodetool
cassandra-stress
sstableloader
sstablescrub
sstablesplit
sstablekeys
sstable2json


nodetool
查看集群信息的命令，在Cassandra安装目录下的bin目录中，可以查看集群统计信息、节点信息、数据环信息、增删节点、刷新Memtable数据到SSTable、合并SSTable等。
常用的命令有将近80个，这里介绍常用的几个命令。

nodetool version
显示当前Cassandra的版本信息

nodetool status
显示当前机器节点信息（UN正常，DN宕机）

nodetool upgradesstables
当进行Cassandra版本升级时，需要运行这个命令更新SSTable
upgradesstables 只升级旧版本的SSTable
upgradesstables -a 升级所有SSTable
upgradesstables keyspace 升级指定keyspace中所有旧版SSTable
upgradesstables keyspace table 升级指定keyspace中指定table的SSTable

nodetool stopdaemon
关闭Cassandra服务

nodetool snapshot
用于创建keyspace或table的快照信息，即数据备份，可用于数据的恢复

nodetool clearsnapshot
当创建了新的快照后，旧的快照并不会自动删除，clearsnapshot用于删除所有快照信息，所以为了避免误删，操作前，先把需要的快照移动到其他位置。
使用 -t snapshotname 指定要删除快照的名字
使用 -- keyspace 指定键空间


数据恢复
把快照文件复制到对应表的目录下(安装目录/data/keyspace/tablename-UUID)
运行nodetool refresh -- keyspace table加载新的SSTables


nodetool refresh -- keyspace  tablename
加载新的SSTables文件到集群中，不需要重启机器节点。


nodetool decommission
关闭当前节点，并把数据复制到环中紧邻的下一个节点。


nodetool describecluster
输出集群信息


nodetool describering
后面需要跟keyspace的名字，显示圆环的节点信息。


nodetool drain
会把memtable中的数据刷新到sstable，并且当前节点会终止与其他节点的联系。执行完这条命令需要重启这个节点。一般在Cassandra版本升级的时候才使用这个命令。如果单纯想把memtable中数据刷新到sstable，可以使用nodetool flush命令。


nodetool flush
会把memtable中的数据刷新到sstable，不需要重启节点。


nodetool getendpoints
查看key分布在哪一个节点上，需要三个参数：keyspace、table、keyname。


nodetool getsstables
查看key分布在哪一个SSTable上，需要三个参数：keyspace、table、keyname。


nodetool netstats
获取节点的网络连接信息，可以指定参数 -h 查看具体节点信息。


nodetool rebuild
当有新的数据中心加入，运行这个命令复制数据到数据中心。


nodetool repair
在删除数据的时候，Casssandra并非真实的删除，而是重新插入一条的数据，记录了删除的记录的信息和时间，叫做tombstone墓碑。使用nodetool repair，可以删除tombstone数据。频繁修改的数据节点可以使用这个命令节省空间、提高读速度。


nodetool tpstats
列出Cassandra维护的线程池的信息，你可以直接看到每个阶段有多少操作，以及他们的状态是活动中、等待还是完成。


nodetool cfstats
查看表的一些信息，包括读的次数，写的次数，sstable的数量，memtable信息，压缩信息，bloomfilter信息。


nodetool cleanup
清理不需要的keyspace，当新增数据节点或者减少数据节点的时候，数据会在节点间重新分发，可以运行这个命令，清除不再分布在这个节点上的keyspace，唯一目的就是为了节省磁盘空间。
后面不带参数清理所有不需要的keyspace
后面紧跟keyspace的名字，则清理对应的keyspace中的冗余


nodetool compact
合并sstable文件。


nodetool compactionstats
显示当前正在压缩的任务进度。





cassandra-stress
用于压力测试，可以模拟写入和读取
./tools/bin/cassandra-stress help option   # 查看帮助
-node 指定连接的节点，多个节点逗号隔开
-port 指定端口，如果修改过端口，那就必须指定
./tools/bin/cassandra-stress write n=1000000    # 插入一百万数据
./tools/bin/cassandra-stress read n=200000        # 读取20万行数据
./tools/bin/cassandra-stress read duration=3m # 持续三分钟，一直读取




sstableloader
用于加载sstable数据
载入大量外部数据至一集群；
将已经存在的SSTable载入到另外一个节点数不同或者复制策略不同的集群；
从快照恢复数据。
直接输入sstableloader会弹出帮助信息



sstablescrub
清洗指定的表的SSTable, 试图删除损坏的部分，保留完好的部分。因为是在节点关闭的状况下可以运行，所以它可以修复nodetool scrub不能修复的问题。
一般出现问题的时候先运行 nodetool scrub
如果第一步没解决问题，使用sstablescrub
关闭节点
运行sstablescrub命令  sstablescrub ks1 student --debug



sstablesplit
切割sstable成小文件。
运行前必须关闭cassandra服务
sstablesplit -s 40 /var/lib/cassandra/data/Keyspace1/Standard1/*
SizeTieredCompactionStrategy写密集型
LeveledCompactionStrategy 读密集型
DateTieredCompactionStrategy按照时间段压缩



sstablekeys
用于查看sstable中有哪些key



sstable2json  
以JSON的形式显示SSTable文件中的内容


=======================================

动态增删 Cassandra 机器节点
新增机器节点的步骤：
准备一个新机器，修改cassandra的配置文件中的listen_address和rpc_address为当前机器的内网地址或者公网地址。
启动新节点
等待数据迁移到新节点
运行./nodetool status命令查看节点状况

删除宕机机器节点步骤：
./nodetool status找出状态为DN的节点
./nodetool removenode host-id移除节点

删除线上机器节点步骤：
运行./nodetool decommission 关闭当前节点
再次运行上面删除宕机节点的步骤




设置 Cassandra 访问密码

修改配置文件 cassandra.yaml，把 authenticator: AllowAllAuthenticator改为authenticator: PasswordAuthenticator
重启cassandra
使用默认用户名cassandra和默认密码cassandra登录，./cqlsh -ucassandra -pcassandra
创建用户CREATE USER myusername WITH PASSWORD 'mypassword' SUPERUSER ; （NOSUPERUSER | SUPERUSER）
删除默认帐号：DROP USER cassandra;
