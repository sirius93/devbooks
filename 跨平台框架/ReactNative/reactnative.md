# ReactNative

## 安装手顺

1. 安装 nvm
```bash
brew install nvm
```

2. 安装 node.js
```bash
nvm install node && nvm alias default node
```

3. 安装 watchman 和 flow，watchman 用于实现热部署，flow 用于进行静态语法检查
```bash
brew install watchman
brew install flow
```

4. 为 nvm 添加淘宝源
```bash
vi ~/.npmrc
```
添加如下内容
```
registry = https://registry.npm.taobao.org
```

5. 安装 react-native 命令行工具
```bash
npm install -g react-native-cli
```

## 使用

新建项目

```bash
react-native init <project_name>
```

运行项目

```bash
react-native run-android
```

为存在的项目添加 react-native 支持

```bash
npm install
```

执行语法检查

```bash
flow check
```

查看日志

```bash
adb logcat *:S ReactNative:V ReactNativeJS:V
```

调试

对于实机需要设置IP 地址才能进行调试，点击 menu 键后选择 Dev Settings -> Debug server host for device。

## Style

### 基本概念

不用带单位
不支持百分比
支持盒模型(padding,margin 等)

### 定义 Style

通过 styles 对象

```js
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  thumbnail: {
    width:60,
    height:100
  }
});
```

直接定义 style

```js
<Text style={{
  fontSize: 20,
  margin: 10
}}>
  Welcome to React Native! hello world
</Text>
```

### 引用 Style

```js
<Text style={styles.welcome}>
  Welcome to React Native! hello world
</Text>
```

引用多个时

```js
<Text style=[{styles.welcome}, {styles.base}]>
  Welcome to React Native! hello world
</Text>
```

### 常用方法

获得屏幕宽高

```js
var Dimensions = require('Dimensions');
var {width,height} = Dimensions.get('window');
```

获得像素密度

```js
var PixelRatio = require('PixelRatio');
var ratio = PixelRatio.get();
```

## Flex

### 基本样式

```js
const styles = StyleSheet.create({
  container: {
    flex: 1, //layout_weight
    justifyContent: 'center',//子元素垂直方向alig，flex-end 为子元素靠底边对齐,center,flex-start,space-between一元素占头，一占尾，其余平级间隔排列,space-around每个元素前后都占据同样空间
    flexDirection: 'column',//linearlayout 排列方式，默认为 column
    backgroundColor: '#F5FCFF',
    // flexWrap: 'nowrap' //子元素占满时是否自动换行，nowrap 默认,wrap
    alignItems: 'flex-end'//子元素水平方向align,stretch 默认拉伸，flex-start靠左排列，center,
  },
  container1: {
    // flex: 1, //填充多余部分
    padding: 20,
    backgroundColor: 'gray',
    alignSelf: 'flex-start' //自身水平方向 align，值同 alignItems
  },
  container2: {
    padding: 20,
    backgroundColor: 'blue'
  },
  container3: {
    // flex: 2,
    padding: 20,
    backgroundColor: 'yellow'
  }
});
```


## Component

### 定义 Component

```js
class MyToast extends Component {
}
```

### 生命周期

```js
componentDidMount() {
  this.fetchData();
}
constructor(props) {
 super(props);
}
```

生命周期列表

componentWillMount  只会在装载之前调用一次，在 render 之前调用
componentDidMount 只会在装载完成之后调用一次，在 render 之后调用

以下不会在首次 render 组件的周期调用

componentWillReceiveProps
shouldComponentUpdate
componentWillUpdate
componentDidUpdate

卸载组件触发

componentWillUnmount

### 导出 Component

```js
export default class MyToast extends Component {
}
```

### 导入 Component

```js
import MyToast from './_plugin_toast';
```

### props

一般用于定义不可变属性

#### 定义 props

第一种方式，定义在类中

```js
class MyToast extends Component {
  static defaultProps = {
    maxLoops: 10
  };
  static propTypes = {
    text: React.PropTypes.string.isRequired,
    maxLoops: React.PropTypes.number.isRequired
  };
}
```

第二种，定义在类外

```js
MyToast.defaultProps = {
  maxLoops: 10
};
MyToast.propTypes = {
  text: React.PropTypes.string.isRequired,
  maxLoops: React.PropTypes.number
};
```

### state

一般用于定义可变状态

第一种

```js
constructor(props) {
  super(props);
  this.state = {loopsRemaining: this.props.maxLoops};
```

第二种

```js
state = {
    loopsRemaining: this.props.maxLoops
};
```

### 事件处理

绑定事件

```js
class LikeButton extends Component {
  getInitialState() {
    return { liked: false };
  }

  handleClick(e) {
    this.setState({ liked: !this.state.liked });
  }

  render() {
    const text = this.state.liked ? 'like' : 'haven\'t liked';
    return (
      <p onClick={this.handleClick.bind(this)}>
          You {text} this. Click to toggle.
      </p>
    );
  }
}
```

## 常用组件

### Text

文本可以嵌套文本

```js
<Text numberOfLines={5}>Hello World</Text>
```

numberOfLines 设置行数限制

### Image

#### 加载图片

##### 网络资源

网络资源由于事先不知道大小，所以引入时必须指定大小才能正常显示

```js
<Image source={{uri: 'http://www.example.com/img/foobar.png'}} style={{width: 400, height: 400}}/>
```

##### 静态资源

路径基于当前的 JS 文件

```js
<Image source={require('./img/check.png')}/>
```

资源文件可以有几种后缀

- 基于平台，如 `foobar.android.png`
- 基于IOS的解析度，如 `foobar@3x.png`，Android 也会自动搜寻最相近的解析度

>重要, 传入 `require` 的资源文件名必须是静态的，不能是字符串拼接，否则只会找名称完全一致的文件名，而不是根据解析度进行匹配。

##### 应用本地资源

Android 的 drawable 和 iOS 的 asset 目录，本地资源不用加后缀，目前不支持 mipmap 下的路径

```js
<Image source={{uri: 'app_icon'}} style={{width: 40, height: 40}} />
```

#### resizeMode

类似 Android 中的 scaleType，有 'cover', 'contain', 'stretch' 三种。

#### 嵌套元素

不同于 HTML，Image 是可以嵌套其它元素的，此时 Image 就相当于作为其它元素的背景图片。这样可以用作图片的 PlaceHolder。

```js
<Image source={require('image!foobar')}>
  <Text>foobar</Text>
</Image>
```

### ListView

```js
var REQUEST_URL = 'https://raw.githubusercontent.com/facebook/react-native/master/docs/MoviesExample.json';
class AwesomeProject extends Component {
  componentDidMount() {
    this.fetchData();
  }
  constructor(props) {
   super(props);
   this.state = {
       dataSource: new ListView.DataSource({
         rowHasChanged: (row1, row2) => row1 !== row2,
       }),
       loaded: false,
     };
 }
 fetchData() {
    fetch(REQUEST_URL)
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(responseData.movies),
          loaded: true,
        });
      })
      .done();
  }

 renderLoadingView() {
   return (
     <View>
      <Text>Loading...</Text>
     </View>
   );
 }

 renderMovie(movie) {
  return (
    <View style={styles.container}>
      <Image
        source={{uri: movie.posters.thumbnail}}
        style={styles.thumbnail}
      />
      <View>
        <Text style={styles.title}>{movie.title}</Text>
        <Text style={styles.year}>{movie.year}</Text>
      </View>
    </View>
  );
}

  render() {
    if (!this.state.loaded){
      return this.renderLoadingView();
    }
    return (
      <View style={styles.container}>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={this.renderMovie}
          />
      </View>
    );
  }
}
```


## 触摸事件

### 点击事件

常用的有四种

```js
<TouchableHighlight
      onPressIn={this._onPressIn}
      onPressOut={this._onPressOut}
      onPress={this._onPress}
      onLongPress={this._onLonePress}>
    <Image source={require('./images/foobar.jpg')} style={{width: 50, height: 50}}/>
  </TouchableHighlight>
```

### 点击效果

共有三种点击效果

#### TouchableOpacity

点击后改变透明度

```js
<TouchableOpacity onPress={this._onPress}>
  <Text>text one</Text>
  <Text>text two</Text>
</TouchableOpacity>
```

#### TouchableHighlight

点击高亮效果，只能有一个子节点

```js
<TouchableHighlight onPress={this._onPress}>
  <View>
    <Text>text one</Text>
    <Text >text two</Text>
  </View>
</TouchableHighlight>
```

#### TouchableNativeFeedback

原生动画效果，只能有一个子节点

```js
<TouchableNativeFeedback onPress={this._onPress}>
  <View>
    <Text>text one</Text>
    <Text >text two</Text>
  </View>
</TouchableNativeFeedback>
```


## 动画效果

### 创建动画

```js
class AwesomeProject extends Component {
  constructor(props: any) {
   super(props);
   this.state = {
     bounceValue: new Animated.Value(0)
   };
  }
  componentDidMount() {
    this.state.bounceValue.setValue(1.5);     // Start large
    Animated.spring(                          // Base: spring, decay, timing
      this.state.bounceValue,                 // Animate `bounceValue`
      {
        toValue: 0.8,                         // Animate to smaller size
        friction: 1,                          // Bouncier spring
      }
    ).start();                                // Start the animation
  }
  render() {
    return (
      <Animated.Image                         // Base: Image, Text, View
        source={{uri: 'http://i.imgur.com/XMKOH81.jpg'}}
        style={{
          flex: 1,
          transform: [                        // `transform` is an ordered array
            {scale: this.state.bounceValue},  // Map `bounceValue` to `scale`
          ]
        }}
      />
    );
  }
}
```



## 原生模块

### Android

步骤

1. 添加 Java 模块，需要继承自 `ReactContextBaseJavaModule`。
```java
public class ToastModule extends ReactContextBaseJavaModule {

  private static final String DURATION_SHORT_KEY = "SHORT";
  private static final String DURATION_LONG_KEY = "LONG";

  public ToastModule(ReactApplicationContext reactContext) {
    super(reactContext);
  }

  // 定义模块名
  @Override
  public String getName() {
    return "SimpleToast";
  }

  // 返回给js的预定义常量
  @Nullable
  @Override
  public Map<String, Object> getConstants() {
    final Map<String, Object> constants = new HashMap<>();
    constants.put(DURATION_SHORT_KEY, Toast.LENGTH_SHORT);
    constants.put(DURATION_LONG_KEY, Toast.LENGTH_LONG);
    return constants;
  }

  // 返回给js的预定义方法
  @ReactMethod
  public void show(String message, int duration) {
    Toast.makeText(getReactApplicationContext(), message, duration).show();
  }
}
```

2. 注册模块

```java
public class AnExampleReactPackage extends MainReactPackage {

    //注册模块
    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactApplicationContext) {
        List<NativeModule> modules = new ArrayList<>();
        modules.add(new ToastModule(reactApplicationContext));
        return modules;
    }

}
```

然后在 ReactActivity 的子类中编写如下代码

```java
@Override
protected List<ReactPackage> getPackages() {
    return Arrays.<ReactPackage>asList(
            new MainReactPackage(),
            new AnExampleReactPackage()
    );
}
```

3. 在 JS 中调用方法

```js
import React, {
  ...,
  NativeModules
} from 'react-native';

NativeModules.SimpleToast.show('foo', Toast.LONG);

var toast = NativeModules.SimpleToast;
toast.show('bar',Toast.LONG);
```

## 设备信息

### 屏幕解析度

```js
var {width,height} = Dimensions.get('window');
var ratio = PixelRatio.get();
```

## Redux

### 安装

```bash
npm install --save redux
npm install --save react-redux
npm install --save redux-thunk
npm install --save-dev redux-devtools
```

### 架构

#### Flux 模式

![](http://cdn4.infoqstatic.com/statics_s1_20160105-0313u5/resource/news/2014/05/facebook-mvc-flux/zh/resources/0519001.png)

数据单向流动

Action -> Dispatcher -> Store -> View -> Action...

Action 发送给 Dispatcher
Store 向 Dispatcher 注册自己
Dispatcher 接收到 Action 后发送给所有注册在上面的 Store
Store 更新状态
View 从 Store 中获得最新状态更新视图

#### Redux

Redux 是 js 的状态容器，提供可预测化的状态管理。由 Flux 演变而来，简化了 Flux 的流程。

Reducer 处理数据，接收不同的 Action Type 和旧的 state，返回新的 state。Reducer 本质表示了如何更新 state。Reducer 应该没有副作用，只要传入的参数一致，必须返回一致的结果，所以不能使用 `Date.now()` 等非纯函数。当遇到未知的 action 时，reducer 应该返回旧的 state。
Action 发送请求（服务器响应，用户输入）到 store，是 store 的唯一数据源。Action 本质是表示了一件事情的发生。
Store 存储 state，一个应用只应有一个 store 存在。


### 使用

文件结构

app/
  actions/
  components/
  containers/
    App.js         应用基本界面
    xxx.js         应用具体界面
  reducers/
index.android.js   注册程序

组件

组件分为容器组件和展示组件。
容器组件工作在路由层，从 Redux 获取 state，向 Redux 派发 actions。
展示组件属于中间层，只是接受数据并展示，不知道数据的来源也不知道 Redux 的存在。

### 异步 Action

一般情况下，每个 API 请求都至少需要 dispatch 三个不同的 action：
一个通知 reducer 请求开始的 action。
一个通知 reducer 请求成功结束的 action。
一个通知 reducer 请求失败的 action。



### 相关插件

#### Bindings

react-redux

提供了在 React 项目中使用 Redux 架构的功能
react-redux 提供两个关键模块：Provider 和 connect
Provider这个模块是作为整个App的容器，在你原有的App Container的基础上再包上一层，它的工作很简单，就是接受Redux的store作为props。
connect([mapStateToProps], [mapDispatchToProps], [mergeProps], [options])
connect就是将store中的必要数据(不是整个 state)作为props传递给React组件来render，并包装action creator用于在响应用户操作时dispatch一个action。connect返回一个函数，它接受一个React组件的构造函数作为连接对象，最终返回连接好的组件构造函数。

#### Middleware

redux-thunk

将 ActionCreator 转换为 Thunk ActionCreator 来实现异步操作

```js
//同步 Creator
function receivePosts(subreddit, json) {
  return {
    type: RECEIVE_POSTS,
    subreddit,
    posts: json.data.children.map(child => child.data),
    receivedAt: Date.now()
  }
}

//异步 Creator
export function fetchPosts(subreddit) {
  return function (dispatch) {
    // 首次 dispatch：更新应用的 state 来通知 API 请求发起了。
    dispatch(requestPosts(subreddit))

    // thunk middleware 调用的函数可以有返回值，
    return fetch(`http://www.subreddit.com/r/${subreddit}.json`)
      .then(response => response.json())
      .then(json =>
        // 这里，使用 API 请求结果来更新应用的 state。
        dispatch(receivePosts(subreddit, json))
      )
  }
}
```

redux-logger

#### Routing

react-router-redux

## JSX

### 元素

JSX 小写的字符串是 HTML 标签，大写开头的变量是 React 组件。

```js
var myDivElement = <div/>;
var myElement = <MyComponent/>;
```

### 属性

元素属性使用 `{}` 定义，而不是 `""`。

```js
var myDivElement = <div className="foo" />;
var myElement = <MyComponent someProperty={true} />;
```

### 注解

类似 JavaScript，但是子元素位置的注解需要使用 `{}` 包裹。

```js
<Node>
  {/* child comment, put {} around */}
  <SubNode>
```

### 展开属性 spread attributes

`...` 展开操作符，可以将属性直接扩散到元素上，无需直接指定。扩散时也可以在元素上指定某一个进行覆盖。

```js
var props = {};
props.foo = x;
props.bar = y;
var component = <Component {...props} foo={'override'}/>;
```

## 常用代码

### 超时

```js
function timer(t){
  return new Promise(resolve=>setTimeout(resolve, t));
}

await Promise.race([ apiGet('/api/foo'), timer(1000)]);
```


## Nuclide 的使用

安装 Nuclide

apm install nuclide

升级插件

apm update

推荐插件

language-babel


cmd+t 快速打开文件
ctrl+tab/ctrl+shift+tab 切换标签
cmd+shift+p 命令窗口

命令
React Native:Start Packager 开启服务器



## 和 Weex 区别

RN 单位采用原生 dp/pt，Weex 则假设宽度为 750px


## 参考资料

- [ReactNative 官方教程](http://facebook.github.io/react-native)
- [Nuclide 官网](http://nuclide.io/)
- [ECMAScript 6入门](http://es6.ruanyifeng.com/#docs/let)
- [Flex 布局教程](http://www.ruanyifeng.com/blog/2015/07/flex-grammar.html?utm_source=tuicool)
- [深入 JSX](https://facebook.github.io/react/docs/jsx-in-depth-zh-CN.html)
- [React 入门教程](https://hulufei.gitbooks.io/react-tutorial/content/jsx-in-depth.html)
- [React Native 的ES5 ES6写法对照表](http://bbs.reactnative.cn/topic/15/react-react-native-%E7%9A%84es5-es6%E5%86%99%E6%B3%95%E5%AF%B9%E7%85%A7%E8%A1%A8)
- [Redux 官方文档](http://redux.js.org/index.html)
- [Redux 中文文档](http://camsong.github.io/redux-in-chinese/index.html)
# ReactNative

