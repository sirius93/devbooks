中国地区安装代理问题
https://flutter.dev/community/china

https://flutter.cn/

教程

https://flutter.cn/docs/cookbook/navigation/passing-data

插件仓库
https://pub.flutter-io.cn

升级
flutter upgrade

安装
flutter doctor

## 创建和运行项目

### 创建项目

vscode的《查看》->《命令面板》->输入"flutter new project"创建项目。

### 运行项目

vscode的《调试》-> 启动调试

### 配置项目

#### 允许访问网络

```
<key>io.flutter.embedded_views_preview</key>
<true/>
<key>NSAppTransportSecurity</key>
```

#### 使用WebView

```
<dict>
  <key>NSAllowsArbitraryLoads</key>
  <true/>
</dict>
```

#### 依赖管理

基本配置都在 `pubspec.yaml` 文件中。

添加新配置后可以执行 `flutter packages get` 获取新包或者选vscode右上角的 `Get Packages` 按钮。

## 基本概念

Scaffold 是 Material 库中提供的一个 widget，它提供了默认的导航栏、标题和包含主屏幕 widget 树的 body 属性。
Flutter中几乎所有都是Widget，包括对齐等。

Stateless widgets 是不可变的，这意味着它们的属性不能改变 —— 所有的值都是 final。
Stateful widgets 持有的状态可能在 widget 生命周期中发生变化，实现一个 stateful widget 至少需要两个类：
1）一个 StatefulWidget 类。
2）一个 State 类，StatefulWidget 类本身是不变的，但是 State 类在 widget 生命周期中始终存在。

## 基本使用

### 第一个应用

```dart
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter layout demo',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Flutter layout demo'),
        ),
        body: Center(
          child: Text('Hello World'),
        ),
      ),
    );
  }
}
```

### 导入与导出

导入语句

语法

`import 'package:包名/资源文件'`

举例

`import 'package:english_words/english_words.dart';`

### Route

#### 基本跳转，也可以用于传参

```
Navigator.push(
   context,
   // 通过构造方法传参
   MaterialPageRoute(builder: (context) => SecondRoute(item: item, index: i)),
 );
```

#### 返回前一个画面

```
Navigator.pop(context);
```

#### 基于路由名进行跳转

定义路由名

```
runApp(MaterialApp(
    title: 'Named Routes Demo',
    initialRoute: '/',
    routes: {
      '/': (context) => FirstScreen(),
      '/second': (context) => SecondScreen(),
    },
));
```

实行跳转

```
Navigator.pushNamed(context, '/second');
```

### Provider

provider用于管理状态

- ChangeNotifier  通过notifyListeners()向监听器发送通知
- ChangeNotifierProvider  用于向其子孙节点暴露ChangeNotifier实例
- Consumer  响应notifyListeners()的内容

`Provider.of<CartModel>(context, listen: false).add(item);`
用于仅响应数据而无需更新UI。

#### 创建Provider

1. 创建无需响应更新的Provder，仅做状态管理的仓库，该Provider只是普通的类，不能继承Notify

```dart
Provider(create: (context) => NoteProvider())
```

2. 创建响应数据的Provder，该Provider需继承自ChangeNotifier，且调用通过notifyListeners()发送数据更新的通知

```dart
ChangeNotifierProvider(
  create: (context) => Counter(),
  child: MyApp(),
),
```

3. 创建依赖于其它Provider的Provider

```dart
// P1
Provider(create: (context) => P1()),

// P2，因为P2需要P1, 所以需要使用代理
ChangeNotifierProxyProvider<P1, P2>(
  create: (context) => new P2(),
  update: (context, p1, preP2) {
    return new P2(p1);
  },
),
```

4. 创建多个Provider

```dart
MultiProvider(
  providers: [p1, p2],
  child: MyApp(),
)
```

#### 定义Provider

```dart
class MyProvider with ChangeNotifier {
  void test(){
    // 发送更新通知
    notifyListeners();
  }
}
```

#### 使用Provider

获得Provder的实例

```dart
var provider = Provider.of<MyProvider>(context, listen: false);
```

在Widget的initState()方法中不要使用listen:true参数，当true时Provider.of()方法会调用context.inheritFromWidgetOfExactType，该方法不能再initState中使用，而false时会调用context.ancestorInheritedElementForWidgetOfExactType。true会导致context重新注册到Provider上，而使画面重绘。

除了`of`还有`value`方法，区别是of提供了dispose()方法来释放资源。

#### 响应更新

每次Provider调用notifyListeners()时会更新

```dart
Consumer<Counter>(
  builder: (context, provider, child) => (
    // TODO
  ),
),
```

### 网络请求

#### FutureBuild

```
// return new FutureBuilder<List<Note>>(
//   future: fetchNotes(http.Client()),
//   builder: (context, snapshot) {
//     if (snapshot.hasError) {
//       print(snapshot.error);
//     } else {
//           print("刷新Note了,${snapshot.data}");
//       // Provider.of<NoteModel>(context, listen: false).setNotes(snapshot.data);
//     }
    return error!=null
        ? Center(child: CircularProgressIndicator())
        : Consumer<NoteModel>(
            builder: (context, noteModel, child) {
              return new ListView.builder(
                padding: const EdgeInsets.all(16.0),
                itemBuilder: (context, i) {
                  return new _ListItem(i, noteModel.getByPosition(i));
                },
                itemCount: noteModel.getCount(),
              );
            },
          );
  // },
// );
```

## 高级使用

### MenuBar



### WebView

webview设置channel

```
javascriptChannels: <JavascriptChannel>[
            _toasterJavascriptChannel(context),
          ].toSet(),

JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
  return JavascriptChannel(
      name: 'Toaster',
      onMessageReceived: (JavascriptMessage message) {
        Scaffold.of(context).showSnackBar(
          SnackBar(content: Text(message.message)),
        );
      });
}
```


调用原生代码

```
void _onShowUserAgent(
    WebViewController controller, BuildContext context) async {
  controller.evaluateJavascript(
      'Toaster.postMessage("User Agent: " + navigator.userAgent);');
}
```

### Clean Architecture

基于provider的flutter工程架构
provider的声明顺序
可独立存在的provider：类或对象，相当于service
依赖其他provider的provider：类或对象，相当于service
ui消费者provider：在ui中直接消费的provider

如果一个widget有逻辑，它应该有自己的model，并命名为xxxModel，文件名为xxx_model
在view的最外层就包裹上provider，注入其它provider时依赖类型推断，不要自己写明类型（依赖注入的特性）
class LoginView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<LoginViewModel>.value(
      value: LoginViewModel(),
      child: Consumer<LoginViewModel>(
        builder: (context, model, child) => Scaffold(
            backgroundColor: backgroundColor,
            body: Center(child: Text('Login View'))),
      ),
    );
  }
}


文件夹
views
viewmodels
