# Dubbo

dubbo 会将注册信息缓存到本地

## 基本概念

### 消息体

必须实现序列化接口

## 安装与配置

### 安装

```xml
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>dubbo</artifactId>
    <version>2.4.10</version>
    <exclusions>
        <exclusion>
            <groupId>org.springframework</groupId>
            <artifactId>spring</artifactId>
        </exclusion>
    </exclusions>
</dependency>
```

## 基本概念

### Provider

Provider 的配置文件需要放在 `resources/META-INF/spring` 目录下

- 服务提供方
- 伴随容器 (Netty )启动
- 服务注册

### Consumer

Consumer 配置文件没有存放位置的要求

- 服务消费者
- 服务发现和接口代理
- 负载均衡和服务容错策略

## Dubbo 协议的使用

### Provider

#### 配置

```xml
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:dubbo="http://code.alibabatech.com/schema/dubbo"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
          http://www.springframework.org/schema/beans/spring-beans-3.0.xsd
       http://code.alibabatech.com/schema/dubbo
       http://code.alibabatech.com/schema/dubbo/dubbo.xsd">

    <!--抽象 protocol 和 service 中公共的部分，作为默认配置,不必要-->
    <!--<dubbo:provider version="" timeout="" register=""/>-->
    <dubbo:provider registry="zk1" protocol="pro1"/>

    <!--配置应用信息（应用名）-->
    <dubbo:application name="my-dubbo-provider"/>

    <!--注册中心配置，用于服务注册,通常为 zk 的地址-->
    <!--<dubbo:registry address="127.0.0.1:2181" protocol="zookeeper" file=""/>-->
    <dubbo:registry id="zk1" address="139.224.200.47:32770" protocol="zookeeper"/>

    <!--采用的协议,默认为 dubbo 协议-->
    <!--<dubbo:protocol name="dubbo" port="20886" host=""/>-->
    <dubbo:protocol id="pro1" name="dubbo" port="20886"/>

    <!--服务注册配置-->
    <!--配置对外提供的接口-->
    <bean name="speak" class="com.mrseasons.spring.dubbo.provider.service.SpeakImpl"/>
    <!--<dubbo:service interface="com.mrseasons.spring.dubbo.provider.service.ISpeak" ref="" version=""/>-->
    <dubbo:service interface="com.mrseasons.spring.dubbo.provider.service.ISpeak"
                   ref="speak"/>
</beans>
```

配置完后可以创建 Application，Main 入口函数使用 `com.alibaba.dubbo.container.Main` 进行启动。待控制台输出 `Dubbo service server started!` 后证明启动成功
之后就可以通过 zkcli 工具看到 zk 根目录创建了 [dubbo, zookeeper] 项目

#### 使用

1. 传输对象需实现 `Serializable` 接口
2. 定义自定义的接口
3. 实现自定义的接口

### Consumer

#### 配置


```xml
<!--注册中心配置，用于服务注册,通常为 zk 地址-->
<dubbo:registry id="zk1" address="139.224.200.47:32770" protocol="zookeeper"/>

<!--抽象 reference 中公共的部分，作为默认配置,不必要-->
<!--<dubbo:consumer timeout="" registry="" loadbalance=""/>-->
<dubbo:consumer registry="zk1"/>

<!--配置应用信息,用于计算依赖关系,每个应用命名尽量独立-->
<dubbo:application name="my-dubbo-consumer"/>

<!--服务注册配置-->
<!--<dubbo:reference interface="" id="speakInterface" version="" url=""/>-->
<dubbo:reference interface="com.mrseasons.spring.dubbo.provider.service.ISpeak" id="speak"/>
```

#### 使用

1. 声明 Provider 提供的接口作为依赖注入的成员变量
2. 编写业务逻辑



## Http 协议的使用

### Provider

#### 配置

```xml
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:dubbo="http://code.alibabatech.com/schema/dubbo"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
          http://www.springframework.org/schema/beans/spring-beans-3.0.xsd
       http://code.alibabatech.com/schema/dubbo
       http://code.alibabatech.com/schema/dubbo/dubbo.xsd http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">

    <context:property-placeholder location="classpath*:dubbo.properties"/>

    <!--配置应用信息-->
    <dubbo:application name="httpDemo"/>

    <!--注册中心配置，用于服务注册,通常为zk 地址-->
    <dubbo:registry id="zk1" address="${dubbo.register.address}" protocol="zookeeper"/>

    <!--采用的协议,默认采用-->
    <dubbo:protocol id="myhttp" name="http" port="8888" host="127.0.0.1" server="jetty" serialization="json"/>
    <dubbo:protocol id="mydubbo" name="dubbo" host="127.0.0.1"/>

    <!--服务注册配置-->
    <!--配置对外提供的接口-->
    <dubbo:provider serialization="fastjson" protocol="myhttp,mydubbo"/>
    <dubbo:service interface="com.mrseasons.spring.dubbohttp.ISpeak"
                   ref="speakInterface"
                   async="true"
                   path="speakInterface"
                   version="0.0.1"/>
</beans>
```

#### 使用

1. 传输对象需实现 `Serializable` 接口
2. 定义自定义的接口
3. 实现自定义的接口，在类似添加 `@Component` 注解，注解属性与 xml 配置中的 `<dubbo:service ref=xxx>` 一致

### Consumer

#### 配置

```xml
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:dubbo="http://code.alibabatech.com/schema/dubbo"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
          http://www.springframework.org/schema/beans/spring-beans-3.0.xsd
       http://code.alibabatech.com/schema/dubbo
       http://code.alibabatech.com/schema/dubbo/dubbo.xsd http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">

    <context:property-placeholder location="classpath*:dubbo.properties"/>

    <!--配置应用信息-->
    <dubbo:application name="dubboDemoTest"/>

    <!--注册中心配置，用于服务注册,通常为zk 地址-->
    <dubbo:registry id="zk1" address="${dubbo.register.address}" protocol="zookeeper"/>

    <!--采用的协议,默认采用-->
    <dubbo:protocol id="myhttp" name="http" port="8888" host="127.0.0.1" server="jetty" serialization="json"/>
    <dubbo:protocol id="mydubbo" name="dubbo" host="127.0.0.1"/>

    <!--服务注册配置-->
    <dubbo:provider serialization="fastjson" protocol="myhttp,mydubbo"/>
    <dubbo:service interface="com.mrseasons.spring.dubbohttp.ISpeak"
                   ref="speakInterface"
                   async="true"
                   path="speakInterface"
                   version="0.0.1"/>
</beans>
```

#### 使用

```java
HttpURLConnection httpUrlConnection = ((HttpURLConnection)new URL("http://127.0.0.1:8888/speakInterface?methods=speak&version=0.0.1&client=jetty").openConnection());
httpUrlConnection.setDoOutput(true);
httpUrlConnection.setDoInput(true);
httpUrlConnection.setUseCaches(false);
httpUrlConnection.setRequestProperty("Content-type", "application/x-java-serialized-object");
httpUrlConnection.setRequestMethod("POST");
httpUrlConnection.connect();
OutputStream outStrm = httpUrlConnection.getOutputStream();
ObjectOutputStream objOutputStrm = new ObjectOutputStream(outStrm);

People people = new People();
people.setAge(1);

RemoteInvocation invocation = new RemoteInvocation("speak", new Class[]{People.class},
new Object[]{people});

objOutputStrm.writeObject(invocation);
objOutputStrm.flush();

InputStream inStrm = httpUrlConnection.getInputStream();
ObjectInputStream input = new ObjectInputStream(inStrm);
RemoteInvocationResult result = (RemoteInvocationResult) input.readObject();
System.out.println(result.getValue());
```



### 整合生产者和消费者

大部分应用都是混合型应用，既是生产者也是消费者。这些应用通常为通过 Main 函数启动的 Java 应用。而 xml 通常都分为,spring, consumer, provider 三个文件。

使用注意点

- 循环依赖
- 超时时间,上层服务可能因为多个下层服务而超时
- 多线程
- 网络环境


#### 配置

spring-dubbo-provider

```xml
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:dubbo="http://code.alibabatech.com/schema/dubbo"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
          http://www.springframework.org/schema/beans/spring-beans-3.0.xsd
       http://code.alibabatech.com/schema/dubbo
       http://code.alibabatech.com/schema/dubbo/dubbo.xsd">

    <dubbo:provider registry="zk1" protocol="mydubbo"/>
    <dubbo:service interface="com.mrseasons.spring.dubboweb.ISpeak" ref="speakInterface"/>
</beans>
```

spring-dubbo-consumer

```xml
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:dubbo="http://code.alibabatech.com/schema/dubbo"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
          http://www.springframework.org/schema/beans/spring-beans-3.0.xsd
       http://code.alibabatech.com/schema/dubbo
       http://code.alibabatech.com/schema/dubbo/dubbo.xsd">

    <dubbo:application name="jikexueyuan-dubbo-provider"/>

    <dubbo:registry id="zk1" address="139.224.200.47:32770" protocol="zookeeper"/>

    <dubbo:protocol id="mydubbo" name="dubbo" port="20886"/>

    <dubbo:consumer registry="zk1"/>

    <dubbo:reference id="growUpInterface" interface="com.mrseasons.spring.dubboweb.IGrowUp"/>

</beans>
```
