陆金所配置

#dubbo zk
dubbo.zookeeper.address=zookeeper://xxx?backup=yyy,zzz


dubbo config
consumer
<dubbo:applicaton name=""/>
<dubbo:registry address="${dubbo.zookeeper.address}" check="false"/>
<dubbo:protocol name="dubbo" port="-1"/>
<dubbo:monitor protocol="registry"/>
<dubbo:service interface="xxx" ref="xxxImpl"/>
<dubbo:reference interface="xxx" id="xxx" async="false" timeout="20000" check="false"/>
