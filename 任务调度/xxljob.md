# XXL-Job

http://www.xuxueli.com/xxl-job/

## 安装

1、先创建MySql数据库，然后执行`https://github.com/xuxueli/xxl-job/blob/2.1.0/doc/db/tables_xxl_job.sql`对应的数据库脚本。

2、执行docker命令，链接该数据库

```
docker run -e PARAMS="--spring.datasource.url=jdbc:mysql://mysql01:3306/xxl_job?Unicode=true&characterEncoding=UTF-8 --spring.datasource.password=zhuxian" -p 38080:8080  -v /tmp:/data/applogs --name xxl-job-admin  --link mysql01:mysql01  -d xuxueli/xxl-job-admin:2.1.0
```

3、访问调度中心

http://aliyun:38080/xxl-job-admin

默认登录账号 "admin/123456"

## 基本概念

- 调度中心：统一管理任务调度平台上调度任务，负责触发调度执行，并且提供任务管理平台。
- 执行器：负责接收“调度中心”的调度并执行；可直接部署执行器，也可以将执行器集成到现有业务项目中。
- 任务：执行器中配置的每一个可执行任务。

## 使用

### 执行器

公司应用：可以只配置为同一个专门负责任务调度的执行器应用。

### 任务

公司应用：所有任务的JobHandler都配置为执行器应用的JobHandler，参数为实际调度的地址
由执行器应用受到调度中心的调度后请求该实际调度地址。
路由策略选择ROUND（轮询）或FAILOVER（故障转移）。
阻塞处理策略选择：
单机串行（默认）：调度请求进入单机执行器后，调度请求进入FIFO队列并以串行方式运行；
丢弃后续调度：调度请求进入单机执行器后，发现执行器存在运行的调度任务，本次请求将会被丢弃并标记为失败；
覆盖之前调度：调度请求进入单机执行器后，发现执行器存在运行的调度任务，将会终止运行中的调度任务并清空队列，然后运行本地调度任务；

88使用方式：有一个通用的commonHandler，参数为url，其它handle可以传参的就是集成在里面的

## 配置

### pom 配置

```xml
<dependency>
     <groupId>com.xuxueli</groupId>
     <artifactId>xxl-job-core</artifactId>
     <version>2.1.0</version>
 </dependency>
```

### application.yml 配置

```yml
### 调度中心部署跟地址 [选填]：如调度中心集群部署存在多个地址则用逗号分隔。
#  执行器将会使用该地址进行"执行器心跳注册"和"任务结果回调"；为空则关闭自动注册；
xxl:
  job:
    ### 管理界面地址
    admin:
      addresses: http://aliyun:38080/xxl-job-admin
    ### 执行器executor配置
    executor:
      ### 执行器心跳注册分组依据；为空则关闭自动注册
      appname: xxl-job-executor-sample
      ### ip默认为空表示自动获取IP
      ip:
      ### 端口小于0则自动获取
      port: 9999

      ### 执行器运行日志文件存储磁盘路径 [选填] ：需要对该路径拥有读写权限；为空则使用默认路径；
      logpath: /data/applogs/xxl-job/jobhandler

      ### 执行器日志保存天数 [选填] ：值大于3时生效，启用执行器Log文件定期清理功能，否则不生效；
      logretentiondays: -1

    ### 执行器通讯TOKEN [选填]：非空时启用；
    accessToken:
```
