[TOC]

# Quartz

Quartz 是一个完全由 Java 编写的开源作业调度框架。

## 配置文件

```xml
<dependency>
    <groupId>org.quartz-scheduler</groupId>
    <artifactId>quartz</artifactId>
    <version>2.2.2</version>
</dependency>
<dependency>
    <groupId>org.quartz-scheduler</groupId>
    <artifactId>quartz-jobs</artifactId>
    <version>2.2.2</version>
</dependency>
```

## 使用

### 基本使用

定义 Job，表示具体的任务内容

```java
public class HelloJob implements Job {
    @Override
    public void execute(
            final JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("Hello, Quartz1! - executing its JOB at " +
                new Date() + " by " + jobExecutionContext.getTrigger().getDescription());
    }
}
```

定义 Scheduler，用以执行任务

```java
SchedulerFactory schedulerFactory = new StdSchedulerFactory();
Scheduler scheduler = schedulerFactory.getScheduler();
scheduler.start();        
```

创建 Job

```java
JobDetail job = JobBuilder.newJob(HelloJob.class)
        .withIdentity("myjob", "group01")
        .build();
```

定义 Trigger，用以表示任务执行的时间和频率

```java
Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity("mytrigger", "groupd01")
                .startNow()
                .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                        .withIntervalInSeconds(10)
                        .repeatForever())
                .build();
```

启动 Job

```java
scheduler.scheduleJob(job, trigger);
```

### 使用 JobDataMap

用于 Job 间的数据交换

修改 Job 实例

```java
JobDetail job = JobBuilder.newJob(HelloJob.class)
                .withIdentity("myjob", "group01")
                        //JobDataMap
                .usingJobData("foo", "bar")
                .usingJobData("pai", 3.14)
                .build();
```

在 Job 中通过上下文获得传入的数据

```java
JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
String foo = jobDataMap.getString("foo");
```

需要注意由于值是通过字符串传递的，所以对于不存在的字符串型的值会直接返回空值，而对于不存在的整型等类型的值由于会发生字符串到目标类型的转换所以会报类型转换异常。

### 处理异常

在 Job 的定义中

```java
try {
    System.out.println(jobDataMap.getInt("not exist key"));
} catch (Exception e) {
    JobExecutionException jobExecutionException = new JobExecutionException(e);
    // 处理 Job
    throw jobExecutionException;
}
```

处理 Job 通常有两种方法

立即重新启动 Job

```java
jobExecutionException.refireImmediately();
```

取消所有触发器，停止 Job

```java
jobExecutionException.setUnscheduleAllTriggers(true);
```




---

可运行实例代码位于 `spring/first_corejava/src/main` 下的 `quartz` 目录下
