# ActiveMQ

ActiveMQ 是 Apache 出品，最流行的，能力强劲的开源消息总线。
ActiveMQ 是一个完全支持 JMS1.1 和 J2EE 1.4 规范的 JMS Provider 实现

## 安装

1. 前往官网 [http://activemq.apache.org](http://activemq.apache.org) 下载压缩包
2. 解压后运行 `/bin/win64/activemq.bat` （根据平台不同或 `/bin/macosx/activemq start`）
3. 访问  `http://localhost:8161/admin` 进入管理界面，用户名/密码默认为 admin/admin

## 管理界面

### 开启用户权限

是否开启的配置文件在 `/conf/jetty.xml` 中，将 `authenticate` 改为 `true` 即可。

例：

```xml
<bean id="adminSecurityConstraint" class="org.eclipse.jetty.util.security.Constraint">
    <property name="name" value="BASIC" />
    <property name="roles" value="admin" />
     <!-- set authenticate=false to disable login -->
    <property name="authenticate" value="true" />
</bean>
```

用户名和密码配置文件在 `/conf/jetty-realm.properties` 中，默认为 admin/admin



## 参考资料

- [ActiveMQ实现负载均衡+高可用部署方案](http://www.open-open.com/lib/view/open1400126457817.html)
- [基于zookeeper+leveldb搭建activemq集群](http://my.oschina.net/xiaohui249/blog/313028)
- [ActiveMQ学习小结](http://blog.csdn.net/czp11210/article/details/8822070)
