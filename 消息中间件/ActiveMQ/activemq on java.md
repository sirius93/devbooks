[TOC]

# ActiveMQ

## 添加依赖

```xml
<dependency>
    <groupId>org.apache.activemq</groupId>
    <artifactId>activemq-all</artifactId>
    <version>5.9.0</version>
</dependency>
```

## 使用

### Producer

```java
ActiveMQConnectionFactory connectionFactory
                = new ActiveMQConnectionFactory("tcp://localhost:61616");
Connection connection = connectionFactory.createConnection();
connection.start();

Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
Queue queue = session.createQueue("foo.bar");

MessageProducer producer = session.createProducer(queue);
producer.setDeliveryMode(DeliveryMode.PERSISTENT);

TextMessage message = session.createTextMessage("你好");
producer.send(message);

session.commit();

connection.close();
```

以上代码执行后可以在控制界面看到一个名为 `foo.bar` 的队列被创建，`Messages Enqueued` 的值为 1，即有 1 条消息入了队列。

![][producer]

### Consumer

```java
ActiveMQConnectionFactory connectionFactory
        = new ActiveMQConnectionFactory("tcp://localhost:61616");
Connection connection = connectionFactory.createConnection();
connection.start();

Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
Queue destination = session.createQueue("foo.bar");

MessageConsumer consumer = session.createConsumer(destination);
consumer.setMessageListener(new MessageListener() {
    @Override
    public void onMessage(Message message) {
        try {
            TextMessage textMessage = (TextMessage) message;
            System.out.println(textMessage.getText());
        } catch (JMSException e) {
            e.printStackTrace();
        }

        try {
            session.commit();
        } catch (JMSException e) {
            e.printStackTrace();
        }

        try {
            connection.close();
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
});
```

以上代码执行后可以看到 `Messages Dequeued` 的值变为 1，即 1 条消息已经被消费了。

![][consumer]



[producer]: capture/producer.png
[consumer]: capture/consumer.png
