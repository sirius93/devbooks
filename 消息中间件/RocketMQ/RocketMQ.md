# RocketMQ

https://rocketmq.apache.org

## 特点

RocketMQ 支持普通消息、顺序消息、事务消息

### RocketMQ 的组成

- nameserver：存储topic 信息
- broker：一个broker 和所有nameserver 建立心跳并发送topic 信息，一个broker 可以配置多个topic，每个broker可以有多个队列，broker 间可以搭建主从架构
- topic：一个topic 在多个broker 上
- 消费者：消费者和nameserver 保持长连接，默认每隔30秒定时获取topic 信息，当nameserver 挂掉，消费者会自动寻找下一个nameserver
一个队列只会被一个消费者消费，若消费者挂掉同一个消费组的消费者会接替其功能
- 生产者：一个生产者和一台nameserver 保持长连接，定时获取topic 信息，当nameserver 挂掉，生产者会自动寻找下一个nameserver

### 持久化机制

broker 持久化有同步刷盘和异步刷盘机制。同步刷盘，消息写入物理磁盘后返回；异步刷盘，机器宕机才会丢失，broker 挂点不会发生。

RocketMQ 会自动进行消息清理，默认每10s 扫描一次，磁盘空间阀值固定到85%时不再接受消息
清理时间默认凌晨4点或磁盘到达阀值后
文件默认保留72小时

RocketMQ 同时支持拉和推两种消费模型，但底层都是通过拉模型来实现，推模型实际是底层将轮询拉的方式进行了封装，通过回调函数通知应用取得消息。

### 消息系统设计

消息系统的设计，就回避不了两个问题：
1. 消息的顺序问题
2. 消息的重复问题

#### 消息顺序

在业务层解决好过依赖消息系统
RocketMQ通过MessageQueueSelector选择发送的队列来实现

#### 消息重复

消费端幂等处理，RocketMQ不处理

## 事务消息的实现

类似两阶段提交
1.发送prepare 消息，获得消息地址
2.执行本地事务
3.根据之前获得的消息地址访问并修改消息的状态
RocketMQ 会定期扫描prepare 的消息，向生产者询问消息的执行状况

## RocketMQ 实践

- 一个应用尽可能用一个 Topic，使用不同的tags 来进行区分
- 每个消息在业务层面的唯一标识码，要设置到 keys 字段
- 消息发送成功或者失败，要打印消息日志，务必要打印 sendresult 和 key 字段
- 消费者要做幂等控制
- 尽量使用批量消费
- 线上应该关闭 autoCreateTopicEnable，即在配置文件中将其设置为false。
- RocketMQ在发送消息时，会首先获取路由信息。如果是新的消息，由于MQServer上面还没有创建对应的Topic，这个时候，如果上面的配置打开的话，会返回默认TOPIC的（RocketMQ会在每台broker上面创建名为TBW102的TOPIC）路由信息，然后Producer会选择一台Broker发送消息，选中的broker在存储消息时，发现消息的topic还没有创建，就会自动创建topic。后果就是：以后所有该TOPIC的消息，都将发送到这台broker上，达不到负载均衡的目的。所以基于目前RocketMQ的设计，建议关闭自动创建TOPIC的功能，然后根据消息量的大小，手动创建TOPIC。
