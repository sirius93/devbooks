## 帮助文档

https://www.rabbitmq.com/getstarted.html

## 启动

```
docker run -d --hostname my-rabbit -P --name rabbitmq1 rabbitmq:management
```

没有management就查看不了管理界面

- 默认端口为： 5672
- 默认vhost为： /
- 默认管理界面地址为：`http://虚拟机IP:15672`
- 默认的登录账号是guest,Password也是guest

## 基本概念

### 角色

- producer
- queue：存储消息
- consumer

- channel
- exchange

### Ack

默认为手动ack，自动需要消费消息时将autoAck设为true

### 消息持久性

要想保证消息不被丢失，需要做到两件事：队列和消息都是持久的。

#### 对于队列

```java
boolean durable = true;
channel.queueDeclare("hello", durable, false, false, null);
```

如果该队列已创建，则无法通过更新配置来更新队列，必须重新创建该队列或使用不同名称的队列。
该语句需要同时放在生产者和消费者的代码上。

#### 对于消息

设置对应的MessageProperties值。

```java
import com.rabbitmq.client.MessageProperties;

channel.basicPublish("", "task_queue",
            MessageProperties.PERSISTENT_TEXT_PLAIN,
            message.getBytes());
```

不足之处：设置该属性后不保证消息绝对不会被丢失。RabbitMq在接收到消息和保存消息之间还是存在一段时间间隔，此外，RabbitMQ也不会每次都刷新数据到硬盘上，可能此时只是保存在内存中。

### 公平分发机制

prefetchCount通知RabbitMQ不要向正在处理前一个消息的workder分发消息。

```java
int prefetchCount = 1;
channel.basicQos(prefetchCount);
```

### Exchange

#### 基本概念

RabbitMQ的核心消息机制是生产者从不直接向队列发送消息，生产者不关心消息是否有发送到队列。
Producer发送消息给Exchange。Exchange负责从Producer处接收消息并发送到队列中。
每个Exchange都必须有一个type，用以决定Exchange应该如何处理接收到的消息。

producer->exchange->queue<-consumer

#### Exchange Type

分类

- direct 发送消息到队列的routingKey与Exchange的bindingKey一致的队列。不同的队列可以有相同的bindingKey，一个队列可以有多个bindingKey。
- topic  根据topic条件发送到指定队列
- headers
- fanout 广播消息到所有队列（无视routingKey）

默认的Exchange名为空字符串，如果存在routingKey则根据routingKey选择队列。

#### 基本使用

声明Exchange（生产者与消费者）

```java
channel.exchangeDeclare(exchangeName, BuiltinExchangeType.DIRECT);
```

参数为Exchange名和Exchange type。

绑定Exchange与队列（消费者）

```java
channel.queueBind(queueName, exchangeName, "");
```

参数为队列名，Exchange名，bindingKey名。bindingKey具体功能依赖于Exchange类型。

使用Exchange发送消息（生产者）

```java
channel.basicPublish(exchangeName, "", null, message.getBytes());
```

参数为Exchange名，routingKey，amqp额外配置，消息体

#### Topic Exchange

Direct Exchange 只能用于条件简单的情况下，对于复杂情况需要使用Topic Exchange。
Topic Exchange的routingKey是使用点（.）分隔的多个单词，这些单词用于表示各种特性，像"stock.usd.nyse", "nyse.vmw", "quick.orange.rabbit"这样的。
单词可以使用通配符，其中，`*`表示正好一个单词，`#`表示0个或多个单词。
使用单独的`#`的Topic Exchange功能和Fanout Exchange一样，如果Topic Exchange不使用通配符则其功能和Direct Exchange一样。


### 非持久化的队列

创建一个非持久化，自动删除的队列，queueDeclare()方法使用无参的版本

```java
String queueName = channel.queueDeclare().getQueue();
```

## 高级应用

### 发送方确认

发送方确认是RabbitMQ提供的可靠性消息投递机制，服务端会异步返回确认消息。该功能基于AMQP 0.9.1 协议，会降低消息发布的吞吐量。

开启发送方确认

```java
Channel channel = connection.createChannel();
channel.confirmSelect();
```

发送消息

```java
channel.basicPublish("", queue, null, body.getBytes());
// 如果超时或者broker宕机会抛出异常
channel.waitForConfirmsOrDie(5000);
```





---

配置

rabbitmq:
host: localhost
port: 5672
username: spring
password: 123456
listener:
  simple:
    concurrency: 3

amqp:
  rent:
    queue: queue.blockchain.rent

消费方

@Component
public class RentMessageListener
    @RabbitListener(queues = "${amqp.rent.queue}")
    @Override
    public void onMessage(Message message) {
    }
}

生产方

private RabbitMessagingTemplate rabbitMessagingTemplate;

rabbitMessagingTemplate.convertAndSend(com.baixiang.chuxing.amqp.model.common.Constants.BLOCK_CHAIN_RENT_QUEUE, JSON.toJSONString(blockChainRentDto));

框架方


import com.baixiang.chuxing.amqp.model.common.ReplyMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate.ConfirmCallback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitmqConfig {
    private static final Logger logger = LoggerFactory.getLogger(RabbitmqConfig.class);

    @Autowired
    public RabbitmqConfig(RabbitTemplate rabbitTemplate) {
        // 异步的生产者ack，需配置spring.rabbitmq.publisher-confirms=true
        // 即使使用同步ack，实际由于网络抖动还是无法完全保证可靠性，只能在应用层实现，在消费者端幂等
        rabbitTemplate.setConfirmCallback((correlationData, ack, cause) -> {
            logger.info("RabbitMq Message Confirm：" + correlationData);
        });
    }

    @Bean
    public Queue replyQueue() {
        return new Queue("queue.reply", true);
    }

    @Bean
    public Queue vehicleStatusQueue() {
        return new Queue("queue.vehiclestatus", true);
    }

    @Bean
    public Queue vehicleAlertQueue() {
        return new Queue("queue.vehiclealert", true);
    }

    @Bean
    public Queue tripEndQueue() {
        return new Queue("queue.tripend", true);
    }

    @Bean
    public Queue electricalFenceQueue() {
        return new Queue("queue.electricalfence", true);
    }

    @Bean
    public Queue updateBluetoothKeyQueue() {
        return new Queue("queue.updatebluetoothkey", true);
    }

    @Bean
    public Queue vehicleAwakenQueue() {
        return new Queue("queue.vehicleawaken", true);
    }

    @Bean
    public Queue vehicleUpdateGpsQueue() {
        return new Queue("queue.vehicleupdategps", true);
    }

    @Bean
    public Queue vehicleStartedStatusQueue() {
        return new Queue("queue.vehiclestartedstatus", true);
    }

    @Bean
    public Queue protocolTboxDatagramQueue() {
        return new Queue("queue.protocoltboxdatagramqueue", true);
    }

    @Bean
    public Queue electricalFenceGpsUpdateQueue() {
        return new Queue("queue.electricalfencegpsupdatequeue", true);
    }

    @Bean
    public Queue blockChainRentQueue() {
        return new Queue("queue.blockchain.rent", true);
    }

    @Bean
    public Queue vehicleFreeStatusQueue() {
        return new Queue("queue.vehiclefreestatusqueue", true);
    }

    @RabbitListener(
        queues = {"queue.reply"}
    )
    public void receiveTestQueue(ReplyMessage replyMessage) {
        System.out.println("RabbitMq Message Return：" + replyMessage);
    }
}
