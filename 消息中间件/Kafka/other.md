## 命令

### Topic

#### 查看 Topic

查看所有 Topic

```bash
./kafka-topics.sh --list --zookeeper <zk_host:zk_port>
```

查看 Topic 详细信息

```bash
./kafka-topics.sh --describe --zookeeper <zk_host:zk_port> --topic <topic_name>
```

#### 修改 Topic

创建 Topic

```bash
./kafka-topics.sh --create --zookeeper <zk_host:zk_port> --replication-factor 1 --partitions 1 --topic <topic_name>
```

- `replication-factor` 表示该 topic 需要在不同的 broker 中保存几份

修改 Topic 的 partitions

```bash
./kafka-topics.sh --zookeeper <zk_host:zk_port> --alter –topic <topic_name>     --partitions <target_n>
```
>partitions 只能增加不能减少，指定的为增加后的值

### Producer 和 Consumer

创建 Producer

```bash
./kafka-console-producer.sh --broker-list localhost:9092 --topic test
```

创建 Consumer

```bash
./kafka-console-consumer.sh --zookeeper <zk_host:zk_port> --topic test --from-beginning
```

### Leader 平衡

```bash
./kafka-preferred-replica-election.sh –zookeeper <zk_host:zk_port>
```
