[TOC]

# Kafka

## 基本概念

### 概述

Kafka 是分布式消息系统。

### 与其它消息系统对比

支持的协议：ActiveMQ 为 OpenWire、STOMP、REST、XMPP、AMQP。RabbitMQ 为 AMQP。Kafka 为仿 AMQP。
事务：只有 ActiveMQ 支持。
只有 Kafka 支持动态扩容（通过 Zookeeper）
都支持集群和负载均衡。

动态扩容即扩容时可以不关闭服务器，对于云服务器尤其重要。

### 应用场景

### AMQP 协议

- 消费者（Consumer）：从消息队列中请求消息的客户端应用程序；
- 生产者（Producer）：向broker发布消息的客户端应用程序；
- AMQP服务器端（broker）：用来接收生产者发送的消息并将这些消息路由给服务器中的队列；

producer ->(push)-> broker(queue) <-(pull)<- consumer

### Kafka 架构

- 主题（Topic）：一个主题类似新闻中的体育、娱乐、教育等分类概念，在实际工程中通常一个业务一个主题；
- 分区（Partition）：一个 topic 中的消息数据按照多个分区组织，分区是 kafka 消息队列组织的最小单位，一个分区可以看做是一个 FIFO 的队列;分区中的消息按从旧到新排列。
- 备份（Replication）：为了保证分布式可靠性, kafka 0.8 开始对每个分区的数据进行备份（不同 Broker 上），防止其中一个 Broker 宕机造成分区数据不可用

生产者向 Topic 发布消息
消费者向 Topic 订阅并消费消息
集群由多个服务组成，一个服务就是一个 Broker
一个 Topic 由多个分区组成，每个分区包含多个消息，每个消息保存一个 offset 表示消息在分区中的位置
每个分区都会在不同的 Broker 中存在备份
每个分区中都会存在一个 Leader 多个 Follower，Leader 负责读和写，Follower 负责备份 Leader。所以每个 Broker 实际都是作为一部分分区的 Leader，一部分分区的 Follower，有效地实现了负责均衡。

Kafka 按照消费保存策略的设定会在一定时间内保存消息，无论该消息是否被消费。这意味着到了一定时间，未消费的消息也会被删除。
消费者需要自行保存消息的偏移量，由该偏移量来决定读取的消息，所以实际消费者可以读取所有还存在的消息。

kafka 的 producer 和 consumer 和 zk 的关系：

1) Producer 端使用 zookeeper 用来"发现" broker 列表,以及和 Topic 下每个 partition leader 建立 socket 连接并发送消息.

2) Broker 端使用 zookeeper 用来注册 broker 信息,以及监测 partition leader 存活性.

3) Consumer 端使用 zookeeper 用来注册 consumer 信息,其中包括 consumer 消费的 partition 列表等,同时也用来发现 broker 列表,并和 partition leader 建立 socket 连接,并获取消息.

如果需要实现广播，只要每个 Consumer 有一个独立的 Group 就可以了。要实现单播只要所有的 Consumer 在同一个 Group 里。

### 消息传递三种语义：

- 至多一次；
- 最少一次；
- 恰好一次；

### 消费者编程模型

- 分区消费模型（队列模式）：有几个分区就有几个 Consumer。Consumers 可以同时从服务端读取消息，每个消息只被其中一个 consumer 读到
- 组消费模型（发布-订阅模式）：多个 Consumer 可以构成一个个组，每个组都会拿到全量消息。消息被广播到所有的 consumer 中。

分区消费模型
- 更灵活
- 需要自己处理各种异常情况；
- 需要自己管理 offset(以实现消息传递的其他语义)；

组消费模型
- 简单，但不灵活
- 不需要自己处理异常情况，不需要自己管理 offset；
- 只能实现 Kafka 默认的最少一次消息传递语义；

### 生产者编程模型

- 同步生产模型
- 异步生产模型

同步生产模型
- 低消息丢失率；
- 高消息重复率(由于网络原因，回复确认未收到)；
- 高延迟

异步生产模型
- 低延迟;
- 高发送性能；
- 高消息丢失率(无确认机制，发送端队列满)


## 集群维护

增加分区是最常用的提高 Kafka 性能的手段。

### Kafka 集群 Leader 平衡机制

每个 partition 的所有 replicas 叫做 “assigned replicas”
“assigned replicas” 中的第一个 replicas 叫 “preferred replica”
刚创建的 topic 一般 “preferred replica” 是 leader。

当机器上下线后，preferred replica 可能不是 Leader，此时需要执行以下语句进行重新平衡

```bash
./kafka-preferred-replica-election.sh –zookeeper <zk_host:zk_port>
```

也可以直接修改配置文件，这样就不用自己来管理了

```
auto.leader.rebalance.enable=true
```

### 集群分区日志迁移

当机器增加后，需要迁移 topic 数据到其他 broker，有以下四个步骤：

1. 写 json 文件，文件格式如下：
topics-to-move.json
```js
{"topics": [{"topic": "foo1"},
            {"topic": "foo2"}],
 "version":1
}
```

2. 使用 `–generate` 生成迁移计划（下面的操作是将 topic: foo1 和 foo2 移动到 broker 5,6）:
```bash
bin/kafka-reassign-partitions.sh --zookeeper localhost:2181 --topics-to-move-json-file topics-to-move.json --broker-list "5,6" –generate
```
这一步只是生成计划，并没有执行数据迁移；

3. 使用 `–execute` 执行计划：
```bash
bin/kafka-reassign-partitions.sh --zookeeper localhost:2181 --reassignment-json-file expand-cluster-reassignment.json –execute
```
执行前最好保存当前的分配情况，以防出错回滚

4. 使用 `–verify` 验证是否已经迁移完成

局部迁移

迁移某个 topic 的某些特定的 partition 数据到其他 broker，步骤与上面一样，但是 json 文件如下面所示:
custom-reassignment.json
```js
{"version":1,"partitions":[{"topic":"foo1","partition":0,"replicas":[5,6]},{"topic":"foo2","partition":1,"replicas":[2,3]}]}
```
可以指定到 topic 的分区编号


kafka-reassign-partitions.sh 工具会复制磁盘上的日志文件，只有当完全复制完成，才会删除迁移前磁盘上的日志文件。
执行分区日志迁移需要注意：
kafka-reassign-partitions.sh 工具的粒度只能到 broker，不能到 broker 的目录(如果broker 上面配置了多个目录，是按照磁盘上面已驻留的分区数来均匀分配的)，所以，如果 topic 之间的数据，或者 topic 的 partition 之间的数据本身就不均匀，很有可能造成磁盘数据的不均匀:
对于分区数据较多的分区迁移数据会花大量的时间，所以建议在 topic 数据量较少或磁盘有效数据较少的情况下执行数据迁移操作；

进行分区迁移时最好先保留一个分区在原来的磁盘，这样不会影响正常的消费和生产，如果目的是将分区5(brober1,5)迁移到borker2,3。可以先将5迁移到2,1，最后再迁移到2,3。而不是一次将1,5迁移到2,3。因为一次迁移所有的副本，无法正常消费和生产，部分迁移则可以正常消费和生产。


## 集群监控

### Kafka Offset Minotor

Kafka Offset Monitor 可以监控 Kafka 集群以下几项：
- Kafka 集群当前存活的 broker 集合；
- Kafka 集群当前活动 topic 集合；
- 消费者组列表
- Kafka 集群当前 consumer 按组消费的 offset lag 数(即当前 topic 当前分区目前有多少消息积压而没有及时消费)

#### 安装

1. 下载 [release 包](https://github.com/quantifind/KafkaOffsetMonitor/releases)
2. 启动 Kafka Offset Minotor
  ```bash
  java -cp KafkaOffsetMonitor-assembly-0.2.0.jar com.quantifind.kafka.offsetapp.OffsetGetterWeb --zk zk-01,zk-02 --refresh
  5.minutes --retain 1.day &
  ```

### Kafka Manager

Kafka Manager 由雅虎开源，提供以下功能：
- 管理几个不同的集群；
- 容易地检查集群的状态(topics, brokers, 副本的分布, 分区的分布) ；
- 选择副本
- 基于集群的当前状态产生分区分配
- 重新分配分区

#### 安装

1. 安装 [sbt](http://www.scala-sbt.org/download.html)
2. 下载后，解压并配置环境变量(将 SBT_HOME/bin 配置到 PATH 变量中)
3. 克隆 Kafka Manager
  ```bash
  git clone https://github.com/yahoo/kafka-manager
  ```
4. 进行编译
  ```
  cd kafka-manager
  sbt clean dist
  ```

#### 部署

1. 修改 `conf/application.conf`，把 `kafka-manager.zkhosts` 改为自己的 zookeeper 服务器地址
2. 开启服务
  ```bash
  bin/kafka-manager -Dconfig.file=conf/application.conf -Dhttp.port=8007 &
  ```



---


Java客户端参数调优

fetchSize: 从服务器获取单包大小;
bufferSize: kafka客户端缓冲区大小;
group.id:  分组消费时分组名


message.send.max.retries: 发送失败重试次数;
retry.backoff.ms :未接到确认，认为发送失败的时间;
producer.type:  同步发送或者异步发送；
batch.num.messages: 异步发送时，累计最大消息数；
queue.buffering.max.ms:异步发送时，累计最大时间；



Kafka

Kafka集群维护


./kafka-topics.sh --create --zookeeper 192.168.1.67:2181 --replication-factor 1 --partitions 1 --topic test


./kafka-topics.sh --describe --zookeeper 192.168.1.67:2181 --topic test




promise/deferred ，发布/订阅，观察者区别

参考资料

- [Kafka使用入门教程](http://www.linuxidc.com/Linux/2014-07/104470.htm)
- [Kafka剖析（一）：Kafka背景及架构介绍](http://www.infoq.com/cn/articles/kafka-analysis-part-1)
