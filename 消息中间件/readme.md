[TOC]

# 消息中间件

## 实现

- [ActiveMQ](http://activemq.apache.org/)
- [RabbitMQ]()
- [Kafka]()
- [RocketMQ]()

RabbitMQ Erlang 编写，支持多种协议，重量级
Redis 轻量级，入队时数据较小时性能高于 RabbitMQ，较大时会下降很多，出队时远高于 RabbitMQ
ZeroMQ 非持久队列，性能最高，过于复杂
ActiveMQ
Kafka/Jafka 分布式，比 ActiveMQ 轻量级

## JMS

### 作用

- 提高服务质量，主要是保证不间断的服务。用 JMS 服务器接收任务，排成队列。应用服务可以暂停做维护，不影响接收的任务。应用服务运行后，再从队列中获取任务。
- 提高系统资源的利用率，主要是任务的派发不是 24 小时平均的，而是高峰时期任务量很多，比如 1 秒 1000 多个，有的时候很低，比如十几秒钟才来一个。应用服务通过 JMS 队列一个一个的取任务，做完一个再领一个，使系统资源的运用趋于平均。

### 应用场景

- 不同语言应用集成
- RPC 的替代者
- 应用间解耦
- 作为事件驱动架构的骨架，如提交一个订单后会立即得到订购成功，而此时事件其实正在执行各种工作
- 提高可扩展性

### 数据流

jms 是 java 中发送消息的中间件，包括 5 种数据流：

- StreamMessage --  Java 原始值的数据流
- MapMessage    --  一套名称-值对
- TextMessage   -- 一个字符串对象
- ObjectMessage --  一个序列化的 Java对象
- BytesMessage  --    一个未解释字节的数据流
