[TOC]

# Mock

## 实现

- [Mockito](https://github.com/mockito/mockito)
- [EasyMock](https://github.com/easymock/easymock)
- [PowerMock](https://github.com/jayway/powermock)

EasyMock 语法较为复杂，有 record, play 等操作，扩展性更强。
Mockito 所有功能都在 Mockito 类中，使用简单，但是隐藏了大部分细节，不容易控制。
PowerMock 可以弥补其它框架无法 mock 静态方法，final 方法和私有方法。



------

可运行实例代码位于 `spring/first_core/src/test` 的 `mock` 目录下
