[TOC]

# PowerMock

PowerMock 可以 Mock 静态方法，私有方法，同时支持结合 Mockito 和 EasyMock 等其它 Mock 框架进行使用。

### 添加依赖

```xml
<dependency>
    <groupId>org.powermock</groupId>
    <artifactId>powermock-module-junit4</artifactId>
    <version>1.6.4</version>
    <scope>test</scope>
</dependency>
<dependency>
    <groupId>org.powermock</groupId>
    <artifactId>powermock-api-mockito</artifactId>
    <version>1.6.4</version>
    <scope>test</scope>
</dependency>
```

### 结合 Mockito 使用

#### 前提条件

```java
@RunWith(PowerMockRunner.class)
@PrepareForTest(value = Calculator.class)
public class PowerMockTest {

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(Calculator.class);
    }
}
```

#### Mock 方法

```java
// 静态方法
PowerMockito.when(Calculator.add(1, 2)).thenReturn(30);
PowerMockito.when(Calculator.add(4, 9)).thenReturn(130);

assertThat(Calculator.add(1, 2)).isEqualTo(30);
assertThat(Calculator.add(4, 9)).isEqualTo(130);
```

#### 验证行为

```java
PowerMockito.verifyStatic(times(1));
Calculator.add(1, 2);
```
