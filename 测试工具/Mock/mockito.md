[TOC]

# Mockito

## Overview

无法 Mock 静态方法，final 方法，匿名方法和私有方法。

常用在 Dao 层和网络访问层，用以在测试时可以不与具体数据库绑定，且不受网络连接质量的影响。

## 添加依赖

```xml
<dependency>
    <groupId>org.mockito</groupId>
    <artifactId>mockito-core</artifactId>
    <version>1.10.19</version>
    <scope>test</scope>
</dependency>
```

### 使用

### Mock 对象

Mock 对象执行的都是 Mock 方法而不是真实方法。

#### 创建 Mock 对象

```java
List<Player> players = new ArrayList<>();
players.add(player01);
players.add(player02);

PlayerDao playerDao = mock(PlayerDao.class);
when(playerDao.insertPlayer(player01)).thenReturn(1);
when(playerDao.getById(1)).thenReturn(player01);
when(playerDao.getById(2)).thenReturn(player02);
when(playerDao.findAll()).thenReturn(players);
```

#### 验证方法的执行结果

```java
Player peter = playerDao.getById(1);
assertThat(peter).isEqualTo(player01);
Player jack = playerDao.getById(2);
assertThat(jack).isEqualTo(player02);
```

#### Mock 异常

```java
List list = mock(List.class);
when(list.get(10)).thenThrow(new ArrayIndexOutOfBoundsException());
```

#### 验证行为

verify 可以验证方法的执行次数

例：

```java
list.get(0);
list.get(0);
verify(list, times(2)).get(anyInt());
```

以上例子中验证 `list.get()` 被调用了 2 次。

另一个例子：

```java
mockedList.add("one");
mockedList.clear();
verify(mockedList).add("one");
verify(mockedList).clear();
```



### Spy 对象

Spy 对象包含一个 Mock 对象和一个真实对象，所以可以根据情形调用真实方法和 Mock 方法。

```java
List spyList = spy(new ArrayList());
list.add(1);
list.add(2);
spyList.add(1);
spyList.add(2);
assertThat(list).hasSize(0);
assertThat(spyList).hasSize(2);

when(list.size()).thenReturn(10);
when(spyList.size()).thenReturn(10);

assertThat(list).hasSize(10);
assertThat(spyList).hasSize(10);
```

#### 自定义返回结果

```java
List<Player> spyPlayers = spy(players);
when(spyPlayers.get(anyInt())).thenAnswer(new Answer<Player>() {
    @Override
    public Player answer(final InvocationOnMock invocation) throws Throwable {
        int index = (Integer) invocation.getArguments()[0];
        if (index < players.size()) {
            return (Player) invocation.callRealMethod();
        } else {
            Player player = new Player();
            player.setName("default");
            return player;
        }
    }
});
peter = spyPlayers.get(0);
assertThat(peter.getName()).isEqualTo("Peter");

Player defaultPlayer = spyPlayers.get(99);
assertThat(defaultPlayer.getName()).isEqualTo("default");
```

#### 基于注解使用

通过注解使用不同的 Runner 来实现 Mock 功能

```java
@RunWith(MockitoJUnitRunner.class)
public class MockAnnotationTest {
    @Mock
    private PlayerDao playerDao;
}
```
