[TOC]

# AssertJ

AssertJ 支持流式操作，自定义功能，集合的展开等操作能够有效地提供测试的可读性。

## 添加依赖

```xml
<dependency>
    <groupId>org.assertj</groupId>
    <artifactId>assertj-core</artifactId>
    <!-- use 3.1.0 for Java 8 projects -->
    <!-- use 2.2.0 for Java 7 projects -->
    <version>3.1.0</version>
    <scope>test</scope>
</dependency>
```

## 使用

#### 流式操作

```java
int i = 10;
assertThat(i).isEqualTo(10);
assertThat(i).isNotEqualTo(20).isIn(10, 20, 30);
```

#### 数字支持

```java
assertThat(10).isEqualTo(10).isGreaterThan(5).isPositive();
```

#### 字符串支持

```java
assertThat("foo").startsWith("f").endsWith("o").isEqualToIgnoringCase("Foo");
```

#### 日期支持

预期值可以是日期也可以是符合 `yyyy-MM-dd HH:mm:ss.SSS` 格式的字符串。

```java
assertThat(new Date()).isAfter("2014-02-01").isBeforeYear(2018)
                .isBetween("2014-01-01", "2026-12-16");
```

#### 集合支持

```java
List<String> list = Arrays.asList("one", "two", "three");
assertThat(list).hasSize(3)
        // 包含
        .contains("two", "one")
        // 完全相等
        .containsExactly("one", "two", "three")
        // 包含且序列正确
        .containsSequence("one", "two")
        .doesNotContain("four");
```

#### 映射支持

```java
Map<String, Object> foo = Maps.newHashMap();
foo.put("A", 1);
foo.put("B", 2);
foo.put("C", 3);
assertThat(foo).isNotEmpty().hasSize(3)
        .containsKeys("A", "B")
        .containsValues(2, 3)
        .contains(MapEntry.entry("A", 1));
```

#### 方法的抽出

```java
List<Person> persons = Arrays.asList(
              new Person("Peter", 20), new Person("Jack", 18), new Person("Jane", 24)
      );
assertThat(persons).hasSize(3)
        .extracting("name").contains("Peter", "Jack");
assertThat(persons)
        .extracting(new Extractor<Person, String>() {
            @Override
            public String extract(Person person) {
                return person.getName();
            }
        }).contains("Jane");
```

#### Android 支持

```java
assertThat(layout).isVisible()
    .isVertical()
    .hasChildCount(4)
    .hasShowDividers(SHOW_DIVIDERS_MIDDLE);
```

## Assert Generator

可以根据现有对象生成专门的断言用的对象。比如说可以直接使用以下形式进行断言：

```java
Player player = new Player();
player.setName("foobar");
PlayerAssert.assertThat(player)
        .hasName("foobar")
        .hasName("God");
```

使用时需要先添加以下依赖项

```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-assertions-generator-maven-plugin</artifactId>
            <version>2.0.0</version>
            <configuration>
                <!--<packages>-->
                    <!--<param>your.first.package</param>-->
                    <!--<param>your.second.package</param>-->
                <!--</packages>-->
                <classes>
                    <param>com.mrseasons.spring.first.corejava.model.Player</param>
                </classes>
                <excludes>
                    <exclude>.*Examples.*</exclude>
                </excludes>
            </configuration>
        </plugin>
    </plugins>
</build>
```

然后在命令行中运行以下代码生成断言对象

```bash
mvn assertj:generate-assertions
```

生成后的断言对象位于 `target/generated-test-sources/assertj-assertions` 中

## Joda-Time 支持

添加依赖

```xml
<dependency>
  <groupId>org.assertj</groupId>
  <artifactId>assertj-joda-time</artifactId>
  <version>1.1.0</version>
  <scope>test</scope>
</dependency>
```

使用

```java
DateTime firstDateTime = new DateTime();
DateTime secondDateTime = new DateTime();
//  时间前后
assertThat(firstDateTime).isBefore(secondDateTime);
//  字符串与时间比较
assertThat(new DateTime("2000-01-01")).isEqualTo("2000-01-01");
assertThat(firstDateTime).isAfter("2004-12-13T21:39:45.618-08:00");
//  忽略秒
assertThat(firstDateTime).isEqualToIgnoringSeconds(secondDateTime);
```
