[TOC]

# 断言

断言主要用于在测试中检查实际值是否符合期望值，不符合时则抛出异常信息。

## 实现

- [AssertJ](http://joel-costigliola.github.io/assertj/)


---

可运行实例代码位于 `spring/first_corejava/src/test` 的 `assertj` 目录下
