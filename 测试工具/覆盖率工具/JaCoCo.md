[TOC]

# JaCoCo

全名 Java Code Coverage。JaCoCo Agent 会被加载到 JVM 中，监控并记录 JVM 中加载的类有哪些部分被运行了。程序运行完毕后 JaCoCo Report 再根据之前的记录生成静态报告。所以 agent 必须在测试执行前被运行，report 必须在测试执行完毕后被运行。

## 配置文件

```xml
<build>
  <plugins>
    <plugin>
         <groupId>org.jacoco</groupId>
         <artifactId>jacoco-maven-plugin</artifactId>
         <version>0.7.5.201505241946</version>
         <executions>
             <execution>
                 <id>jacoco-initialize</id>
                 <goals>
                     <goal>prepare-agent</goal>
                 </goals>
             </execution>
             <execution>
                 <id>jacoco-post</id>
                 <phase>package</phase>
                 <goals>
                     <goal>report</goal>
                 </goals>
             </execution>
         </executions>
     </plugin>
  </plugins>
</build>
```

## 使用

根据以上 `jacoco-post` 的 `phase` 的不同，执行不同的命令，如 `mvn package` 等，生成的报告在 `/taget/jacoco.exec` 和 `/target/site/jacoco`。



---

可运行实例代码位于 `spring/first_corejava` 工程 
