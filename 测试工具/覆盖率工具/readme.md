[TOC]

# 代码覆盖率工具

## 实现

- [Emma](http://emma.sourceforge.net/)
- [JaCoCo](http://eclemma.org/jacoco/)
- [Cobertuna](http://cobertura.github.io/cobertura/)
- [Clover](https://confluence.atlassian.com/display/CLOVER/Comparison+of+code+coverage+tools)

其中 Clover 为商用软件。
