[TOC]

# 单元测试

## 实现

- [JUnit](www.junit.org)
- [Selenium](http://docs.seleniumhq.org/)


## JUnit

TestRunner 执行测试程序，实际为命令行工具
TestCase 包含一组测试方法
TestSuite 包含一组 TestCase
TestRule 决定如何运行测试方法，在测试方法加载前后和包含测试方法的类的加载前后执行 Hook 程序 （setUp, tearDown 等）


## Selenium

Selenium 2.0 主要用于浏览器测试，默认只支持 IE，但是能够通过第三方驱动支持大部分浏览器。可以模拟点击，表单填充，通过虚拟节点获得页面上的任意元素来编写测试用例。
