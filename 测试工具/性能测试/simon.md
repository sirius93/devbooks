
## Java Simon

### 安装

JavaSimon 有多个不同的组件用于不同的场景，以下是最基本的组件

```xml
<dependency>
    <groupId>org.javasimon</groupId>
    <artifactId>javasimon-core</artifactId>
    <version>4.1.0</version>
</dependency>
```

### 使用

#### 基本使用

```java
//设置名字
Stopwatch stopwatch = SimonManager.getStopwatch(HelloWorld.class.getName() + "-stopwatch");

//开始监控
Split split = stopwatch.start();

//执行监控的方法
Thread.sleep(5000);

//停止监控
Split stopSplit = split.stop();

//打印监控时间
System.out.println(SimonUtils.presentNanoTime(stopSplit.runningFor()));
System.out.println(SimonUtils.presentNanoTime(split.runningFor()));
System.out.println(stopSplit.toString());
```

#### 与 Spring 结合

#### 测试 JDBC

#### Web Console
