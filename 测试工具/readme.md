[TOC]

# 测试工具

## 实现

- [Gatling](http://gatling.io/#/)
- [JMeter](http://jmeter.apache.org/)

## Gatling vs JMeter

## Gatling

基于 Scala 的性能测试工具

### 安装

#### 使用压缩包

[下载 Gatling bundle(zip)](http://gatling.io/#/download)

#### 使用 Maven

```xml
<dependencies>
  <dependency>
        <groupId>io.gatling.highcharts</groupId>
        <artifactId>gatling-charts-highcharts</artifactId>
        <version>2.1.7</version>
        <scope>test</scope>
    </dependency>
</dependencies>
 <build>
    <plugins>
        <plugin>
            <groupId>io.gatling</groupId>
            <artifactId>gatling-maven-plugin</artifactId>
            <version>2.1.7</version>
            <executions>
                <execution>
                    <goals>
                        <goal>execute</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```

### 使用

#### 使用压缩包

测试文件放在 `user-files` 中

运行

```bash
cd bin
./gatling.sh
```

#### 使用 Maven

运行

```bash
mvn gatling:execute
```

运行指定类

```bash
mvn gatling:execute -Dgatling.simulationClass=computerdatabase.BasicSimulation
```


## JMeter

可以通过 GUI 进行设置，方便不懂程序设计的人

### 安装

直接下载压缩包解压

### 使用

```bash
cd bin
chmod 777 jmeter
./jmeter
```
