[TOC]

# Gatling

基于 Scala 的性能测试工具

## 安装

### 压缩包方式

[下载 Gatling bundle(zip)](http://gatling.io/#/download)

### Maven 方式

```xml
<dependencies>
  <dependency>
        <groupId>io.gatling.highcharts</groupId>
        <artifactId>gatling-charts-highcharts</artifactId>
        <version>2.1.7</version>
        <scope>test</scope>
    </dependency>
</dependencies>
 <build>
    <plugins>
        <plugin>
            <groupId>io.gatling</groupId>
            <artifactId>gatling-maven-plugin</artifactId>
            <version>2.1.7</version>
            <executions>
                <execution>
                    <goals>
                        <goal>execute</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```

## 使用

### 基于压缩包

测试文件放在 `user-files` 中

运行

```bash
cd bin
./gatling.sh
```

### 基于 Maven

运行

```bash
mvn gatling:execute
```

运行指定类

```bash
mvn gatling:execute -Dgatling.simulationClass=computerdatabase.BasicSimulation
```
