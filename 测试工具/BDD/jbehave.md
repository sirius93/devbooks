[TOC]

# JBehave

## 基本概念

JBehave 共有以下几个概念

- Story：系统想要具有的功能
- Scenario：Story 描述的功能的 Key-Example
- Steps：Scenario 中描述的实例的具体执行步骤

## 添加依赖

```xml
<dependency>
    <groupId>org.jbehave</groupId>
    <artifactId>jbehave-core</artifactId>
    <version>4.0.4</version>
</dependency>
```

IDEA 需安装 JBehave 相关插件才能完成代码提示功能。

## 使用

### 基本步骤

1. 编写 Stories
2. 编写 Steps
3. 加载配置
4. 运行测试

### 编写 Stories

Story 可以是任何文件，通常是后缀为 `story` 的文本文件。

Story 分为三部分

- Given 前提条件
- When 添加
- Then 期待结果

例：

resources/student.story

```
Narrative: In order to get a new student,as a teacher, I want to add a student into the Class

Scenario: Add a student into the class

Given There is a student
And his name is 'Lincoln'
And his age is 18
When system add the student into class
Then we can get student 'Lincoln' from class
```

### 编写 Steps

编写 steps 时需对照 story，将每个 step 语句与一个方法对应起来

例：

```java
public class StudentSteps {

    private ClassRoom classRoom;
    private Student student;

    @Given("There is a student")
    public void initStudent() {
        student = new Student();
    }

    @Given("his name is '$name'")
    public void setName(@Named("name") String name) {
        student.setName(name);
    }

    @Given("his age is $age")
    public void setAge(@Named("age") Integer age) {
        student.setAge(age);
    }

    @When("system add the student into class")
    public void addStudentIntoClass() {
        classRoom = new ClassRoom();
        classRoom.addStudent(student);
    }

    @Then("we can get student '$studentName' from class")
    public void checkGetStudent(@Named("studentName") String studentName) {
        assertThat(student).isEqualTo(classRoom.getStudent(studentName));
    }
}
```

### 加载 Story 配置文件

Story 类需继承 `JUnitStories`，继承后可以直接运行。

```java
public class StudentStories extends JUnitStories {

    public Configuration configuration() {
        return new MostUsefulConfiguration()
                // where to find the stories
                .useStoryLoader(new LoadFromClasspath(this.getClass()))
                .useStoryReporterBuilder(new StoryReporterBuilder()
                        .withDefaultFormats()
                        .withFormats(Format.CONSOLE, Format.TXT, Format.HTML));
    }

    @Override
    public InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(),
                new StudentSteps());
    }

    @Override
    protected List<String> storyPaths() {
        return new StoryFinder().findPaths(
                CodeLocations.codeLocationFromClass(this.getClass()), "**/*.story", "");
    }
}
```

然后就可以直接运行 Story 用例了。

### Composite

Composite 可以将多个前提添加结合在一起编写

story 文件

```
Given there is a default student
When system add the student into class
Then we can get student 'default' from class
```

step 文件

```java
@Given("there is a default student")
@Composite(steps = {
        "Given There is a student",
        "Given his name is 'default'",
        "Given his age is 20"
})
public void createDefaultStudent() {
}
```

#### ExamplesTable

ExamplesTable 可以提供一个参数的表格

story 文件

```
Given there is a student with details:
|name|age|
|Peter|20|
|Jane|21|
When system add the student into class
Then we can get student 'Peter' from class
```

step 文件

```java
@Given("there is a student with details:$details")
public void createStudentWithDetails(@Named("details") ExamplesTable details) {
    student = new Student();
    Parameters parameters = details.getRowAsParameters(0);
    student.setName(parameters.valueAs("name", String.class));
    student.setAge(parameters.valueAs("age", Integer.class));
}
```
