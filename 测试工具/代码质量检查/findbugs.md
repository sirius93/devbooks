[TOC]


# FindBugs

FindBugs 是一个静态分析工具，它检查类或者 JAR 文件，将字节码与一组缺陷模式进行对比以发现可能的问题。

## 配置文件

```xml
<reporting>
    <plugins>
        <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>findbugs-maven-plugin</artifactId>
            <version>3.0.3</version>
        </plugin>
    </plugins>
</reporting>
```

## 使用

生成报告文件，默认文件生成在 `target/findbugsXml.xml`。site 相关所有文件生成在 `target/site` 目录。

```bash
mvn site
```

更改报告文件的位置

```xml
<configuration>
    <xmlOutput>true</xmlOutput>
    <xmlOutputDirectory>target/site</xmlOutputDirectory>
</configuration>
```

显示 GUI 界面

```bash
mvn findbugs:gui
```

完整示例

```xml
<plugin>
    <groupId>org.codehaus.mojo</groupId>
    <artifactId>findbugs-maven-plugin</artifactId>
    <version>3.0.3</version>
    <configuration>
        <xmlOutput>true</xmlOutput>
        <findbugsXmlOutput>true</findbugsXmlOutput>
        <xmlOutputDirectory>target/site/findbugs</xmlOutputDirectory>
        <findbugsXmlOutputDirectory>target/site/findbugs</findbugsXmlOutputDirectory>
        <outputEncoding>UTF-8</outputEncoding>
    </configuration>
</plugin>
```
