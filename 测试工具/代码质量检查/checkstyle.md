[TOC]

# CheckStyle

用于检查代码风格

## 配置文件

```xml
<reporting>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-checkstyle-plugin</artifactId>
            <version>2.17</version>
            <configuration>
                <configLocation>google_checks.xml</configLocation>
                <failOnViolation>false</failOnViolation>
            </configuration>
        </plugin>
    </plugins>
</reporting>
```

默认使用的是 `sun_checks.xml`，也可以切换成 `google_checks.xml`。可以下载这些格式模板后自行修改。Google 的 [checkstyle 配置文件](https://github.com/checkstyle/checkstyle/blob/master/src/main/resources/google_checks.xml)

## 使用

生成报告文件，默认文件生成在 `target/checkstyle-result.xml` 和 `target/site/checkstyle.html`。site 相关所有文件生成在 `target/site` 目录。

```bash
mvn site
```

只执行 checkstyle

```bash
mvn compile
mvn checkstyle:checkstyle
```
