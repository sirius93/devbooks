[TOC]

# PMD

静态分析工具

## 配置文件

```xml
 <plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-pmd-plugin</artifactId>
    <version>3.6</version>
</plugin>
```

## 使用

生成报告文件，默认文件生成在 `target/pmd.xml` 和 `target/site/pmd.html`

```bash
mvn pmd:pmd
```
