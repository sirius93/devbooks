[TOC]


# SONAR

## 安装

### 压缩包方式

下载 [安装包](http://www.sonarqube.org/downloads/) 并解压

### 基于 Docker

#### 安装 MySQL

运行 MySQL 容器

```bash
docker run --name mysql01 -p 3306:3306 -e MYSQL_ROOT_PASSWORD=tiger -d mysql
```

连接 MySQL 客户端

```bash
docker run -it --link mysql01 --rm mysql sh -c 'exec mysql -h172.17.0.2 -P3306 -uroot -ptiger'
```

创建 SONAR 用的数据库和用户

```msyql
CREATE DATABASE sonar CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE USER 'sonar' IDENTIFIED BY 'sonar';
GRANT ALL ON sonar.* TO 'sonar'@'%' IDENTIFIED BY 'sonar';
GRANT ALL ON sonar.* TO 'sonar'@'localhost' IDENTIFIED BY 'sonar';
FLUSH PRIVILEGES;
```

#### 安装 SONAR

运行 SONAR 容器

```bash
docker run -d --name sonarqube \
    -p 9000:9000 -p 9092:9092 \
    -e SONARQUBE_JDBC_USERNAME=sonar \
    -e SONARQUBE_JDBC_PASSWORD=sonar \
    -e SONARQUBE_JDBC_URL="jdbc:mysql://192.168.1.67:3306/sonar?useUnicode=true&characterEncoding=utf8" \
    sonarqube:5.1
```

## 使用

### 访问控制台

URL 为 `http://192.168.1.67:9000/`，默认用户名和密码都是 `admin`。

### 提交工程到 SONAR

```bash
mvn sonar:sonar -Dsonar.host.url=http://192.168.1.67:9000 -Dsonar.jdbc.url="jdbc:mysql://192.168.1.67:3306/sonar?useUnicode=true&characterEncoding=utf8"
```

### 安装插件

Settings -> System -> Update Center -> Available Plugin


## 异常

1. 报 `jacoco.exec: Incompatible version 1007` 是因为Jacoco 版本太新，换回 0.7.4.201502262128 就可以了。
