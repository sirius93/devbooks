[TOC]

# 代码质量检查

## 实现

- [FindBugs](https://github.com/gleclaire/findbugs-maven-plugin)
- [CheckStyle](https://maven.apache.org/plugins/maven-checkstyle-plugin/)
- [PMD](https://pmd.github.io/)
- [SONAR](http://www.sonarqube.org/)





## 参考

- [常用 Java 静态代码分析工具的分析与比较](http://www.oschina.net/question/129540_23043)


---

可运行实例代码位于 `spring/first_corejava` 工程下
