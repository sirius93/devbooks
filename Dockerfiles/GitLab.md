
## 搭建步骤

步骤

1. 创建 postgresql 容器
```bash
docker run --name gitlab-postgresql -d \
    --env 'DB_NAME=gitlabhq_production' \
    --env 'DB_USER=gitlab' --env 'DB_PASS=ejusdk' \
    --env 'DB_EXTENSION=pg_trgm' \
    --volume /srv/docker/gitlab/postgresql:/var/lib/postgresql \
    sameersbn/postgresql:9.4-22
```

注意修改过 DB 相关参数

2. 创建 redis 容器
```bash
docker run --name gitlab-redis -d \
    --volume /srv/docker/gitlab/redis:/var/lib/redis \
    sameersbn/redis:latest
```

3. 创建 gitlab 容器
```bash
docker run --name gitlab2 -d \
    --link gitlab-postgresql:postgresql --link gitlab-redis:redisio \
    --publish 10022:22 --publish 10080:80 \
    --env 'GITLAB_PORT=10080' --env 'GITLAB_SSH_PORT=10022' \
    --env 'GITLAB_HOST=10.0.60.115' \
    --env 'GITLAB_BACKUPS=weekly' \
    --env 'GITLAB_SECRETS_DB_KEY_BASE=long-and-random-alpha-numeric-string' \
    --volume /srv/docker/gitlab/gitlab:/home/git/data \
    sameersbn/gitlab:8.9.6-1
```

注意修改 GITLAB 相关参数，其中 GITLAB_HOST 必选，否则 clone 时的默认地址都会变为 localhost
