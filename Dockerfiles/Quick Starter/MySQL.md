Dockerfile

```
FROM mysql:5.6
```


客户端

```bash
docker run -it --link mysql01 --rm mysql sh -c 'exec mysql -h172.17.0.2 -P3306 -uroot -ptiger'
```

服务端

```bash
docker run -d --name mysql01 -p 3306:3306 -v ~/opt/data/mysql:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=543646366 mysql
```

将容器内数据持久化到本地 ~/opt/data/mysql 目录下，并设置 root 用户密码
