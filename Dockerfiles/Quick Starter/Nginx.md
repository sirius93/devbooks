FROM nginx:1.9

ADD nginx.confg /etc/nginx/nginx.conf
ADD sites-enabled/* /etc/nginx/conf.d/

RUN mkdir /opt/htdocs && mkdir /opt/logo && mkdir /opt/log/nginx
RUN chown -R www-data.www-data /opt/htdocs /opt/log

VOLUMN ["/opt"]
