Dockerfile

```
FROM redis:alpine
```

服务端

```bash
docker run --name redis1 -p 6379:6379 -d redis
```

客户端

```bash
docker exec -ti redis1 /bin/bash
redis-cli
```
