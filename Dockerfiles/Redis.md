[TOC]

## 构建镜像

基础镜像

```
FROM centos:7
MAINTAINER SidneyXu <siriuseddy@gmail.com>

ENV REDIS_MIRROR http://download.redis.io/releases/redis-3.0.5.tar.gz
ENV GEM_MIRROR http://ruby.sdutlinux.org/

# 安装必要环境
RUN yum -y install gcc make ruby zlib

# 安装 Redis
RUN curl -sSL "$REDIS_MIRROR" -o redis.tar.gz \
    && mkdir -p /opt/redis \
    && tar -zxf redis.tar.gz -C /opt/redis --strip-components=1 \
    && rm redis.tar.gz \
    && make -C /opt/redis

# 安装 Ruby Redis 客户端
RUN gem sources --remove https://rubygems.org/ \
    && gem sources -a $GEM_MIRROR \
    && gem install redis

# 建立工程目录
RUN mkdir -p /var/log/redis \
    && mkdir -p /var/lib/redis

VOLUME ["/opt/redis", "/var/log/redis/", "var/lib/redis/"]

EXPOSE 6379
```

构建基础镜像

```bash
docker build -t sidneyxu/base_redis .
```

主镜像

```
FROM sidneyxu/base_redis
MAINTAINER SidneyXu <siriuseddy@gmail.com>

WORKDIR /opt/redis/

ENTRYPOINT ["src/redis-server"]
CMD ["redis.conf"]
```

构建主镜像

```bash
docker build -t sidneyxu/redis_master .
```

## 单机配置

运行 Redis 服务器

```bash
docker run --name redis1 -d sidneyxu/redis_master
```

测试服务器

运行 Redis 客户端并连接 Redis 服务器

```bash
docker run --rm --link redis1 -t -i sidneyxu/base_redis /bin/bash
```

测试客户端连接

```bash
/opt/redis/src/redis-cli -h $REDIS1_PORT_6379_TCP_ADDR -p $REDIS1_PORT_6379_TCP_PORT
```

尝试存取值

```
192.168.0.1:6379> set foo bar
OK
192.168.0.1:6379> get foo
"bar"
```

## 集群配置

基于 Ruby 搭建 Redis 集群最少需要 6个节点，三个主节点，三个从节点

准备集群配置文件，修改内容如下，放置在宿主机器的任意位置，这里放在 `/var/local/docker/redis/cluster/redis.conf`

```
port 6379
logfile "/var/log/redis/redis.log"
dir /opt/redis/data
cluster-enabled yes
# 单机伪集群需要使用
# cluster-config-file /opt/redis/7001/nodes.conf
# 节点最大超时时间
cluster-node-timeout 15000
# 一个主节点在拥有多少个好的从节点的时候就要割让一个从节点出来
cluster-migration-barrier 1
# 集群兼容部分失败
cluster-require-full-coverage yes
appendonly yes
```

运行 6 台 Redis 服务器

```bash
docker run --name redis100 -d sidneyxu/redis_master
docker run --name redis200 -d sidneyxu/redis_master
docker run --name redis300 -d sidneyxu/redis_master
docker run --name redis400 -d sidneyxu/redis_master
docker run --name redis500 -d sidneyxu/redis_master
docker run --name redis600 -d sidneyxu/redis_master
```

复制宿主上的配置文件到容器内

```bash
docker cp /var/local/docker/redis/cluster/redis.conf redis100:/opt/redis/redis.conf
docker cp /var/local/docker/redis/cluster/redis.conf redis200:/opt/redis/redis.conf
docker cp /var/local/docker/redis/cluster/redis.conf redis300:/opt/redis/redis.conf
docker cp /var/local/docker/redis/cluster/redis.conf redis400:/opt/redis/redis.conf
docker cp /var/local/docker/redis/cluster/redis.conf redis500:/opt/redis/redis.conf
docker cp /var/local/docker/redis/cluster/redis.conf redis600:/opt/redis/redis.conf
```

重启 6 台服务器

```bash
docker restart redis100
docker restart redis200
docker restart redis300
docker restart redis400
docker restart redis500
docker restart redis600
```

启动另一个容器连接所有节点

```bash
docker run --rm -ti --link redis100 --link redis200 --link redis300 --link redis400 --link redis500 --link redis600 sidneyxu/base_redis /bin/bash
```

执行 `env` 获得节点的 IP 修改以下命令建立集群

```bash
/opt/redis/src/redis-trib.rb create --replicas 1 172.17.0.4:6379 172.17.0.8:6379 172.17.0.9:6379 172.17.0.17:6379 172.17.0.18:6379 172.17.0.19:6379
```

命令行提示 `Can I set the above configuration? (type 'yes' to accept):` 选择 `yes`，等待建立完成。

测试集群

使用客户端集群模式进行连接

```bash
/opt/redis/src/redis-cli -c -h $REDIS100_PORT_6379_TCP_ADDR -p $REDIS100_PORT_6379_TCP_PORT
```

查看集群状态

```bash
cluster nodes
```

实际生产环境下构建完集群后，需要确认以下两件事：
- 一台机器只能有一个 Master
- 一组 Master 和 Slaver 不应存在于同一台机器上
