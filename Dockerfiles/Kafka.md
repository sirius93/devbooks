[TOC]

## 构建镜像

基础镜像

```
FROM   centos:7
MAINTAINER "SidneyXu" <siriuseddy@gmail.com>

ENV KFK_MIRROR http://apache.fayea.com/kafka/0.8.2.0/kafka_2.11-0.8.2.0.tgz

RUN     curl -SL --cookie "gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u60-b27/jdk-8u60-linux-x64.rpm" -o java8.rpm \
        && yum localinstall -y java8.rpm \
        && rm -rf java8.rpm \
        && yum clean all
RUN     curl -sSL $KFK_MIRROR -o kfk.tgz \
        && mkdir -p /opt/kafka \
        && tar -xzf kfk.tgz -C /opt/kafka --strip-components=1 \
        && rm kfk.tgz

ENV JAVA_HOME /usr/java/latest
ENV KAFKA_HOME /opt/kafka

EXPOSE 9092

WORKDIR /opt/kafka

VOLUME ["/opt/kafka/config"]
```

构建镜像

```bash
docker build -t sidneyxu/base_kfk .
```

主镜像

```
FROM sidneyxu/base_kfk
MAINTAINER SidneyXu <siriuseddy@gmail.com>

CMD ["/opt/kafka/bin/zookeeper-server-start.sh"]
ENTRYPOINT ["/opt/kafka/bin/kafka-server-start.sh"]
CMD ["/opt/kafka/config/server.properties"]
```

构建主镜像

```bash
docker build -t sidneyxu/kfk_master .
```

## 单机配置

运行 Kafka 服务器

```bash
docker run --name kfk01 -ti -P --link zk3 sidneyxu/base_kfk
```

修改 `/opt/kafka/config/server.properties` 中的 zookeeper 地址。

启动 Kafka

```bash
/opt/kafka/bin/kafka-server-start.sh /opt/kafka/config/server.properties
```


---

docker run  -d --name kafka1 -p 9092:9092 -e KAFKA_BROKER_ID=0 -e KAFKA_ZOOKEEPER_CONNECT=139.224.200.47:32770 -e KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://139.224.200.47:9092 -e KAFKA_LISTENERS=PLAINTEXT://0.0.0.0:9092 -t wurstmeister/kafka


https://blog.csdn.net/lblblblblzdx/article/details/80548294


mac本地kafka
https://www.jianshu.com/p/dd2578d47ff6

启动zk
zkServer start
