
## 搭建步骤

步骤

1. 搭建 jira 数据库
```bash
docker run --name jira-postgres -d \
    -e 'POSTGRES_USER=jira' \
    -e 'POSTGRES_PASSWORD=fjoaf8wfyodayuki' \
    -e 'POSTGRES_DB=jiradb' \
    -e 'POSTGRES_ENCODING=UTF8' \
    -e 'POSTGRES_COLLATE=C' \
    -e 'POSTGRES_COLLATE_TYPE=C' \
    blacklabelops/postgres
```

注意修改 DB 相关数据

2. 登录 jira-postgres 后创建 confluence 数据库

3. 运行 jira 容器
```bash
docker run -d --name jira01 \
      -e "JIRA_DATABASE_URL=postgresql://jira@jira-postgres/jiradb" \
      -e "JIRA_DB_PASSWORD=fjoaf8wfyodayuki"  \
      --link jira-postgres \
      -p 20081:8080 blacklabelops/jira      
```

4. 运行 confluence 容器
```bash
docker run -d --name confluence01 \
      --link jira-postgres \
      -p 20080:8090 blacklabelops/confluence
```


## 使用指南

### 修改密码方式

- Jira 右上小三角，选择 Profile -> Change Password
- Confluence 右上小三角，选择 Settings -> Password
