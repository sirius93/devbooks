[TOC]

## 安装

基础镜像

```
FROM centos:7
MAINTAINER SidneyXu <siriuseddy@gmail.com>

ENV NGINX_MIRROR http://nginx.org/download/nginx-1.8.0.tar.gz

RUN yum -y install gcc gcc-c++ autoconf automake make
RUN yum -y install pcre pcre-devel zlib zlib-devel
RUN curl -sSL $NGINX_MIRROR -o nginx.tar.gz \
	&& mkdir -p /opt/nginx \
	&& tar -zxf nginx.tar.gz -C /opt/nginx --strip-components=1 \
	&& rm nginx.tar.gz
RUN cd /opt/nginx \
	&& ./configure \
	&& make \
	&& make install

EXPOSE 80 443

CMD ["/usr/local/nginx/sbin/nginx", "-g", "daemon off;"]
```

构建镜像

```bash
docker build -t sidneyxu/nginx .
```

运行服务器

```bash
docker run --name nginx -P -d sidneyxu/nginx
```

运行完后访问容器 80 端口可以看到 `Welcome to nginx!` 的网页，证明安装完成。
