[TOC]

## 构建镜像

基础镜像

```
FROM        centos:7
MAINTAINER "SidneyXu" <siriuseddy@gmail.com>

ENV ZK_MIRROR http://mirrors.cnnic.cn/apache/zookeeper/zookeeper-3.4.6/zookeeper-3.4.6.tar.gz

RUN     curl -SL --cookie "gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u60-b27/jdk-8u60-linux-x64.rpm" -o java8.rpm \
        && yum localinstall -y java8.rpm \
        && rm -rf java8.rpm \
        && yum clean all
RUN     curl -sSL $ZK_MIRROR -o zookeeper.tar.gz \
        && mkdir -p /opt/zookeeper \
        && tar -zxf zookeeper.tar.gz -C /opt/zookeeper --strip-components=1 \
        && rm zookeeper.tar.gz \
        && cp /opt/zookeeper/conf/zoo_sample.cfg /opt/zookeeper/conf/zoo.cfg \
        && mkdir -p /var/lib/zookeeper /var/log/zookeeper

ENV JAVA_HOME /usr/java/latest

EXPOSE 2181 2888 3888

WORKDIR /opt/zookeeper

VOLUME ["/opt/zookeeper/conf", "/var/lib/zookeeper", "/var/log/zookeeper"]
```

构建镜像

```bash
docker build -t bookislife.com/base_zk .
```

主镜像

```
FROM sidneyxu/base_zk
MAINTAINER SidneyXu <siriuseddy@gmail.com>

ENTRYPOINT ["/opt/zookeeper/bin/zkServer.sh"]
CMD ["start-foreground"]
```

构建主镜像

```bash
docker build -t bookislife.com/zk_master .
```

## 单机配置

运行 Zookeeper 容器

```bash
docker run --name zk1 -d sidneyxu/zk_master
```

测试容器

运行 Zookeeper 客户端并连接 Zookeeper 容器

```bash
docker run --rm --link zk1 -t -i sidneyxu/base_zk /bin/bash
```

测试客户端连接

```bash
/opt/zookeeper/bin/zkCli.sh -server $ZK1_PORT_2181_TCP_ADDR:$ZK1_PORT_2181_TCP_PORT
```

成功后会看到控制台输出 `Welcome to ZooKeeper!` 表示运行正常。

## 集群配置

运行三台 Zookeeper 服务器

```bash
docker run --name zk100 -d sidneyxu/zk_master
docker run --name zk200 -d sidneyxu/zk_master
docker run --name zk300 -d sidneyxu/zk_master
```

查找三台机器对应的 IP 地址

```bash
docker inspect zk100 | grep IPAddress
docker inspect zk200 | grep IPAddress
docker inspect zk300 | grep IPAddress
```

准备集群配置文件 `zoo.cfg`，将放在容器的 `/opt/zookeeper/conf/zoo.cfg`，配置文件中的地址为上述命令中获得的地址

```
dataDir=/var/lib/zookeeper
dataLogDir=/var/log/zookeeper
server.1=172.17.0.1:2888:3888
server.2=172.17.0.2:2888:3888
server.3=172.17.0.3:2888:3888
```

复制宿主上的配置文件到容器内

```bash
docker cp /var/local/docker/zookeeper/cluster/zoo.cfg zk100:/opt/zookeeper/conf/zoo.cfg
docker cp /var/local/docker/zookeeper/cluster/zoo.cfg zk300:/opt/zookeeper/conf/zoo.cfg
docker cp /var/local/docker/zookeeper/cluster/zoo.cfg zk300:/opt/zookeeper/conf/zoo.cfg
```

分别挂载三台机器并添加 `myid` 文件，其中 "1"是第一台服务器的id，第二三台服务器id是"2", "3"

```bash
docker run --rm -ti --volumes-from zk100 centos:7 /bin/bash
echo "1" > /var/lib/zookeeper/myid
```

重启三台机器，重启后地址就变了

```bash
docker restart zk100
docker restart zk200
docker restart zk300
```

测试连接

```bash
docker run --rm --link zk100 --link zk200 --link zk300 -t -i sidneyxu/base_zk /bin/bash
```

测试客户端连接

```bash
/opt/zookeeper/bin/zkCli.sh -server $ZK100_PORT_2181_TCP_ADDR:$ZK100_PORT_2181_TCP_PORT
```


??docker 地址会变

echo "1" > /var/lib/zookeeper/myid

"1"是第一台服务器的id，第二三台服务器id是"2", "3"。

创建/opt/zookeeper/conf/zoo.cfg，内容如下：

server.1=10.224.23.103:2888:3888
server.2=10.250.2.102:2888:3888
server.3=10.250.3.27:2888:3888


使用

```bash
ls /

# 创建 node
create /zk "myData"

# 获取 node
get /zk

# 修改 node 对应的名称
set /zk "foobar"

# 删除 node
delete /zk
```
