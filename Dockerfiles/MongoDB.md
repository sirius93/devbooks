[TOC]

## 构建镜像

基础镜像

```
FROM centos:7
MAINTAINER SidneyXu <siriuseddy@gmail.com>

ENV MONGODB_MIRROR https://repo.mongodb.org/yum/redhat/7/mongodb-org/3.2/x86_64/

RUN echo -e "[mongodb]\n\
name=MongoDB Repository\n\
baseurl=$MONGODB_MIRROR\n\
gpgcheck=0\n\
enabled=1" >> /etc/yum.repos.d/mongodb.repo

RUN yum install -y mongodb-org \
    && mkdir -p /data/db \
    && yum clean all

VOLUME ["/etc/","/var/lib/mongo/","/var/log/mongodb"]

EXPOSE 27017 28017
```

构建基础镜像

```bash
docker build -t sidneyxu/base_mongodb .
```


## 单机配置

运行 MongoDB 服务器

```bash
docker run --name mongodb1 -d sidneyxu/base_mongodb /bin/mongod
```

测试服务器

运行 MongoDB 客户端并连接 MongoDB 服务器

```bash
docker run --rm --link mongodb1 -t -i sidneyxu/base_mongodb /bin/bash
```

测试客户端连接

```bash
/bin/mongo --host $MONGODB1_PORT_27017_TCP_ADDR --port $MONGODB1_PORT_27017_TCP_PORT
```

尝试存取值

```
> db.test.insert({"x":1})
WriteResult({ "nInserted" : 1 })
> db.test.findOne()
{ "_id" : ObjectId("5667f2963d01ac5b68815af6"), "x" : 1 }
```

## 集群配置

### 副本集

集群架构：三台数据服务器

配置副本集时应该尽可能使用奇数节点，如果节点数为偶数的话，就必须有一个仲裁节点来参与投票才能保证故障恢复功能的正常运行。

为了简单起见，实验只使用作为数据节点的三台 MongoDB 服务器，不使用仲裁节点。

运行三台 MongoDB 服务器

```bash
docker run --name mongodb1 -d sidneyxu/base_mongodb /bin/mongod --dbpath /var/lib/mongo \
  --replSet rs --logpath /var/log/mongo
docker run --name mongodb2 -d sidneyxu/base_mongodb /bin/mongod --dbpath /var/lib/mongo \
  --replSet rs --logpath /var/log/mongo
docker run --name mongodb3 -d sidneyxu/base_mongodb /bin/mongod --dbpath /var/lib/mongo \
  --replSet rs --logpath /var/log/mongo
```

初始化副本集

连接三个节点

```bash
docker run --rm --link mongodb1 --link mongodb2 --link mongodb3 -t -i sidneyxu/base_mongodb /bin/bash
```

运行 MongoDB 客户端并连接任一节点

```bash
/bin/mongo --host $MONGODB1_PORT_27017_TCP_ADDR --port $MONGODB1_PORT_27017_TCP_PORT
```

执行 `env` 获得各节点的 ip 地址后修改以下文件初始化副本集

```bash
config_test = {
   "_id": "rs",
   "members": [
       {
           "_id": 0,
           "host": "172.17.0.1:27017",
           "priority": 2
       },
       {
           "_id": 1,
           "host": "172.17.0.2:27017",
           "priority": 1
       },
       {
           "_id": 2,
           "host": "172.17.0.3:27017",
           "priority": 1
       }
   ]
}
rs.initiate(config_test)
```

测试副本集

运行 MongoDB 客户端并连接任意一个 MongoDB 服务器

```bash
docker run --rm --link mongodb1 --link mongodb2 --link mongodb3 -t -i sidneyxu/base_mongodb /bin/bash
```

测试客户端连接

```bash
/bin/mongo --host $MONGODB1_PORT_27017_TCP_ADDR --port $MONGODB1_PORT_27017_TCP_PORT
```

注意命令行控制符会由 `>` 变为 `rs:PRIMARY>`，这表示当前节点为活动节点，或者变为 `rs:SECONDARY>` 表示此为非活动节点。

查看副本集状态

``` bash
rs.status()
```

查看当前节点是否为活动节点

``` bash
db.isMaster()
```

### 分片

集群架构：三台构成一个副本集的数据服务器，三台配置服务器，一台路由服务器

1. 运行配置服务器

```bash
docker run --name mongocfg1 -d sidneyxu/base_mongodb /bin/mongod \
  --dbpath /var/lib/mongo \
  --configsvr \
  --logpath /var/log/mongo \
  --port 27017
docker run --name mongocfg2 -d sidneyxu/base_mongodb /bin/mongod \
  --dbpath /var/lib/mongo \
  --configsvr \
  --logpath /var/log/mongo \
  --port 27017
docker run --name mongocfg3 -d sidneyxu/base_mongodb /bin/mongod \
  --dbpath /var/lib/mongo \
  --configsvr \
  --logpath /var/log/mongo \
  --port 27017
```

获得配置服务器的 IP 地址

```bash
docker inspect mongocfg1 | grep IPAddress
```

2. 运行路由服务器

将以下地址换成配置服务器的地址，多个用逗号分隔

```bash
docker run --name mongos1 -d sidneyxu/base_mongodb /bin/mongos \
  --port 27017 \
  --configdb \
172.17.0.5:27017,\
172.17.0.9:27017,\
172.17.0.10:27017
```

3. 添加分片服务器

运行 MongoDB 客户端并连接路由服务器

```bash
docker run --rm --link mongos1 -t -i sidneyxu/base_mongodb /bin/bash
```

建立客户端连接

```bash
/bin/mongo --host $MONGOS1_PORT_27017_TCP_ADDR --port $MONGOS1_PORT_27017_TCP_PORT
```

添加分片服务器

``` bash
sh.addShard("rs/172.17.0.1:27017")
```

以上 `rs` 为副本集名，添加主节点后 MongoDB 会自动关联到其对应的副节点和仲裁节点。

4. 指定分片的数据库

```bash
use admin
db.runCommand( { enablesharding : "test"})
```

5. 指定片键

```bash
db.runCommand( { shardcollection : "test.cities", key : {name : 1}})
```

6. 测试分片

导入测试数据，验证所有数据所在的数据库

``` bash
use test
var bulk = db.cities.initializeUnorderedBulkOp();
for (var i = 1; i <= 100000; i++) bulk.insert( { name : i } );
bulk.execute();
```


## 参考资料

- [install-mongodb-on-red-hat](http://docs.mongodb.org/manual/tutorial/install-mongodb-on-red-hat/)
- [install-mongodb-on-centos-7](https://devops.profitbricks.com/tutorials/install-mongodb-on-centos-7)
- [mongod Options](https://docs.mongodb.org/manual/reference/program/mongod/)
