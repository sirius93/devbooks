## 安装信息

Nginx

源码目录 /opt/nginx
安装目录 /usr/local/nginx
可执行目录 /usr/local/nginx/sbin, /opt/nginx/objs
配置文件 /usr/local/nginx/conf/nginx.conf

---

Redis

安装目录  /opt/redis
开放端口  6379
挂载目录  /opt/redis

集群状况
主：redis100  从：redis400
主：redis200  从：redis500
主：redis300  从：redis600

---

MongoDB

写 db 和 log 区别

数据目录 /var/lib/mongo
日志目录 /var/log/mongodb
开放端口 27017 28017
可执行目录 /bin/
挂载目录 /etc  /var/lib/mongo    /var/log/mongodb
配置文件 /etc/mongod.conf

集群状况
rs副本集: rs1 rs2 rs3
配置服务器：conf1 conf2 conf3
路由服务器：mongos
mongos - [conf1 conf2 conf3]
mongos - rs 副本集

---

ZooKeeper

安装目录 /opt/zookeeper
数据目录 /var/lib/zookeeper
日志目录 /var/log/zookeeper
配置文件 /opt/zookeeper/conf/zoo.cfg
myid文件 /var/lib/zookeeper/myid
开放端口 2181 2888 3888


---

Kafka

安装目录 /opt/kafka
配置文件 /opt/kafka/confg/server.properties
开放端口 9092
