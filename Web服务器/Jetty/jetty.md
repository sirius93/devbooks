[TOC]

# Jetty

官方网站 [http://www.eclipse.org/jetty/](http://www.eclipse.org/jetty/)

## 安装

下载链接 [http://eclipse.org/jetty/downloads.php](http://eclipse.org/jetty/downloads.php)

## 运行

### 显示帮助

```bash
mvn jetty:help
```

### 以 Jar 包方式运行

```bash
java -jar start.jar
```

### 以嵌入式容器方式运行

默认情况下，Jetty 依赖的资源文件，如 `web.xml` 等都会放在在 `src/main/webapp` 中。

```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.eclipse.jetty</groupId>
            <artifactId>jetty-maven-plugin</artifactId>
            <version>9.3.0.M1</version>
            <!-- 自定义配置 -->
            <configuration>
                <!--自动热部署-->
                <scanIntervalSeconds>5</scanIntervalSeconds>
                <webApp>
                    <!--上下文路径-->
                    <contextPath>/test</contextPath>
                    <!--资源文件位置-->
                    <resourceBases>
                        <resourceBase>${project.basedir}/src/main/webapp</resourceBase>
                    </resourceBases>
                </webApp>
                <!--端口配置-->
                <httpConnector>
                    <port>8300</port>
                </httpConnector>
                <!--记录日志-->
                <requestLog implementation="org.eclipse.jetty.server.NCSARequestLog">
                    <filename>target/access-yyyy_mm_dd.log</filename>
                    <filenameDateFormat>yyyy_MM_dd</filenameDateFormat>
                    <logDateFormat>yyyy-MM-dd HH:mm:ss</logDateFormat>
                    <logTimeZone>GMT+8:00</logTimeZone>
                    <append>true</append>
                    <logServer>true</logServer>
                    <!--日志文件保存的天数-->
                    <retainDays>120</retainDays>
                    <logCookies>true</logCookies>
                </requestLog>

            </configuration>
        </plugin>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <version>3.3</version>
            <configuration>
                <source>1.8</source>
                <target>1.8</target>
            </configuration>
        </plugin>
    </plugins>
</build>
```

运行

需要注意热部署时必须先对修改的代码进行手动编译，然后编译过的代码才会被自动加载

```bash
mvn package jetty:run
```

常用 maven 命令

```bash
mvn jetty:run
mvn jetty:start
mvn jetty:stop
```


## Jetty 编程

### 添加依赖

```xml
<dependency>
    <groupId>javax.servlet</groupId>
    <artifactId>javax.servlet-api</artifactId>
    <version>3.1.0</version>
    <scope>provided</scope>
</dependency>
<dependency>
    <groupId>org.eclipse.jetty.aggregate</groupId>
    <artifactId>jetty-aggregate-project</artifactId>
    <version>8.1.16.v20140903</version>
</dependency>
```

### 使用

```java
Server server = new Server(1234);

ServletContextHandler handler = new ServletContextHandler(ServletContextHandler.SESSIONS);
handler.setContextPath("/foo");
server.setHandler(handler);

// 加载 Servlet 对象
handler.addServlet(new ServletHolder(new HelloServlet()), "/hello");

try {
    server.start();
    server.join();
} catch (Exception e) {
    e.printStackTrace();
}
```

启动后可以在浏览器输入 [http://localhost:1234/foo/hello](http://localhost:1234/foo/hello) 来访问加载的 HelloServlet 对象。



---

可运行实例代码位于 `spring/first_jetty/src/main` 的 `server` 目录下