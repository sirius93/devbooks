[TOC]

# Nginx 实战

## Nginx 基本使用

### 启动

```bash
cd /usr/local/nginx/sbin/nginx
./nginx
```

或者指定配置文件 `./nginx -c /usr/local/nginx/conf/nginx.conf`

启动完成后执行 `ps -ef | grep nginx` 查看 master 进程是否存在。

### 停止

三种方式

获得进程 ID

```bash
ps -ef | grep nginx
```

- 从容停止
    ```bash
    kill -QUIT <pid>
    ```
    或
    ```bash
    nginx -s quit
    ```

- 快速停止
    ```bash
    kill -TERM <pid>
    ```
    或
    ```bash
    nginx -s stop
    ```

- 强制停止
    ```bash
    kill -9 nginx
    ```

### 重启

如果是更改配置后需要先验证配置文件

```bash
cd /usr/local/nginx/sbin
./nginx -t [-c /usr/local/nginx/conf/nginx.conf]
```

验证完后可以使用以下两种方式进行重启

第一种方式

```bash
./nginx -s reload
```

第二种方式

```bash
kill -HUP <pid>
```

### 升级 nginx

平滑升级：不会停掉原来的进程，这些进程继续处理请求，但不会接受新请求，处理完后进程即自动停止

1. 备份旧版本 nginx
    ```bash
    cp /usr/local/nginx/sbin/nginx /usr/local/nginx/sbin/nginx.old
    ```

2. 启动平滑升级
    ```bash
    kill -USR2 <old_pid>
    ```

    执行完后，旧版本的进程 ID 文件为会被重命名为 `.oldbin`，可以通过 `ls /usr/local/nginx/logs` 来查看

3. 使用新版本替换旧版本
    ```bash
    cp -rfp <new_location> /usr/local/nginx/sbin/nginx
    ```

4. 启动新版本的 nginx
    ```bash
    ./nginx
    ```

5. 关闭旧版本的工作进程
    ```bash
    kill -WINCH <old_pid>
    ```

6. 使用 `ps -ef | grep nginx` 查看旧工作进程是否已完成了所有请求


### 信号控制

信号控制

- HUP 重启
- QUIT 从容关闭
- TERM 快速关闭
- INT 从容关闭
- USR1 切换日志文件
- USR2 平滑升级可执行进程
- WINCH 从容关闭工作进程(工程进程即 worker process，没有工作进程时 nginx 不会处理请求)

## 负载均衡

### 概念

负载均衡有多种实现方式，Nginx 通过反向代理来实现负载均衡。

用户的访问首先会访问到 Nginx 服务器，然后 Nginx 服务器再从服务器集群表中选择压力较小的服务器，并将该访问请求引向该服务器。若服务器集群中的某个服务器崩溃，那么从待选服务器列表中将该服务器删除，也就是说一个服务器假如崩溃了，那么 Nginx 就肯定不会将访问请求引入该服务器了。

### 配置负载均衡

```
http{
	#任意名称，可以使用有名的网站的 ip 作测试
	upstream myproject {
		server 182.18.22.2:80;
		server 118.144.78.52;
	}
	server {
		listen 8080;
		#访问根目录
		location / {
			proxy_pass http://myproject;
		}
	}
}
```

以上配置了当访问根目录时自动跳转到 myproject 指定的两个 ip 的一个。

### HTTP Upstream 模块

Upstream 模块实现在轮询和客户端 ip 之间实现后端的负载均衡。常用的指令有 ip_hash 指令、server 指令和 upstream 指令等。

#### ip_hash 指令

在负载均衡系统中，假如用户在某台服务器上登录，那么如果该用户第二次请求的时候，因为我们是负载均衡系统，每次请求都会重新定位到服务器集群中的一个服务器，那么此时如果将已经登录服务器 A 的用户再定位到其他服务器，显然不妥。故而，我们可以采用 ip_hash 指令解决这个问题，如果客户端请求已经访问了服务器 A 并登录，那么第二次请求的时候，会将该请求通过哈希算法自动定位到该后端服务器中。

```
upstream myproject {
	ip_hash;
	server 182.18.22.2:80;
	server 118.144.78.52;
}
```

#### server 指令和 upstream 指令

用于指定服务器的名称和参数。

设置权重

```
upstream myproject {
	server 182.18.22.2:80 weight=2;
	server 118.144.78.52 weight=1;
}
```

upstream 指令主要是用于设置一组可以在 proxy_pass 和 fastcgi_pass 指令中使用的额外代理服务器，默认负载均衡方式为轮询。


### 其他负载均衡方法

负载均衡实现的方式分为软件实现和硬件实现两种，如果中间的代理机构是硬件，那么就是通过硬件设备来实现负载均衡的方式，如果中间的代理机构为软件，就是软件实现负载均衡的方式。而其中，软件又可以是服务器软件、系统软件以及应用软件等充当。

不同实现方式的优缺点：
假如使用硬件的方式实现负载均衡，那么中间的转发机构就是硬件，这个时候运行的效率非常高，但是对应的成本也非常高。
如果我们采用软件的方式来实现负载均衡，那么中间的转发机构就是软件，这个时候，运行效率不如硬件，但是成本相对来说低得多。而使用 Nginx 服务器实现负载均衡，那么就是通过软件的方式来实现负载均衡，并且 Nginx 本身支持高并发等。故而使用 Nginx 服务器实现负载均衡，能大大节约企业的成本，并且由于 Nginx 是服务器软件，其执行效率也是非常高。
