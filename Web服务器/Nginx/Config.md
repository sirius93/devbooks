[TOC]

# 配置

## 配置文件

主配置文件 conf/nginx.conf 或 Server 配置文件 site-enabled/default.conf

```
#设置使用的用户（nobody, www, root 等），使用 nobody 可以提高安全性
user nobody;

#工作衍生进程数，通常设为 cpu 核心数或其 2 倍（n, auto）
worker_processes  1;

#设置 pid 存放路径（pid 是控制系统中重要文件）
pid /run//nginx.pid;

#设置最大连接数
events {
    worker_connections  1024;
}

http {
    #开启gzip压缩
    gzip  on;

    server {
        listen       80;
        server_name  localhost;

    	  access_log /var/log/nginx/access.log;
        error_log /var/log/nginx/error.log;
    }
    location / {
        root   html;
        index  index.html index.htm;
    }
}
```

## 虚拟主机配置

为了使每个服务器可以供更多用户使用，可以将一个服务器分为很多虚拟的子服务器，每个子服务器都是互相独立的。这些服务器是根据虚拟化技术分出来的，这样，一台服务器就可以虚拟成很多台子服务器。我们把子服务器叫做虚拟主机。

### 步骤

Nginx 中配置虚拟主机的步骤主要有两个，第一步是配置 IP 地址，第二步是绑定 IP 地址与虚拟主机。

1. 配置本机 IP 地址
  ```bash
  ifconfig eth0 <ip> netmask 255.255.255.0
  ```

2. 配置虚拟主机 IP 地址，以下配置了两台虚拟主机
  ```bash
  ifconfig eth0:1 <ip> broadcast <eth0_Bcast> netmask 255.255.255.0
  ifconfig eth0:2 <ip> broadcast <eth0_Bcast> netmask 255.255.255.0
  ```

3. 配置虚拟主机
  ```bash
  vi xunizhuji.conf
  ```
  修改内容
  ```  
  server{
    #工程目录
  	root /usr/share/nginx/serverl;
    #虚拟主机配置
  	listen 192.168.1.2:80;
  	index index.php index.html index.htm;
  	server_name server1;
  	location / {
  	# First attempt to serve request as file, then
             # as directory, then fall back to index.html
            try_files $uri $uri/ /index.php?q=$uri&$args;
  	  root html/server1
  	}
  }
  server{
  	root /usr/share/nginx/server2;
  	listen 192.168.1.3:80;
  	server_name server2;
  	location / {
  	  root html/server2
  	}
  }
  ```

## 日志文件配置

### 配置日志格式

语法: `log_format name <string>;`
默认值: `log_format combined <string>;`
配置段: `http`

其中 `combined` 无需配置，默认为
```
log_format  combined  '$remote_addr - $remote_user  [$time_local]  '
                                   ' "$request"  $status  $body_bytes_sent  '
                                   ' "$http_referer"  "$http_user_agent" ';
```

配置实例

```
http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';
}
```

### 配置日志位置

access_log

语法: `access_log path [format [buffer=size [flush=time]]];`
默认值: `access_log logs/access.log combined;`
配置段: `http, server, location, if in location, limit_except`

error_log

语法: `error_log file | stderr | syslog:server=address[,parameter=value] [debug | info | notice | warn | error | crit | alert | emerg];`
默认值: `error_log logs/error.log error;`
配置段: `main, http, server, location`

以上指定了日志存放的位置以及使用的日志格式。


配置实例

```
http {
    access_log  logs/access.log  main;
    error_log /dev/stdout info;
}
```

### 日志文件切割

#### 手动切割

1. 备份原文件
```bash
mv old.log target.log
```
2. 创建新文件
```bash
ps -ef|grep nginx
kill -USER1 <nginx_pid>
```

### 自动切割

1. 编写脚本，`touch cutlog.sh`

```
D=$(date +%Y%m%d)
mv /usr/local/nginx/logs/access.log ${D}.log
kill -USER1 $(cat /usr/local/nginx/nginx.pid)
```

开启定时任务，`crontab -e`

```
23 59 *** /bin/bash /usr/local/nginx/logs/cutlog.sh
```

### 缓存配置

缓存文件到本地

```
#所有以 `.jpg` 结尾的文件
location ~* \.(jpg|jpeg|gif|png|ico|xml)$ {
  # 5 天
  expires           5d;
}
location ~* \.(css|js)$ {
  # 1 小时
  expires           1h;
}
```

### 压缩功能配置

`gzip` 压缩可以压缩 30%，压缩后的结果保存在内存缓存中

```
http {
 gzip on;

 #不压缩小文件
 gzip_min_length 1k;

 #设置gzip申请内存的大小，其作用是按块大小的倍数申请内存空间，4个16k的数据流
 gzip_buffers 4 16k;

 #最小使用版本
 gzip_http_version 1.1;

 #开启判断客户端是否支持gzip
 gzip_vary on;
}
```

### 自动列目录配置

当目录中不存在 index 之类的默认首页文件使用目录列表

```
location / {
 autoindex on;
}
```
