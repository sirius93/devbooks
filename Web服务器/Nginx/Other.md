user nobody;
worker_processes 4;
events{
worker_connections 1024;
}
http{
	server{
		listen 127.0.0.1:80;
		server_name 127.0.0.1;
		access_log logs/server1.access.log combined;
		location ~ \.(jsp|jspx|do)?$
		{
			root /usr/local/tomcat/tomcat8/webapps;
			index index.jsp;
			proxy_set_header X-Forwarded-Host $host;
	    proxy_set_header X-Forwarded-Server $host;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_pass http://localhost:8080;
	}
}
}
