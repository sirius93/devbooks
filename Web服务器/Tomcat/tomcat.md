[TOC]

Tomcat 热部署》》
Manager 的使用


# Tomcat

## 术语

$CATALINA_HOME 指 Tomcat 安装的根目录

部署分为静态部署和动态部署

静态部署需要在启动前部署，依赖 `appBase` 属性，值可以是未压缩的 exploded web application 的值也可以是压缩过的 war 资源。默认值为 `$CATALINA_BASE/webapps`。只有当主机的 deployOnStartup 属性为 true, 应用才会在 Tomcat 启动时进行自动部署。

动态部署在 Tomcat 运行中进行部署。如果主机的 autoDeploy 属性为 true，主机就会在必要时尝试着动态部署并更新 Web 应用。
动态部署依赖于类似 Tomcat 的 Jetty。

## 使用

### Tomcat Manager

作用

在无需关闭或重启整个容器的情况下，部署新的 Web 应用或者取消对现有应用的部署。

访问 Manager

http://localhost:8080/manager/html

在使用前需要先配置角色权限

$CATALINA_BASE/conf/tomcat-users.xml

- manager-gui 能够访问 HTML 界面。
- manager-status 只能访问“服务器状态”（Server Status）页面。
- manager-script 能够访问文档中描述的适用于工具的纯文本界面，以及“服务器状态”页面。
- manager-jmx 能够访问 JMX 代理界面以及“服务器状态”（Server Status）页面




<user username="dev" password="dev" roles="manager-gui"/>

如果主机配置中将 unpackWARs 设为 true，而且你部署了一个 war 文件，那么这个 war 文件将解压缩至主机的 appBase 目录下的一个目录中。

如果应用的 war 文件或目录安装在主机的 appBase 目录中，那么或者主机应该被部署为 autoDeploy 为 true，或者上下文路径必须匹配目录名或不带 .war 后缀的 war 文件名。



http://tomcat.apache.org/tomcat-8.0-doc/config/


## 虚拟主机

### 含义

一台物理机上搭建多个 WEB 站点，每个站点间独立运行，这些站点就是虚拟主机。

### 实现

#### 分类

共三种

1. 基于主机名的虚拟主机
多个域名解析到同一个 IP 地址，在 WEB 服务器里添加多个站点，每个站点设定一个主机名。HTTP 协议请求里包含了主机名信息，当 WEB 服务器收到访问请求时，就可以根据不同的主机名来访问不同的网站。

2. 基于端口号的虚拟主机
一个 IP 地址，通过不同的端口实现不同网站的访问。

3. 基于 IP 地址的虚拟主机
服务器绑定多个 IP，然后配置 WEB 服务器，把多个网站绑定在不同的 IP 上。

### 基于主机名

Host 即主机名

例：

配置两个主机 www.jike1.com www.jike2.com

Connector 负责接收请求

```xml
<Connector port=“80" protocol="HTTP/1.1"
               connectionTimeout="20000"  redirectPort="8443" />

<Host name="www.jike1.com"  appBase="F:/vhost1" unpackWARs="true" autoDeploy="true"></Host>

<Host name="www.jike2.com"  appBase="F:/vhost2" unpackWARs="true" autoDeploy="true"></Host>
```

### 基于端口号

例：

配置两个端口号

www.jike1.com:8080    
F:/vhost1

www.jike1.com:80         
F:/vhost2

```xml
<Service name="Catalina">
    <Connector port="8080"  ……/>
    <Engine name="Catalina" ……>
      <Host name="www.jike1.com"  
		appBase="F:/vhost1" ……>
            ……
      </Host>
    </Engine>
</Service>

<Service name="Catalina2">
    <Connector port="80"  ……/>
    <Engine name="Catalina" ……>
      <Host name="www.jike1.com"  
		appBase="F:/vhost2" ……>
            ……
      </Host>
    </Engine>
</Service>
```

## 集群

### Session复制配置

放入 `<Engine>` 或 `<Host>` 元素中

```xml
<Cluster className="org.apache.catalina.ha.tcp.SimpleTcpCluster"/>
```

### 自定义配置

```xml
<Cluster className="org.apache.catalina.ha.tcp.SimpleTcpCluster"
                channelSendOptions="8">

   <Manager className="org.apache.catalina.ha.session.DeltaManager"
                  expireSessionsOnShutdown="false"
                  notifyListenersOnReplication="true"/>
```

其中 Manager 指定了复制方式，DeltaManager 为全局复制，会将 Session 复制到所有 Tomcat 节点，即使这个节点没有部署该应用，适合小型集群。BackupManager 需要部署备份节点，只复制到部署了该应用的节点。

Cluster 指定了会话复制如何进行持久化
- 保存到文件系统中
- 保存到数据库中
- 内存复制，SimpleTcpCluster
