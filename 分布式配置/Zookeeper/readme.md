[TOC]

# ZooKeeper

## 基本概念

### 概述

ZooKeeper 是分布式协调服务，是高性能的分布式数据一致性解决方案。
ZooKeeper 基于 Java 开放，所以运行时需要有 Java 环境。
ZooKeeper 集群只要有一半节点能够正常工作集群就能正常提供服务，即3台中只要有两台。

ZooInspector

### 特点

- 顺序一致
- 原子性
- 单一视图
- 可靠性
- 实时性

### 应用场景

1. **发布/订阅**  
  数据订阅两种方式：拉模式（轮询），推模式
  ZooKeeper 使用两种方式结合。
2. **负载均衡**
3. **命名服务**  
  比如说顺序增长的 ID（普通自增长ID无法在分布式环境中使用，只能用在单库单表）
4. **分布式协调/通知**  
  如心跳检测，机器在 zk 中注册临时节点，然后判断节点是否可用还不是直接去 ping 每台机器


### 各种概念

*集群角色*

- Leader
- Follower
- Observer

*会话*

客户端与服务器端的TCP长连接

*Watcher*

客户端可以在一个节点上注册多个 Watcher，当节点发生变化，Watcher 会发送给对该变化感兴趣的客户端。

### 数据模型

ZooKeeper 的数据模型是一棵树。
数据模型中的数据单元 ZNode，分为持久节点和临时节点。其中临时节点在连接不可达时会被自动删除。


### ACL 权限控制

五种 ACL

- CREATE
- READ
- WRITE
- DELETE
- ADMIN 设置 ACL 权限


## 使用

### zkCli 的使用

#### 连接集群

```bash
./zkCli.sh -timeout 5000 -server $ZK1_PORT_2181_TCP_ADDR:$ZK1_PORT_2181_TCP_PORT
```
其参数定义如下：  
- -timeout 单位毫秒
- -r 只读，当集群过半无法连接时 ZK 服务会不可用，而指定此模式可以在此时继续以只读模式保存连接

#### 基本命令

- `h` 输出所有命令
- `ls <path>` 输出目录结构
- `stat <path>` 输出状态信息，参数信息如下：
  - `cZxid` 表示创建时的事务 id
  - `mZxid` 表示最后一次更新时的事务 id
  - `pZxid` 子节点列表最有一个次更新时的事务 id
  - `dataversion` 当前数据节点数据内容的版本
  - `cversion` 当前数据节点最近更新的子节点的版本
  - `aclversion` 当前数据节点 ACL 变更版本
- `get <path>` 输出数据内容及状态信息
- `ls2 <path>` 等于 ls + stat
- `create [-s] [-e] <path> <data> [<acl>]` 创建节点，参数信息如下：
  - `-s` 顺序节点，顺序节点生成的 `path` 为指定的 `path` 加上 `000000001` 这样的后缀。
  - `-e` 临时节点，断开连接后临时节点会被删除
- `set <path> <data> [version]` 修改节点，每次修改版本号都会自动增加，如果指定版本号则必须与期望值一致，不能变大或变小
- `delete <path> [version]` 删除没有子节点的节点
- `rmr <path>` 递归删除节点

#### 配额命令

配额命令用于对节点进行限制。但是在 Zookeeper 中，超过配额后不会抛出异常，而是会向 `zookeeper.out` 日志中输出警告信息。

配额指令

- `setquota [-n|-b] <val> <path>` 设置配额，参数含义如下：
  - `-n` 限制子节点个数
  - `-b` 限制数据长度
  - `val` val 根据 `-n` 和 `-b` 有不同含义
- `listquota <path>` 查看配额，其中 `-1` 表示无限制。
- `delquota [-n|-b] <path>` 删除配额

#### 历史指令与重复执行

- `history` 查看历史指令
- `redo <id>` 重复执行指定指令，指令 id 可以通过 `history` 命令查看。

#### 权限指令

授权指令用于分配资源给特定的客户端。

权限指令有以下几种概念

- 权限模式(Schema)，分为两种:IP 和 Digest，第一种用于限制特定 IP，第二种用于限制特定用户
- 授权对象(ID)，按照 Schema 的不同分为两种，IP 地址和 `username:Base64(SHA-1(username:password))` 格式的字符串。
- 权限(Permission)，5 种权限可以按照开头字母进行缩写（如：c -> CREATE, r -> READ）

详细指令

- `create <path> <schema>:<id>:<permission>` 创建权限
- `setAcl <path> <schema>:<id>:<permission>` 修改权限
- `addauth <schema> <id>` 授权

例

```
create /node_1 ip:192.168.1.106:crwda
setAcl /node_1 ip:192.168.1.106:crwda
addauth digest peter:123456
```

### Java API

#### 建立连接

ZooKeeper 的连接是异步的，这意味着如果方法 `main()` 中很可能还未连接成功方法就走完了。

```java
String ip = "192.168.1.67";
int port = 32775;
int timeout = 5000;
ZooKeeper zooKeeper = new ZooKeeper(
        ip + ":" + port,
        timeout,
        new Watcher() {
            @Override
            public void process(WatchedEvent watchedEvent) {
                //断线重连时 SyncConnected 可能发生多次
                if (watchedEvent.getState() == Event.KeeperState.SyncConnected) {
                  if (!init && watchedEvent.getType() == Event.EventType.None) {
                    //初始化函数
                    init = true;
                  } else {
                    //do something
                  }
                }
            }
        });
System.out.println(zooKeeper.getState());
Thread.sleep(Integer.MAX_VALUE);
```

#### 创建节点

同步方式

```java
String path = zooKeeper.create("/node_4",
                    "foobar".getBytes(),
                    ZooDefs.Ids.OPEN_ACL_UNSAFE,
                    CreateMode.PERSISTENT);
```

异步方式

```java
zooKeeper.create("/node_5",
                "foobar".getBytes(),
                ZooDefs.Ids.OPEN_ACL_UNSAFE,
                CreateMode.PERSISTENT_SEQUENTIAL, new AsyncCallback.StringCallback() {
                    @Override
                    public void processResult(int retCode, String path, Object context, String name) {
                        // 返回 0 表示执行成功
                        System.out.println(retCode);    //  0
                        System.out.println(path);
                        System.out.println(context);
                        System.out.println(name);
                    }
                }, "async context");
```

参数依次为节点路径，节点数据，ACL，创建模式，回调，回调上下文。

创建模式即节点类型，有以下几个参数：

- `PERSISTENT` 持久节点
- `PERSISTENT_SEQUENTIAL` 持久及顺序节点
- `EPHEMERAL` 临时节点
- `EPHEMERAL_SEQUENTIAL` 临时及顺序节点

>需要注意当创建的是顺序节点时异步返回的 path 和 name 是不一样的。

#### 获取子节点

同步方式

```java
List<String> list = zooKeeper.getChildren("/", true);
```

以上第二个参数表示 Watcher 是否继续注册，ZooKeeper 事件注册后就会被消费，需要继续注册。

响应子节点改变时的事件

以下事件注册在 Watcher 中，`watchedEvent.getPath()` 返回变化的节点路径。

```java
if (watchedEvent.getType() == Event.EventType.NodeChildrenChanged) {
    zooKeeper.getChildren(watchedEvent.getPath(), true);
}
```

#### ACL 处理

分配权限

除了自定义 ACL 之外，也可以使用内置的 `ZooDefs.Ids.ANYONE_ID_UNSAFE` 等权限。

```java
ACL aclIp = new ACL(ZooDefs.Perms.READ, new Id("ip", "192.168.1.8"));
ACL aclDigest = new ACL(ZooDefs.Perms.READ | ZooDefs.Perms.WRITE,
        new Id("digest", DigestAuthenticationProvider.generateDigest("peter:123456")));
List<ACL> acls = Arrays.asList(aclIp, aclDigest);

String path = zooKeeper.create("/node_10",
        "foobar".getBytes(),
        acls,
        CreateMode.PERSISTENT);
```

授权

```java
zooKeeper.addAuthInfo("digest","peter:123456".getBytes());
```


###  ZooKeeper实战

#### Master 选举

基本概念

- 服务注册：各服务器分别向 ZK 注册节点
- 服务发现：其它连接可以通过 ZK 上的节点来了解有哪些服务器可用

架构

zk master节点
zk servers节点

步骤
1）各服务器启动时向 zk servers 节点注册代表自身的子节点
2）各服务器尝试在 zk 上创建 master 节点，创建成功的节点作为 master 节点
3）所有服务器都关注 master 节点的删除状态，获得删除事件后重复 2）的步骤

网络抖动优化

master 到 zk 发生网络抖动，zk 会误认为 master 宕机并删除。此时，所有机器会重新选举，如果重新选举后不是上一次的节点的话会发生资源迁移。
优化方式就是以上一次的节点优先，即其它节点申请成为 master 时加一段延时。

#### 发布和订阅

适用于当一个对象的改变需要同时改变其它对象，且不知道要改变多少对象时。
在分布式服务中主要用于配置管理和服务发现
配置管理：各机器配置做同一管理

步骤
1）各服务器启动时向 zk servers 节点注册代表自身的子节点
2）control server 向 zk command 节点写入命令信息
3）manage server 订阅 command 节点信息和 servers 节点信息
4）manage server 获得 command 后发送到 servers 中随机的一个 server 子节点对应的 server 服务器


#### 负载均衡

架构

客户端
1. 连接到 zk 服务器
2. zk 计算后获得最佳地址，返回给客户端

服务端
1. 向 zk 注册自己
2. 监听客户端连接
3. 连接到客户端后向 zk 自己的节点中增加负载，连接断开时则减少负载

#### 分布式锁

一般锁指单进程多线程锁。
分布式则是跨网络，跨主机

架构

1. 到 zk locker 节点下创建临时顺序节点 node_n，获取 Lock 下所有子节点
2. 对子节点排序，判定 node_n 是否是排序结果的第一个，是则获得锁。否则监听比 node_n 小的 node_n-1 的删除事件，删除后继续步骤 1）。


#### 分布式队列

通过顺序节点实现

架构

1. producer 在 zk 的 queue 节点下的子节点生成数据
2. consumer 在消费 queue 节点下的子节点，获得所有子节点排序，消费最小节点并删除


#### 命名服务

提供类似 jndi 的功能，把各种服务的地址，配置存放在 zk 中
利用顺序节点制作分布式的 id 生成器，数据库主键无法用在分布式环境中，而 uuid没 有规律，难以理解
作为命名服务时需要注意创建的节点需要定时删除，否则 zk 容量会倍增

### ZooKeeper 运维

#### 运维配置

##### 基础配置

- `clientPort` 对外服务端口，默认 2181
- `dataDir` 快照目录
- `tickTime` 最小 tick 时间，很多时间都是 tickTime 的倍数

##### 高级配置

- `dataLogDir` 事务日志目录，默认和快照存储在一起。zk 在事务请求返回时需要保证完成日志的写入才会返回，所以事务日志对磁盘性能要求很高。磁盘性能直接决定了 zk 事务的吞吐量，所以应尽量使用单独的磁盘或挂载点。
- `globalOutstandingLimit` 限制同时处理的请求数，默认 1000
- `preAllocSize` 事务日志文件预分配的磁盘大小，默认 64mb
- `snapCount` 一份快照文件的事务数量，默认为 100,000


#### 四字命令

##### 基本使用

两种方式

1. 使用 telnet 登录到服务器 `telnet <host> <port>` 后执行
2. 通过 nc 命令 `echo <cmd> | nc <host> <port>`

##### 四字命令

- `stat` 输出状态
- `conf` 输出基本参数
- `cons` 输出连接到当前服务器的信息
- `crst` 重置所有客户端
- `dump` 输出当前所有会话信息
- `envi` 环境信息
- `ruok` 是否正在运行 (are you ok)
- `srvr` 类似 stat，但不会输出客户端信息
- `srst` 重置所有服务器统计信息
- `wchs` 输出 watcher 概要信息
- `wchc` 输出 watcher 详细信息
- `wchp` 类似 wchc,以节点路径为单位分组
- `mntr` 类似 stat,但更详细

#### 在运维中使用 JMX

步骤

1. 修改 zk 启动脚本

  ```
  -Dcom.sun.management.jmxremote.local.only=false
  -Djava.rmi.server.hostname=192.168.1.105
  -Dcom.sun.management.jmxremote.port=8899
  -Dcom.sun.management.jmxremote.ssl=false
  -Dcom.sun.management.jmxremote.authenticate=false
  ```
2. 重启服务器
3. 使用 jconsole
  远程进程填入 zk 地址，端口号为脚本中配置的端口
4. 进入 MBean 标签页进行各种操作

#### 监控平台搭建和使用

主要使用 exhibitor 和 zabbix 结合使用

##### exhibitor 安装和使用

1. Github 上下载 [源代码](https://github.com/Netflix/exhibitor)
2. 解压后进入 `cd exhibitor-standalone` 目录
3. 执行 `cd src/main/resources/buildscripts/standalone/maven`
4. 编译代码，执行 `mvn clean package`
5. 上传生成的 jar 到 zk 服务器
6. 执行 `java -jar target/exhibitor.jar -c file`
7. 访问 `8080/exhibitor/v1/ui/index.html`

nohup java -jar exhibitor.jar -c file &

Config页
打开Editing
Servers  S:1:host,S:2:host,S:3:host

## 注意事项

### 解决 ZooKeeper 的硬盘占用率问题

手段

- 将事务日志和快照日志分开存放
- 自动定时清理日志

具体步骤

使用自动清理日志功能，在 `zoo.cfg` 中配置以下参数

- `autopurge.snapRetainCount` 需要保留的日志文件数，默认为 3
- `autopurge.purgeInterval` 清理频率，单位为小时，默认为 0 表示不进行清理


## 参考资料

- [阿里中间件团队](http://jm-blog.aliapp.com/?tag=zookeeper)
