从设计模式角度来看，是一个基于观察者模式设计的分布式服务管理框架

使用场景
主从集群
主节点在 ZK 上创建一个当连接断开时自动删除的节点（由 ZK 心跳维护），从节点都也连接到节点上并watch 创建的节点
一旦主节点断开，从节点收到事件后重新在 ZK 上创建节点

作用
分布式任务调度模块
统一命名服务
配置管理：将配置信息保存在 Zookeeper 的某个目录节点中，然后将所有需要修改的应用机器监控配置信息的状态，一旦配置信息发生变化，每台应用机器就会收到 Zookeeper 的通知，然后从 Zookeeper 获取新的配置信息应用到系统中
集群管理
队列管理
屏障（Barriers）：分布式系统使用屏障（barriers）来阻塞某一节点集的任务，直到满足特定的条件该节点集的所有节点才能正常向前推进，就像屏障一样，在当条件不满足时，屏障阻塞了任务的执行，只有当条件满足后屏障才会被拆除，各节点才能推进自己正在执行的任务。
队列
分布式锁

架构
实例一
杭州机房  >=3台 (构建leader/follower的zk集群)
青岛机房  >=1台 (构建observer的zk集群)
美国机房  >=1台 (构建observer的zk集群)
香港机房  >=1台 (构建observer的zk集群)
单个机房组成投票集群，外围机房组成 observer 集群

ZK 扩展一 优先集群
优先集群的问题，即美国机房需要优先访问美国机房的 ZK，而不是杭州机房的
默认在zookeeper3.3.3的实现中，认为所有的节点都是对等的。并没有对应的优先集群的概念，单个机器也没有对应的优先级的概念。
扩展二 异步 Watcher 处理
扩展三 重试处理
