目录

[TOC]

摘要

>Junit，ScalaTest，Spec2

## Test

### 基于 Junit

```scala
class JunitTest extends TestCase{

  def test01(): Unit ={
    println("abc")
  }
}
```

### 基于 ScalaTest

```scala
object StringUtils {

  def splitCamelCase(s: String): String = {
    return s.replaceAll(
      String.format("%s|%s|%s",
        "(?<=[A-Z])(?=[A-Z][a-z])",
        "(?<=[^A-Z])(?=[A-Z])",
        "(?<=[A-Za-z])(?=[^A-Za-z])"
      ),
      " "
    ).replaceAll("  ", " ")
  }

}

@RunWith(classOf[JUnitRunner])
class StringUtilsTest extends FunSuite with BeforeAndAfter {

  test("splitCamelCase works on FooBarBaz") {
    val s = StringUtils.splitCamelCase("FooBarBaz")
    assert(s.equals("Foo Bar Baz"))
  }

  test("splitCamelCase works on a blank string") {
    val s = StringUtils.splitCamelCase("")
    assert(s.equals(""))
  }

  test("splitCamelCase fails with a null") {
    val e = intercept[NullPointerException] {
      val s = StringUtils.splitCamelCase(null)
    }
    assert(e != null)
  }

  before {
    println("before")
  }

}
```

### 基于 Spec2

```scala
class FactorialSpec extends Specification {
  args.report(color = false)

  def factorial(n: Int) = (1 to n).reduce(_ * _)

  "The 'Hello world' string" should {
    "factorial 3 must be 6" in {
      factorial(3) mustEqual 6
    }
    "factorial 4 must greaterThan 6" in {
      factorial(4) must greaterThan(6)
    }
  }
}
specs2.run(new FactorialSpec)
```

这个例子是测试一个阶乘函数。使用 `should/in` 来建立测试用例。测试是默认并发执行的。

