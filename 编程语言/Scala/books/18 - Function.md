目录

[TOC]

摘要

>function

## Function

函数是一些特质的集合，比如说具有一个参数的函数是 Function1 特质的一个实例

```scala
object addOne extends Function1[Int, Int] {
  override def apply(v1: Int): Int = v1 + 1
}
addOne(2)
```

这种 Function 特质的下标从 0 到 22。

也可以简写为

```scala
object addTwo extends ((Int) => Int) {
  override def apply(v1: Int): Int = v1 + 1
}
```

比如说如下代码就定义了一个名为 plus1 的变量

```scala
val plus1: (Int => Int) = (x: Int) => x + 1
println(plus1(3)) //4
```

该代码在 Scala 编译时会被展开为如下代码

```scala
val plus1: Function1[Int, Int] = new Function1[Int, Int] {
  override def apply(v1: Int): Int = v1 + 1
}
```

这其实在底层是定义了一个匿名类，并且实现了该匿名类的 `apply()` 方法。

