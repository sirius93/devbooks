目录

[TOC]

摘要

>Scala -> Java，Java -> Scala

## 与 Java 互相调用

### Scala 调用 Java

Java

```java
public class JavaBean {

    private String name;

    public JavaBean(String name) {
        this.setName(name);
    }

    public String say(){
        return "hello java";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
```

Scala

```scala
val bean = new JavaBean("JavaBean")
println(bean.getName)
println(bean.say())
```

### Java 调用 Scala

Scala

```scala
class ScalaBean(@BeanProperty var name: String) {
  def say = "hello scala"
}
```

以上使用了 `@BeanProperty` 注解来生成 Java Style 的 Bean。

Java

```java
ScalaBean scalaBean = new ScalaBean("bean");
System.out.println(scalaBean.name());   //bean
System.out.println(scalaBean.getName());    //bean
System.out.println(scalaBean.say());    //hello scala
```



