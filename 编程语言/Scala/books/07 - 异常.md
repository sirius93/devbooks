目录

[TOC]

摘要

>try...catch

## 异常

- Scala 没有检查异常
- `throw` 表达式的类型是 `Nothing`

```scala
try {
  throw new IOException()
} catch {
  //not care exception
  case _: IOException => println("io exception")
  //care exception
  case e: Exception => e.printStackTrace();
} finally {
}
```

以上，`_` 用于不需要使用产生的异常对象时使用。

