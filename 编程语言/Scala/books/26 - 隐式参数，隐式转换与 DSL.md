目录

[TOC]

摘要

>隐式参数，隐式转换，DSL

## 隐式参数，隐式转换与 DSL

### 隐式参数

#### 概述

所谓的隐式参数即是调用方法时，可以省略特定的参数，由 Scala 根据该方法的隐式参数的类型来寻找对应的值进行填充

#### 使用

语法

`implicit 参数`

例

```scala
object Context {
  def print(str: String)(implicit prefix: String) = {
    println(prefix + " " + str)
  }
}

import Context._
implicit val x: String = "Hello"
print(" World") //  Hello World
print(" Bye")("Good") //  Good Bye
```

以上的例子中，`print()` 方法需要接收两个字符串作为参数，如果只传入第一个参数时，Scala 会根据上下文寻找对应的 String 类型参数（上面例子的 x）进行填充。

更复杂的例子

```scala
abstract class SemiGroup2[A] {
  def add(x: A, y: A): A
}

abstract class Monoid2[A] extends SemiGroup2[A] {
  def unit: A
}

def sum[A](xs: List[A])(implicit m: Monoid2[A]): A = if (xs.isEmpty) m.unit
else m.add(xs.head, sum(xs.tail))

implicit val stringMonoid = new Monoid2[String] {
  def add(x: String, y: String): String = x.concat(y)
  def unit: String = ""
}

implicit val intMonoid = new Monoid2[Int] {
  def add(x: Int, y: Int): Int = x + y
  def unit: Int = 0
}

// 如果没有使用隐式参数
sum(List("a", "bc", "def"))(stringMonoid)
sum(List(1, 2, 3))(intMonoid)

// 使用了隐式参数
sum(List("a", "bc", "def"))
```

#### 注意

- 由于隐式参数是根据参数类型来确定值的，所以如果有两个相同类型的隐式参数时 Scala 会报 "ambiguous implicit values" 错误。
- 使用隐式参数时，需要特别注意参数的使用范围
- 使用隐式参数时，需要特别注意使用 String 等常用类型作为隐式参数可能引发的问题

### 隐式转换

#### 概述

定义一个转换函数，可以在使用相应类型的时候自动转换。使用时需要特别注意隐式转换的 Scope，不受控制的 Scope 往往会引发不可预知的错误。

#### 使用

语法

`implicit 方法定义`

例

```scala
class SuperInt(val a: Int) {
  def triple = a * a * a
}
object Context {
  implicit def int2SuperInt(a: Int): SuperInt = new SuperInt(a)
}

import Context._
println(3.triple) //  27
```

这个例子可以将 `Int` 自动转换为 `SuperInt` 类型。隐式转换是实现 DSL 的重要工具。

#### 隐式转换原则

- 二义性的隐式转换不被允许
- 不会同时使用多种隐式转换

### 隐式类

#### 概述

- 必须定义在另外一个类中
- 构造器只能有一个不是 implicit 的参数
- 作用域中不能有与隐式类相同名的成员变量，方法等

#### 使用

语法

`implicit 类定义`

例

```scala
object Context {
  implicit class A(a: Int) {
    def add2 = a + 2
  }
}

import Context._
println(1.add2) //  3
```

### 类型约束

#### <:<

判断该类是否是指定类型的子类型

```scala
class T {}
class T2 extends T {}

def f1[A](a: A)(implicit ev: A <:< T): Unit = {
  println("f1", a.getClass)
}

val t = new T
val t2 = new T2
f1(t)
f1(t2)
```
#### =:=

判断该类是否是指定类型

```scala
class T {}
class T2 extends T {}

def f1[A](a: A)(implicit ev: A <:< T): Unit = {
  println("f1", a.getClass)
}

val t = new T
val t2 = new T2
f2(t)
//	f2(t2)	error
```

### 视图界定

视图界定的含义是对于类型 A 来说，必须存在一个隐式转换可以使类型 A 变为类型 B

```scala
class B {}
class B2 {}

//符号 "<%" 表示视图界定
def f1[A <% B](a: B) = {}

implicit def b2ToB(t: B2): B = new B

val b = new B2
f1(b)
```

现在视图界定的写法已经被废弃，应使用隐式转换的写法来代替

```scala
def f1[A](a: B)(implicit ev1: A => B) = {}
```

### 上下文界定

上下文界定的含义是对于 [T: Bound] 来说，必须存在一个隐式转换可以使类型 [T: Bound] 变为  Bound[T]

```scala
class C {}

//上下文界定
def f[A: List](a: A) = {}

implicit val list = List[C]()
val c = new C
f(c)
```

上下文界定本质是下述写法的语法糖

```scala
def f[A](a: A)(implicit ev: List[A]) = {}
```

### DSL

#### 概述

DSL 是 Scala 最强大武器，可以使一些描述性代码变得极为简单。

#### 使用

```scala
import java.util.Date
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._

case class Twitter(id: Long, text: String, publishedAt: Option[java.util.Date])

var twitters = Twitter(1, "hello scala", Some(new Date())) ::
  Twitter(2, "I like scala tour", None) :: Nil

var json = ("twitters"
  -> twitters.map(
  t => ("id" -> t.id)
    ~ ("text" -> t.text)
    ~ ("published_at" -> t.publishedAt.toString())))

println(pretty(render(json)))
```

这个例子是使用 DSL 生成 JSON。Scala 很多看似是语言级的特性也是用 DSL 做到的。

