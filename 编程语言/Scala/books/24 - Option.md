目录

[TOC]

摘要

>返回 Option，模式匹配，getOrElse，List

## Option

### 概述

Scala 提供了 `Option` 机制来解决代码中需要不断检查 `null` 的问题。

### 使用

####  返回 Option

```scala
def getProperty(name: String): Option[String] = {
  val value = System.getProperty(name)
  if (value != null) Some(value) else None
}

val osName = getProperty("os.name")
```

这个例子包装了 `getProperty()` 方法，使其返回一个 `Option`。 这样就可以不再漫无目的地进行 `null` 检查。只要使用 `Option` 类型的值即可。

####  使用 Option

可以使用 pattern match 来获取 Option的值,也可以使用 `getOrElse()` 来提供当为 `None` 时的默认值。

```scala
osName match {
  case Some(value) => println(value)
  case _ => println("none")
}

println(osName.getOrElse("none"))
```

#### List

`Option` 还可以看作是最大长度为 1 的 `List`，`List` 的强大功能都可以使用。

```scala
osName.foreach(print _)
```





