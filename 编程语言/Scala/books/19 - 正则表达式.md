目录

[TOC]

摘要

>构造正则对象，返回所有匹配结果，替换

## 正则表达式

### 构造正则对象

使用 `String` 的 `r` 方法

```scala
val numPattern = "[0-9]+".r
```

为避免转义符的干扰，可以使用`"""`表示原样输出

### 匹配

#### 返回所有匹配结果

```scala
for (matchString <- numPattern.findAllIn("99 bottles, 98 bottles")) {
  println(matchString)
}
```

####  返回第一条匹配结果

```scala
val first = numPattern.findFirstIn("99 bottles, 98 bottles")
```

#### 匹配开始

```scala
val isMatched = numPattern.findPrefixOf("99 bottles, 98 bottles")
```

#### 替换

```scala
val result = numPattern.replaceAllIn("99 bottles, 98 bottles", "xxx")
println(result) //xxx bottles, xxx bottles
```