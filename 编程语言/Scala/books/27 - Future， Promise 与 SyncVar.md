目录

[TOC]

摘要

>future，promise，syncVar

## Future， Promise 与 SyncVar

### Future

#### 概述

Future 是一种持有某种值的对象，这个值通常都是某些其它操作的运算结果。

#### 使用

```scala
val s = "Hello"
//建立一个 Future，该 Future 保存一个 String
val f1: Future[String] = Future {
  s + " future!"
}
f1 onSuccess {
  case msg => println(msg, Thread.currentThread().getName)
}
f1 onComplete {
  case msg => println("onComplete")
}
println("---1---")
//outputs:
// onComplete
// ---1---
// (Hello future!,ForkJoinPool-1-worker-5)
```

### Promise

#### 概述

- Promise 对象可以持有一个表示完成的值或者表示失败的异常。


#### 使用

```scala
val f = Future {
  1
}
val p = Promise[Int]()
p completeWith f
//A promise p completes the future returned by p.future
p.future onSuccess {
  case x => println(x, Thread.currentThread().getName)
}
println("---3---")
//outputs:
//  ---3---
//  (1,ForkJoinPool-1-worker-5)
```

另一个例子

```scala
def heavyFuture = {
  val p = Promise[Int]()
  Future {
    Thread.sleep(1000)
    val result = 10
    //调用 future.onSuccess()方法
    p.success(result)
  }
  p.future
}

val f = heavyFuture
f onSuccess { case x => println(x) } //10

Thread.sleep(3000)
```

### SyncVar

#### 概述

SyncVar 的所有方法都是同步的，可以用于安全地访问各种可变对象。

#### 使用

```scala
val v = new SyncVar[Int]
v.put(1)
//get 为阻塞操作
var result = v.get
println("result", result, Thread.currentThread().getName) //(result,1,main)

result = v.take()
println("take", result, Thread.currentThread().getName) //(take,1,main)

//由于 "1" 已被取出，所以以下代码会一直阻塞下去
//result = v.get
//println("get", result, Thread.currentThread().getName)
println("---2---")
```
