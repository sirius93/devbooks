[TOC]

### pickle

pickle 是 Python 中的特殊格式，用于序列化对象。

```python
f = open('files/students.pkl', 'wb')
item = [1, 2, 3, 4, 5, 6, 'hello', 'world', ['foo', 'bar']]
pickle.dump(item, f)
f.close()

f = open('files/students.pkl', 'rb')
item2 = pickle.load(f)
print(item2)
f.close()
```

以上参数 `wb` 意思是写入二进制文件。
