[TOC]

### List

Python 没有数组，只有列表，但是其列表本身实际就是可变数组。

空列表

```python
empty = []
emptyList = list()
```

创建时指定元素

```python
nums = [1, 2, 3]
```

修改列表

```python
# empty[0] = 10     index out of range
empty.append(10)
nums.append(10)
nums.insert(0, 100)
nums.extend([-1, -2]) # 同时增加多个元素
```

读取数组元素

```python
print('nums[0] is', nums[0])
print('nums[1] is', nums[-1])
print('nums[1:3] is', nums[1:3])
```

删除元素

```python
nums.remove(10)  # not exist will throw an exception
nums.pop()
```

判断元素是否存在

```python
print(123 in num3)
print(123 not in num3)
```

所有迭代器对象都可以转换为列表

```python
nl = list((1, 2, 3))
sl = list('hello')
tl = list([1, 2, 3])
print(nl)
print(sl)
print(tl)
```

### Slice 切片

Slice 是对于列表的拷贝，使用 `[m:n]` 定义。

```python
s = nums[1:3]

s.append(10)
print(s)  # [1, 2, 10]
print(nums)  # [-1, 1, 2, 3, 4]
print(nums[:3])
print(nums[:])  # a copy of original list
```

### Turple

Turple 元素不可以改变

```python
t1 = (1, 2, 3, 4)
print(t1)
print(t1[0])
print(t1[2:])

one = (1,)
two = 1,
print(type(one))
print(type(two))
```

### Set

```python
set1 = {1, 2, 3, 4, 4}
set2 = set([1, 2, 3, 1, 2, 3])
print(set1)

set2.add(10)
print(set2)
print(1 in set1)
```

不可变 Set

```python
num3 = frozenset([1, 2, 3])
```
