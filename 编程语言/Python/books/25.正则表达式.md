[TOC]

### 正则表达式

#### 创建正则表达式

```python
regex = r'(\d{4})-(\d{2})'
```

#### Matching

```python
print(re.match(regex, inputs) is not None)
```

#### Searching

```python
result = re.search(regex, '2015-10')
print(result.string)  # 2015-10
```
