[TOC]

### I/O

#### 目录

创建目录

以下不支持多级目录，多级目录使用 `makedirs` 进行替换。

```python
if not os.path.exists('files'):
    os.mkdir('files')
```

改变当前工作目录

```python
os.chdir('files')
```

遍历目录

```python
entries = os.listdir('files')
for entry in entries:
    print(entry)
```

#### 文件

打开文件

```python
file = open('test.txt', 'w', encoding='UTF-8')
```

文件模式

- w 只写
- x 写并创建文件
- r 只读（默认）
- a 追加
- b 二进制文件
- + 可读可写

读取文件

读取全部

```python
file = open('test.txt', 'r')
print('read:', file.read())
```

读取到列表中

```python
lines = list(file)
print(lines)
```

按行读取

```python
for line in file:
    print(line)
```

写入文件

以下方法都需要自行追加换行

```python
file = open('test.txt', 'w', encoding='UTF-8')
file.write('foobar')
file.write('world\n')
file.write('bye')
file.close()
```

判断文件是否存在

```python
os.path.isfile('files/test.txt')
```

try...resources

```python
try:
    with open('test.txt', 'r') as f:
        for line in f:
            print(line)
except OSError:
    print('OSError occurs')
else:
    print('end read')
```
