[TOC]

### Dict

字典即是其它语言中的哈希表。

创建 Dict

```python
peter = {'name': 'Peter', 'age': 18, 'height': 180}
jack = dict((('name', 'Peter'), ('age', 18), ('height', 180)))
jane = dict(name='Peter', age=18, height=180)
```

空字典

```python
empty = {}
```

访问元素

访问不存在的元素会报错

```python
print(peter.get('not_exist'), -1)
print(peter.get('name'))
```

修改元素

`setdefault()` 只有在元素不存在时才会执行修改操作。

```python
peter['sex'] = 'male'
peter.setdefault('no', 10)
```

删除元素

由于字典没有顺序，所以 `popitem()` 相当于删除任意元素。

```python
peter.pop('age')
peter.popitem()
```

获得长度

```python
print(len(peter))
```

遍历 Dict

```python
keys = peter.keys()
values = peter.values()
entries = peter.items()
for (k, v) in entries:
    print(k, '=', v)
print()
```


判断键值对的存在

```python
print('name' in peter)
```

Dict 默认值

```python
dict = peter.fromkeys(('foo', 'bar'), 'default')
```

浅复制

```python
dict2 = dict.copy()
```
