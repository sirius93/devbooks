[TOC]

### 安装

Mac

```bash
brew install python3
```

Windows

直接访问[官网](https://www.python.org/downloads/)进行下载

验证安装

```bash
python3 -v
```

### 第一个程序

hello.py

```python
print('Hello World!')
```

```bash
python3 hello.py 
```
