[TOC]

### 变量与常量

#### 变量

Python 中使用下划线或小写字母开头的标示符表示变量。

```python
foo = 'foo'
```

#### 常量

Python 从语法层面来讲没有常量类型，通常只是使用全大写字母表示常量这一概念，但是本身没有任何方法保证其不会被更改。

```python
FOO = 'foobar'
```

#### 并行赋值

Python 支持并行赋值，通常并行赋值主要用于交换变量的值。

```python
one = 1
two = 2
one, two = 2, 1
print(one, two)
```

#### 删除定义

Python 中可以通过 `del` 删除一个变量的定义，此时再使用会报 undefined 异常。

```python
del foo
```

#### 随机数

```python
import random

# 左右都闭合
print('random', random.randint(1, 10))
```
