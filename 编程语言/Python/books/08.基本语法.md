[TOC]

### 注释

#### 单行注释

使用符号 `#`

```python
# Single Comment
```

### 操作符

数学计算不支持 `++` 操作

```python
print(3 + 2 < 5 - 7)  # False
x = 10
x += 1
print(x)  # 11

print(2 ** 3)  # 8
print(9 / 3)  # 3.0
print(9 // 3)  # 3
```

逻辑操作

```python
print(True and 1 == 2)  # False
print(True or 1 == 2)  # True
print(not (True))  # False
print(not (1 == 1))  # False
print(3 < 4 < 5)
```
