[TOC]

### 日期与时间

当前时间

```python
import datetime
import time as timer

print(timer.ctime())
print(datetime.datetime.now())
```

获得秒为单位的时间戳

```python
print(timer.time())
```

获得时间对应的字段

```python
t = datetime.datetime.now()
print(t.year)
print(t.month)
print(t.day)
```

时间格式化

```python
print(timer.strftime('%Y-%m-%d %H:%M:%S'))
```

字符串转为时间

字符串需符合此格式

```python
print(timer.strptime('2014-04-23', '%Y-%m-%d'))
```

时间计算

```python
print(t - datetime.timedelta(seconds=-10))
```
