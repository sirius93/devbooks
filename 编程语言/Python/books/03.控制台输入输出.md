[TOC]

[TOC]

### 控制台输入输出

#### 输入输出

输出

`print` 输出字符串并换行

输入

`input(str)` 获得一行输入

```python
age = input('How old are you? ')
height = input('How tall are you? ')
weight = input('How much do you weigh? ')

print("So, you're {age} old, {height} tall and {weight} heavy.".format(age=age, height=height, weight=weight))
```
