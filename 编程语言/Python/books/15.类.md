[TOC]

### 类

#### 创建类

其中 `pass` 用于表示没有类体。

```python
class Person:
  pass
```

新建类的实例

```python
p = Person()
```

#### 成员变量

Python 中成员变量可以直接声明在类中或者在方法中通过 `self` 声明。在类外可以直接进行引用，并且无需在类中预先定义变量。

此外可以变量或方法名前加 `__` 来定义私有成员。但是这实际是伪私有的，可以通过 `obj._classNameFieldName` 的方式进行访问。



```python
class Person:
  __private_name = 'private name'

  def show(self):
      return "name=" + self.name + ",age=" + str(self.age)

p = Person('Peter')
print(p._Person__private_name)
p.x = 10
```

#### __init__

`__init__()` 方法就是其它语言中的构造方法，用于构造对象时进行初始化操作。

```python
def __init__(self, name):
    self.name = name
    self.weight = 67
    self.age = 0
```

#### 类对象

Python 在类外使用类名表示类对象，通过类对象可以声明类变量。如果类中有同名变量时并且修改过后则优先使用类变量。

```python
class Person:
  count = 0

p1.count = 10
Person.count = 100
print('p1=', str(p1.count)) # 10
print('p2=', str(p2.count)) # 100
```

#### Inspect

Python 中可以通过 `__dict__` 查看对象的所有属性。

```python
print(p1.__dict__)
print(Person.__dict__)
```
