[TOC]

# 头文件


## 保存方法在单独的文件中

Hi.h

```c
#ifndef Hi_h
#define Hi_h

void sayHi();

#endif
```

Hi.c

```c
#include "Hi.h"

void sayHi(){
    printf("Hello World, C");
}
```

## 引用头文件

两种方式

C 语言方式

```c
#include "Hi.h"
```

OC 方式

```c
#import "Hih.h"
```
