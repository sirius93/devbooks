[TOC]

# IO 操作

### 控制台输出

C 语言风格，没有换行符

```c
printf("hello %s %d\n ", "world", 100);
```

OC 风格，每一句后会追加换行符

```
NSLog(@"\nHello, %s %d %@!", "world", 100, name);
```

OC 风格的输出语句中可以追加各种占位符，其中 `%@` 表示任意的 NSObject 对象，会调用 NSObject 的 `description()` 方法，类似 Java 的 `toString()`。
