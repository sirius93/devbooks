[TOC]

# Switch

```c++
int age = 19;
switch (age) {
   case 10:
       cout << "age >= 10" <<endl;
       break;
   case 19:
       cout << "age >=19" << endl;
   case 3:
       cout << "no break" << endl;
       break;
   default:
       cout << "default" << endl;
       break;
}
```
