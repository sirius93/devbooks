[TOC]

# 基本语法

## main 方法

main 方法是入口，返回 0 表示程序正常结束。定义 main 方法时可以省略参数的定义。

```c++
int main(){
    return 0;
}
```

也可以使用完整的定义方式

```c++
int main(int argc, const char * argv[]) {
    return 0;
}
```
