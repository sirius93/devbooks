[TOC]

# 安装与运行

## 第一个 C++ 程序

```c++
#include <iostream>

using namespace std;

int main(){
    cout << "Hello World\n";
    cout << "This " << "is " << "awesome!" << endl;
    cout << "foobar";
    return 0;
}
```

代码分析

- `#` 表示预编译指令
- `using namespace` 相对于引入包，否则需要使用 `std::cout` 的形式。
- `endl` 或者 `\n` 都可以表示换行。
