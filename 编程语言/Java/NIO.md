
[TOC]

# NIO

## 概述

### Channel 和 Buffer

Channel 有点象流。 数据可以从 Channel 读到 Buffer 中，也可以从 Buffer 写到 Channel中。

![][1]

Channel 有如下实现

- FileChannel
- DatagramChannel
- SocketChannel
- ServerSocketChannel

Buffer 有如下实现

- ByteBuffer
- CharBuffer
- DoubleBuffer
- ...

### Selector

Selector允 许单线程处理多个 Channel。

![][2]

要使用 Selector，得向 Selector注册 Channel，然后调用它的 select()方法。这个方法会一直阻塞到某个注册的通道有事件就绪。一旦这个方法返回，线程就可以处理这些事件，事件的例子有如新连接进来，数据接收等。

## Channel

Channel 与流的区别

- Channel 是双向的，可以读也可以写
- Channel 可以异步读写
- Channel 总是要先读写到 Buffer 中

## Buffer

### Buffer的基本用法

使用 Buffer 读写数据一般遵循以下四个步骤：

- 写入数据到 Buffer
- 调用 flip() 方法
- 从 Buffer 中读取数据
- 调用 clear() 方法或者 compact() 方法

当向 buffer 写入数据时，buffer 会记录下写了多少数据。一旦要读取数据，需要通过 flip() 方法将 Buffer 从写模式切换到读模式。在读模式下，可以读取之前写入到 buffer 的所有数据。

一旦读完了所有的数据，就需要清空缓冲区，让它可以再次被写入。有两种方式能清空缓冲区：调用 clear() 或 compact() 方法。clear() 方法会清空整个缓冲区。compact() 方法只会清除已经读过的数据。任何未读的数据都被移到缓冲区的起始处，新写入的数据将放到缓冲区未读数据的后面。

### 向 Buffer 中写数据

1. 从 Channel 向 Buffer 写数据，`int bytesRead = inChannel.read(buf);`
2. 通过 put()方法写入到 Buffer，`buffer.put(100);`

### 从 Buffer 中读数据

1. 从Buffer 中读取数据到 Channel，`int bytesWritten = inChannel.write(buf);`
2. 通过 get()方法，`byte b = buffer.get();`

## Scatter/Gather

分散（scatter）从Channel中读取是指在读操作时将读取的数据写入多个buffer中。因此，Channel将从Channel中读取的数据“分散（scatter）”到多个Buffer中。

聚集（gather）写入Channel是指在写操作时将多个buffer的数据写入同一个Channel，因此，Channel 将多个Buffer中的数据“聚集（gather）”后发送到Channel。

### Scattering Reads

Scattering Reads是指数据从一个channel读取到多个buffer中

### Gathering Writes

Gathering Writes是指数据从多个buffer写入到同一个channel。

## Selector

Selector（选择器）是Java NIO中能够检测一到多个NIO通道，并能够知晓通道是否为诸如读写事件做好准备的组件。这样，一个单独的线程可以管理多个channel，从而管理多个网络连接。

### 创建 Selector

```java
Selector selector = Selector.open();
```

### 注册 Channel

```java
channel.configureBlocking(false);
SelectionKey key = channel.register(selector,
        SelectionKey.OP_READ);
```

第二个参数为interest， 表示 Selector 对什么感兴趣，即监听什么事件，可以用 '|' 监听多个事件。返回的 SelectionKey 包含有 Channel 和 Selector 的信息。

与Selector一起使用时，Channel必须处于非阻塞模式下。这意味着不能将FileChannel与Selector一起使用，因为FileChannel不能切换到非阻塞模式。而套接字通道都可以。

### 选择键

```java
int readyChannels = selector.select();

if (readyChannels == 0) return;
Set<SelectionKey> selectKeys = selector.selectedKeys();
```

注册完 Channel 后，可以调用 Selector 的 `select()`方法，该方法返回当前有多少个该 Selector 感兴趣的 Channel 已经就绪了。当返回值大于0时，可以接着调用 `selectedKeys()` 来获得含有这些 Channel 信息的 SelectionKey。

### 示例

```java
Selector selector = Selector.open();

//TODO
SocketChannel channel = null;

channel.configureBlocking(false);
SelectionKey key = channel.register(selector,
        SelectionKey.OP_READ);

while (true) {
    int readyChannels = selector.select();
    if (readyChannels == 0) continue;
    Set<SelectionKey> selectKeys = selector.selectedKeys();
    for (SelectionKey selectionKey : selectKeys) {
        if (selectionKey.isAcceptable()) {

        } else if (selectionKey.isReadable()) {

        } else if (selectionKey.isWritable()) {

        }
    }
}
```

##  FileChannel

FileChannel无法设置为非阻塞模式，它总是运行在阻塞模式下。

### transfer

当两个通道之间有一个是 FileChannel 的话，就可以通过 transfer 操作在通道间传递数据。

## SocketChannel

### 打开 Channel

```java
SocketChannel socketChannel = SocketChannel.open();
socketChannel.connect(new InetSocketAddress("http://127.0.0.1", 80));
```

### 读取数据

```java
ByteBuffer buf = ByteBuffer.allocate(48);
int bytesRead = socketChannel.read(buf);
```

### 写入数据

```java
String newData = "New String to write to file..."
        + System.currentTimeMillis();
buf = ByteBuffer.allocate(48);
buf.clear();
buf.put(newData.getBytes());
buf.flip();
while (buf.hasRemaining()) {
    socketChannel.write(buf);
}
```

### 非阻塞 Channel

```java
SocketChannel socketChannel = SocketChannel.open();
socketChannel.configureBlocking(false);

socketChannel.connect(new InetSocketAddress("http://127.0.0.1", 80));
while (!socketChannel.finishConnect()) {
    //wait, or do something else...

}
```

## ServerSocketChannel

```java
ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
serverSocketChannel.socket().bind(new InetSocketAddress(9999));
serverSocketChannel.configureBlocking(false);

while (true) {
    SocketChannel socketChannel =
            serverSocketChannel.accept();

    if (socketChannel != null) {
        //do something with socketChannel...
    }
}
```

## DatagramChannel

```java
DatagramChannel channel = DatagramChannel.open();
channel.socket().bind(new InetSocketAddress(9999));

//  Receive
ByteBuffer buf = ByteBuffer.allocate(48);
buf.clear();
channel.receive(buf);

//  Sender
String newData = "New String to write to file..." + System.currentTimeMillis();
buf = ByteBuffer.allocate(48);
buf.clear();
buf.put(newData.getBytes());
buf.flip();
int bytesSent = channel.send(buf, new InetSocketAddress("jenkov.com", 80));
```

## Pipe

Java NIO 管道是2个线程之间的单向数据连接。Pipe有一个source通道和一个sink通道。数据会被写到sink通道，从source通道读取。

原理

![][3]

```java
Pipe pipe = Pipe.open();

// Write to pipe
Pipe.SinkChannel sinkChannel = pipe.sink();
String newData = "New String to write to file..." + System.currentTimeMillis();
ByteBuffer buf = ByteBuffer.allocate(48);
buf.clear();
buf.put(newData.getBytes());
buf.flip();

while (buf.hasRemaining()) {
    sinkChannel.write(buf);
}

// Read from pipe
Pipe.SourceChannel sourceChannel = pipe.source();
buf = ByteBuffer.allocate(48);
int bytesRead = sourceChannel.read(buf);
```

## NIO vs IO

区别

|IO           |     NIO |
|--------|--------|
|面向流         |   面向缓冲
|阻塞IO          |  非阻塞IO
|无               | 选择器

**面向流与面向缓冲**
Java NIO和IO之间第一个最大的区别是，IO是面向流的，NIO是面向缓冲区的。 Java IO面向流意味着每次从流中读一个或多个字节，直至读取所有字节，它们没有被缓存在任何地方。此外，它不能前后移动流中的数据。如果需要前后移动从流中读取的数据，需要先将它缓存到一个缓冲区。 Java NIO的缓冲导向方法略有不同。数据读取到一个它稍后处理的缓冲区，需要时可在缓冲区中前后移动。这就增加了处理过程中的灵活性。但是，还需要检查是否该缓冲区中包含所有您需要处理的数据。而且，需确保当更多的数据读入缓冲区时，不要覆盖缓冲区里尚未处理的数据。

**阻塞与非阻塞IO**
Java IO的各种流是阻塞的。这意味着，当一个线程调用read() 或 write()时，该线程被阻塞，直到有一些数据被读取，或数据完全写入。该线程在此期间不能再干任何事情了。 Java NIO的非阻塞模式，使一个线程从某通道发送请求读取数据，但是它仅能得到目前可用的数据，如果目前没有数据可用时，就什么都不会获取。而不是保持线程阻塞，所以直至数据变的可以读取之前，该线程可以继续做其他的事情。 非阻塞写也是如此。一个线程请求写入一些数据到某通道，但不需要等待它完全写入，这个线程同时可以去做别的事情。 线程通常将非阻塞IO的空闲时间用于在其它通道上执行IO操作，所以一个单独的线程现在可以管理多个输入和输出通道（channel）。

**选择器（Selectors）**
Java NIO的选择器允许一个单独的线程来监视多个输入通道，你可以注册多个通道使用一个选择器，然后使用一个单独的线程来“选择”通道：这些通道里已经有可以处理的输入，或者选择已准备写入的通道。这种选择机制，使得一个单独的线程很容易来管理多个通道。



[1]: capture/overview-channels-buffers1.png
[2]: capture/overview-selectors.png
[3]: capture/pipe.bmp