## Clojure 篇

### 函数

#### 定义函数

定义函数使用关键字 `defn`，又称作为定义宏，共有两种形式

基本语法

方式一

```clj
(defn 函数名 [参数列表...] (方法体))
```

方式二

```clj
(def 函数名 (fn [参数列表...] (方法体)))
```

例：

```clj
(defn add [x,y] (+ x y))
(def add2 (fn [x] (+ x 2)))
```

调用函数

```clojure
(println (add 2 3))
(println (add2 10))
```

#### Varargs

Clojure  使用 `&` 修身变参

```clj
(defn sum [& num] (apply + num))
(println (sum 1 2 3 4 5))
```

#### 参数占位符

```clojure
(let [plus-numbers #(+ 1 %1 %2 %3)]
  (plus-numbers 10 20 30))
```

#### 方法文档

方法名后可以紧跟描述该方法的文档

```clojure
(defn add10
  "add 10 to passed value"
  [x]
  (+ x 10))
```

#### Meta Data

方法文档后可以紧跟描述该方法的元数据

```clojure
(defn say-hello
  "Takes name argument and say hello to the name"
  {:added "1.0"
   :static true}
  [name]
  (println (str "Hello, " name)))
```

可以将元数据转换为 `var` 后进行打印

```clojure
(println (meta (var say-hello)))
(println (get (meta (var say-hello)) :added))
(println (get (meta (var say-hello)) :doc))
(println (meta #'say-hello))
```

以上 `:added` 和 `:static` 就是元数据

#### 匿名函数

```clojure
(fn [] (println "Hello world"))
```

`#()` 可以作为 `fn` 的简写形式

```clojure
(println (#(+ 1 1)))
```
