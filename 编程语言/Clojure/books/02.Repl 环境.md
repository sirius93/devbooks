
REPL 为 "Read Evaluate Print Loop" 的简写，即 "读取-求值-打印-循环"，是一些动态语言的标准特性，主要用于进行探索式编程。也就是说当你不清楚某项计算的结果或者忘记了某个方法的作用时，可以输入代码片段并立即获得结果。
<!--more-->

## Clojure 篇

在命令行中输入 `java -cp clojure-1.8.0.jar clojure.main` 就可以进入 Clojure 的 REPL 环境，可以看到提示符会变为 "user=>"。

输入语句 `(+ 2 3)` 后回车就可以立即看到结果。

由于 Clojure 是以括号作为结尾的，所以可以直接编写多行代码。
