
介绍

Clojure 是在 JVM 上重新实现的 Lisp 语言。
Java 等程序将变量（盒子）当作保存内容的容器，容器中的内容是可变的。而 Clojure 则认为值是真正重要的概念，所以值一旦被创建就不可再改变，但是符号可以被绑定到不同的不可变值上，即重新绑定。
符号通常都是小写，使用连字符 "-" 连接，而不是用驼峰式。
false 和 nil 都为假，其余都为真

安装 Clojure

下载 Jar 包，在 Clojure 所在目录输入如下命令，进入 REPL 环境

`java -cp clojure-1.6.0.jar clojure.main`

完成后命令提示符会变为 `user=>`

第一个 Clojure 程序

```clojure
// 定义 hello 函数
(def hello (fn [] "Hello World"))
// 调用函数
(hello)
```

上述代码中的 `def` 实际是 `defn` 的变体。


配置 IDEA 开发环境

1. [下载 IDEA 14.1 版本](https://www.jetbrains.com/idea/download/)
2. 配置 -> Plugins -> Browse Repositories -> Manage Repositories -> +
3. 添加 URL，"https://cursiveclojure.com/plugins-14.1.xml"
4. 添加完后检索 "Cursive"，安装该插件

绑定

Clojure 使用关键字 `def` 表示符号和值之间的绑定关系

语法

`def <name>, <value>`

定义函数

定义函数使用关键字 `defn`，又称作为定义宏。

```clojure
(defn hello [] (println "Hello Night"))
```

上述 `println` 表示输出字符串，返回值为 `nil`


Clojure 特殊形式

| 特殊形式 | 含义 |
|--------|--------|
|(def<符号><值?>)|符号绑定到值上（如果有的话），如有必要创建符号对应的 var        |
|(fn<名称>?[<参数>*]<表达式>*)|返回带有特定参数的函数值，通常和 (def) 结合变为(defn)        |
|(if<test><then><else>?)|条件表达式        |
|(let[<绑定>*]<表达式>*|给局部名称分配值，并隐式定义作用域，使 let 作用域内所有表达式都能获得该别名|
|(do<表达式>*)|do 循环|
|(quote<形式>)|原样返回，只接受一个参数，其余都会被忽略|
|(var<符号>)|返回与符号对应的 var（返回一个 ClojureJVM 对象，不是值）|


列表

列表类似 LinkedList
列表使用 `()` 定义，但是 Clojure 也使用此表示函数的定义与调用，所以它会默认将第一个元素当作函数名，其余为参数。解决办法是使用 `quote<形式>` 或其简写形式 `''`（因为 quote 不会计算值，只是原样返回）

```clojure
'(1 2 3)
(quote (1 2 3))
```

向量

向量类似 ArrayList，可以用方括号表示

```clojure
(vector 1 2 3)
(vec '(1 2 3))
[1 2 3]
```

其中，vec 以一个列表为参数生成向量

nth

函数 `nth` 有两个参数，集合和索引。类似 Java 中 List 的 get() 方法，可以用在向量和列表上

```clojure
(nth '(1 2 3) 1)
```


映射

映射类似 HashMap

定义

```clojure
(def foo{"key1" "value1" "key2" "value2"})
```

取值

```clojure
(foo "key2")
```

关键字

前面带冒号的映射键被称为 “关键字”。

```clojure
(def martijn {:name "Jane", :city "London"})
(martijn :name)
(:name martijn)
:name
```

- 关键字可以使用两种形式：(map : key) 或 (:key map)
- 关键字作为值使用时返回自身

集

集类似 HashSet

```clojure
{"apple" "pair" "peach"}
```

相等与其他操作

Clojure 没有 Java 那样的操作符，所以只能用函

```clojure
(+ 3 4 5)
```

以上 "+" 实际是 clojure.core 命名空间下的函数。Clojure 中的函数一般都支持变参。

相等

Clojure 有两个相等符号：(=) （检查内容）和 (identical?)（检查是否为同一对象），注意这里是 "=" 而不是 "=="。

```clojure
(def list-int '(1 2 3 4))
(def vect-int (vec list-int))
(= vect-int list-int)
(identical? vect-int list-int)
```


---

函数

```clojure
(defn const-fun1 [y] 1)
(defn ident-fun [y] y)
(defn list-maker-fun [x f]
  (map (fn [z] (let [w z]
    (list w (f w))
  )) x))
```

其中 `const-fun1` 接受一个参数，返回 1，称作常量函数
`ident-fun` 接受一个数值并返回数组本身，称作恒等函数

函数中使用向量表示函数的参数，(let)形式中用的也是向量

上述 `list-maker-fun`接受两个参数：向量和函数

使用函数

```clojure
(list-maker-fun [2 1 3] ident-fun)
```

循环

JVM无法保证尾递归优化
Clojure中有不会增加栈空间占用的结构，最常用的是loop-recur

```clojure
(defn like-for [counter]
  (loop [ctr counter]
    (println ctr)
    (if (< ctr 10)
      (recur (inc ctr))
      ctr
    )
  )
)
```

当程序执行到了 `recur` 时，程序会返回到loop的定义处

关键字

Clojure中的关键字被称作读取器宏。

| 字符 |  名称 | 含义 |
|--------|--------|
|   '     | 引号 	  | 展开为quote，不进行计算
|   ;     |  注释      | 行注释
|   \     |  字符      | 产生一个字面字符
|   @     |  解引用      |展开为deref，接受var对象并返回对象中的值
|   ^     |   元数据     |将元数据的映射附加到对象上
|   `     |   语法引用     |用在宏定义中的引号形式
|   #     |     派发   | 见下表

派发读取器宏的子形式

| 派发形式 |  含义 |
|--------|--------|
|   #'     | 展开为var       |
|   #{}     | 创建一个集字面值       |
|   #()     | 创建匿名函数字面值       |
|   #_   | 跳过下一个形式，#_(...多行...)可用于多行注释       |
|   #"<模式>"     | 创建正则       |

在REPL中定义一个函数可以看到返回的是 `#'函数名`，所以定义函数实际是创建了一个var对象

匿名函数

```clojure
(defn schwartz [x f]
  (map #(nth %1 0)
    (sort-by #(let [w %1]
      (list w (f w))
    x ))
  )
)
```

以上 `%1` 为参数的占位符

函数式编程和闭包

```clojure
(defn adder [constToAdd] #(+ constToAdd %1))
(def plus2 (adder 2))
(plus2  3)
```

---

序列

序列可以看作是拥有不可变特性的集合

序列函数

| 函数 | 作用 |
|--------|--------|
| (seq <coll>)       |    返回序列    |
| (first <coll>)       |  返回第一个元素      |
| (rest <coll>)       |  返回去掉第一个元素的新序列      |
| (seq? <o>)       |  是序列的话返回true      |
| (cons <elt> <coll>)       | 返回在集合前面增加了新元素的序列       |
| (conj <coll> <elt>)       |  返回在向量尾端或列表头部增加了新元素的序列      |
| (every? <pref-fn> <coll>)       | 如果pref-fn 对集合中每个元素都返回true，则返回true|

```clojure
(rest '(1 2 3))
(first '(1 2 3))
(rest [1 2 3])
(seq ())
(seq [])
(cons 1 [2 3])
(every? is-prime [2 3 5 7 11])
```

懒序列

懒序列就是序列本身可以是不完整的，其中的值可以在被请求时才取得（如需要通过函数计算后生成）

```clojure
(defn next-big-n [n] (let [new-val (+ 1 n)]
  (lazy-seq
    (cons new-val (next-big-n new-val))
  )
))

(defn natural-k [k]
  (concat [k] (next-big-n k))
)

(take 10 (natural-k 3))
```


以上例在假设在一个栈空间不受限制的机器上，一个线程生成无穷的序列，另一个使用该序列

lazy-seq 标记发生了无限递归的点
concat 限制递归
take 从懒序列中取出所需的元素

序列和变参函数

Clojure中函数参数可变被称作为函数的变元，而带有变元的函数被称作变参函数。

```clojure
(defn const-fun
  ([] 1)	;没有参数
  ([x] 1)	;一个参数
  ([x & more] 1)	;参数为序列，且为变参版本
)
```

以上定义了函数的一组行为，类似Java中的方法重载


Clojure与Java的互相操作

所有用def定义的值都实际都继承自java.lang.Object类，所以可以直接调用Object上的所有方法

```clojure
(defn lenStr[y] (.length (.toString y)))
```

以上 `.` 是Clojure中的一个宏，表示在紧接着的参数上调用该方法

调用静态方法

```clojure
(System/getProperty "java.vm.version")
```
静态变量

```clojure
Boolean/True
```

完整例子

```clojure
(import '(java.util.concurrent CountDownLatch LinkedBlockingQueue))

(def cdl (new CountDownLatch 2))
(def lbq (LinkedBlockingQueue.))
```

`new` 可以用于创建Java对象，而`类名.` 则是其的简便形式


Clojure代理

`proxy` 宏用于创建扩展Java类的Clojure对象

语法

`(proxy [类/接口] [<args> <命名函数的实现]+)`

第一个参数如果是类的话必须放在向量的第一个元素

例

```clojure
(import '(java.util.concurrent CountDownLatch LinkedBlockingQueue))

(def cdl (new CountDownLatch 2))
(def lbq (LinkedBlockingQueue.))

(def msgRdr (proxy [Runnable] []
  (run [] (.toString (.poll lbq)))
))
```

Java中使用Clojure
确保 `clojure.jar` 放在 `classpath` 上

```java
ISeq seq=StringSeq.create("foobar");
while(seq !=null){
 Object first=seq.first();
 seq=seq.next();

}
```


---

并发

并发模型

未来式(future)，并行调用(pcall)，引用形式(ref)，代理(agent)

未来式

不分享状态

```clojure
(def simple-future(
  future (do
    (println "Line 0")
    (Thread/sleep 3000)
    (println "Line 1")
    (Thread/sleep 3000)
    (println "Line 2")
  )
))

以上创建了一个future并且马上执行。

;;检查状态，非阻塞
(future-done? simple-future)

;;解引用，阻塞
@simple-future
```

并行调用

```clojure
(defn wait-with-for [limit]
  (let [counter 1])
    (loop [ctr counter]
      (Thread/sleep 500)
      (println (str "Ctr=" ctr))
      (if (< ctr limit)
        (recur (inc ctr))
        ctr
      )
    )
)

(defn wait-1 [] (wait-with-for 1))
(defn wait-2 [] (wait-with-for 2))
(defn wait-3 [] (wait-with-for 3))

(def wait-seq (pcalls wait-1 wait2 wait-3))

(first wait-seq)
(first (next wait-seq))
```

pcalls函数会在线程池上执行代码，并返回一个懒序列，任何试图访问该序列中没有完成的元素会导致访问线程被阻塞。


ref形式

可以在线程间共享状态，基于内部事务的管理，比较重量级

```clojure
(defn make-new-acc [account-name opening-balance]
  (ref {:name account-name :bal opening-balance})
)
(defn alter-acc [acc new-name new-balance]
  (assoc acc :bal new-balance :name new-name)
)
(defn loop-and-debit [account]
  (loop [acc account]
    (let [balance (:bal @acc)
      my-name (:name @acc)]
      (Thread/sleep 1)
      (if (> balance 0)
        (recur (dosync (alter acc alter-acc my-name (dec balance)) acc))
      acc
      )
    )
  )
)

(def my-acc (make-new-acc "Ben" 5000))

(defn my-loop[] (let [the-acc my-acc]
  (loop-and-debit the-acc)
))
(pcalls my-loop my-loop my-loop my-loop my-loop)
```

dosync设置事务
在事务之内，用alter形式来修改ref
对值进行操作的alter-acc函数必须返回一个值，所操作的是当前事务中线程可以将的本地值，即事务内的值。在退出dosync定义的事务块之前，这个值对外界不可见。

代理

与ref形式不同，可以用于只需偶尔进行线程间通信的环境

应用到代理上的函数在代理的线程上运行。用户代码只会看到最后的状态，而不会看到代理的中间态。

```clojure
(defn wait-and-log [coll str-to-add]
  (do (Thread/sleep 10000)
    (let [my-coll (conj coll str-to-add)]
      (Thread/sleep 10000)
      (conj my-coll str-to-add)
    )
  )
)

(def str-coll (agent []))

(send str-coll wait-and-log "foo")

@str-coll
```




