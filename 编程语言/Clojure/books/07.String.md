## Clojure 篇

### 声明字面量

```clojure
(def s "Hello World")
```

<!--more-->

### 多行文本

```clojure
(def text "1, 2, 3
one, two, three
\"x, \"y\", \"z\"")
```

### 模板 Template

```clojure
(def salary 100.1)
(println (format "name=%s has %.5f"
           "Peter" 5.0))
```

以上代码会输出以下结果

```
name=Peter has 5.00000
```

### 字符串连接

Clojure 需要使用 `str` 连接字符串

```clojure
(def ret (str "x" "y"))
(println ret)
```
