
# Begin


## 开发环境

本系列的主要开发环境如下

- OS
  	- Linux, Mac, Windows(安装 Cygwin)

- 语言
  	- Java 1.8.0_25
  	- Clojure 1.8.0

- IDE
  	- IDEA

- 构建工具
  	- Maven 或 Gradle

## 安装开发语言

- Clojure

	安装包下载 [this](http://clojure.org/community/downloads)

以上全部安装完毕后可以在命令行中执行以下语句来验证是否安装成功

```bash
java -cp clojure-1.8.0.jar clojure.main
```

## Clojure 篇

### 简介

Clojure 是一门运行在 JVM 上 的动态语言，同时也是一种 Lisp 方言，所以和其它 JVM 语言从语法上又非常大的区别。

### 第一个 Clojure 程序

建立文件 `hello.clj`，并输入以下内容

```clojure
(println "Hello, Clojure!")
```

然后执行以下命令

```bash
java -cp clojure-1.8.0.jar clojure.main hello.clj
```

成功的话可以看到命令行输出 "Hello, Clojure!" 字符串，第一个 Clojure 程序就这样运行成功了。
