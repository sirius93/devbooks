## Swift 篇

### Swift 工程调用 OC

添加 OC 代码后 XCode 自动提示生成 `<productName>-Bridge-Header.h` 文件，也可以手动在 `Build Settings` -> `Objective-C Bridging Header` 一栏手动指定头文件.

在以上头文件中引入所有需要使用的 OC 代码的头文件后 Swift 就可以调用 OC 代码了

### OC 工程调用 Swift

添加 Swift 代码后 XCode 自动提示生成 `<productName>-Swift.h` 文件

然后在 OC 代码头部引入以上头文件即可
