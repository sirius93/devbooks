## Swift 篇

### Switch

```swift
let vegetable = "red pepper"
switch vegetable {
case "celery":
    print("Object equals")
case "cucumber", "watercress":
    print("Or")
case "1"..<"5":
    print("Range contains")
case let x where x.hasSuffix("pepper"):
    print("Expression")
default:
    print("Default")
}

let anotherPoint = (2, 0)
switch anotherPoint {
case (let x, 0):
    print("tuple case \(x)")
case let (x, y):
    print("somewhere else at (\(x), \(y))")
}
```

`fallthough` 可以用于实现 c 风格的 switch，即满足条件后继续进入下一个处理语句

```swift
let num = 1
switch num {
case 1:
    print("is 1")
    fallthrough
case 2:
    print("is 2")
default:
    print("default")
}
//is 1
//is 2
```
