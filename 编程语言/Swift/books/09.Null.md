## Swift 篇

###  nil

Swift 使用 `nil` 表示空值，对于一般对象使用 `==` 进行空值的判断，对于字符串还提供了 `str.isEmpty` 属性

```swift
var x = nil

//  判断是否为空字符串
print(x.isEmpty)
```

### Nullable 和 Non-Null

Swift 变量本身分为 `Nullable` 和 `Non-Null` 类型，前者必须指定类型，且默认值就是空，后者不能指定为空值且没有默认值。

```swift
//  Nullable
var x:String? = nil

//  Non-Null
var nonNullString: String = "foo"
```

### 安全操作符

```swift
print(x?.isEmpty)
```

也可以像以下语法一样处理空值

```swift
var suffix:String? = "world"
var prefix = "hello,"
if let msg = suffix{
    prefix = "\(prefix) \(msg)"
}
print(prefix) // hello world
```

#### ! 操作符

`!` 操作符用于将 `Nullable` 转换为 `Not-null` 类型。

```swift
var foo:String?
foo = "foo"
var y:String = foo!
```
