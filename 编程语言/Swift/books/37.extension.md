## Extension

### 扩展类的行为

先定义一个需要被扩展的类

```swift
class Num {
    var x:Int
    init(x:Int) {
        self.x=x
    }
    func add(_ y: Int) {
        x += y
    }
}
```

以上定义一个 `Num`，该类包含一个方法 `add()`，用于将内置的变量与一个参数进行相加。

接下来扩展该类，为其添加一个将内置变量与两个参数进行相加的方法，语法为 `extension 类{}`

```swift
extension Num{
    func add(_ a:Int, _ b:Int)  {
        self.x+=a+b
    }
}
```

接着就可以正常进行调用了

```swift
var n: Num = Num(x: 10)
n.add(4)
n.add(1, 3)
print(n.x)   //  18
```

扩展功能不仅可以用于自定义的类，也可以用于内置的类。但是和 Kotlin 不同， Swift 无法扩展已经存在的方法，否则会编译失败。

如果想在方法中修改自身，也可以使用 `mutating` 关键字

```swift
extension Int {
    mutating func square() {
        self = self * self
    }
}
var someInt = 3
someInt.square()
```

### 扩展类的属性

扩展类的属性用法基本同扩展类的行为。以下展示为内置类 List 添加一个能够直接访问其第二个元素的新的属性

```swift
extension Double {
    var km: Double { return self * 1_000.0 }
    var m: Double { return self }
    var cm: Double { return self / 100.0 }
    var mm: Double { return self / 1_000.0 }
    var ft: Double { return self / 3.28084 }
}
```

调用该属性

```swift
let aMarathon = 42.km + 195.m
```

### 扩展初始化方法

```swift
extension Rect {
    init(center: Point, size: Size) {
        let originX = center.x - (size.width / 2)
        let originY = center.y - (size.height / 2)
        self.init(origin: Point(x: originX, y: originY), size: size)
    }
}
```

### 扩展 Subscript

```swift
extension Int {
    subscript(digitIndex: Int) -> Int {
        var decimalBase = 1
        for _ in 0..<digitIndex {
            decimalBase *= 10
        }
        return (self / decimalBase) % 10
    }
}

746381295[0]
```
