## Swift 篇

### 三元操作符

```swift
let contentHeight = 40
let hasHeader = true
let rowHeight = contentHeight + (hasHeader ? 50 : 20) //90
```

同时 Swift 还提供了类似 Groovy 的猫王操作符

```swift
let defaultColorName = "red"
var userDefinedColorName: String?
var colorNameToUse = userDefinedColorName ?? defaultColorName //red
```

### ==

`==` 用于一般类型之间的比较，比如 `Int`, `String`, `Array` 等

### ===

`===` 用于两个实例之间的比较
