## Swift 篇

### 创建一个枚举类

一个枚举值的变量名就是该值的名字，枚举值在类中的定义顺序就是该值的索引。

```swift
enum WeekDay{
    case Monday
    case Tuesday
    case Wednesday
    case Thursday
    case Friday
    case Saturday
    case Sunday
    func isWorkingDay() -> Bool {
        switch self {
        case .Saturday:
            return false
        case .Sunday:
            return false
        default:
            return true
        }
    }
}
```

使用该枚举类

```swift
//  直接获得枚举值
let mondy = WeekDay.Monday

//  调用枚举中的方法
print(mondy.isWorkingDay())
```

### 获取值

定义枚举

```swift
enum ServerResponse {
    case result(String, String)
    case failure(String)
}
```

获取对应值

```swift
let success = ServerResponse.result("6:00 am", "8:09 pm")
let failure = ServerResponse.failure("Out of cheese.")

switch success {
case let .result(sunrise, sunset):
    print("Sunrise is at \(sunrise) and sunset is at \(sunset).")
case let .failure(message):
    print("Failure...  \(message)")
}
```
