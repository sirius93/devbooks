## Swift 篇

### 方法

#### 定义方法

完整的 swift 方法定义语法为

```
func 方法名(参数列表) [-> 返回值类型] {}
```

swift 可以省略变量定义的类型声明，但是在定义参数列表和定义返回值类型时则必须明确指定类型。

例

```swift
func add(x: Int, y: Int) -> Int {
    return x + y
}
```

#### Label

swift 默认以参数名作为标签名且调用方式时必须指定参数名，如果想省略参数名则可以使用 `_` 明确指定标签。

默认参数名为标签

```swift
func add(x: Int, y: Int) -> Int {
    return x + y
}
add(x: 2, y: 10)
```

省略标签

```swift
func add(_ x: Int, _ y: Int) -> Int {
    return x + y
}
add(2, 10)
```

#### Varargs

swift 使用 `...` 修饰参数类型来表示变参。

声明一个变参方法

```swift
func sumOf(numbers: Int...) -> Int {
    var sum = 0
    for number in numbers {
        sum += number
    }
    return sum
}
```

调用该方法

```swift
sumOf()
sumOf(numbers: 42, 597, 12)
```

#### 参数默认值

swift 同 Scala 一样支持参数默认值，但是一旦使用参数默认值时，参数列表的最后一个或最后几个参数都必须有默认值。

```swift
func say(_ name: String, _ word: String = "Hello") {
    print("\(word) \(name)")
}

say("Peter")
```

#### 返回值

```swift
func add(x: Int, y: Int) -> Int {
    return x + y
}
```

可以使用 Tuple 来同时返回多个值

```swift
func swap(x: Int, y: Int) -> (Int, Int) {
    return (y, x)
}

swap(x: 3, y: 2)
```

#### 方法嵌套

swift 支持方法嵌套，即一个方法可以定义在另一个方法中，且内层方法可以访问外层方法的成员。

```swift
func returnFifteen() -> Int {
    var y = 10
    func add() {
        y += 5
    }
    add()
    return y
}
returnFifteen()
```

#### inout

默认参数按值传递，可以通过 `inout` 关键字按引用传递

```swift
func swapTwoInts(_ a: inout Int, _ b: inout Int) {
    let temporaryA = a
    a = b
    b = temporaryA
}
var someInt = 3
var anotherInt = 107
swapTwoInts(&someInt, &anotherInt)
```

#### 方法作为参数

```swift
func hasAnyMatches(list: [Int], condition: (Int) -> Bool) -> Bool {
    for item in list {
        if condition(item) {
            return true
        }
    }
    return false
}
func lessThanTen(number: Int) -> Bool {
    return number < 10
}
var numbers = [20, 19, 7, 12]
hasAnyMatches(list: numbers, condition: lessThanTen)
```










### 闭包

#### 创建一个闭包

闭包的基本语法如下：

```
{ (parameters) -> return_type in
    statements
}
```

由于闭包是个代码块，所以最简单的闭包形式如下

```swift
{ -> println("foo") }
```

#### 字面量

闭包可以存储在一个变量中，这一点是实现函数是一等公民的重要手段。

```swift
var excite = { (word: String) -> String in
    return "\(word)!!"
}
```

#### 调用闭包

```swift
excite("Java")
```

#### 多参数

同 Scala 一样，swift 中闭包的参数不能有默认值。

```swift
var plus = { (x: Int, y: Int) -> Void in
    print("\(x) plus \(y) is \(x + y)")
}
```

#### shorthand

Swift 中可以将 `$0`,`$1` 等占位符用于闭包中表示对应位置的参数

```swift
let names = ["Chris", "Alex", "Ewa", "Barry", "Daniella"]
var reversedNames = names.sorted(by: { $0 > $1 } )
```

也可以通过比较符直接省略参数

```swift
reversedNames = names.sorted(by: >)
```

#### Varargs

swift 中闭包不支持变参


#### 闭包作为参数

```swift
func max(numbers: Array<Int>, s: (Array<Int>) -> Int)-> Int {
    return s(numbers)
}
```

传入闭包

```swift
var maxValue = max(numbers:[3, 10, 2, 1, 40], s:{(arr:Array<Int>) -> Int in
    arr.max()!
})
```

#### @escaping

`@escaping` 用于标示可以将作为函数参数的闭包作为值来使用

```swift
var completionHandlers: [() -> Void] = []
func someFunctionWithEscapingClosure(completionHandler: @escaping () -> Void) {
    completionHandlers.append(completionHandler)
}
```

当使用了 `@escaping` 后如果希望在闭包中使用成员变量需要显示调用 `self` 才可以

```swift
func someFunctionWithNonescapingClosure(closure: () -> Void) {
    closure()
}

class SomeClass {
    var x = 10
    func doSomething() {
        someFunctionWithEscapingClosure { self.x = 100 }
        someFunctionWithNonescapingClosure { x = 200 }
    }
}
```
