## swift 篇

### 自定义异常

```swift
enum MyException:Error{
    case state(errorCode:Int, errorMessage:String)
    case notFoundException
    case otherException
}
```

### 抛出异常

使用 `throw` 关键字抛出异常，方法上使用 `throws` 进行声明

```swift
func add(x:Int, y:Int) throws -> Int{
    if(x<0){
        throw MyException.state(errorCode: 100, errorMessage: "number should larger than 0")
    }
    return x+y
}
```

### try

`try` 用于将调用的方法中的异常继续向上抛

```swift
func tryAdd() throws {
   try add(x: 2, y: 3)
}
```

### do...catch

```swift
do {
    try add(x: -1, y: 3)
} catch is MyException {
    print("abc")
}
```

### 结合 optional

```swift
let sum = try? add(x: 2, y: 3)
let sumNotNull = try! add(x: 2, y: 3)
```
