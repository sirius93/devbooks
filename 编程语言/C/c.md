[TOC]

# C

## 基本命令

### 编译代码

```bash
gcc hello.c [-o <output>] [-l <library>]
```

默认生成的为 `a.out` 或者 Cygwin 为 `a.exe`。

使用 C99 编译代码

```bash
gcc -std=c99 hello.c
```

编译的快捷方式是使用 `make` 指令。

```bash
make hello
```

注意没有后缀，此时控制台会显示 `make` 实际调用的指令

### 查看帮助

```bash
man 3 sleep
```

3 表示页数，sleep为需要查看的命令，通常可以查看其使用方法和所在的头文件

### 调试代码

调试
gdb <name>
run
break main     在main方法设置一个断点
break 23
next
print x
step
continue

## 基本概念

1. string 是以 `\0` 结尾的 char*，`typedef char *string;`。

## 基本语法

1. sizeof(int_value)
2. 定义结构
	```
    typedef struct {
		int id;
		char *name;
	} student；
    student class[3];

    typedef struct node{
		int value;
		struct node *next;
	} node;
	```
	如果希望在struct中引用自己就需要在Struct后跟着声明名字，且在结构内部定义变量时也要加上struct关键字。
3. 为字符串分配空间
	`malloc((strlen(str)+1)*sizeof(char))`
	- malloc从堆中分配，返回第一个地址
	- free() 释放malloc使用的内存
4. `uint16_t`	不会因为平台而改变大小




- - -




使用VC引起错误"预编译头文件来自编译器的早期版本，或者预编译头为 C++ 而在 C 中使用它(或相反)"

在项目设置里关闭预编译头的选项


基本语法
输出函数(printf)和输入函数(scanf)
#include<stdio.h>  // 包含stdio.h头文件
int max(int, int);  // 函数声明
int main(){
    int a, b, n;  // 声明两个整型变量
    printf("Input two integers: ");  // 以空格为分隔
    // 以'|'为分隔符，将输入的两个整数分别赋值给a, b
    scanf("%d|%d", &a, &b);
    // 以整数形式输出a、b和最大值，a, b, max(a,b)为参数列表
    n = max(a, b);
    printf("The max between %d and %d is %d.\n", a, b, n);
    return 0;
}
// 函数定义
int max(int num1, int num2){  // num1, num2为形式参数(形参)
    if(num1>num2){  // 如果num1大于num2
        return num1;  // 返回num1并结束max函数
    }else{  // 如果num2大于num1
        return num2;  // 返回num2并结束max函数
    }
}

数据类型、运算符与表达式

数据类型可分为：基本数据类型，构造数据类型，指针类型，空类型四大类。

基本数据类型：整型，字符型，浮点数（单精度，双精度），枚举
构造类型：数组，结构体，共用体
指针类型
空类型

整型
整型数据的一般分类如下：
基本型：类型说明符为int，在内存中占2个字节。
短整型：类型说明符为short int或short。所占字节和取值范围均与基本型相同。
长整型：类型说明符为long int或long，在内存中占4个字节。
无符号型：类型说明符为unsigned。

无符号型又可与上述三种类型匹配而构成：
无符号基本型：类型说明符为unsigned int或unsigned。
无符号短整型：类型说明符为unsigned short。
无符号长整型：类型说明符为unsigned long。

无符号数也可用后缀表示，整型数的无符号数的后缀为“U”或“u”。例如：358u、0x38Au、235Lu均为无符号数。

常量与变量
常量
#define 标识符 常量
#define PRICE 30

变量
类型说明符  变量名, 变量名, ...;
类型说明符 变量1= 值1, 变量2= 值2, ……;
int num,total;
double price = 123.123;
char a = 'a', abc;

浮点
float x,y;  // x,y为单精度实型量
double a,b,c;  // a,b,c为双精度实型量
float 32
double 64
long double 128

字符
char a,b;

字符占一个字节的内存空间。字符串占的内存字节数等于字符串中字节数加1。增加的一个字节中存放字符"\0" (ASCII码为0)。这是字符串结束的标志

数据类型转换
