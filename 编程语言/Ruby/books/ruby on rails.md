

## Ruby on Rails


```ruby
class MessagesController < ApplicationController
  def show
    @tag = Tag.find(params[:id])
    @destinations = @tag.destinations
  end

 def show
  @tag = Tag.find(params[:id])
  @destinations = @tag.destinations
end
def edit
  @destination = Destination.find(params[:id])
end
def update
  @destination = Destination.find(params[:id])
  if @destination.update_attributes(destination_params)
    redirect_to(:action => 'show', :id => @destination.id)
  else
    render 'edit'
  end
end
private
  def destination_params
    params.require(:destination).permit(:name, :description)
  end
end
```


```ruby
get 'messages' => 'messages#index'
get 'messages/new' => 'messages#new'
post 'messages' => 'messages#create'
get '/tags/:id' => 'tags#show', as: :tag  # as: means name this route
get '/destinations/:id/edit' => 'destinations#edit', as: :edit_destination # y giving the edit route a name of "edit_destination", Rails automatically creates a helper method named edit_destination_path. Use it to generate a URL to a specific destination's edit pat
patch '/destinations/:id' => 'destinations#update'
```

create view

app/views/pages/home.html.erb
app/assets/stylesheets/pages.css.scss

new.html.erb

```html

<div class="cards row">
     <% @tags.each do |t| %>
     <div class="card col-xs-4">
       <%= image_tag t.image %>
       <h2><%= t.title %></h2>
       <%= link_to "Learn more", tag_path(t) %>
     </div>
     <% end %>
</div>
```


### 连接数据库



change 方法表示如何更新数据库，create_table 方法表示创建数据库
t.text 表示创建一个名为 content 的字段
t.timestamps 表示创建created_at 和 updated_at 两个会自动更新的字段

```ruby
class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.text :content
      t.string :title
      t.references :tag # 引用
      t.timestamps
    end
  end
end
```

接着


其它

models

建立多个 Model的关系

一对多关系

Tag

```ruby
class Tag < ActiveRecord::Base
	has_many :destinations #表示一个 tag 可以由多个 Destination
end
```

Destination

```ruby
class Destination < ActiveRecord::Base
	belongs_to :tag #表示每个 Destination 属于一个 Tag
end
```
