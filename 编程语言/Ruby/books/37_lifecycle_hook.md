[TOC]

### LifeCycle Hook

元编程中可能需要监控方法的声明周期，这时需要使用 hook 方法。

#### method_added

`method_added` 用于当类中增加了新的实例方法时被调用。

```ruby
class Dojo
  @@method_added = []

  def self.method_added(method_name)
    puts "a new method '#{method_name}' has added"
    @@method_added << method_name
  end

  def self.all_added_methods
    "class contains #{@@method_added}"
  end

  def foo

  end

  def bar

  end
end

puts Dojo.all_added_methods
```

以上结果输出

```
a new method 'foo' has added
a new method 'bar' has added
class contains [:foo, :bar]
```

#### singleton_method_added

`singleton_method_added` 用于监控单例方法的添加。

```ruby
d1 = Dojo.new

def d1.singleton_method_added(method_name)
  puts "a new singleton method '#{method_name}' has added"
  @singleton_methods_added ||= []
  @singleton_methods_added << method_name
end

def d1.all_added_singleton_methods
  "instance contains #{@singleton_methods_added}"
end

def d1.inc(x)
  x+ 1
end

puts d1.inc(3)
puts d1.all_added_singleton_methods
```

以上输出

```
a new singleton method 'singleton_method_added' has added
a new singleton method 'all_added_singleton_methods' has added
a new singleton method 'inc' has added
4
instance contains [:singleton_method_added, :all_added_singleton_methods, :inc]
```

#### 其它方法

与之类似还有定义方法的删除的 `methods_removed` 和方法未定义的 `methods_undefined` 以及这两个的单例版本。

#### 模块或类的引入

`included()` 用于监控一个模块或类的引用情况。

```ruby
module SparringArea
  @@included_into = []

  def self.included_into
    @@included_into
  end

  def self.included(class_or_module)
    puts "class_or_module '#{class_or_module}' is included"
    @@included_into << class_or_module
  end
end

include SparringArea
puts SparringArea.included_into
```

#### 对象的扩展

`extended()` 用于监控对象从某一个模块中进行扩展。

```ruby
class Dojo
end

module SparringArea
  @@extended_objects = []

  def self.extended_objects
    @@extended_objects
  end

  def self.extended(object)
    puts "object '#{object}' is extended"
    @@extended_objects << object
  end
end

dojo1 = Dojo.new
dojo2 = Dojo.new
dojo1.extend(SparringArea)
dojo2.extend(SparringArea)
```

#### 继承

`inherited()` 用于监控一个类是否被继承。

```ruby
class Room
  @@subclasses = []

  def self.subclasses
    @@subclasses
  end

  def self.inherited(subclass)
    puts "subclass '#{subclass}' inherits Room"
    @@subclasses << subclass
  end
end
class SmallRoom < Room
end
```
