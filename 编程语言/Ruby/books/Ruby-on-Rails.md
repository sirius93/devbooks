[TOC]


## Quick Start

创建工程，默认使用 sqlite

```bash
rails new <site_name> [-d sqlite|mysql|oracle|postgresql]
```

安装依赖，依赖信息来自 Gemfile

```bash
cd <site_name>
bundle install
```

启动服务

```bash
rails server
```

启动后通过 `http://localhost:3000` 进行访问。

## 程序结构

- `app` 存放业务逻辑
- `config` 存放配置文件
  - `environments` 存放对应环境的配置文件
  - `database.yml` 配置数据库信息
  - `routes.rb` 路由配置
  - `secrets.yml` session 加密方式
- `db` 数据库文件，如果用的是 sqlite 则存放在这里
  - `migrate` 数据库迁移文件
  - `seed.rb` 创建初始数据
- `lib` 扩展库
- `log` 日志文件
- `public` 静态文件
- `test` 测试文件
- `vendor` 第三方代码库
- `Gemfile`

erb 是 Ruby 的标准库（Standard Library）之一， 它允许是把 Ruby 代码签入到 html 中。

## rails 命令

### 分类

- `generate`    生成资源文件 (简写 "g")
- `console`     运行调试控制台 (简写 "c")
- `server`      运行 Rails 服务 (简写 "s")
- `dbconsole`   运行数据库调试控制台 (简写 "db")
- `new`         创建新的 Rails 项目。 你也可以 "rails new ." 它会在当前目录下创建
- `destroy`     删除 "generate" 创建的文件 (简写 "d")
- `plugin new ` 创建一个 plugin  
- `runner`      在 Rails 环境下，执行一段 Ruby 代码


查看帮助

```bash
rails generate -h
```

## 资源

Rails 中 controller, model, assets 都是一个资源的不同部分，可以一个个创建，也可以一起创建。

### scaffold 命令

用于创建一个完整的资源，属性列表中属性名和类型以 ":" 分隔，默认类型为 string

```bash
rails g scaffold [资源名] [属性列表] [选项]
```

例

```bash
rails g scaffold variants product_id:integer price:decimal{'8,2'} size
```

### CSS 加载图片

三种方式

直接使用地址

```css
background-image: url("/assets/logo.png");
```

加载 public 目录下的资源

```css
background-image: url("/images/logo.png");
```

加载 assets 目录下的资源

```css
background-image: asset-url('logo.png');
```


## Controller

### 创建 Controller

```bash
rails generate controller <controller_name> [methods...]
```

例

```bash
rails g controller home index welcome about contact
```

执行完成后会生成以下文件

```
app/controllers/messages_controller.rb
app/views/messages
test/controllers/messages_controller_test.rb
app/helpers/messages_helper.rb
app/assets/javascripts/messages.coffee
app/assets/stylesheets/messages.scss
```

### 创建 Action

Action 用于接收特定的路由并执行特定的方法。Rails 含有 7 种内置的 Action。

| Http方法 | 路径 | Action | 作用 |
|:--------:|:--------:|:--------:|:--------:|
|    GET    |  /messages      |  messages#index     |  显示数据列表      |
|    GET    |  /messages/new      |  messages#new      |  返回用于创建数据的表单      |
|    POST    |  /messages      |  messages#create      |  创建数据      |
|    GET    |  /messages/:id     |  messages#show     |  显示某条数据      |
|    GET    |  /messages/:id/edit      |  messages#edit      |  返回用于编辑数据的表单     |
|    PATCH/PUT    |  /messages/:id      |  messages#update      |  更新某条数据，PATCH 基本同 PUT 但可以用于局部更新      |
|    DELETE    |  /messages/:id      |  messages#destroy      |  删除某条数据      |

例

```ruby
class MessagesController < ApplicationController
  def index
    @messages = Message.all
  end

  def new
    @message = Message.new
  end

  def create
    @message = Message.new(message_params)
    if @message.save
      redirect_to '/messages'
    else
      render 'new'
    end
  end

  private
  def message_params
    params.require(:message).permit(:content)
  end
end
```


## Model

### Active Record 模式

一个 Active Record（简称 AR）对象包含了持久数据（保存在数据库中的数据）和数据操作（对数据库里的数据进行操作）。

Active Record 就是个 ORM 框架。

### Active Record 中的约定

命名约定

数据表名：复数，下划线分隔单词（例如 book_clubs）
模型类名：单数，每个单词的首字母大写（例如 BookClub）

Schema 约定

外键 - 使用 singularized_table_name_id 形式命名，例如 item_id，order_id。创建模型关联后，Active Record 会查找这个字段；
主键 - 默认情况下，Active Record 使用整数字段 id 作为表的主键。使用 Active Record 迁移创建数据表时，会自动创建这个字段；

在数据库字段命名的时候，有几个特殊意义的名字，尽量回避：

created_at - 创建记录时，自动设为当前的时间戳
updated_at - 更新记录时，自动设为当前的时间戳
lock_version - 在模型中添加乐观锁定功能
type - 让模型使用单表继承，给字段命名的时候，尽量避开这个词
`(association_name)_type` - 多态关联的类型
`(table_name)_count` - 保存关联对象的数量。例如，posts 表中的 comments_count 字段，Rails 会自动更新该文章的评论数

### Migration

前缀是时间戳，他们按照时间的先后顺序排列，当运行数据库迁移时，他们按照时间顺序先后被执行。

rake db:migrate 执行迁移，这里会判断，哪个迁移文件是还没有被执行的。
rake db:rollback [STEP=n] 回滚迁移个数
rake db:drop
rake db:create

### 创建 Model

```bash
rails generate model <model_name>
```

例

```bash
rails generate model Message
```

执行完后会生成以下文件

```
db/migrate/20160119095411_create_messages.rb
app/models/message.rb
test/models/message_test.rb
test/fixtures/messages.yml
```

### CRUD in 命令行

#### 插入数据

```bash
# 第一种
product = Product.create

# 第二种
product = Product.new
product.save
```

插入失败后返回的是暂态的实例，可以使用以下方法查看错误信息

```bash
product.errors.full_messages
product.invalid?
```

#### 查询数据

##### Find

Rails 给我们提供了这些常用的查询方法：

方法名称 | 含义 | 参数 | 例子 | 找不到时
--- | --- | --- | --- | ---
find | 获取指定主键对应的对象 | 主键值 | Product.find(10) | 异常
take | 获取一个记录，不考虑任何顺序 | 无 | Product.take | nil
first | 获取按主键排序得到的第一个记录 | 无 | Product.first | nil
last | 获取按主键排序得到的最后一个记录 | 无 | Product.last | nil
find_by | 获取满足条件的第一个记录 | hash | Product.find_by(name: "T恤") | nil

表中的四个方法不会抛出异常，如果需要抛出异常，可以在他们名字后面加上 `!`，比如 Product.take!。

如果将上面几个方法的参数改动，我们就会得到集合：

方法名称 | 含义 | 参数 | 例子 | 找不到时
--- | --- | --- | --- | ---
find | 获取指定主键对应的对象 | 主键值集合 | Product.find([1,2,3]) | 异常
take | 获取一个记录，不考虑任何顺序 | 个数 | Product.take(2) | []
first | 获取按主键排序得到的第N个记录 | 个数 | Product.first(3) | []
last | 获取按主键排序得到的最后N个记录 | 个数 | Product.last(4) | []
all | 获取按主键排序得到的全部记录 | 无 | Product.all | []

Rails 还提供了一个 find_by 的查询方法，它可以接收多个查询参数，返回符合条件的第一个记录。比如：

```
Product.find_by(name: 'T-Shirt', price: 59.99)
```

`find_by` 有一个常用的变形，比如：

```
Product.find_by_name("Hat")
Product.find_by_name_and_price("Hat", 9.99)
```

如果需要查询不到结果抛出异常，可以使用 `find_by!`。通常，以`!`结尾的方法都会抛出异常，这也是一种约定。不过，直接使用 find，会查询主索引，查询不到直接抛出异常，所以是没有 `find!` 方法的。

使用 find_by 的时候，还可以使用 sql 语句，比如：

```
Product.find_by("name = ?", "T")
```

这是一个有用的查询，当我们搜索多个条件，并且是 OR 关系时，可以这样做：

```
User.find_by("id = ? OR login = ?", params[:id], params[:id])
```

这句话还可以改写成：

```
User.find_by("id = :id OR login = :name", id: params[:id], name: params[:id])
```

或者更简洁的：

```
User.find_by("id = :q OR login = :q", q: params[:id])
```

##### Where

查询形式 | 实例
--- | ---
数组（Array）查询 | Product.where("name = ? and price = ?", "T恤", 9.99)
哈希（hash）查询 | Product.where(name: "T恤", price: 9.99)
Not查询 | Product.where.not(price: 9.99)
空 | Product.none

使用 where 查询，常见的还有模糊查询：

```
Product.where("name like ?", "%a%")
```

查询某个区间：

```
Product.where(price: 5..6)
```

以及上面提到的，sql 的查询：

```
Product.where("color = ? OR price > ?", "red", 9)
```

#### 更新

类方法是 update，比如：

```
Product.update(1, name: "T-Shirt", price: 23)
```

1 是更新目标的 ID，如果该记录不存在，update 会抛出 `ActiveRecord::RecordNotFound` 异常。

`update` 也可以更新多条记录，比如：

```
Product.update([1, 2], [{ name: "Glove", price: 19 }, { name: "Scarf" }])
```

如果要更新全部记录，可以使用 update_all ：

```
Product.update_all(price: 20)
```

在使用 update 更新记录的时候，会调用 Model 的 validates（校验） 和 callbacks（回调），保证我们写入正确的数据，这个是定义在 Model 中的方法。但是，update_all 会略过校验和回调，直接将数据写入到数据库中。

和 update_all 类似，update_column/update_columns 也是将数据直接写入到数据库，它是一个实例方法：

```
product = Product.first
product.update_column(:name, "")
product.update_columns(name: "", price: 0)
```

虽然为 product 增加了 name 非空的校验，但是 update_column(s) 还是可以讲数据写入数据库。

当我们创建迁移文件的时候，Rails 默认会添加两个时间戳字段，created_at 和 updated_at。

当我们使用 update 更新记录时，触发 Model 的校验和回调时，也会自动更新 updated_at 字段。但是 Model.update_all 和 model.update_column(s) 在跳过回调和校验的同时，也不会更新 updated_at 字段。

我们也可以用 save 方法，将新的属性保存到数据库，这也会触发调用和回调，以及更新时间戳：

```
product = Product.first
product.name = "Shoes"
product.save
```

#### 删除记录

##### Delete 删除

使用 delete 删除时，会跳过回调，以及关联关系中定义的 `:dependent` 选项，直接从数据库中删除，它是一个类方法，比如：

```
Product.delete(1)
Product.delete([2,3,4])
```

当传入的 id 不存在的时候，它不会抛出任何异常，看下它的源码：

```
# File activerecord/lib/active_record/relation.rb, line 502
def delete(id_or_array)
  where(primary_key => id_or_array).delete_all
end
```

它使用不抛出异常的 where 方法查找记录，然后调用 delete_all。

delete 也可以是实例方法，比如：

```
product = Product.first
product.delete
```

在有具体实例的时候，可以这样使用，否则会产生 `NoMethodError: undefined method `delete' for nil:NilClass`，这在我们设计逻辑的时候要注意。

delete_all 方法和 delete 是一样的，直接发送数据删除的命令，看一下 api 文档中的例子：

```ruby
Post.delete_all("person_id = 5 AND (category = 'Something' OR category = 'Else')")
Post.delete_all(["person_id = ? AND (category = ? OR category = ?)", 5, 'Something', 'Else'])
Post.where(person_id: 5).where(category: ['Something', 'Else']).delete_all
```

##### Destroy 删除

destroy 方法，会触发 model 中定义的回调（before_remove, after_remove , before_destroy 和 after_destroy），保证我们正确的操作。它也可以是类方法和实例方法，用法和前面的一样。

需要说明，delete/delete_all 和 destroy/destroy_all 都可以作用在关系查询结果，也就是`ActiveRecord::Relation`上，删掉查找到的记录。

如果你不想真正从数据库中抹掉数据，而是给它一个删除标注，可以使用 [https://github.com/radar/paranoia](https://github.com/radar/paranoia) 这个 gem，他会给记录一个 deleted_at 时间戳，并且使用 `restore` 方法把它从数据库中恢复过来，或者使用 `really_destroy!` 将它真正的删除掉。


#### 高级查询

##### N+1

```ruby
users = User.limit(10)

users.each do |user|
  puts user.address.phone
end
```

以上代码在 each 中又会发出 sql 语句，改写成以上形式后 sql 语句就只会有两句

sql 预加载

```ruby
users = User.includes(:address).limit(10)
```

##### Scope

Scope 可以将 where 中的多个条件进行重复使用。

```ruby
Product.where(name: "T-Shirt", hot: true, top: true)
```

使用 Scope 后

```ruby
class Product < ActiveRecord::Base
  scope :hot, -> { where(hot: true) }
  scope :top, -> { where(top: true) }
end

Product.top.hot.where(name: "T-Shirt")
```

##### 直接使用 SQL

```ruby
Client.find_by_sql("SELECT * FROM clients
  INNER JOIN orders ON clients.id = orders.client_id
  ORDER BY clients.created_at desc")

Client.connection.select_all("SELECT first_name, created_at FROM clients WHERE id = '1'")
```

##### pluck

pluck 可以直接在 Relation 实例的基础上，使用 sql 的 select 方法，得到字段值的集合（Array），而不用把返回结果包装成 ActiveRecord 实例，再得到属性值。在查询属性集合时，pluck 的性能更高。

```ruby
Client.where(active: true).pluck(:id) #=> SELECT id FROM clients WHERE active = 1
```

##### ids

ids 返回主键集合

```ruby
Person.ids
```

##### 查询记录数量

```ruby
Client.exists?(1)
Client.exists?(id: [1,2,3])
Client.exists?(name: ['John', 'Sergei'])

Article.where(published: true).any?
Article.where(published: true).many?

Client.count
Client.average("orders_count")
Client.minimum("age")
Client.maximum("age")
Client.sum("orders_count")
```

#### Relation

##### 一对一关系

```ruby
class User < ActiveRecord::Base
  has_one :address
end

class Address < ActiveRecord::Base
  belongs_to :user
end
```

在一对一关系中，`belongs_to :user` 中，`:user` 是单数，`has_one :address` 中，`:address` 也是单数。



TODO

https://github.com/liwei78/rails-practice/blob/master/Chapter_4/4.3.md



## View

创建 Controller 后会自动创建对应的 View，目录位于 `app/views` 中。
在 View 中可以直接访问 Controller 中定义的成员变量。

### layouts

`app/views/layouts` 里存放的是网站的布局文件，如果只有一种布局使用 `application.html.erb` 就可以了，也可以为每个资源创建单独的文件 `app/views/layouts/products.html.erb`。

#### yield

`<%= yield %>` 默认将模板文件填充到当前位置。

`<%= yield(:title) %>` 将模板文件中的 `:title` symbol 填充到当前位置。
`<%= content_for?(:title) %>` 判断模板文件中是否存在 `:title` symbol。

例

```ruby
<title><%= content_for?(:title) ? yield(:title) : "Shop" %></title>
```

#### 使用自定义布局

controller 使用自定义布局

```ruby
class AdminController < ApplicationController
  layout "admin"
```

以上对应的资源为 `app/views/layouts/admin.html.erb`。

action 使用自定义布局

```ruby
def new
  @product = Product.new
  render layout: "another_layout"
end
```

#### 不使用布局

```ruby
def edit
  render layout: false
end
```

### templates

`app/views/<source name>` 中定义的是各种模板文件，在 `layouts` 中可以通过 `yield` 将模板文件填充到布局文件中。

#### content_for

定义 symbol 内容，以便用于填充 layouts 中对应的内容

例

```ruby
<%= content_for :title do %>
    自定义标题
<% end %>
```

### helper 方法

helper 方法可以用在布局和模板中，以上提到的 `yield` 和 `content_for` 都是 helper 方法。

#### link_to

例

```ruby
<%= link_to "Link1", "/path1"  %>
<%= link_to "网店演示", root_path, class: "navbar-brand" %>
```

复杂的用法

```ruby
<%= link_to "删除", product, :method => :delete, :data => { :confirm => "点击确定继续" }, :class => 'btn btn-danger btn-xs' %></li>
```

#### image_tag

例

```ruby
image_tag("icon.png") #=>  <img alt="Icon" src="/assets/icon.png" />
image_tag("/icons/icon.gif", height: '32', width: '32') #=> <img alt="Icon" height="32" src="/icons/icon.gif" width="32" />
```

#### auto_discovery_link_tag

用于增加 rss 和 atom 订阅连接，有两种添加方式

第一种，添加在 `<head>` 中

```ruby
<head>
<%= auto_discovery_link_tag(:rss, {controller: "products", action: "index"}, {title: "RSS Feed"}) %>
<%= auto_discovery_link_tag(:atom, {controller: "products", action: "index"}, {title: "ATOM Feed"}) %>
</head>
```

第二种，添加在页面中

```ruby
<%= link_to "rss", products_url(format: "rss") %>
<%= link_to "atom", products_url(format: "atom") %>
```

定义 rss 和 atom，在资源目录中创建 `index.rss.builder` 和 `index.atom.builder` 目录，这表示当请求地址结尾为 `rss` 时访问 `rss` 格式。

index.atom.builder

```ruby
atom_feed do |feed|
  feed.title "商品列表"
  feed.updated @products.maximum(:updated_at)

  @products.each do |product|
    feed.entry product, published: product.updated_at do |entry|
      entry.title product.name
      entry.content product.description
      entry.price product.price
    end
  end
end
```

index.rss.builder

```ruby
xml.instruct! :xml, version: "1.0"
xml.rss version: "2.0" do
  xml.channel do
    xml.title "商品列表"
    xml.description "这是商品列表"
    xml.link products_url

    @products.each do |product|
      xml.item do
        xml.title product.name
        xml.description product.description
        xml.price product.price
        xml.link product_url(product)
        xml.guid product_url(product)
      end
    end
  end
end
```

可以访问 ` http://localhost:3000/products.rss` 来观察结果。

#### stylesheet_link_tag

例

```ruby
<head>
<%= stylesheet_link_tag "simplex", :media => "all" %>
</head>
```

#### javascript_include_tag

例

```ruby
<%= javascript_include_tag "application" %>
<%= yield(:page_javascript) if content_for?(:page_javascript) %>
```

### 局部模板（partial）

```
<% @products.each do |product| %>
  <%= render partial: "product", locals: { product: product } %>
<% end %>
```

partial 指定了使用哪个模板，locals 向模板里传递了一个变量。以上例子中使用了 `_product.html.erb` 模板。

### 表单

#### 表单辅助方法

```ruby
<%= text_area_tag(:message, "Hi, nice site", size: "24x6") %>
<%= password_field_tag(:password) %>
<%= hidden_field_tag(:parent_id, "5") %>
<%= search_field(:user, :name) %>
<%= telephone_field(:user, :phone) %>
<%= date_field(:user, :born_on) %>
<%= datetime_field(:user, :meeting_time) %>
<%= datetime_local_field(:user, :graduation_day) %>
<%= month_field(:user, :birthday_month) %>
<%= week_field(:user, :birthday_week) %>
<%= url_field(:user, :homepage) %>
<%= email_field(:user, :address) %>
<%= color_field(:user, :favorite_color) %>
<%= time_field(:task, :started_at) %>
<%= number_field(:product, :price, in: 1.0..20.0, step: 0.5) %>
<%= range_field(:product, :discount, in: 1..100) %>
```

以上对应

```html
<textarea id="message" name="message" cols="24" rows="6">Hi, nice site</textarea>
<input id="password" name="password" type="password" />
<input id="parent_id" name="parent_id" type="hidden" value="5" />
<input id="user_name" name="user[name]" type="search" />
<input id="user_phone" name="user[phone]" type="tel" />
<input id="user_born_on" name="user[born_on]" type="date" />
<input id="user_meeting_time" name="user[meeting_time]" type="datetime" />
<input id="user_graduation_day" name="user[graduation_day]" type="datetime-local" />
<input id="user_birthday_month" name="user[birthday_month]" type="month" />
<input id="user_birthday_week" name="user[birthday_week]" type="week" />
<input id="user_homepage" name="user[homepage]" type="url" />
<input id="user_address" name="user[address]" type="email" />
<input id="user_favorite_color" name="user[favorite_color]" type="color" value="#000000" />
<input id="task_started_at" name="task[started_at]" type="time" />
<input id="product_price" max="20.0" min="1.0" name="product[price]" step="0.5" type="number" />
<input id="product_discount" max="100" min="1" name="product[discount]" type="range" />
```

#### 搜索表单

格式

`form_tag(action_path, method)`

或者

`form_tag({action: action_path}, method)`

其中第二种方式如果找不到路由会报 `no route`。

例

```
<%= form_tag(products_path, method: "get") do %>
    <%= label_tag(:query) %><!--query fro label (for)-->
    <%= text_field_tag(:query) %><!--query for textfield (name,id)-->
    <%= submit_tag("搜索") %>
<% end %>
```

#### ransack

ransack gem 可以帮助进行检索。

Gemfile

```
gem 'ransack'
```

页面

```
<%= form_tag products_path, method: :get, class: "navbar-form navbar-left" do %>
    <div class="form-group">
      <%= text_field_tag "q[name_cont]", params["q"] && params["q"]["name_cont"], class: "form-control input-sm search-form", placeholder: "输入商品名称" %>
    </div>
<% end %>
```

controller

```ruby
def index
  @q = Product.ransack(params[:q])
  @products = @q.result(distinct: true)
end
```

#### 绑定模型到表单

将一个资源和表单绑定，以下将 controller 中的 @product 和它绑定。form_for 会判断 @product 是否为一个新的实例（你可以看看 @product.new_record?），从而将 form 的地址指向 create 还是 update 方法。
f 是一个表单构造器（Form Builder）实例。

```
<%= form_for @product, :html => { :class => 'form-horizontal' } do |f| %>
```

可以使用 [simple_form](https://github.com/plataformatec/simple_form) 以节省更多的代码。

#### 注册和登录

可以使用 [devise](https://github.com/plataformatec/devise)

Gemfile

```
gem 'devise'
```

执行命令

```bash
rails generate devise:install User
rails generate devise User
rails g devise:views
rake db:migrate
```

## Route

路由位于 `app/config/routes.rb` 中。

### 基于路由

基本语法为

```ruby
<method> <url_resources> => <controller_name>#<action_name>
```

示例

```ruby
get 'messages'  => 'messages#index'
get 'messages/new'  => 'messages#new'
post 'messages' =>  'messages#create'
```

或者也可以使用如下形式

```ruby
get 'home/index'
get 'home/welcome'
```

或者

```ruby
get '/welcome', to: 'home#welcome'
```

如果是根路径还可以采用以下方式定义

```ruby
root 'home#index'
```

第一行表示的是当 Http 路径为 `http://localhost:3000/messages` 时访问 `messages_controller` 中的 `index` 函数。

除了 Rest 风格也可以使用自定义的格式

```ruby
get '/custom_path/:controller/:name/:action'
```

以上会被这样解析：custom_path 为自定义前缀，:controller 代表 controller 的名字，:name 是参数，:action 是 controller 中的方法。

### resources

#### 基于 resources 的路由

```ruby
resources :products
```

查看该 resources 所生成的路由

```bash
rake routes | grep product
```

以上会默认创建以下路由

```
products GET    /products(.:format)          products#index
            POST   /products(.:format)          products#create
new_product GET    /products/new(.:format)      products#new
edit_product GET    /products/:id/edit(.:format) products#edit
    product GET    /products/:id(.:format)      products#show
            PATCH  /products/:id(.:format)      products#update
            PUT    /products/:id(.:format)      products#update
            DELETE /products/:id(.:format)      products#destroy
```

#### 扩展 resources

```ruby
resources :products do
  collection do
    get :top
  end
  member do
    post :buy
  end
end
```

以上会添加两个新的路由

```
top_products GET    /products/top(.:format)                        products#top
buy_product POST   /products/:id/buy(.:format)                    products#buy
```

即 `collection` 是在 products 上添加方法，而 `member` 是在某一个具体的 product 上添加方法。

#### 单个 resources

对应单个资源可以使用单数进行声明，这时生成的路由是不带 `:id` 的。

例

```
resource :profile
```

#### 指定 Action

resources 默认生成 7 个 action，可以通过以下形式只生成指定的 action。

其中 `only` 表示需要的方法,`except` 表示不需要的方法。

```ruby
resources :users, only: [:index, :show]
resources :products, except: [:destroy]
```

#### 嵌套的路由

即访问某个 resource 下的子 resource

```ruby
resources :products do
  resources : variants
end
```

以上嵌套形式会生成以下路由

```
product_variants GET    /products/:product_id/variants(.:format)          variants#index
                   POST   /products/:product_id/variants(.:format)          variants#create
new_product_variant GET    /products/:product_id/variants/new(.:format)      variants#new
edit_product_variant GET    /products/:product_id/variants/:id/edit(.:format) variants#edit
   product_variant GET    /products/:product_id/variants/:id(.:format)      variants#show
                   PATCH  /products/:product_id/variants/:id(.:format)      variants#update
                   PUT    /products/:product_id/variants/:id(.:format)      variants#update
                   DELETE /products/:product_id/variants/:id(.:format)      variants#destroy
```

#### 命名空间

```ruby
namespace :admin do
  resources :products
end
```

之后其下的资源都必须通过 `http://localhost:3000/admin/ ` 来访问。

反过来，使用以下方式则 `/admin/articles` 会访问 `ArticleController` 的资源。

```ruby
scope '/admin' do
  resources :articles
end
```

#### 引用定义好的资源

```ruby
# 定义可以被引用的资源
concern :commentable do
  resources :comments
end

# 引用资源
resources :messages, concerns: :commentable
resources :articles, concerns: [:commentable, :image_attachable]
```

#### 路由中的参数

*:as 别名*

创建别名地址

```ruby
get 'home/welcome', as: :welcome
```

*shallow*

```ruby
resources :products do
  resources :comments, shallow: true
end
```

它把 index、new 和 create 方法保留在了 products/:id 这个资源下，而把其他方法，重新放回到 /comments 下。这样的考虑是避免过多的实用嵌套 routes。

*constraints*

```ruby
get 'photos/:id', to: 'photos#show', constraints: { id: /[A-Z]\d{5}/ }
```

这时，id 为 A 到 Z 开头，且后面为5位数字的 id，才符合路由条件，转入到 show 方法。而 `products/A123456` 将会提示 "No route matches"。

## DB

创建 Model 后会自动生成 DB 文件，文件位于 `app/db` 中。其中共有三种文件。

`migrate` 下保存用于修改数据库 Schema 的程序。
`schema.rb` 保存数据库的 Schema 文件。
`seeds.rb` 用于向数据库插入测试数据。

通过 `migrate` 文件更新数据库结构，其中 rake 是 Ruby 中类似 make 的工具

```ruby
rake db:migrate
```

使用 `seeds` 创建测试数据

```ruby
rake db:seed
```

常用 rake 的指令

```ruby
rake db:create   # 创建数据库
rake db:migrate  # 更新数据库，更新的文件来自 db/migrate/
rake db:seed     # 执行 seed.rb 文件的内容，通常是创建一个默认的数据
rake db:drop     # 删除数据库
rake routes      # 列出所有路由
```

以上命令默认在开发环境执行，要想在产品环境执行需要使用以下代码：


```bash
RAILS_ENV=production rake db:migrate
```


### 创建表

修改 `20160119095411_create_messages.rb`

```ruby
class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.text :content
      t.string :title
      t.timestamps null: false
    end
  end
end
```

### 创建测试数据

```
messages = Message.create([{title: 'msg 01', content: 'content 01'}, {title: 'msg 02', content: 'content 02'}, {title: 'msg 03', content: 'content 03'}])
```

## Ajax 请求



## 测试

### minitest

scaffold 默认使用 minitest，会在 `test` 目录下生成测试文件。

### Rspec

#### 安装

Gemfile

```
group :development, :test do
  gem 'rspec-rails'
end
```

生成 rspec 结构，测试文件保存在 `spec` 目录下。

```bash
rails generate rspec:install
```

添加测试文件

```
rails generate rspec:<type> <name>
```

例

```bash
rails generate rspec:model product
rails generate rspec:controller products
```

进行测试

```bash
rspec
```

## 参考资料

- [Rails API](http://api.rubyonrails.org/)
