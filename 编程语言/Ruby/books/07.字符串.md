[TOC]

### 定义字符串

Ruby 可以使用单引号或双引号定义字符串，其中单引号字符串不支持转义也不支持字符串插值。
可以使用 '\' 或者单引号和双引号的交替来避免特殊字符。

```ruby
s = 'Hello World'
ds = "Hello World"
```

字符串间可以使用 `+` 或 `<<` 进行连接

```ruby
puts 'foo' + 'bar'
puts 'foo' << 'bar'
```

### 遍历字符

```ruby
for c in s.chars
  puts c
end
```

### 多行字符串

`%q{}` 表示单引号字符串，`%Q{}` 表示双引号字符串

```ruby
text = %q{
    1, 2, 3
    one, two, three
    "x, "y", "z"
}
dtext = %Q{
    1, 2, 3
    one, two, three
    "x, "y", "z"
}
puts text
puts dtext
```

也可以使用 Symbol 来定义多行字符串，格式为 `<<SYMBOL` 开始到 `SYMBOL` 结束，支持字符串插值。

```ruby
symbol_text = <<NAME
    1, 2, 3, #{s}
    one, two, three
    "x, "y", "z"
NAME
```

也可以同时定义多个 Symbol

```ruby
puts <<'foo',<<'bar'
"hello"
foo
'world'
bar
```

### 字符串插值 String Interpolation

字符串插值使用符号 `#{}` 定义，且不可以省略括号。只有双引号字符串支持字符串插值。

```ruby
name = 'Peter'
str = "name=#{name}, #{if name.length>10; else name.length end}"
puts str  # => name=Peter, 5

single_quote = 'name=#{name}, #{if name.length>10; else name.length end}'
puts single_quote  # => name=#{name}, #{if name.length>10; else name.length end}
```

### 常用方法

#### 截取字符串

左右都是闭区间

```ruby
puts 'Hello World'[6..9]  # => Worl
```

#### 格式化字符串

```ruby
puts 'name=%s&length=%d' % [name, name.length]  # => name=Peter&length=5
```

#### 替换字符串

`gsub()` 替换全部，`sub()` 替换第一个匹配的，默认返回字符串的拷贝，`!` 表示修改原来的字符串

```ruby
puts 'Hello World'.gsub!(/o/, '-')  # => Hell- W-rld
```

#### 重复

```ruby
puts 'a'  # => aaa
```
