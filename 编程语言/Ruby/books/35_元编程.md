[TOC]

### 元编程

类似 Java 的反射

#### 修改方法

Ruby 元编程支持动态增加方法，重定义方法和删除方法。

```ruby
class EmptyClass
end

# 添加方法
class EmptyClass
  def everything_changes
    'Wait, what? You just added a method to me!'
  end
end

puts EmptyClass.new.everything_changes

# 重定义方法
class EmptyClass
  def everything_changes
    'Redefine everything changes!'
  end
end

puts EmptyClass.new.everything_changes
```

#### 动态方法调用

动态方法调用主要依靠 `send()` 方法，该方法第一个参数为需要调用的方法名。

```ruby
class Glider
  def lift
    'do lift'
  end

  def turn(dir)
    "do turn #{dir}"
  end
end
class Nomad
  def initialize(glider)
    @glider = glider
  end

  def do(action, args = nil)
    if args==nil
      @glider.send(action)
    else
      @glider.send(action, args)
    end
  end
end

nomad = Nomad.new(Glider.new)
puts nomad.do('lift')
puts nomad.do('turn', 'left')
```

#### 方法缺失

动态方法调用时很可能调用不存在的方法，这是就会发生 `method missing` 的异常。
此时可以通过定义 `method_missing()` 方法来处理所有不存在的方法的调用。

```ruby
class Spy
  def method_missing(method_name, *args, &block)
    puts "#{method_name} was called with #{args} and #{block}"
  end
end
spy = Spy.new
spy.hello('hello', 'world') { |n| puts n } # => hello was called with ["hello", "world"] and #<Proc:
```

#### send 与代理模式

`send()` 结合方法缺失可以实现基本的代理模式。

```ruby
class Wrapper
  def initialize(agent)
    @agent = agent
  end

  def method_missing(method_name, *args, &block)
    @agent.send(method_name, *args)
  end
end

wrapper= Wrapper.new([10, 20, 30])
puts wrapper.max # => 30
```

#### 另一种定义方法的方式

```ruby
class Doctor
  define_method('perform_rhinoplasty') do |argument|
    "performing rhinoplasty on #{argument}"
  end

  define_method('perform_checkup') do |argument|
    "performing checkup on #{argument}"
  end

  define_method('perform_interpretive_dance') do |argument|
    "performing interpretive dance on #{argument}"
  end
end

doctor = Doctor.new
puts doctor.perform_rhinoplasty("nose") # => performing rhinoplasty on nose
puts doctor.perform_checkup("throat") # => performing checkup on throat
puts doctor.perform_interpretive_dance("in da club") # => performing interpretive dance on in da club
```

这种方式最大的特点就是可以通过 `each` 语句同时定义多个方法。以上示例可以写成以下形式：

```ruby
class SuperDoctor
  ["rhinoplasty", "checkup", "interpretive_dance"].each do |action|
    define_method("perform_#{action}") do |argument|
      "performing #{action.gsub('_', ' ')} on #{argument}"
    end
  end
end
```

#### Inspect Method

```ruby
class Mock
  def add(x, y)
    x+y
  end

  private
  def info
    'info'
  end
end

mock = Mock.new
method_add = mock.method(:add)
p method_add
p method_add.parameters
# the object on which the method is bound
p method_add.receiver
# the class that object belongs to
p method_add.owner

p mock.methods
p mock.public_methods
```

`method(sym)` 用于获得对象拥有的方法
`parameters()` 用于获得方法对象的参数
`receiver()` 用于获得该方法绑定的对象
`owner()` 用于获得定义该方法的类

#### 常量引用

`const_get()` 可以用于获得已定义的常量对象。不属于任何类的常量存在于 `Module` 对象中。

```ruby
class Mock
  Foo = 'foo'
end

CONSTANT_A = 100
puts CONSTANT_A
puts Module.const_get('CONSTANT_A')
puts Mock.const_get('Foo')
```

由于类名通常也是常量，所以也可以通过这种方式创建对象

```ruby
puts Module.const_get('Mock').new.add(2, 3)
```


#### 成员变量引用

类似常量，成员变量使用 `instance_variable_get()` 进行引用。

```ruby
p mock.instance_variable_get('@sum')
```
