[TOC]

### 数组

定义数组

Ruby 中数组是可变的，创建时没有指定元素的位置会用 `nil` 进行填充。

空数组

```ruby
arr = Array.new
arr = []
```

创建时指定长度

```ruby
arr = Array.new(3)
```

创建时指定元素

```ruby
arr2 = [1, 2, 3]
arr4 = Array(1..9)
arr5 = Array[1, 2, 3]
arr6 = Array.new(3) { |x| x * 3 }
```

修改数组元素

```ruby
arr[0] = 10
arr2.push(15)
arr3 << 10
```

读取数组元素

```ruby
puts arr[0]
puts arr[-1]
puts arr.first
puts arr.last
puts arr[1..2]
```

删除元素

```ruby
arr2.delete_at 2  # => 指定的索引
arr2.delete(15) # => 指定的元素
[1, 2, 3, 4, 5, 6, 7, 8, 9].delete_if { |i| i % 2 == 0 }
```

数组长度

```ruby
puts arr.length
puts arr.size
```

遍历数组

```ruby
for i in arr2
  puts i
end
```

数组的展开

Ruby 支持使用多个变量来对数组的值进行展开

```ruby
x, y = [1, 2]
puts x # => 1
puts y # => 2
```

常用操作

```ruby
[1, 2, 3, 4, 5].map { |i| i * 3 }
['one', 'two', 'three', 'four', 'six'].select { |name| name.length > 3 }
[1,2,3].each do |number|
    puts number
end
```

由于 Ruby 支持任意的方法重写，所以也可以自己实现 `each` 方法

```ruby
class FibonacciNumbers
    NUMBERS = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55]

    def each
			NUMBERS.each {|number| yield number }
    end
end

f=FibonacciNumbers.new
f.each do |fibonacci_number|
  puts "A Fibonacci number multiplied by 10: #{fibonacci_number*10}"
end
```

### 范围 Range

Range 使用 `..` 表示左右都是闭区间，使用 `...` 表示左闭右开

```ruby
(1..3).each { |i| print i } #  => 1 2 3
(1...3).each { |i| print i } #  => 1 2
```

范围除了数字上也可以用在字符上

```ruby
('a'..'d').each { |c| print c }
```

以上等于

```ruby
'a'.upto('d').each { |c| print c }
```
