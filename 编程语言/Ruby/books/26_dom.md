[TOC]

### Dom

#### 解析 XML

创建 Document

Document 可以使用文件或字符串进行创建

```ruby
file = File.new('path')
Document.new(file)
content = '<a></a>'
Document.new(content)
```

解析 XML

```ruby
require 'rexml/document'

include REXML

xmlfile=<<ST
<langs type='current'>
  <language>Java</language>
  <language>Groovy</language>
  <language>Scala</language>
  <language>Kotlin</language>
</langs>
ST

xmldoc = Document.new(xmlfile)

root = xmldoc.root
puts "Root element : " + root.attributes["type"]

xmldoc.root.elements.each('language') { |e|
  puts e.text
}
```

#### XPath

获取第一个元素

```ruby
langs = XPath.first(xmldoc, "//langs//language")     
puts "langs:\n #{langs}"                             
```

遍历标签

```ruby
XPath.each(langs, "//language") { |e| puts "type: #{e.text}" }          
```

查找标签

```ruby
names = XPath.match(xmldoc, "//language").map { |x| "language #{x.text}" }    
puts names                                                                    
```
