[TOC]

### null

Ruby 中空值使用 `nil` 表示

```ruby
x = nil
puts x == nil # => true
puts x.equal? false # => false
puts x.nil? # => true
```
