[TOC]

### Socket

#### Server 端

```ruby
require 'socket'

class Server
  def connect
    server = TCPServer.open(2000)
    loop {
      Thread.start(server.accept) do |client|
        client.puts(Time.now.ctime)
        client.puts 'Closing the connection. Bye!'
        client.close
      end
    }
  end
end
```

#### Client 端

```ruby
require 'socket'

class Client
  def initialize(name)
    @name = name
  end

  def connect
    socket = TCPSocket.open('localhost', 2000)
    while (line = socket.gets)
      puts "#{@name} #{line.chop}"
    end
    socket.close
  end
end
```
