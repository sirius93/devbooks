[TOC]

### alias

alias 可以为全局变量和方法设置别名，语法为 `alias <new_name> <old_name>`

#### 全局变量

```ruby
$PI = 3.14
alias $FOO $PI
$PI = 5
puts $PI # => 5
puts $FOO # => 5
```

即对于全局变量的别名而言，修改原始值后别名的值也会被改变了。

#### 方法

```ruby
def calc(x, y)
  x + y
end

alias f calc

def calc(x, y)
  x - y
end

puts calc(5, 3) # => 2
puts f(5, 3) # => 8
```

即对于方法的别名而言，修改方法的实现后别名还是指向原来的方法而不是新的实现。


### undef

`undef` 可以用于取消方法的定义。

```ruby
undef calc
```
