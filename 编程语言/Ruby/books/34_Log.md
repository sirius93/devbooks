[TOC]

### Log

#### caller

`caller` 用于打印方法的调用栈

```ruby
def c
  puts "I'm in C. You know who called me?"
  puts caller
end

def b
  c
end

def a
  b
end

a
```

以上会输出

```
I'm in C. You know who called me?
34_log.rb:7:in `b'
34_log.rb:11:in `a'
34_log.rb:14:in `<top (required)>'
-e:1:in `load'
-e:1:in `<main>'
```

#### Logging

创建 Logger 对象

```ruby
require 'logger'
logger = Logger.new($stdout)
logger.warn('This is a warning')
logger.info('This is an info')
```

设置 Log 级别

```ruby
logger.level = Logger::INFO
logger.debug('(INFO) This a debug message')
logger.unknown('(INFO) Something unknown. Oh, mystery and suspense!')
logger.error('(INFO) Error! Run! Panic!')
logger.warn('(INFO) This is a warning.')
```

#### Benchmark

Benchmark 用于计算方法的执行时间

```ruby
require 'benchmark'

puts Benchmark.measure { 602214.times { 3.14159 * 6.626068 } }
```

或者以下方法

```ruby
n=4000
Benchmark.bm do |benchmark|
  benchmark.report do
    a=[]; n.times { a = a + [n] }
  end
  benchmark.report do
    a=[]; n.times { a << n }
  end
  benchmark.report do
    a=[1..n].map {|number| number}
  end
end
```
