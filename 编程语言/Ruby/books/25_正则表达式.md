[TOC]

### 正则表达式

#### 创建正则表达式

一共有三种方式

```ruby
regex = /[0-9]+\W/
regex = Regexp.new('[0-9]+\W')
regex = %r{[0-9]+\W}
```

创建正则时可以通过 `i` 指定忽略大小写，通过 `m` 指定多行匹配

```ruby
/Hello/im
```

#### Matching

使用符号 `=~`，返回匹配的索引

```ruby
input =~ regex
```

#### Searching

使用方法 `match()`，返回 `MatchData` 实例

```ruby
result = /(\d{4})-(\d{2})/.match '2015-10'
puts result.size # => 3
puts result # => 2015-10
result.captures.each { |m| puts "match: #{m}" } # => 2015 10
```

#### Replacing

替换第一条

```ruby
phone = '2004-959-559 #This is Phone Number'
phone = phone.sub!(/#.*$/, '')
puts "Phone Num : #{phone}" # => Phone Num : 2004-959-559
```

替换全部

```ruby
puts 'abbc'.gsub(/a/, 'b') # bbbc
puts 'abbc'.gsub!(/a/, 'b') # bbbc
puts 'abbc'.gsub(/d/, 'a') # abbc
puts 'abbc'.gsub!(/d/, 'a') # nil
```
