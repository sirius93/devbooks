[TOC]

### eval

#### eval

`eval` 接收并执行传入的字符串，可以计算表达式也可以定义方法。

执行表达式

```ruby
result = eval('7 * 6')
puts result
```

定义方法

```ruby
eval('def zen; 42; end')
puts zen
```

#### instance_eval

`instance_eval` 可以用于类上为类添加新的类方法，也可以作用于实例上，访问类的属性和方法，包括那些私有的。

```ruby
class Monk
  def initialize
    @x = 100
  end
end

Monk.instance_eval('def zen; 42; end', __FILE__, __LINE__)
puts Monk.zen
m = Monk.new
puts m.instance_eval('@x')
```

以上例子中，`instance_eval` 的后两个参数是可选的，但是通常都要加入它们，这样在执行字符串语句时如果发生错误会报告发生错误的文件和行号。

`instance_eval` 与 `eval` 还有一个区别是它的参数除了是字符串也可以说是语句块。

```ruby
puts m.instance_eval { @x +=1 }
```

以上例子中如果直接调用 `m.x` 是非法的，但是通过这种方法就能实现。

#### class_eval

`class_eval` 类似 `instance_eval`，主要用于为类添加实例方法。

```ruby
Monk.class_eval('def sum(x,y); x+y; end')
Monk.class_eval('def inc(x); sum(x, 1); end')
mm = Monk.new
puts mm.inc(19)
```
