[TOC]

### Hash

创建 Hash

Ruby 支持任何数据类型作为键，也支持 Symbol 作为键

以下几种方式都可以定义 Hash

```ruby
peter = {:name => 'Peter', :age => 18, 'height' => 180, 'nickname' => 'Pen'}
jack = [:name, 'Jack', :age, 20, :height, 178]
jane = {name: 'Jane', age: 16, height: 160}
```

空 Hash

```ruby
empty = {}
hash = Hash.new
```

访问元素

```ruby
puts peter[:name]
puts peter['height']
```

修改元素

```ruby
peter[:sex] = 'male'
peter['no'] = 10
```

删除元素

```ruby
peter.delete(:age)
```

获得长度

```ruby
puts peter.length
```

遍历 Hash

```ruby
peter.each { |k, v| puts "#{k} = #{v}" }
peter.each_key{|k| print k}
```

判断键值对的存在

```ruby
puts peter.include? :name
```

Hash 默认值

Hash 可以在创建时指定默认值，如果键不存在的话可以直接返回默认值

```ruby
hash = Hash.new('foo')
puts hash['abc']
```

常用操作

获取键值

```ruby
keys = months.keys
```

排序

```ruby
frequencies = frequencies.sort_by do |key,value|
    value
end
frequencies.reverse!
```

Select

```ruby
good_movies = movie_ratings.select{ |k,v| v > 3}
```
