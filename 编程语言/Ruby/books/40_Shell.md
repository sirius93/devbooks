[TOC]

### Shell

返回 True 表示成功，一共有 6 种用法，以下两种最常用，特别是第二种。

第一种

```Ruby
ret = system 'ls'
puts ret
```

第二种

```Ruby
ret = `ls`
puts ret
```
