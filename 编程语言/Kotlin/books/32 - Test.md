目录

[TOC]

摘要

>a

## Test

### 基于 Junit

```scala
fun getHelloString(): String {
    return "Hello World!"
}

class HelloTest : TestCase() {
    fun testAssert() : Unit {
        assertEquals("Hello, world!", getHelloString())
    }
}
```