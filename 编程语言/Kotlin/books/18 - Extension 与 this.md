目录

[TOC]

摘要

>扩展方法，扩展成员，扩展伴生类，this 表达式

## Extension 与 this

### Extension

#### 概念

- Extension 用于对类的行为或者属性进行拓展（包括内置的类）。
- 当扩展的行为或属性与类内部定义的行为或属性重名时，扩展功能无效。

#### 扩展类的行为

定义一个类

```scala
class Num(var x: Int) {
    fun add(y: Int) {
        x += y
    }

    fun foo() {
        println("foo in Num")
    }
}
```

扩展类

```scala
fun Num.add(x: Int, y: Int) {
    this.x += x + y
}
```

使用扩展的行为

```scala
var n: Num = Num(10)
n.add(1, 3)
```

#### 扩展类的成员

为 List 类添加一个成员

```scala
val <T> List<T>.second: T
    get() = get(1)
```

使用扩展的成员

```scala
val l: List<Int> = listOf(1, 2, 3)
println(l.second)  //  2
```

#### 扩展伴生类

```scala
class MyClass {
    companion object {}
}
class MyClass {
    companion object {}
}
MyClass.foo()
```

### this

```scala
class A { // 隐式标签 @A

    fun bar(){
      val t = this	//类的成员，表示当前对象
    }

    inner class B { // 隐式标签 @B

		// 扩展类的行为
        fun Int.foo() { // 隐式标签 @foo
            val a = this@A // A's this
            val b = this@B // B's this

			// 没有限定符时，表示当前所在的最小范围
            val c = this // foo()的接收器，这里是数字 10
            val c1 = this@foo // foo()的接收器，这里是数字 10

            val funLit = { ->
                val d = this // funLit 的接收器，这里是 Function0<Unit>
            }

            val funLit2 = { s: String ->
                val d1 = this// funLit2 的接收器，这里是 Function1<String, Unit>
            }
        }

        fun info() {
            10.foo()
        }
    }
}

A().B().info()
```


