目录

[TOC]

摘要

>Kotlin 调用 Java，Java 调用 Kotlin（类，伴生对象，重载，异常）

## 与 Java 互相调用

### Kotlin 调用 Java

Java

```java
public class JavaBean {

    private String name;

    public JavaBean(String name) {
        this.name = name;
    }

    public void hello() {
        System.out.println("hello,my name is " + name);
    }

    public boolean is(String name){
        return this.name.equals(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
```

Kotlin

```scala
val javaBean = JavaBean("Peter")
javaBean.hello()
```

对于 `is` 等在 Kotlin 中是关键字而在 Java 中不是关键字的方法，在调用时需要加上 `` 符号。

```scala
javaBean.`is`("Peter")
```

### Java 调用 Kotlin

#### 调用类

Kotlin

```scala
class KotlinBean(val name: String) {
    fun hello() {
        println("hello,my name is " + name)
    }
}
```

Java

```java
KotlinBean bean = new KotlinBean("Peter");
bean.hello();
```

#### 调用伴生对象的方法

Java 调用伴生对象的方法时需要加上注解 `platformStatic`

Kotlin

```scala
class KotlinBean(val name: String) {
    fun hello() {
        println("hello,my name is " + name)
    }

    companion object {
        platformStatic fun foo() {
            println("companion foo")
        }

        fun bar() {
            println("companion bar")
        }
    }
}
```

Java

```java
KotlinBean.foo();
```

以上 KotlinBean 的伴生对象中 `foo()` 对于Java 来说是静态方法，而 `bar()`则不是。

#### Java 重载

Kotlin 方法可以通过注解 `jvmOverloads` 实现 Java 调用中的方法重载

Kotlin

```scala
jvmOverloads fun f(a: String, b: Int = 0, c: String = "abc") {
}
```

Java

```java
obj.f("str");
obj.f("str", 1);
obj.f("str", 1, "foo");
```

#### 检查型异常

由于 Kotlin 没有 CheckException，所以如果需要在 Java 中抛出此种异常，需要添加注解 `@throws`。

Kotlin

```scala
@throws(IOException::class) fun e() {
    throw IOException()
}
```

Java

```java
try {
    obj.e();
} catch (IOException e) {
    e.printStackTrace();
}
```
