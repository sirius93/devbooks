目录

[TOC]

摘要

>Data 类的声明和使用，When 的匹配

## Data Class 与 When

### Data Class

#### 特点

- Data 类是使用关键字 `data` 声明的类
- Data 类默认基于构造方法实现了 `toString()`, `componentN()`, `copy()`, `equals()` 和 `hashCode()` 方法，不在构造方法中定义的属性不会产生在 `toString()` 结果中。
- Data 类可以直接使用 `==`进行比较，同样不在构造方法中定义的属性不会用在比较上
- Data 类只表示数据，不能拥有行为

#### 创建 Data 类

```scala
data class Customer(var name: String, var email: String)
```

#### 使用 Data 类

创建对象

```scala
val peter = Customer("Peter", "peter@example.com")
```

复制对象

```scala
val peter2 = peter.copy()
val peter3 = peter.copy(name = "")
```

ComponentN

```scala
val (name, email) = peter
println("name=$name,email=$email")
```

### When

#### 特点

- When 类似 `switch` 但是功能更加强大，且不需要 `break` 语句。
- 模式匹配可以匹配值，类型

#### When 中的表达式

When 中可以使用的表达式类型

- 类名，用于进行类型匹配
- 范围，用于匹配范围
- 函数，用于匹配参数
- else，匹配其它情况

#### 匹配值和范围

```scala
    val y = when (x) {
        1 -> 2
        3 -> 4
        3, 10 -> 30
        in 10..20 -> 20
        !in 20..30 -> 40
        else -> 0
    }
```

####  匹配类型

```scala
val x = 10
when (x) {
    is Int -> println("long")
    else -> println("else")
}
```

#### 匹配参数

```scala
private fun add(x: Int): Int {
    return x + 1
}

val x = 10
when (x) {
    add(x) -> println("x=" + x)
    else -> println("else")
}
```





