目录

[TOC]

摘要

>创建函数，调用函数，默认参数，带名参数，可变长参数，Unit，函数作用域

## 函数

### 定义函数

格式

```scala
fun functionName(argumentName: arguemntType): returnType {function body}
```

- 以 fun 关键字进行定义，函数只有一句时可以加上 "=" 省略 "{}"
- 参数名写在参数类型之前

例

```scala
//	只有一句时
fun addOne(m: Int): Int = m + 1

fun addTwo(m: Int): Int {
    return m + 2
}
```

### 调用函数

```language
addOne(2)
```

### 函数参数

#### 默认参数

调用某些方法时可以不指定所有参数，而使用默认值

```scala
fun decorate(str: String, left: String = "[", right: String = "]") = left + str + right

println(decorate("abc"))    //  [abc]
```

#### 带名参数

可以在调用函数时，指定参数的名称

```scala
decorate("abc", right = ">")
```

#### 可变长度参数

- 可变长参数使用 `vararg` 修饰
- 使用变长参数时只能一个个传值，不能直接使用外部的 `Array`，除非使用 `*` 表示将 `Array` 中的每个元素当做参数处理

```scala
fun capitalizeAll(vararg args: String): List<String> {
    return args.map { arg ->
        arg.capitalize()
    }
}
println(capitalizeAll("a", "b", "c"))  //  [A, B, C]

val array = arrayOf("d", "e", "f")
println(capitalizeAll("a", "b", "c", *array))   //  [A, B, C, D, E, F]
```

### Unit

当返回类型为 `Unit` 时，表示没有返回值，相当于 Java 中的 `void`，此时也可以省略 `Unit` 的声明。

```scala
fun echo(s: String): Unit {
    println(s)
}

fun echo2(s: String) {
    println(s)
}
```

### 函数作用域

Kotlin 中函数可以直接定义在文件中，而不需要依托任何类。其中定义在 class 或 object 中的函数又被称为成员函数 (Member Function）。除此之外，Kotlin 中还有一种特殊的函数：本地函数 (Local Function)，本地函数是定义在函数内部的函数。

```scala
fun factorialTail(n: Int): Int {
    val zero = 0
    //	本地函数 loop()
    fun loop(acc: Int, n: Int): Int =
            if (n == zero) acc else loop(n * acc, n - 1)
    return loop(1, n)
}
```

本地函数可以访问外部函数的私有成员




