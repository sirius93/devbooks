目录

[TOC]

摘要

>创建不可变映射，创建可变映射，访问元素，更新元素，遍历，多重声明

## Map 及多重声明

### Map

#### 构造映射

不可变映射

```scala
val map = mapOf("a" to 1, "b" to 2, "c" to 3)
```

可变映射

```scala
val mMap = hashMapOf("a" to 1, "b" to 2, "c" to 3)
```

#### 访问元素

```scala
mMap.get("d")
```

如果试图访问不存在的 `key` 时，会抛出 `NullPointerException` 异常，所以需要在访问前先进行判断

```scala
val x = if (mMap.containsKey("e")) mMap.get("e") else 0
```

也可以使用上述的简写方式

```scala
mMap.getOrDefault("e", 10)
```

#### 更新元素

##### 可变映射

更新或插入新元素

```scala
mMap.put("d", 20)
```

删除元素

```scala
mMap.remove("c")
```

##### 不可变映射

不可变映射不可以被修改


#### 遍历

遍历 entry

```scala
for ((k, v)in map) {
    println("$k -> $v")
}
```

只遍历 key 或 value

```scala
val keys = map.keySet()
val values = map.values()
for (k in keys) {
}
```

### 多重声明

只要类实现了 `componentN()` 方法，该类的对象就可以实现多重构造

```scala
class Person(val name: String, val age: Int) {
    fun component1(): String {
        return name
    }

    fun component2(): Int {
        return age
    }
}


val person = Person("Jane", 20)
val  (name, age) = person	//	多重声明
val pname = person.component1()
println(name.toString() + ", " + age + ", " + pname)    //  Jane, 20, Jane
```

以上 Person 实现了 `component1` 和 `component2` 方法，所以多重声明时第一个参数赋值给 name，第二个参数赋值给 age。


