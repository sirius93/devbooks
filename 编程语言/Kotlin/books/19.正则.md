目录

[TOC]

摘要

>创建正则表达式，匹配所有，替换

## 正则表达式

### 构造正则对象

使用 `String` 的 `toRegex` 方法

```scala
val numPattern = "[0-9]+".toRegex()
```

为避免转义符的干扰，可以使用`"""`表示原样输出

### 匹配

#### 返回所有匹配结果

```scala
for (matchResult in numPattern.matchAll("99 bottles, 98 bottles")) {
    println(matchResult.value)
}
```

####  返回第一条匹配结果

```scala
val first = numPattern.match("99 bottles, 98 bottles")
println(first?.value)
```

#### 替换

```scala
val result = numPattern.replace("99 bottles, 98 bottles", "xxx")
println(result) //xxx bottles, xxx bottles
```