目录

[TOC]

摘要

>显示转换，String templates

## Basic Types

###  概要

Kotlin 中一切皆是对象

### Numbers

#### 分类

与 Java 相似，但是 Kotlin 没有自动向上转型（ 如 Java 中的 int 可以自动向上转型为 long ）。

|Type	|Bitwidth
|--------|--------|
|Double	|64
|Float	|32
|Long	|64
|Int	|32
|Short	|16
|Byte	|8

*Kotlin 中字符不是 Number*

#### Literal Constants

- Decimals: 123
	- Longs are tagged by a capital L: 123L
- Hexadecimals: 0x0F
- Binaries: 0b00001011

*Long 结尾不能为小写的 `l`*
*Kotlin 不支持 8 进制字面常量*

#### Representation

数字的装箱操作不会保持相同性，但是会保持相等性

```scala
val a: Int = 10000
val boxedA: Int? = a
val anotherBoxedA: Int? = a
println(a identityEquals a) // Prints 'true'
println(boxedA identityEquals anotherBoxedA) // !!!Prints 'false'!!!

println(a == a) // Prints 'true'
println(boxedA == anotherBoxedA) // Prints 'true'
```

#### Explicit Conversions

#### 语法

所有 Number 类型都支持类似 toByte()，toInt() 这样的显示转换的方法。

例

```scala
val a:Int = 1
// val b:Long = a 错误，不支持自动向上转型
val b:Long = a.toInt()
```

#### 字面量

字面量可以根据上下文推导类型，所以可以不用显示转换

```scala
val a:Byte = 1
val b:Long = 1
b = 1.toLong() + 2 //Long + Int => Long
```

#### 为什么不支持自动向上转型

因为 Kotlin 最终会被编译为 Java 代码，比如说

```scala
val a:Int = 2	// java.lang.Integer a = 2
// 如果支持向上转型
val b:Long = a	// java.lang.Long b = a
```

假设支持向上转型，那么 a 就可以赋值给 b，这样 `a==b` 表达式会返回 false，因为两者类型不同，而这种错误只有在运行时才会发现。
Kotlin 通过强制使用类型转换，使编译时就能发现这种错误，从而提高了程序的安全性。

#### Operations

Kotlin 支持标准的操作符，并且在对应的类上定义为成员方法。

常用操作

- shl(bits) – 有符号左移 (Java’s <<)
- shr(bits) – 有符号右移 (Java’s >>)
- ushr(bits) – 无符号右移 (Java’s >>>)
- and(bits) – 按位与
- or(bits) – 按位或
- xor(bits) – 按位异或
- inv() – 按位取反

例

```scala
val b = 1 shl 2	//4
```

### Characters

Char 使用单引号 `''` 来声明，不能直接当做数字来使用，需要使用 toInt()等方法

```scala
val c: Char = 'a'
```

### Booleans

Boolean 值为 `true` 或者 `false`

```scala
val flag: Boolean = true && false
```

### String

#### Usage

```scala
val s = "Hello World"
for (x in s) {
    print(x)    //  Hello World
}
```

#### Templates

String templates 可以使用变量值来替换字符串中的值

```scala
val name = "Peter"
val str = "name=$name, $name.length, ${name.length()}"
println(str)    //  name=Peter, Peter.length, 5
```








