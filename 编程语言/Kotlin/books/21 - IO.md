目录

[TOC]

摘要

>读取行，读取字符串

## IO 操作

### 读取操作

#### 逐行读取

```scala
var source = File("coffeetime-kotlin/myfile.txt")
val lines = source.readLines("UTF-8")
for (l in lines) {
    println(l)
}
```

#### 读取到字符串中

```scala
val contents = source.readText("UTF-8")
```

