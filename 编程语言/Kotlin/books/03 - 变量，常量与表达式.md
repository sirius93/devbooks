目录

[TOC]

摘要

>声明变量，声明常量，表达式

## 变量，常量与表达式

### 变量

使用 `var` 关键字声明，类型写在变量名后

```scala
var x = 5
var y: Int = 5
```

### 常量

使用 `val` 关键字声明，又称作 "值"

```scala
val a: Int = 1
val b = 2
val c: Int
c = 3
```

### 表达式

Kotlin 也是一门面向表达式的语言，所以可以直接将一个表达式绑定在特定的变量上。

```scala
val z = if (a > b) a else b
```