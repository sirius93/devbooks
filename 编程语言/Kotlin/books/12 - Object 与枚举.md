目录

[TOC]

摘要

>创建单例对象，创建伴生对象，伴生对象与类的关系，使用对象表达式，创建枚举

## Object 与 Enumeration

### Object

#### 对象声明

##### 单例对象

- 与类不同，单例对象使用 `object` 进行声明
- Kotlin 没有静态属性和方法，需要使用单例对象来实现类似的功能。
- 对象的构造器不能提供构造器参数，在第一次使用时会被初始化。

```scala
object Singleton {
  private var num = 0

  fun sequence(): Int {
    num += 1
    return num
  }
}

Singleton.sequence()
```

单例对象可以用于提供常量及共享不可变对象。

##### 伴生对象

- 伴生对象可以用于让一个类即拥有实例化方法又有静态方法。
- 伴生对象必须声明在类中，且使用 `companion object` 关键字进行声明。
- 伴生对象与类可以互相访问各自的私有成员。

```scala
class Companion(private var balance: Int = 0) {
    companion object Factory {
        private var num = 0

        fun create(): Companion = Companion()

        private fun sequence(): Int {
            num += 1
            return num
        }


        fun getInfo(comp: Companion): String {
            return "balance is " + comp.balance
        }
    }

    val id = Companion.sequence()
}
```

#### 对象表达式

对象表达式用于对类的功能进行修改，但是又不用显示创建类，类型 Java 中的匿名类。

```scala
open class A(x: Int) {
    public open val y: Int = x
}

interface B {
    fun info()
}
```

首先定义了一个类和一个接口，其中 `open` 用于表名该类可以被继承。

```scala
val ab = object : A(1), B {
    override fun info() {
        println("info")
    }

    override val y: Int
        get() = 15
}
println(ab.y)
```

以上值 ab 继承了实现了 A 和 B，并重写了其两个方法，实际上本质是创建了匿名类。

对象表达式也可以没有任何父类或父接口

```scala
val adHoc = object {
    var x: Int = 1
    var y: Int = 2
}
println(adHoc.x)
```

#### 对象声明与对象表达式的区别

- 对象声明是延迟初始化的，直到第一次访问才会被初始化。
- 对象表达式在使用时则会被立即初始化。

### Enumeration

**构造枚举**

```scala
enum class TrafficColor(val info: String) {
    RED("stop"), YELLOW("warning"), GREEN("walking"), BLUE("walking")
}

enum class ProtocolState {
    WAITING {
        override fun signal() = TALKING
    },
    TALKING {
        override fun signal() = WAITING
    };
    abstract fun signal(): ProtocolState
}
```

**使用枚举**

```scala
//直接获得枚举
var green = TrafficColor.GREEN

TrafficColor.GREEN.name()	//返回  name
TrafficColor.GREEN.ordinal()	//返回 id
```
