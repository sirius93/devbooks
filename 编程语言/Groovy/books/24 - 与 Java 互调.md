目录

[TOC]

摘要

>Groovy 调用 Java，Java 调用 Groovy（Shell，ClassLoader，ScriptEngine）

## 与 Java 互调

### Groovy 调用 Java

Groovy 中可以无缝直接调用任何 Java 代码

### Java 调用 Groovy

#### 分类

有三种方式

- Shell
- ClassLoader
- ScriptEngine

#### Shell

功能简单，仅仅用于计算表达式和脚本的值

```java
Binding binding = new Binding();
binding.setVariable("x", 3);
binding.setVariable("y", 2.1);

GroovyShell shell = new GroovyShell(binding);
Object value = shell.evaluate("x+y");
assertEquals(5.1, value);
```

#### ClassLoader

通过生成 Groovy 对应的 GroovyObject 对象，来执行各种方法

Groovy 代码

```groovy
class GroovyBean {
  def name;
  def calc(x, y) {
    x + y
  }
}
```

Java 代码

```java
GroovyClassLoader loader = new GroovyClassLoader();
File file = new File("src/main/groovy/org/mrseasons/coffeetime/groovy/groovyjava/GroovyBean.groovy");

Class<?> groovyClass = loader.parseClass(file);
GroovyObject groovyObject = (GroovyObject) groovyClass.newInstance();
List<Integer> list = new ArrayList<>();
list.add(5);
list.add(3);
Object[] arguments = {list};
Object value = groovyObject.invokeMethod("calc", arguments);
assertEquals(8, value);
```

#### ScriptEngine

运行脚本，脚本中的变量可以由 ScriptEngine 提供，以此实现动态脚本更新，运行时修改系统属性等功能。

Groovy 脚本

```groovy
println "using engine"
def result = x + y
```

Java 代码

```java
String[] roots = {"src/main/groovy/org/mrseasons/coffeetime/groovy/_23_groovy_java"};
GroovyScriptEngine engine = new GroovyScriptEngine(roots);
Binding binding = new Binding();
binding.setVariable("x", 3);
binding.setVariable("y", 2.1);

Object output = engine.run("engine.groovy", binding);
assertEquals(5.1, output);
```

