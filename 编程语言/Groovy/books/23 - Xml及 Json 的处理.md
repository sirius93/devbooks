目录

[TOC]

摘要

>MarkupBuilder，StreamingMarkupBuilder，XmlParser，XmlSlurper，JsonBuilder

## Xml 及 Json 的处理

### Xml 的处理

#### 创建 Xml

##### MarkupBuilder

```groovy
def sw = new StringWriter()
def xml = new MarkupBuilder(sw)
xml.langs(type: "current") {
  language("Java")
  language("Groovy")
  language("JavaScript")
}
```

生成结果

```xml
<langs type='current'>
  <language>Java</language>
  <language>Groovy</language>
  <language>JavaScript</language>
</langs>
```

##### StreamingMarkupBuilder

比 MarkupBuilder 更强大，支持 CDATA，命名空间等的创建
并且只有当 bind() 被调用时，xml 才会被创建，所以可以先在多个线程中构建 xml 的结构。

```groovy
def comment = "<![CDATA[<!-- address is new to this release -->]]>"
builder = new StreamingMarkupBuilder()
builder.encoding = "UTF-8"
def person = {
  mkp.xmlDeclaration()
  mkp.pi("xml-stylesheet": "type='text/xsl' href='myfile.xslt'")
  mkp.declareNamespace('': 'http://myDefaultNamespace')
  mkp.declareNamespace('location': 'http://someOtherNamespace')
  person(id: 100) {
    firstname("Jane")
    lastname("Doe")
    mkp.yieldUnescaped(comment)
    location.address("123 Main")
  }
}
def writer = new FileWriter("person.xml")
writer << builder.bind(person)
```

生成结果

```xml
<?xml version='1.0' encoding='UTF-8'?>
<?xml-stylesheet type='text/xsl' href='myfile.xslt'?>
<person id='100' xmlns='http://myDefaultNamespace' xmlns:location='http://someOtherNamespace'>
    <firstname>Jane</firstname>
    <lastname>Doe</lastname><![CDATA[<!-- address is new to this release -->]]>
    <location:address>123 Main</location:address>
</person>
```

#### 解析 Xml

##### XmlParser

从文件中解析

```groovy
def langs = new XmlParser().parse("langs.xml")
println "type = ${langs.attribute("type")}"         //type = current
langs.language.each {
  println it.text()
}
```

从字符串中解析

```groovy
def xml = """
<langs type='current' count='3' mainstream='true'>
  <language flavor='static' version='1.5'>Java</language>
  <language flavor='dynamic' version='1.6.0'>Groovy</language>
  <language flavor='dynamic' version='1.9'>JavaScript</language>
</langs>
"""
langs = new XmlParser().parseText(xml)
println langs.attribute("count")
langs.language.each {
  println it.text()
}
```

##### XmlSlurper

更为强大，支持类似 XPATH 的语法

```groovy
def langs = new XmlSlurper().parseText(xml)
println langs.@count
langs.language.each {
  println it
}
println langs.language[1].@flavor               //dynamic
```

### Json 的处理

#### 创建 Json

只含有 JsonObject

```groovy
def json = new JsonBuilder()
json.call {
    results {
        result("x")
        result("y")
    }
}
println(json.toPrettyString())
```

- 输出
```json
{
    "results": {
        "result": "y"
    }
}
```

含有 JsonArray

```groovy
def list = [
        [code: "111", value: "222"],
        [code: "333", value: "444"]
]
def builder = new JsonBuilder(list)
println builder.toPrettyString()
```
- 输出
```json
[
    {
        "code": "111",
        "value": "222"
    },
    {
        "code": "333",
        "value": "444"
    }
]
```

混合 JsonObject 和 JsonArray

```groovy
def root = new JsonBuilder()
root {
    data(
            list.collect {
                [
                        code : it.code,
                        value: it.value
                ]
            }
    )
}
println root.toPrettyString()
```
- 输出
```json
{
    "data": [
        {
            "code": "111",
            "value": "222"
        },
        {
            "code": "333",
            "value": "444"
        }
    ]
}
```

#### 解析 Json

```groovy
def json = """
{
    "data": [
        {
            "code": "111",
            "value": "222"
        },
        {
            "code": "333",
            "value": "444"
        }
    ]
}
"""
def data = new JsonSlurper().parseText(json)
println data.data[0].code           //111
```

