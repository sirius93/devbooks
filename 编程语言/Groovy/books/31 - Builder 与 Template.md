目录

[TOC]

摘要

>MarkupBuilder，SimpleTemplateEngine，SqlTemplateEngine

##  Builder 和 Template

###  Builder 构造器

#### 概述

Builder（构造器）可以用于构造各种树形结构的数据。

#### 分类

- *NodeBuilder*	- 创建节点树
- *DomBuilder*	- 创建 W3C DOM 树
- *SwingBuilder*	- 创建 Swing 界面
- *AntBuilder*	- 创建 Ant 脚本
- *MarkupBuilder*	- 创建 xml 或 html

#### MarkupBuilder

Xml 部分可以参见 XML 章节的代码

```java
def page = new MarkupBuilder()
// 伪方法 （html, head） 等
page.html {
    head { title 'Hello' }
    body {
        ul {
            for (count in 1..3) {
                li 'li $count'
            }
        }
    }
}
```

#### SwingBuilder

```java
def sb = new SwingBuilder()
def frame = sb.frame(title: 'ToyStore', location: [100, 100],
        size: [400, 300], defaultCloseOperation:
        WindowConstants.EXIT_ON_CLOSE) {
    label(text: 'hello world')
    button(text: 'OK', actionPerformed: {
        println 'press OK'
    })
} )
frame.pack()
frame.setVisible(true)
```

### Template 模版

#### 概述

Template  结合  Builder 可以根据模板输出数据。

#### 输出 Html 和 Xml

程序代码

```java
def file = new File("coffeetime-groovy/src/test/resources/toy_html.template");

def toys = []
def toy1 = new Toy(toyName: 'toy1', unitPrice: 100)
def toy2 = new Toy(toyName: 'toy2', unitPrice: 200)
def toy3 = new Toy(toyName: 'toy3', unitPrice: 300)
toys << toy1 << toy2 << toy3

def binding = ["title": "Display Toys", "toys": toys];
def engine = new SimpleTemplateEngine()
def template = engine.createTemplate(file).make(binding)
println(template.toString())

def outXml = new File('coffeetime-groovy/outputs/toy.html')
outXml.write(template.toString())
```

模版

```html
<html>
    <head>
        <title>${title}</title>
    </head>
    <body>
        <table>
            <th>Toy Name</th><th>Unit Price</th>
            <%for(toy in toys){%>
                <tr>
                    <td>${toy.toyName}</td>
                    <td>${toy.unitPrice}</td>
                </tr>
            <%}%>
        </table>
    </body>
</html>
```

输出结果

```html
<html>
    <head>
        <title>Display Toys</title>
    </head>
    <body>
        <table>
            <th>Toy Name</th><th>Unit Price</th>
                <tr>
                    <td>toy1</td>
                    <td>100</td>
                </tr>
                <tr>
                    <td>toy2</td>
                    <td>200</td>
                </tr>
                <tr>
                    <td>toy3</td>
                    <td>300</td>
                </tr>
        </table>
    </body>
</html>
```

Xml 的生成方式与 Html 基本一致

#### 利用模版输出 Sql 结果到 Html

程序

```java
Sql sql = Sql.newInstance(db, username, password, driver)

def binding = ["title": "Display Toys", "sql": sql];
def engine = new SimpleTemplateEngine()
def template = engine.createTemplate(file).make(binding)
println(template.toString())

def outXml = new File('coffeetime-groovy/outputs/toy_sql.html')
outXml.write(template.toString())
```

模版

```html
<html>
    <head>
        <title>${title}</title>
    </head>
    <body>
        <table>
            <th>Toy Name</th><th>Unit Price</th>
            <%sql.eachRow("select * from toys"){toy->%>
                <tr>
                    <td>${toy.toyName}</td>
                    <td>${toy.unitPrice}</td>
                </tr>
            <%}%>
        </table>
    </body>
</html>
```

