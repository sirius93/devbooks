目录

[TOC]

摘要

>groovlet，gsp

##  Web开发

### Web上下文

要使用 Groovy 开发 Web 程序，首先需要配置上下文，更新 web.xml 文件。

```xml
<servlet>
    <servlet-name>GroovyServlet</servlet-name>
    <servlet-class>groovy.servlet.GroovyServlet</servlet-class>
</servlet>
<servlet>
    <servlet-name>GroovyTemplate</servlet-name>
    <servlet-class>groovy.servlet.TemplateServlet</servlet-class>
</servlet>
<servlet-mapping>
    <servlet-name>GroovyServlet</servlet-name>
    <url-pattern>*.groovy</url-pattern>
</servlet-mapping>
<servlet-mapping>
    <servlet-name>GroovyTemplate</servlet-name>
    <url-pattern>*.gsp</url-pattern>
</servlet-mapping>
```

上述配置定义了以 `groovy` 结尾的请求都将发送给 `groovy.servlet.GroovyServlet`，以 `gsp` 结尾的请求都将发送给 `groovy.servlet.TemplateServlet`

###  Groovlet

####  概念

Groovlet 以 Servlet 为基础，但是无需继承 HttpServlet，也无需实现 doGet()，doPost() 方法。
实际上使用 Groovlet 可以像脚本一样，无需创建任何类。
并且 groovy 文件通常也是放在 `web 目录`下而不是 `src 目录`。

####  简单的 Groovlet

文件名：hello.groovy
Url：http://localhost:8080/hello.groovy

```java
println """
<html><head>
<title>Groovlets 101</title>
</head>
<body>
<p>
Welcome to Groovlets 101. As you can see
this Groovlet is fairly simple.
</p>
<p>
This course is being run on the following servlet container: </br>
${application.getServerInfo()}
</p>
</body>
</html>
"""
```

#### Groovlet 隐藏对象

|隐藏对象|绑定到|
|--------|--------|
| request	| ServletRequest|
|response	|ServletResponse|
|context	|ServletContext|
|application|	ServletContext|
|session	|request.getSession(true)|
|out	|request.getWriter()|
|sout	|request.getOutputStream()|
|html	|new MarkupBuilder(out)|

### GSP

#### 概念

GSP 框架其实就是一个模板引擎，也可以访问 Groovlet 中的各种隐藏对象

#### 简单的 GSP

```html
<html>
<head><title>index.gsp</title></head>
<body>
<b><% println "hello gsp" %></b>
<p>
<% wrd = "Groovy"
   for (i in wrd){
 %>
 <h1> <%=i%> <br/>
<%} %>
</p>
<form action="groovy/addToy.groovy">
    <table>
        <tr><td>ToyName:</td><td><input type="text" name="toyName"></td></tr>
        <tr><td>UnitPrice:</td><td><input type="text" name="unitPrice"></td></tr>
        <tr><td><input type="submit" name="submit"></td></tr>
    </table>
</form>
</body>
</html>
```

### 配置运行环境

#### IDEA 的配置

Modules Settings

- Modules
	- `web 目录`也设置为 Sources
- Facets
	- 确保 `Sources Roots` 包含 `web 目录`
- Artifacts
	- `WEB-INF` 下建立 `lib 目录`，导入相关 Groovy 及其它库

配置Tomcat

- `Open browser` 下的目录为 Tomcat 的根路径
- `Deployment` 页可以更改项目路径


