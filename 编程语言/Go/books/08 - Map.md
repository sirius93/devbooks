目录

[TOC]

摘要

>创建 Map，make，setter，getter，size，delete，检查键的存在，遍历


## Map

### 创建 Map

定义后再初始化

```go
var m1 map[int]string
m1 = map[int]string{}	//m1 map[]
```

使用 make 函数

格式为

`make([keyType]valueType, cap)`	容量可以省略

```go
var m2 map[int]string = make(map[int]string)
```

使用简写

```go
m3 := make(map[int]string)
```

### Setter 和 Getter

Setter

```go
m3[1] = "a"		//m3 map[1:a]
```

这里的 "1" 是 key，不是索引

Getter

```go
a := m3[1]
b := m3[2]
fmt.Println("a", a) //a a
fmt.Println("b", b) //b
```

获取不存在的 key 时返回空

### Size

```go
var l = len(m3)
```

### 删除一个元素

```go
delete(m3, 1)
fmt.Println("a", a)   //a a
fmt.Println("m3", m3) //m3 map[]
```

### Map 嵌套

一个 Map 的 Value 是另一个 Map

```go
var m map[int]map[int]string = make(map[int]map[int]string)
m[1] = make(map[int]string)
m[1][1] = "a"
fmt.Println("m", m)       //m map[1:map[1:a]]
fmt.Println("m[1]", m[1]) //m[1] map[1:a]
```

### 检查键是否存在

使用简写方式同时返回多个值时，map 返回的第二个参数是布尔值，表示键是否存在

```go
m3[1] = "a"
m3[2] = "b"
v, exist := m3[3]			//第二个参数为布尔值，表示键是否存在
fmt.Println("v", v)         //v
fmt.Println("exist", exist) //exist false
```

### 遍历

```go
for k, v := range m3 {
    fmt.Println(k, v)
}
```

其中 k 为键，如果想忽略 k 的使用可以使用占位符"_"

```go
//忽略值
for k := range w {
    fmt.Print(k)
}

//忽略键
for _, v := range w {
    fmt.Print(v)
}
```



