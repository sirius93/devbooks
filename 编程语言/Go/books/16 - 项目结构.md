目录

[TOC]

摘要

>gopath，目录结构，packageName

## Go 项目结构

### GOPath

#### 概念

- `GOPath` 用于告诉 go 指令去哪里才能找到安装在你的系统上的包。
- `GOPath` 中的每一项都指定了一个 `Workspace`。
- 一个 `Workspace` 由三部分组成
	- `src` 包含所有 Go 源文件，其目录下的每个子目录都是一个独立的包
	- `pkg` 包含编译好的包文件
	- `bin` 包含可执行命令

###  PackageName

`packageName` 为包名，通常声明时包名定义为目前源文件所在的目录名

当导入包时，则包名为从 Workspace 开始以 "/" 分隔到指定的源文件的包名

包只有先被安装后，IDE 工具才能正确识别，弹出提示。

### 实践

使用LiteIDE工具

1.添加 GOPath

单击工具栏上的 Go 按钮，在自定义目录填入指定的工作空间，如 "workspace"

2.新建项目

菜单栏【文件】选择新建，在弹出的对话框中，左侧 GOPATH 双击选择之前创建的工作空间，选择模板 "Go1 Command Project"，并填入项目名 "example"

3.新建子包

在建好的项目 "example/src" 目录中创建目录 "bean"，在其中创建任一源文件，声明包名为 "package bean"

4.安装子包

打开上一部创建的源文件，单击 "B" 按钮，选择 "Install"

成功后 pkg 目录下会生成对应的 ".a" 文件

5.导入子包

回到 "main.go" 文件，导入包 "import example/bean"

6.调用子包内容

在 main.go 中使用如下语句，"bean.xxx"

Example:

如下目录结构

```shell
...workspace/example/src
...workspace/example/src/main.go
...workspace/example/src/bean/x.go

//x.go
package bean
func Hello(){}

//main.go
import "example/bean"
bean.Hello()
```














