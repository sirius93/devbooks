目录

[TOC]

摘要

>创建数组（指定长度，不指定长度，指定索引），数组指针与指向数组的指针，new

## 数组

### 创建数组

直接指定长度

```go
var score [3]int	//[0 0 0]
```

指定长度并初始化

```go
score1 := [2]int{3, 4} //[3 4]
```

利用索引初始化

```go
score2 := [10]int{3: 4, 9: 1} //[0 0 0 4 0 0 0 0 0 1]
```

不直接指定长度

```go
score3 := [...]int{1, 2, 3, 4, 5} //[1 2 3 4 5]
```

### 数组的比较

Go 中数组为值类型，可以直接使用 `==` 比较

### 数组与指针

#### 指向数组的指针

```go
var pscore *[5]int = &score3 //&[1 2 3 4 5]
```

#### 数组指针

```go
a := 1
b := 2
pscore2 := [...]*int{&a, &b} //[0x20818a2c0 0x20818a2c8]
```

### New

可以使用 `new` 创建数组，此方法返回一个指向数组的指针

```go
p := new([10]int) //&[0 0 0 0 0 0 0 0 0 0]
```

### 多维数组

```go
arr := [2][3]int{{1, 1, 1}, {2, 2, 2}} //[[1 1 1] [2 2 2]]
```



