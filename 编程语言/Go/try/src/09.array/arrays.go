package main

import "fmt"

func main() {
	//定义数组
	//指定长度
	var score [3]int
	fmt.Println(score) //[0 0 0]

	//指定长度并初始化
	score1 := [2]int{3, 4} //[3 4]
	fmt.Println(score1)

	//指定长度并使用索引进行初始化
	score2 := [10]int{3: 4, 9: 1} //[0 0 0 4 0 0 0 0 0 1]
	fmt.Println(score2)

	//不直接指定长度
	score3 := [...]int{1, 2, 3, 4, 5}
	fmt.Println(score3) //[1 2 3 4 5]

	score4 := [...]int{1, 2, 3, 4, 5}
	score5 := [...]int{1, 3, 2, 4, 5}
	fmt.Println(score4 == score3) //true
	fmt.Println(score4 == score5) //false

	//数组与指针
	//指向数组的指针
	var pscore *[5]int = &score3 //&[1 2 3 4 5]
	fmt.Println(pscore)
	//数组指针
	a := 1
	b := 2
	pscore2 := [...]*int{&a, &b} //[0x20818a2c0 0x20818a2c8]
	fmt.Println(pscore2)

	//new
	p := new([10]int)
	fmt.Println(p) //&[0 0 0 0 0 0 0 0 0 0]

	//多维数组
	arr := [2][3]int{{1, 1, 1}, {2, 2, 2}}
	fmt.Println(arr) //[[1 1 1] [2 2 2]]
}
