package main

import "fmt"

func main() {

	//切片
	var s []int
	fmt.Println(s) //[]

	//数组
	var arr [5]int
	fmt.Println(arr) //[0 0 0 0 0]

	//创建指定范围的切片
	a := [...]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	s1 := a[9]
	fmt.Println("s1", s1) //s1 10

	s2 := a[5:10]
	fmt.Println("s2", s2) //s2 [6 7 8 9 10]

	s3 := a[5:]
	fmt.Println("s3", s3) //s3 [6 7 8 9 10]

	s4 := a[:5]
	fmt.Println("s4", s4) //s4 [1 2 3 4 5]

	//使用make 创建
	s5 := make([]int, 3, 10)
	fmt.Println("s5", s5)           //s5 [0 0 0]
	fmt.Println("len(s5)", len(s5)) //len(s5) 3
	fmt.Println("cap(s5)", cap(s5)) //cap(s5) 10

	s6 := make([]int, 3)
	fmt.Println("len(s6)", len(s6)) //len(s6) 3
	fmt.Println("cap(s6)", cap(s6)) //cap(s6) 3

	//s7 := a[2:12]	//invalid slice index 12 (out of bounds for 10-element array)

	//Reslice
	w := []byte{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k'}
	wa := w[2:5]
	wb := wa[3:5]
	fmt.Println("w", w)   //w [97 98 99 100 101 102 103 104 105 106 107]
	fmt.Println("wa", wa) //wa [99 100 101]
	fmt.Println("wb", wb) //wb [102 103]

	//遍历
	for i, v := range w {
		fmt.Println(i, v)
	}
	for i := range w {
		fmt.Print(i)
	}
	for _, v := range w {
		fmt.Print(v)
	}
	fmt.Println("===")

	//Append
	n1 := make([]int, 3, 6)
	fmt.Println("n1", n1) //n1 [0 0 0]

	n2 := append(n1, 1, 2)
	fmt.Println("n1", n1)                   //n1 [0 0 0]
	fmt.Println("n2", n2, len(n2), cap(n2)) //n2 [0 0 0 1 2]	5	6

	//没有超过原slice 的容量，会影响其它slice
	n3 := append(n1, 3)
	fmt.Println("n1", n1) //n1 [0 0 0]
	fmt.Println("n2", n2) //n2 [0 0 0 3 2]
	fmt.Println("n3", n3) //n3 [0 0 0 3]

	n4 := append(n1, 4, 5, 6)
	fmt.Println("n1", n1) //n1 [0 0 0]
	fmt.Println("n2", n2) //n2 [0 0 0 4 5]
	fmt.Println("n3", n3) //n3 [0 0 0 4]
	fmt.Println("n4", n4) //n4 [0 0 0 4 5 6]

	//超过原slice 的容量，指向新地址，不会影响其它slice
	n5 := append(n1, 7, 8, 9, 10)
	fmt.Println("n1", n1) //n1 [0 0 0]
	fmt.Println("n2", n2) //n2 [0 0 0 4 5]
	fmt.Println("n3", n3) //n3 [0 0 0 4]
	fmt.Println("n4", n4) //n4 [0 0 0 4 5 6]
	fmt.Println("n5", n5) //n5 [0 0 0 7 8 9 10]

	//Copy
	c1 := a[2:5]
	c2 := a[5:8]
	fmt.Println("c1", c1) //c1 [3 4 5]
	fmt.Println("c2", c2) //c2 [6 7 8]

	copy(c1, c2)
	fmt.Println("c1", c1) //c1 [6 7 8]
	fmt.Println("c2", c2) //c2 [6 7 8]

	c3 := a[2:3]
	c4 := a[3:9]
	fmt.Println("c3", c3) //c3 [6]
	fmt.Println("c4", c4) //c4 [7 8 6 7 8 9]

	copy(c3, c4)
	fmt.Println("c3", c3) //c3 [7]
	fmt.Println("c4", c4) //c4 [7 8 6 7 8 9]
}
