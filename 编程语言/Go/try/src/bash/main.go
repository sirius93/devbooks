package main

import (
	"github.com/spf13/cobra"
	"log"
	"os"
)

var (
	itemRepository ItemRepository
)

func main() {

	processStdin()
	rootCmd := &cobra.Command{Use: "kobito"}
	cmdLs(rootCmd)
	rootCmd.Execute()
}

func directoryExists(dir string) bool {
	info, err := os.Stat(dir)
	if err != nil {
		return false
	}
	return info.IsDir()
}

type ItemRepository interface {
	Items() (items []*Item, err error)
	ItemOfId(id int) (item *Item, err error)
}

type Item struct {
	Id       int
	Title    string
	Html     string
	Markdown string
}

func failOnError(err error) {
	if err != nil {
		fatal(err)
	}
}

func fatal(message interface{}) {
	log.Fatal(message)
}
