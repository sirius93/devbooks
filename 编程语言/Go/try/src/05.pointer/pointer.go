package main

import "fmt"

func main() {
	a := 1
	var pa *int = &a
	fmt.Println(pa)  //0xc0820062e0
	fmt.Println(*pa) //1

	var pb *int
	fmt.Println(pb) //nil
}
