package main

import "fmt"

func main() {
	//使用接口
	var pc Usb
	pc = PhoneConnector{name: "phoneConnector"}
	pc.connect() //phoneConnector connect

	//作为参数的接口
	disconnect(pc) //phoneConnector disconnect

	disconnect2(pc) //phoneConnector disconnect2

	//空接口
	test(pc) //is PhoneConnector {phoneConnector}

	//接口类型转换，只能向上转换，调用父接口的方法
	var u Usb
	pc2 := PhoneConnector{name: "pc2"}
	u = Usb(pc2)
	u.connect() //pc2 connect

	//nil 接口
	var e interface{}
	fmt.Println(e == nil) //true

	var p *int = nil
	e = p
	fmt.Println(e == nil) //false
}

//定义接口
type Usb interface {
	getName() string
	connect()
}

type PhoneConnector struct {
	name string
}

//PhoneConnector类型隐式实现了 Usb 接口
func (pc PhoneConnector) getName() string {
	return pc.name
}
func (pc PhoneConnector) connect() {
	fmt.Println(pc.name, "connect")
}

//接口作为参数
func disconnect(u Usb) {
	fmt.Print(u.getName())
	fmt.Println(" disconnect")
}

func disconnect2(u Usb) {
	if c, matched := u.(PhoneConnector); matched {
		fmt.Print(c.getName())
	} else {
		fmt.Print("unknown")
	}
	fmt.Println(" disconnect2")
}

//嵌入接口
type Linker interface {
	link()
}
type DynamicLinker interface {
	getName() string
	Linker
}

//空接口，由于没有任何方法，意味着任何类型都实现了此接口
type Empty interface{}

func test(s interface{}) {
	switch t := s.(type) {
	case PhoneConnector:
		fmt.Println("is PhoneConnector", t)
	default:
		fmt.Println("unkown type", t)
	}
}
