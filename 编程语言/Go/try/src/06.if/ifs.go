package main

import "fmt"

func main() {
	if 1 < 2 {
		fmt.Println(1) //1
	}
	if a := 5; a > 3 {
		fmt.Println(a) //5
	}
	//fmt.Println(a)	error
}
