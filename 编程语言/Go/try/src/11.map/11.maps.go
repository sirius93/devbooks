package main

import (
	"fmt"
)

func main() {
	//创建Map
	var m1 map[int]string
	m1 = map[int]string{}
	fmt.Println("m1", m1) //m1 map[]

	var m2 map[int]string = make(map[int]string)
	fmt.Println("m2", m2) //m2 map[]

	m3 := make(map[int]string)
	fmt.Println("m3", m3) //m3 map[]

	//Setter
	m3[1] = "a"
	fmt.Println("m3", m3) //m3 map[1:a]

	//Getter
	a := m3[1]
	b := m3[2]
	fmt.Println("a", a) //a a
	fmt.Println("b", b) //b

	//Size
	var l = len(m3)
	fmt.Println("len", l) //len 1

	//Delete an element
	delete(m3, 1)
	fmt.Println("a", a)   //a a
	fmt.Println("m3", m3) //m3 map[]

	//复杂Map
	var m map[int]map[int]string = make(map[int]map[int]string)
	m[1] = make(map[int]string)
	m[1][1] = "a"
	fmt.Println("m", m)       //m map[1:map[1:a]]
	fmt.Println("m[1]", m[1]) //m[1] map[1:a]

	//检查键是否存在
	m3[1] = "a"
	m3[2] = "b"
	v, exist := m3[3]
	fmt.Println("v", v)         //v
	fmt.Println("exist", exist) //exist false

	//遍历
	for k, v := range m3 {
		fmt.Println(k, v)
	}
}
