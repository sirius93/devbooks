package main

import "fmt"

func main() {
	//第一种形式
	a := 1
	for {
		a++
		if a > 3 {
			break
		}
		fmt.Print(a, ":") //2:3:
	}
	fmt.Println()

	//第二种形式
	a = 1
	for a < 3 {
		a++
		fmt.Print(a, ":") //2:3:
	}
	fmt.Println()

	//第三种形式
	a = 1
	for i := 0; i < 3; i++ {
		a++
		fmt.Print(a, ":") //2:3:4:
	}
}
