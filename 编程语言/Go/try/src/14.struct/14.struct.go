package main

import "fmt"

//创建结构
type person struct {
	name string
	age  int

	//匿名结构
	address struct {
		street string
	}
}

//结构组合
type student struct {
	name string
	no   int
	person
}

func main() {
	//使用结构
	p1 := person{}
	fmt.Println("p1", p1) //p1 { 0}
	p1.name = "Peter"
	p1.age = 18
	fmt.Println("p1", p1) //p1 {Peter 18}

	p2 := person{name: "Jack", age: 20}
	fmt.Println("p2", p2) //p2 {Jack 20}

	//在方法中改变属性
	changeAge(p2)
	fmt.Println("p2", p2) //p2 {Jack 20}

	changeAge2(&p2)
	fmt.Println("p2", p2) //p2 {Jack 99}

	//使用指针
	p3 := &person{name: "Andy"}
	fmt.Println("p3", p3) //p3 &{Andy 0 {}}

	//匿名结构
	p4 := struct {
		name string
		age  int
	}{
		name: "Joe",
		age:  20, //不能少逗号
	}
	fmt.Println("p4", p4) //p4 {Joe 20}

	//结构的组合
	stu := student{name: "Andy", no: 20, person: person{name: "Tony", age: 20}}
	stu.person.age = 21
	stu.age = 22
	fmt.Println(stu) //{Andy 20 {Tony 22 {}}}

	stu.name = "A"
	stu.person.name = "B"
	fmt.Println(stu) //{A 20 {B 22 {}}}
}

func changeAge(p person) {
	p.age = 99
	fmt.Println(p) //{Jack 99}
}

func changeAge2(p *person) {
	p.age = 99
	fmt.Println(p) //&{Jack 99}
}
