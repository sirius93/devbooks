package main

import "fmt"

func main() {
	str := "world"
	switch str {
	case "hello":
		fmt.Println(1)
	case "world":
		fmt.Println(2)
	default:
		fmt.Println(3)
	}
	//output: 2

	switch a := 5; {
	case a == 1:
		fmt.Println("a")
	case a > 3:
		fmt.Println("b")

		fallthrough
	case a > 4:
		fmt.Println("c")
	}
	//output: b c
}
