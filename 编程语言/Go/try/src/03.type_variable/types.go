package main

import "fmt"
import "strconv"

//类型别名
type (
	number rune
)

var (
	s  string
	n  number
	is bool
)

var a int = 10
var b = 20

//c:=30	error

//多个变量的声明
var java, scala, groovy string
var (
	x, y = 1, 2
)

func main() {
	//零值
	fmt.Println("s:", s)   //""
	fmt.Println("n:", n)   //0
	fmt.Println("is:", is) //false

	//短声明，不能用在函数外
	c := 30
	m, n := 40, 50
	var j, k, _, l = 100, 200, 300, 400
	fmt.Println(c, m, n, j, k, l)

	//类型转换
	var i int = 42
	var f float64 = float64(i)
	var u uint = uint(f)

	//更简单的方式
	ii := 42
	ff := float64(i)
	uu := uint(f)
	fmt.Println(i, f, u, ii, ff, uu)

	//字符与数字的转换
	var a int = 65
	fmt.Println(string(a)) //A

	var result = strconv.Itoa(a)
	fmt.Println(result) //65

	fmt.Println(strconv.Atoi(result)) //65<nil>

}
