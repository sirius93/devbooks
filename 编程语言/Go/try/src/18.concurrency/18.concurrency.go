package main

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

func main() {
	//使用多核
	runtime.GOMAXPROCS(runtime.NumCPU())

	//goroutine
	go test01() //test01...
	time.Sleep(1 * time.Second)

	//channel
	//传递消息
	//只读
	//var ch1 chan<- int
	//只写
	//var ch2 <-chan int

	queue := make(chan int, 1)
	go producer(queue)
	go consumer(queue)
	time.Sleep(1 * time.Second)

	// 同步
	//没有缓存的channel
	ch := make(chan bool)
	go test02(ch)
	<-ch //test02... waiting...

	ch = make(chan bool)
	go test02(ch)
	for v := range ch {
		fmt.Println(v)
	}
	<-ch //test02...waiting...true

	//有缓存的channel
	ch = make(chan bool, 10)
	go test05(ch)
	d := <-ch
	fmt.Println("d", d) //test05... waiting test05... waiting01 waiting02 waiting03 true

	//多线程的同步操作
	//使用Channel
	ch = make(chan bool, 10)
	for i := 0; i < 10; i++ {
		go test03(ch, i)
	}
	for i := 0; i < 10; i++ {
		<-ch
	}

	//使用WaitGroup
	wg := sync.WaitGroup{}
	wg.Add(10)
	for i := 0; i < 10; i++ {
		go test04(&wg, i)
	}
	wg.Wait()

	//Select
	c1, c2 := make(chan int), make(chan string)
	o := make(chan bool)
	go func() {
		for {
			select {
			case v, ok := <-c1:
				if !ok {
					o <- true
					break
				}
				fmt.Println("c1", v)

			case v, ok := <-c2:
				if !ok {
					o <- true
					break
				}
				fmt.Println("c2", v)
			}
		}
	}()
	c1 <- 1
	c2 <- "hi"
	c1 <- 2
	c2 <- "hello"

	close(c1)
	close(c2)

	<-o
	//c1 1 -> c2 hi -> c1 2 -> c2 hello

	//timeout
	select {
	case <-time.After(2 * time.Millisecond):
		fmt.Println("timeout")
	}
}

func test01() {
	fmt.Println("test01...")
}

func test02(ch chan bool) {
	fmt.Println("test02...")
	fmt.Println("waiting...")
	ch <- true
	close(ch)
}

func test03(ch chan bool, index int) {
	a := 1
	for i := 0; i < 1000; i++ {
		a += i
	}
	fmt.Println("test03", index, a)
	ch <- true
}

func test04(wg *sync.WaitGroup, index int) {
	a := 1
	for i := 0; i < 1000; i++ {
		a += i
	}
	fmt.Println("test04", index, a)
	wg.Done()
}

func test05(ch chan bool) {
	fmt.Println("test05...")
	fmt.Println("waiting test05...")
	ch <- true
	fmt.Println("waiting 01...")
	fmt.Println("waiting 02...")
	fmt.Println("waiting 03...")
	close(ch)
}

func producer(queue chan<- int) {
	for i := 0; i < 10; i++ {
		queue <- i
		fmt.Println("send:", i)
	}
	close(queue)
}
func consumer(queue <-chan int) {
	for i := 0; i < 10; i++ {
		v := <-queue
		fmt.Println("receive:", v)
	}
}
