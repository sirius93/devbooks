package main

import "fmt"

type person struct {
	name string
}
type circle struct {
}

//类型别名
type number int

func main() {
	p := person{name: "Jane"}
	p.print()

	c := circle{}
	c.print()
	circle.print(c)

	var n number = 5
	var m = n.add10()
	fmt.Println("n", n) //n 5
	fmt.Println("m", m) //m 50
}

func (p person) print() {
	fmt.Println("person")
}
func (c circle) print() {
	fmt.Println("circle")
}

//非法，Go 没有方法重载
//func (c circle) print(i int) {
//	fmt.Println("circle", i)
//}

//为类型别名添加方法
func (n number) add10() number {
	return n * 10
}
