package main

import "fmt"

func main() {
	//定义函数
	x, y, z := test4()
	fmt.Println(x, y, z) //1 2 3

	m, n := 10, 20
	args(m, n)
	fmt.Println("m,n", m, n) //m,n 10 20

	//匿名函数
	ann := func() {
		fmt.Println("func ann")
	}
	ann() //func ann

	//闭包与柯里化
	//闭包
	s := make([]func(), 5)
	closure(s)
	fmt.Println(s)
	for i, v := range s {
		fmt.Print(i, ",")
		v()
	}
	//-->outputs
	//0,5
	//1,5
	//2,5
	//3,5
	//4,5

	//柯里化
	adder := curry(2)
	fmt.Println(adder, adder(3)) //0x2950 5

}

//单个返回值
func test0(a int, b string) int {
	return a
}

//多个返回值
func test1(a int, b string) (int, string) {
	return a, b
}

//无返回值
func test2(a int, b string) {}

//参数简写
func test3(a, b int) {}

//命名返回值
func test4() (a, b, c int) {
	a, b, c = 1, 2, 3
	return
	//或
	//return a,b,c
}

//变参
func args(a ...int) {
	fmt.Println("before args", a)
	for i, v := range a {
		a[i] = v * 10
	}
	fmt.Println("after args", a)
}

//闭包与柯里化
func closure(s []func()) {
	for i := 0; i < 5; i++ {
		s[i] = func() {
			fmt.Println(i)
		}
	}
}
func curry(x int) func(int) int {
	return func(y int) int {
		return x + y
	}
}
