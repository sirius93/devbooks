package main

import "fmt"

const (
	text, size = "text", len(text)
)

//不指定默认值时使用上行表达式
const (
	a = 1
	b
	c
)

//iota
const (
	d = "D"
	e
	f = iota
	g
)
const (
	k = iota
)

func main() {
	fmt.Println(text)
	fmt.Println(a, b, c)    //1 1 1
	fmt.Println(d, e, f, g) //D D 2 3
	fmt.Println(k)          //0
}
