package main

import "fmt"

func main() {
	//defer
	testDefer() //a d c b

	//改变返回值
	//wrong
	x := changeResult(8)
	fmt.Println("x", x) //x 8

	//correct
	x = changeResult2(8)
	fmt.Println("x", x) //x 9

	//panic and recover
	//panic 导致程序挂掉，不会向下执行
	//broken()
	//-->outputs
	//func one
	//panic: func broken with Panic

	//recover之后，逻辑并不会恢复到panic那个点去，函数还是会在defer之后返回
	catch()
	//-->outputs
	//func one
	//func catch with Recover
}

//defer
func testDefer() {
	fmt.Println("a")
	defer fmt.Println("b")
	defer fmt.Println("c")
	defer func() {
		fmt.Println("d")
	}()
}
func changeResult(x int) int {
	defer func() {
		x++
	}()
	return x
}
func changeResult2(x int) (result int) {
	defer func() {
		result++
	}()
	return x
}

//panic
func one() { fmt.Println("func one") }
func two() { fmt.Println("func two") }
func broken() {
	one()
	panic("func broken with Panic")
	two()
}
func catch() {
	one()
	//需定义在panic 之前
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("func catch with Recover")
		}
	}()
	//panic("func broken with Panic")
	two()
}
