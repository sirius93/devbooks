package main

import (
	"fmt"
	"os/exec"
)

func main() {
	out, err := exec.Command("date").Output()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("The date is %s\n", out)

	exec.Command("cd ..")
	out, err = exec.Command("ls", "*").Output()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("The date is ", out)
}
