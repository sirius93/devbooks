//程序运行的入口是包 "main"
package main

//导入包"fmt"（指定别名为std）和"math/rand"
import (
	std "fmt"
	"math/rand"
)

// 常量的定义
const PI = 3.14
const (
	const1 = "1"
	const2 = 2
)

// 全局变量的声明与赋值
var name = "gopher"

// 函数体内不能定义变量组
var (
	name1 = "1"
	name2 = 2
)

// 一般类型声明
type newType int

type (
	type1 float32
	type2 string
)

// 结构的声明
type gopher struct{}

// 接口的声明
type golang interface{}

// 函数的声明
func add(x int, y int) int {
	return x + y
}

// 当两个或多个连续的函数命名参数是同一类型，则除了最后一个类型之外，其他都可以省略。
func add2(x, y int) int {
	return x + y
}

// 由 main 函数作为程序入口点启动
func main() {
	std.Println("My favorite number is", rand.Intn(10))
	//fmt.Println(add(42, 13))	error
}
