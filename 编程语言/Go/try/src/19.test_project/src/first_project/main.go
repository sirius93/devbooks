// first_project project main.go
package main

import (
	"first_project/bean"
	"fmt"
	"time"
)

func main() {
	fmt.Println("Hello World!")

	var p = bean.Person{Name: "Jane", Age: 15}
	fmt.Println(p)
	p.Info()

	//time format
	t := time.Now()
	fmt.Println(t)

	tv := t.Format(time.ANSIC)
	fmt.Println(tv)
}
