package bean

import "fmt"

type Person struct {
	Name string
	Age  int
}

func (p Person) Info() {
	fmt.Println(p.Name, " is ", p.Age, " years old.")
}
func (p Person) say() {
	fmt.Println(p.Name, " is ", p.Age, " years old.")
}
