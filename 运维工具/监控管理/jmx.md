# JMX

JMX （Java Management Extensions）是一个为应用程序，设备，系统等植入管理功能的框架。
通常使用JMX来监控系统的运行状态或管理系统的某些方面，比如清空缓存、重新加载配置文件等。

JMX体系结构分为以下四个层次：
设备层（Instrumentation Level）：主要定义了信息模型。一个JMX管理资源可以是一个Java应用、一个服务或一个设备，它们可以用Java开发，或者至少能用Java进行包装，并且能被置入JMX框架中，从而成为JMX的一个管理构件(Managed Bean)，简称MBean。在JMX中，各种管理对象以管理构件（MBean）的形式存在，需要管理时，向MBean服务器进行注册。
代理层（Agent Level）：主要定义了各种服务以及通信模型。该层的核心是一个MBean服务器，所有的管理构件都需要向它注册，才能被管理。
分布服务层（Distributed Service Level）：主要定义了能对代理层进行操作的管理接口和构件，这样管理者就可以操作代理。
附加管理协议API定义的API主要用来支持当前已经存在的网络管理协议，如SNMP、TMN、CIM/WBEM等。

MBean 有四种类型
标准类型 MBean
动态类型 MBean
开放类型 MBean
模型类型 MBean

通常使用 JConsole 来查看 JMX 的管理情况

相关概念
可管理资源：可以是任何能被 Java 程序访问或包装的实体
MBean 暴露出来的接口可以管理资源，其实就是将一个 Java 对象包装成可管理的对象（实现 MBean 的接口）
JMX agent：MBean Server 的容器，是一个进程。

通过 JMX 可以监控 JVM 的运行情况，执行垃圾回收，不用重启服务就修改连接池参数

## JMX with Jetty

[指南](http://www.eclipse.org/jetty/documentation/current/jmx-chapter.html#using-jmx)

### 安装

修改 Jetty 插件

```xml
<configuration>
  <jettyXml>src/main/resources/jetty-jmx-remote.xml</jettyXml>
  <scanIntervalSeconds>5</scanIntervalSeconds>
</configuration>
```

jetty-jmx-remote.xml

```xml
<Configure>
 <Call name="addBean">
    <Arg>
      <New id="ConnectorServer" class="org.eclipse.jetty.jmx.ConnectorServer">
        <Arg>
          <New class="javax.management.remote.JMXServiceURL">
            <Arg type="java.lang.String">rmi</Arg>
            <Arg type="java.lang.String"><Property name="jetty.jmxremote.rmihost" deprecated="jetty.jmxrmihost" default="localhost"/></Arg>
            <Arg type="java.lang.Integer"><Property name="jetty.jmxremote.rmiport" deprecated="jetty.jmxrmiport" default="1099"/></Arg>
            <Arg type="java.lang.String">/jndi/rmi://<Property name="jetty.jmxremote.rmihost" deprecated="jetty.jmxrmihost" default="localhost"/>:<Property name="jetty.jmxremote.rmiport" deprecated="jetty.jmxrmiport" default="1099"/>/jmxrmi</Arg>
          </New>
        </Arg>
        <Arg>org.eclipse.jetty.jmx:name=rmiconnectorserver</Arg>
      </New>
    </Arg>
  </Call>
</Configure>
```

### 使用

运行 Jetty

```bash
mvn jetty:run
```

以后台模式运行 JConsole

```bash
jconsole &
```

完成后可以在 MBean 看到相关信息，可以查看 MBean 的属性，调用其方法


### 自定义 MBean

1. 定义 MBean
```java
public interface ControllerMBean {
    public void setName(String name);

    public String getName();

    public String change(String status);

    public void start();

    public void stop();
}
```

2. 编写实现
```java
public class Controller implements ControllerMBean {

    private String name;
    private String status;

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String change(String status) {
        return status;
    }

    @Override
    public void start() {
        status = "start";
    }

    @Override
    public void stop() {
        status = "stop";
    }
}
```

3. 在代码中注册到 Jetty 的 Agent 中
```java
ControllerMBean superController = new Controller();
MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
mBeanServer.registerMBean(superController, new ObjectName("myAppBean:name=controller"));
```

完成后可以在 JConsole 的 MBean 看到相关情况。

## Jolokia with JMX

与 JSR-160 连接器不同的是，它使用基于 HTTP 的 JSON 格式作为通讯协议，提供 JMX 批量操作等。

## 安装

下载 war 包。

使用客户端程序需要加入以下依赖

```xml
<dependency>
    <groupId>org.jolokia</groupId>
    <artifactId>jolokia-client-java</artifactId>
    <version>1.3.2</version>
</dependency>
```

修改 Jetty Plugin，将 war 包放入一个目录中使用 Jetty 同时加载项目本身和 jolokia war

```xml
<configuration>
    <contextHandlers>
        <contextHandler implementation="org.eclipse.jetty.maven.plugin.JettyWebAppContext">
            <war>${project.basedir}/src/main/jolokia/jolokia-war-1.3.2.war</war>
            <contextPath>/jolokia</contextPath>
        </contextHandler>
    </contextHandlers>
</configuration>
```

## 使用

浏览器访问 `http://localhost:8300/jolokia/` 验证 jolokia 是否有正确启起来。

之后可以通过 JConsole 查看 MBean 信息或者通过 Jolokia 客户端以 Json 格式查看 MBean 信息（可以构建自己的 UI 界面）

```java
//基于 war 包的 Jolokia服务端
J4pClient j4pClient = new J4pClient("http://localhost:8300/jolokia");
//查看 ObjectName 为 java.lang:type=Memory 的 MeanBean 的 HeapMemoryUsage 属性
J4pReadRequest req = new J4pReadRequest("java.lang:type=Memory",
        "HeapMemoryUsage");
J4pReadResponse resp = j4pClient.execute(req);

resp.getAttributes().forEach(s ->
        System.out.println("" + resp.getValue(s))
);

//查看自定义的 MBean 属性
req = new J4pReadRequest("myAppBean:name=controller", "Name");
J4pReadResponse resp2 = j4pClient.execute(req);
resp2.getAttributes().forEach(s ->
        System.out.println("" + resp2.getValue(s))
);
```
