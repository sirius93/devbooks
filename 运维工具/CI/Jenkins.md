# Jenkins

## 安装

第一种，从 [官网地址](http://jenkins-ci.org/) 直接下载 war 包，放到 Tomcat 下即可运行

第二种，直接运行 war 包，`
java -jar jenkins.war`

## 运行

地址 `http://localhost:8080/jenkins/`

## 用户管理

### 添加一个默认用户

Manage Jenkins (系统管理) -> Configure Global Security

![][01]

1. 勾选 "Enable security" (启用安全)
2. 勾选 "Access Control" (访问控制) -> "Security Realm" (安全域) -> "Jenkins’ own user database" (Jenkins专有用户数据库) -> "Allow users to sign up" (允许用户注册)
3. 勾选 "Authorization" (授权策略) -> "Logged-in users can do anything" (登录用户可以做任何事)
4. 点击 "Save" (保存)

![][02]

输入用户名，密码，邮箱等注册新用户

### 更改授权策略

在添加了一个默认用户后，我们可以将上一节的 "Allow users to sign up" (允许用户注册) 关闭来保证访问安全或者修改授权策略让直接进行过授权的用户才能进行各种操作。

![][03]

## 配置插件

1. 选择 "Manage Jenkins" (系统管理) -> "Manage Plugins" (管理插件) 下载需要的插件
2. 安装好插件，进入 `系统管理` 修改插件的配置，如 `Gradle`，`Maven` 等。

安装完成后可能需要重启服务

## 配置用户


## 新建项目


## Android项目

环境变量配置 "ANDROID_HOME"后重启Jenkins，在"系统管理 -> 系统信息" 中如果找到 "ANDROID_HOME" 则配置成功。

安装 "GIT plugin"

1
2
3
4. `构建` -> `Execute Shell` -> 输入 `./gradlew build`
`构建后操作` -> 选择 `Archive the artifacts -> 输入 `**/*.apk`
环境变量配置 ANDROID_HOME 

尝试构建查看成果物的 apk 是否正常产生。

http://qiita.com/usamao/items/93535df778916ee70ad8

1. 选择 `新建`，勾选 `构建一个自由风格的软件项目`。
2. `源码管理` 勾选对应的版本控制仓库地址
3. `Additional Behaviours` -> 选择 `Clean before checkout`
3. 构建触发器，选择 `Build periodically`，输入 `H 9 * * 1-6`，代表每周一至周六上午九点中的某一时间自动build一次。
4. 构建，选择 `Invoke Gradle Script`，Switches 输入 `clean build`
5. 构建后，选择 `Archive the artifacts`，指定构建后产生的文件的路径，如 `app/build/outputs/apk/app-debug.apk`。
6. 


[01]:	config.png
[02]:	security.png
[03]:	security2.png
