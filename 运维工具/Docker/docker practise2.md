# 实践：在 Docker 中运行 Mongo 服务
# 建立测试用目录 /var/local/docker/mongodb
[root@133-130-52-73 ~]# mkdir -p /var/local/docker/mongodb
[root@133-130-52-73 ~]# cd /var/local/docker/mongodb
[root@133-130-52-73 mongodb]# touch Dockerfile
[root@133-130-52-73 mongodb]# vi Dockerfile

# Dockerfile
FROM        ubuntu:14.04
# Import MongoDB public GPG key AND create a MongoDB list file
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list

RUN         mkdir -p /data/db
RUN         apt-get update
RUN         apt-get install -y mongodb-org
EXPOSE      27017
ENTRYPOINT  ["/usr/bin/mongod"]

# 创建镜像
# -t 指定 tag
[root@133-130-52-73 mongodb]# sudo docker build -t bookislife.com/mongodb:0.0.1 .

# 运行服务
# 运行容器
# -d 后台运行
[root@133-130-52-73 mongodb]# sudo docker run --name=mongodb -d bookislife.com/mongodb:0.0.1
# 创建用于连接 Redis 容器的应用程序容器 ubuntu
# -t 使用伪终端 -i 使容器保持打开输入
[root@133-130-52-73 mongodb]# sudo docker run -link mongodb -i -t ubuntu:14.04 /bin/bash

# 在 ubuntu 容器中执行以下命令
# 安装 MongoDB
root@59ada23d8dc2:/# sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
root@59ada23d8dc2:/# echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list
root@59ada23d8dc2:/# sudo apt-get update
root@59ada23d8dc2:/# apt-get -y install mongodb-org

# 连接 MongoDB 容器
root@59ada23d8dc2:/# env
root@59ada23d8dc2:/# mongo --host $MONGODB_PORT_27017_TCP_ADDR --port 27017

# 测试 Mongo
172.17.0.9:27017> use test
172.17.0.9:27017> db.test.insert({"x":1})
172.17.0.9:27017> db.test.find()
172.17.0.9:27017> exit
