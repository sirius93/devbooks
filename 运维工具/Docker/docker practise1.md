
# 安装 Docker
[root@133-130-52-73 ~]# sudo yum install docker
[root@133-130-52-73 ~]# sudo systemctl start docker.service
[root@133-130-52-73 ~]# sudo systemctl status docker.service
[root@133-130-52-73 ~]# sudo systemctl enable docker.service

# 安装 Tomcat 测试 Docker 功能
[root@133-130-52-73 ~]# docker run -it --rm tomcat:8.0
# 对外端口：容器端口
[root@133-130-52-73 ~]# docker run -it --rm -p 8888:8080 tomcat:8.0

# 实践：在 Docker 中运行 Redis 服务
# 建立测试用目录 /var/local/docker/redis
[root@133-130-52-73 ~]# mkdir -p /var/local/docker/redis
[root@133-130-52-73 ~]# cd /var/local/docker/redis
[root@133-130-52-73 redis]# touch Dockerfile
[root@133-130-52-73 redis]# vi Dockerfile

# Dockerfile
FROM        ubuntu:14.04
RUN         apt-get update
RUN         apt-get -y install redis-server
EXPOSE      6379
ENTRYPOINT  ["/usr/bin/redis-server"]

# 创建镜像
# -t 指定 tag
[root@133-130-52-73 redis]# sudo docker build -t bookislife.com/redis:0.0.1 .

# 运行服务
# 运行容器
# -d 后台运行
[root@133-130-52-73 redis]# sudo docker run --name=redis -d bookislife.com/redis:0.0.1
# 创建用于连接 Redis 容器的应用程序容器 ubuntu
# -t 使用伪终端 -i 使容器保持打开输入
[root@133-130-52-73 redis]# sudo docker run -link redis -i -t ubuntu:14.04 /bin/bash

# 在 ubuntu 容器中执行以下命令
# 安装 redis-cli
root@59ada23d8dc2:/# apt-get update
root@59ada23d8dc2:/# apt-get -y install redis-server
root@59ada23d8dc2:/# service redis-server stop

# 连接 Redis 容器
root@59ada23d8dc2:/# env
root@59ada23d8dc2:/# redis-cli -h $REDIS_PORT_6379_TCP_ADDR

# 测试 Redis
172.17.0.9:6379> set docker awesome
172.17.0.9:6379> get docker
172.17.0.9:6379> exit
