[TOC]

## 容器的跨主机连接

### 使用网桥实现

Docker默认使用docker0，通过ifconfig可以看到，修改为自定义的网桥
安装网桥管理工具
apt-get install bridge-utils
两台虚拟机
ip1:10.211.55.3
ip2:10.211.55.5
修改配置 /etc/network/interfaces
auto br0
iface br0 inet static
address 10.211.55.3
netmask 255.255.255.0
gateway 10.211.55.1
bridge_ports eth0
Docker设置
修改/etc/default/docker 文件
DOCKER_OPTS="-b=br0 --fixed-cidr='xxxxxx'"
-b=br0 指定使用的自定义网桥
--fixed-cidr 限制ip地址分配范围

### 使用 Open vSwitch 实现

Open vSwitch是虚拟交换机
通过GRE隧道协议进行容器间连接
双网卡 host-only 和 nat
安装Open vSwitch
apt-get install openvswitch-switch
安装网桥管理工具
apt-get install bridge-utils
两台虚拟机
ip1:192.168.59.103
ip2:192.168.59.104

查看状态
ovs-vsctl show

步骤
1.建立ovs网桥
ovs-vsctl add-br obr0
2.添加gre连接
ovs-vsctl add-port obr0 gre0
ovs-vsctl set interface gre0 type=gre options:remote_ip=其它ip
3.配置docker容器虚拟网桥
brctl addbr br0
ifconfig br0 192.168.1.1 netmask 255.255.255.0
4.为虚拟网桥添加ovs接口
brctl addif br0 obr0
5.添加不同Docker容器网段路由
DOCKER_OPTS="-b=br0"
重启docker
ip route add 192.168.2.0/24 via 192.168.59.104 dev eth0

### 使用 weave 实现

安装weave

## 参考资料

- [Docker容器的跨主机连接](http://www.mamicode.com/info-detail-1168129.html)
- [配置Docker多台宿主机间的容器互联](http://ylw6006.blog.51cto.com/470441/1606239/)
- [Docker学习总结之跨主机进行link](http://www.tuicool.com/articles/RfQRny)
