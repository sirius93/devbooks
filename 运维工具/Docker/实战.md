## Docker 坑

### Docker ulimit 问题

http://www.dockone.io/article/522

### 无法启动容器，报 cannot allocate memory

现象
实际物理内存还有剩余但是 Docker 容器无法启动

原因
Docker 守护进程预留了一部分内存，导致启动新的镜像时报错。

解决办法
调整 Linux 的 `overcommit_memory` 设置，默认为 0，调整为 1

```bash
echo 1 > /proc/sys/vm/overcommit_memory
```

`overcommit_memory` 决定是否接受超大内存请求的条件。这个参数有三个可能的值：
- 0 — 默认设置。内核执行启发式内存过量使用处理，方法是估算可用内存量，并拒绝明显无效的请求。遗憾的是因为内存是使用启发式而非准确算法计算进行部署，这个设置有时可能会造成系统中的可用内存超载。
- 1 — 内核执行无内存过量使用处理。使用这个设置会增大内存超载的可能性，但也可以增强大量使用内存任务的性能。
- 2 — 内存拒绝等于或者大于总可用 swap 大小以及 overcommit_ratio 指定的物理 RAM 比例的内存请求。如果您希望减小内存过度使用的风险，这个设置就是最好的。”


参考资料

[lxc-start cannot allocate memory when free memory is available but no swap is available](https://github.com/docker/docker/issues/2495)


### 解决 `/var/lib/docker` 占用空间过大步骤

现象
/var/lib/docker 占用空间过大

原因
/var/lib/docker 会随着 Docker 实例的数据增加而变大

解决方法
复制 /var/lib/docker 到空间大的目录（opt），建立软连接，重启 docker 服务

解决步骤

```bash
1，docker kill all，docker rm all
2，service docker stop（停掉docker服务，不然/var/lib/docker目录无法删除）
3，cp /var/lib/docker /opt/ -fr
4，rm /var/lib/docker -fr
5，ln -s /opt/docker /var/lib/docker
6，service docker start，然后启动所有docker景象

如果无法删除/var/lib/docker，cat /proc/mounts | grep "mapper/docker" ，尝试umount里面列举的设备
```

### ssh 登录 Docker Toolbox

确认虚拟机为桥接模式

```bash
ssh docker@192.168.99.100
```

密码为 `tcuser`
