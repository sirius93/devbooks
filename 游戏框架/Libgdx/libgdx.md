[TOC]

# Libgdx

API 文档

https://libgdx.badlogicgames.com/nightlies/docs/api/

## 坐标系

batch.draw() 左下角是原点
绘制原点在左下角

## 依赖

### Tools

Desktop Dependency:

```groovy
compile "com.badlogicgames.gdx:gdx-tools:$gdxVersion"
```

### Box2D

Core Dependency:

```groovy
compile "com.badlogicgames.gdx:gdx-box2d:$gdxVersion"
```

Desktop Dependency:

```groovy
compile "com.badlogicgames.gdx:gdx-box2d-platform:$gdxVersion:natives-desktop"
```

Android Dependency:

```groovy
compile "com.badlogicgames.gdx:gdx-box2d:$gdxVersion"
natives "com.badlogicgames.gdx:gdx-box2d-platform:$gdxVersion:natives-armeabi"
natives "com.badlogicgames.gdx:gdx-box2d-platform:$gdxVersion:natives-armeabi-v7a"
natives "com.badlogicgames.gdx:gdx-box2d-platform:$gdxVersion:natives-x86"
```

iOS Dependency:

```groovy
compile "com.badlogicgames.gdx:gdx-box2d:$gdxVersion"
natives "com.badlogicgames.gdx:gdx-box2d-platform:$gdxVersion:natives-ios"
```

### FreeTypeFont

Core Dependency:

```groovy
compile "com.badlogicgames.gdx:gdx-freetype:$gdxVersion"
```

Desktop Dependency:

```groovy
compile "com.badlogicgames.gdx:gdx-freetype-platform:$gdxVersion:natives-desktop"
```

Android Dependency:

```groovy
compile "com.badlogicgames.gdx:gdx-freetype:$gdxVersion"
natives "com.badlogicgames.gdx:gdx-freetype-platform:$gdxVersion:natives-armeabi"
natives "com.badlogicgames.gdx:gdx-freetype-platform:$gdxVersion:natives-armeabi-v7a"
natives "com.badlogicgames.gdx:gdx-freetype-platform:$gdxVersion:natives-x86"
```

iOS Dependency version 1.6.1+:

```groovy
compile "com.badlogicgames.gdx:gdx-freetype-platform:$gdxVersion:natives-ios"
```

## 纹理与精灵

### Texture

```java
Texture img = new Texture(Gdx.files.internal("data/test.jpeg"));
```
### SpriteBatch

```java
SpriteBatch batch = new SpriteBatch();

batch.begin()
batch.draw()
batch.end()
```

draw 时左下角是原点

### TextureRegion

- 从 Texture 中建立 TextureRegion 时原点在 Texture 的左上角
- 进行定位时原点在 TextureRegion 的左下角
- 变换时锚点默认在左下角

```java
TextureRegion region = new TextureRegion(texture, 512, 0, 512, 100);
```

### Sprite

默认锚点为左下角

```java
Sprite sprite = new Sprite(region);
sprite.setOrigin(sprite.getWidth() / 2, sprite.getHeight() / 2);
```

### 清屏

```java
Gdx.gl.glClearColor(1, 0, 0, 1);
Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
```

##  Font

### BitmapFont

文本锚点在左下角

#### 使用 Hiero

有三种方式可以使用 Hiero

1.直接使用 Jar 包
[Hiero 下载地址](https://code.google.com/p/libgdx/downloads/detail?name=hiero.jar&can=2&q=)

2.使用 libgdx nightly 版
[libgdx nightly 版下载](http://libgdx.badlogicgames.com/nightlies/)

然后在根目录下运行

```
java -cp gdx.jar:gdx-natives.jar:gdx-backend-lwjgl.jar:gdx-backend-lwjgl-natives.jar:extensions/gdx-tools/gdx-tools.jar com.badlogic.gdx.tools.hiero.Hiero
```

3.使用 Gradle

确保添加了 `Tool` 依赖，在桌面项目执行 `Hiero.main(args);`

之后将生产的 `png` 和 `fnt` 文件拷贝到 `asset` 目录下。

#### 绘制文本

单行文本

```java
BitmapFont font = new BitmapFont(Gdx.files.internal("data/first.fnt"), Gdx.files.internal("data/first.png"), false);
        font.setColor(0.5f, 0.4f, 0.6f, 1);

font.draw(batch, "字体功能测试加上显示不出的字", 200, 160);
```

多行文本

加入 "\n" 表示换行

格式

```java
draw (Batch batch, CharSequence str, float x, float y, float targetWidth, int halign, boolean wrap)
```

- targetWidth	宽度
- halign	 对齐方式
- wrap	宽度设置是否有效

例

```java
font.draw(batch, "字体multiple\n first\n second wrap", 200, 200, 120, Align.left, true);
```

GlyphLayout

```java
GlyphLayout glyphLayout = new GlyphLayout();
boolean wrap = true;
String text = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, " +
        "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
glyphLayout.setText(font, text, Color.TEAL, Gdx.graphics.getWidth() * 0.5f, Align.center, wrap);
font.draw(batch, glyphLayout, 0, Gdx.graphics.getHeight());
```

###  TTF

```java
FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("data/fangsong.ttf"));
FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
parameter.size = 25;
parameter.characters = FreeTypeFontGenerator.DEFAULT_CHARS + "字体功能测试";
BitmapFont font = generator.generateFont(parameter);

font.draw(batch, "字体功能测试加上显示不出的字", 200, 160);
```

常量 DEFAULT_CHARS，包括所有英文和数字

## Animation

如果需要动画显示为30FPS，而动画共有30帧，则每帧为1s/30=0.033秒

### 使用动画

```java
Animation animation;
TextureRegion currentFrame;
float stateTime = 0;

private static final int FRAME_COLS = 6;
private static final int FRAME_ROWS = 5;

//	create()
Texture walkSheet = new Texture(Gdx.files.internal("data/walker.png"));
TextureRegion[][] regions = TextureRegion.split(walkSheet, walkSheet.getWidth() / FRAME_COLS,
        walkSheet.getHeight() / FRAME_ROWS);

TextureRegion[] walkFrames = new TextureRegion[FRAME_COLS * FRAME_ROWS];
int index = 0;
for (int i = 0; i < FRAME_ROWS; i++) {
    for (int j = 0; j < FRAME_COLS; j++) {
        walkFrames[index++] = regions[i][j];
    }
}
animation = new Animation(0.025f, walkFrames);
stateTime = 0f;

//	draw()
stateTime += Gdx.graphics.getDeltaTime();
currentFrame = animation.getKeyFrame(stateTime, true);

batch.begin();
batch.draw(currentFrame, 50, 50);
batch.end();
```

### 播放模式

animation.setPlayMode(playMode)

```java
public enum PlayMode {
    NORMAL,
    REVERSED,
    LOOP,
    LOOP_REVERSED,
    LOOP_PINGPONG,
    LOOP_RANDOM,
}
```

## UIComponent

### Label

```java
Label.LabelStyle labelStyle = new Label.LabelStyle(font, font.getColor());
Label label = new Label("Hello World", labelStyle);
label.setPosition(50, 80);
label.setFontScale(2);
label.setColor(Color.BLUE);
stage.addActor(label);
```

### Image

```java
Texture texture = new Texture(Gdx.files.internal("badlogic.jpg"));
Image image = new Image(texture);
image.setColor(Color.PINK);
image.setPosition(230, 100);
image.setOrigin(0, 0);
image.setScale(0.5f);
image.setRotation(45);
stage.addActor(image);
```

### Button

```java
texture = new Texture(Gdx.files.internal("data/btn.png"));
TextureRegion[][] regions = TextureRegion.split(texture, 120, 120);
TextureRegion up = regions[0][0];
TextureRegion down = regions[0][1];
TextureRegionDrawable upDrawable = new TextureRegionDrawable(up);
TextureRegionDrawable downDrawable = new TextureRegionDrawable(down);
ImageButton imageButton = new ImageButton(upDrawable, downDrawable);
imageButton.setPosition(500, 100);
stage.addActor(imageButton);
```

## Actor

## Stage

必须监听输入事件

```java
Stage stage = new Stage(new FitViewport(480, 320));
stage.setDebugAll(true);
Gdx.input.setInputProcessor(stage);
```

## Audio

### Sound

适用于 1MB 以下的音效

```java
Sound sound = Gdx.audio.newSound(Gdx.files.internal("data/mysound.mp3"));
long id = sound.play(1.0f);

//音高
sound.setPitch(id, 2);

//左右声道
//设置左声道正常音量
sound.setPan(id, -1, 1);

sound.setLooping(id, true);
sound.stop(id);

sound.dispose();
```

### Music

```java
Music music = Gdx.audio.newMusic(Gdx.files.internal("data/mymusic.mp3"));
music.setVolume(0.5f);                 // sets the volume to half the maximum volume
music.setLooping(true);                // will repeat playback until music.stop() is called
music.stop();                          // stops the playback
music.pause();                         // pauses the playback
music.play();                          // resumes the playback
boolean isPlaying = music.isPlaying(); // obvious :)
boolean isLooping = music.isLooping(); // obvious as well :)
float position = music.getPosition();  // returns the playback position in seconds

music.dispose();
```

##  TiledMap

官方示例：https://github.com/libgdx/libgdx/wiki/Tile-maps
Tiled 下载地址：http://www.mapeditor.org/

### 使用 TMX 地图

TiledMap 的原点在左上角

```java
//	create()
TiledMap map = new TmxMapLoader().load("data/level1.tmx");

//The unit scale tells the renderer how many pixels map to a single world unit.
// In the following case 32 pixels would equal one unit.
float unitScale = 1 / 32f;
TiledMapRenderer renderer = new OrthogonalTiledMapRenderer(map, unitScale);

OrthographicCamera camera = new OrthographicCamera();
float w = Gdx.graphics.getWidth();
float h = Gdx.graphics.getHeight();

//view (w / h) x 10 x 20 tiles of your map on screen
camera.setToOrtho(false, (w / h) * 10, 10);
camera.update();


//	render()
camera.update();
renderer.setView(camera);
renderer.render();
```

### 直接使用 Tiled 图片

```java
//	create()
Texture tiles = new Texture(Gdx.files.internal("data/tiles.png"));
TextureRegion[][] splitTiles = TextureRegion.split(tiles, 32, 32);
TiledMap map = new TiledMap();
MapLayers layers = map.getLayers();
for (int l = 0; l < 20; l++) {
    TiledMapTileLayer layer = new TiledMapTileLayer(150, 100, 32, 32);
    for (int x = 0; x < 150; x++) {
        for (int y = 0; y < 100; y++) {
            int ty = (int) (Math.random() * splitTiles.length);
            int tx = (int) (Math.random() * splitTiles[ty].length);
            TiledMapTileLayer.Cell cell = new TiledMapTileLayer.Cell();
            cell.setTile(new StaticTiledMapTile(splitTiles[ty][tx]));
            layer.setCell(x, y, cell);
        }
    }
    layers.add(layer);
}


renderer = new OrthogonalTiledMapRenderer(map);


//	render()
camera.update();
renderer.setView(camera);
renderer.render();
```

### MapProperties

MapProperties 可以用于在 Layer，Tile，Object 等上设置各种属性

```java
//  from Map
MapProperties properties = map.getProperties();
properties.get("custom-property", String.class);

//  from Layer
TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(0);
layer.getProperties().get("another-property", Float.class);

//  from Object
MapObject mapObject = layer.getObjects().get(0);
mapObject.getProperties().get("foo", Boolean.class);
```

## Skin

用于集中管理各种样式

```java
Texture texture = new Texture(Gdx.files.internal("badlogic.jpg"));
Skin skin = new Skin();
skin.add("logo", texture);
Texture logo = skin.get("logo", Texture.class);

skin.add("red", Color.BLUE);
Color blue = skin.getColor("red");
```

Skin 每次都会返回同一个对象，如果需要创建新对象，需要使用如下方法

```java
Drawable redDrawable = skin.newDrawable("red", Color.RED);
```

## 粒子

### 使用 Particle Editor

有三种方式可以使用 Particle Editor

1.直接使用 Jar 包
libgdx 源码的 gdx-tools 工程下

2.使用 libgdx nightly 版
[libgdx nightly 版下载](http://libgdx.badlogicgames.com/nightlies/)

然后在根目录下运行

```
java -cp gdx.jar:gdx-natives.jar:gdx-backend-lwjgl.jar:gdx-backend-lwjgl-natives.jar:extensions/gdx-tools/gdx-tools.jar com.badlogic.gdx.tools.particleeditor.ParticleEditor
```

3.使用 Gradle

确保添加了 `Tool` 依赖，在桌面项目执行 `ParticleEditor.main(args);`

之后将生产的 `.p` 文件拷贝到 `asset` 目录下。

```java
//	create()
particleEffect = new ParticleEffect();
particleEffect.load(Gdx.files.internal("data/test.p"), Gdx.files.internal("data"));
particleEffect.setPosition(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
Array<ParticleEmitter> emitters = new Array(particleEffect.getEmitters());
particleEffect.getEmitters().clear();
particleEffect.getEmitters().add(emitters.get(0));


//	render()
particleEffect.draw(batch, delta);
```

也可以使用 Pool

```java
ParticleEffectPool particleEffectPool = new ParticleEffectPool(particleEffect, 1, 1);
ParticleEffectPool.PooledEffect effect = particleEffectPool.obtain();
```



## Camera

### OrthographicCamera

```java
//	create
OrthographicCamera cam = new OrthographicCamera(30, 30 * (h / w));
cam.position.set(cam.viewportWidth / 2f, cam.viewportHeight / 2f, 0);
cam.update();


//	render
cam.update();
batch.setProjectionMatrix(cam.combined);


//	resize
cam.viewportWidth = 30f;
cam.viewportHeight = 30f * height/width;
cam.update();
```

### PerspectiveCamera

```java
//	create
PerspectiveCamera cam = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
cam.position.set(7f, 7f, 7f);
cam.lookAt(0,0,0);
cam.near = 1f;
cam.far = 300f;
cam.update();

CameraInputController camController = new CameraInputController(cam);
Gdx.input.setInputProcessor(camController);


//	render
camController.update();

Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
```

## 工具类

### Log

```java
Gdx.app.log(tag, message);
Gdx.app.error(tag, message);
Gdx.app.debug(tag, message);
```

### 屏幕方向

```java
int orientation = Gdx.input.getRotation();

```
返回 0, 90, 180 or 270.

或者

```java
Orientation nativeOrientation = Gdx.input.getNativeOrientation();
```

返回 Orientation.Landscape 或 Orientation.Portrait

### 多线程

```java
Gdx.app.postRunnable(runnable);
```

### 文件

#### Preferences

```java
Preferences prefs = Gdx.app.getPreferences("My Preferences");
prefs.putString("name", "Donald Duck");
String name = prefs.getString("name", "No name stored");
prefs.flush();
```

### 处理不同平台

```java
switch(Gdx.app.getType()){
    case Android:
        // android specific code
        break;
    case Desktop:
        // desktop specific code
        break;
    case WebGl:
        // HTML5 specific code
        break;
    default:
        // Other platforms specific code}
```

### 触摸操作

```java
boolean isPressed = Gdx.input.isKeyPressed(Keys.A);

boolean isTouched = Gdx.input.isTouched();

boolean firstFingerTouching = Gdx.input.isTouched(0);
boolean secondFingerTouching = Gdx.input.isTouched(1);
boolean thirdFingerTouching = Gdx.input.isTouched(2);

boolean justTouched = Gdx.input.justTouched();
```

### 菜单键和返回键

```java
Gdx.input.setCatchBackKey(true);

Gdx.input.setCatchMenuKey(true);
```

### 加速器

```java
float accelX=Gdx.input.getAccelerometerX();
float accelY=Gdx.input.getAccelerometerY();
float accelZ=Gdx.input.getAccelerometerZ();
```

### 网络请求

#### TCP

```java
SocketHints socketHints = new SocketHints();
socketHints.connectTimeout = 3000;
Socket socket = Gdx.net.newClientSocket(Protocol.TCP, "127.0.0.1", 8888, socketHints);

//server
ServerSocketHints serverSocketHints = new ServerSocketHints();
serverSocketHints.acceptTimeout = 3000;
ServerSocket serverSocket = Gdx.net.newServerSocket(Protocol.TCP, 6666, serverSocketHints);
```

#### Http

```java
Net.HttpRequest request = new Net.HttpRequest("GET");
request.setUrl("http://www.baidu.com");
Gdx.net.sendHttpRequest(request, new Net.HttpResponseListener() {
    @Override
    public void handleHttpResponse(Net.HttpResponse httpResponse) {
        String result = httpResponse.getResultAsString();
        Gdx.app.log("result", result);
    }

    @Override
    public void failed(Throwable t) {
        Gdx.app.log("error", t.getMessage());
    }

    @Override
    public void cancelled() {
        Gdx.app.log("cancel", "cancel");
    }
});
```

#### 浏览器

```java
Gdx.net.openURI("http://www.baidu.com");
```

### 图形

获得帧率

```java
Gdx.graphics.getFramesPerSecond()
```

全屏

```java
boolean fullscreen = Gdx.graphics.isFullscreen();

// Set full-screen and vSync
// fullscreen
cfg.fullscreen = true;
// vSync
cfg.vSyncEnabled = true;
Gdx.graphics.setVSync(true);
```

### Android 相关操作

Android 版本

```java
int androidVersion = Gdx.app.getVersion();
```
