[TOC]

# 样式工具

## 实现

- [sass](http://sass-lang.com)
- [less](http://lesscss.org)
- [postcss](http://postcss.org/)
- [koala](https://github.com/oklai/koala)
- [Bootstrap](http://getbootstrap.com/)

koala 是一个前端预处理器语言图形编译工具，支持 Less、Sass、Compass、CoffeeScript，帮助 web 开发者更高效地使用它们进行开发。

## Sass

### 安装

需要先安装 Ruby

```bash
gem install sass
```

安装完后可以执行 `sass -v` 进行检查。

### 使用

Sass 文件后缀为 `.scss`

#### 命令行

在命令行中打印生成的 css 文件

```bash
sass base.scss  
```

将指定的 Sass 生成为 Css 文件

```bash
sass base.scss base.css
# 生成压缩后的 css
sass --style compressed base.scss base.css
```

监控指定的文件夹

```bash
sass --watch <sass dir>:<css dir>
```

#### 样式

```css
//variable
$blue: #23599b;

//import into one css
@import "foo";

.main {
  width: 100px;
  height: 100px;
  color: aqua;
  background-color: $blue;

  //support nested
  p {
    font-style: italic;
  }
  a {
    display: block;
    padding: 6px 12px;
    text-decoration: none;
  }
}

//mixin and include
@mixin box-sizing($sizing) {
  -webkit-box-sizing: $sizing;
  -moz-box-sizing: $sizing;
  box-sizing: $sizing;
}

.box-border {
  border: 1px solid #ccc;
  @include box-sizing(border-box);
}

//extend
.message {
  border: 1px solid #ccc;
  padding: 12px;
  color: #333;
}

.success {
  @extend .message;
  border-color: green;
}

.error {
  @extend .message;
  border-color: red;
}

//calc
.container {
  width: 100%;
}

article[role="main"] {
  float: left;
  width: 600px / 960px * 100%;
}

//color function
$linkColor: #08c;
a {
  text-decoration: none;
  color: $linkColor;
  &:hover {
    color: darken($linkColor, 10%);
  }
}
```


## PostCSS

使用 JavaScript 来转换 CSS，通过各种插件来支持各种功能，比如说添加后缀，使用未来的样式等等

### 安装

```bash
npm install postcss-cli
```


### 使用



## Bootstrap

### 安装

```html
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="//cdn.bootcss.com/jquery/1.11.3/jquery.min.js"></script>
<script src="//cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
```

### 使用

## 参考资料

- [Bootstrap 中文站](http://www.bootcss.com/)
- [Bootstrap 教程](http://www.runoob.com/bootstrap/bootstrap-tutorial.html)
