# Semantic-ui

## 使用

### 定义组件

语法

`ui <state> <feature> <componentName>`

例:

ui hover blue button

## Semantic-React-ui

### 组件

Segment 聚合一群组件
Container  组件容器，用于限制内部组件的宽度
Header  页头
List    列表
Button  按钮
Image   图片

### 属性

inverted boolean  反向颜色
vertical boolean  对齐方式
textAlign string  居中
divided  boolean  可以拥有分割线
columns enum      列数量，可以为 equal
stackable boolean         
widescreen enum
as                可以用于指定 H1,H2等
link boolean      可以使用link样式
content           内容
size              大小
primary boolean
labelPosition string 标签位置
icon string       图标
verticalAlign
width




