# CSS

## 引入样式

### 外部文件

```html
<link rel="stylesheet" href="example.css">
```

## 语法

`选择器 属性 值`

## 选择器

### 类型选择器

```css
p {
   color: red;
   font-size:130%;
}
```

### id 选择器

```css
#intro {
   color: white;
   background-color: gray;
}
```

### 类型选择器

```css
.first {font-size: 200%;}
```

### 后代选择器 Descendant

```css
#intro .first em {
   color: pink;
   background-color:gray;
}
```

## font

### font-size

em 是相对大小，默认为 em = pixels / 16

```css
h1 {
   font-size: 1.25em; /* 20px; */
}
```

### font-weight

字体粗细，值为：normal (default size), bold, bolder, lighter 或者 100 (thin) 到 900 (thick) 的数字

### text-align

水平对齐：值为： left, right, center, justify
设置 justify 后每行字体间距都会被拉伸直到每行都是一样宽

### vertical-align

垂直对象，值为：top，middle，bottom, baseline, sub, super, px(pt, cm, %)

vertical-align 不是对所有元素都有效，有些需要特别设置。如：div 需要设为 display: table-cell 且 父 div 设置为 display: inline-table。

### text-decoration

文字被如果修饰，值为：none, inherit（继承自父组件）, overline, underline,line-through（删除线）

### text-indent

缩进，值为：inherit 或具体值

### text-transform

大小写控制，值为 uppercase，lowercase

### letter-spacing

字符间距，值为 none 或具体值

### word-spacing

单词间距，值为 normal 或具体值

### white-space

空白字符处理方式，值为 normal, inherit, nowrap(压缩空白字符)

## 盒模型

相关元素：margins, borders, paddings, content
属性的顺序: top, right, bottom, left

### border

值为：size, style, color

例:

```css
p {
   padding: 10px;    
   border: 5px solid green;
}
```

border 属性也可以分别进行指定，此时使用以下属性名，需要注意如果 `border-style` 没有设置则其它属性不会有效

border-width
border-color
border-style 值：none， dotted, dashed, double，groove，ridge，inset，outset，hidden

## 样式

### 宽度和高度

支持准确值和百分比

min-width
max-width
min-height
max-height

### 背景样式

background-color：背景色
background-image： url("css_logo.png")
background-repeat：决定背景如何铺垫，值为：repeat-x，repeat-y，inherit, repeat, no-repeat
background-attachment：决定滚动时背景是否固定还是滚动，值：fixed, scroll

### 列表样式

list-style: square outside none;

以上属性也可以像以下形式分隔开

list-style-type：值：lower-alpha，circle，square
list-style-image: url("logo.jpg");
list-style-position 决定列表标志的位置，值为 inside


### 表格属性

border-collapse: separate;
border-spacing: 20px 40px;

caption-side：决定 caption（标题标签） 的位置，值：top, bottom
empty-cells 决定空白单元格是否显示边框，值：hide, show

table-layout  如何计算单元格宽度，值为 auto, fixed


### Link 属性

伪类选择器

a:link
a:visited
a:active
a:hover

需要注意 hover 必须设置在 link 和 visited 之后
active 必须设置在 hover 之后

样式

text-decoration: none 是否显示下划线

### display

block
inline
none  隐藏元素

### visibility

hidden 隐藏元素
none  隐藏元素，不占据空间
visible

### position

static 按自然顺序
fixed 固定位置
relative

### float

设置元素的环绕方式，通常用于图片上实现图文混排

right
left
none 默认

同时设置多个连续的元素时这些元素会排列在一行中

clear 属性可以用于取消 float

left
right
none
both

### overflow

visible 默认值
scroll
hidden
auto

## CSS 3

### 属性前缀

-webkit
-moz

### 属性

border-radius 值依次为 top-left, top-right, bottom-right, bottom-left
box-shadow 值依次为 [inset] horizontal offset, vertical offset, [color]
text-shadow 值依次为 left-offset, right-offset, color
background-size 值依次为 width, height 或者 container, cover
background-clip 值为：border-box, padding-box, content-box
opacity

word-wrap 表示文字在 div 中是否自动换行，值为：normal，break-word

### 字体

```css
@font-face {
  font-family: Delicious;
  font-weight: bold;
  src: url('Delicious-Bold.otf');
}
```

### 渐变

Linear
Radial

语法

参数可以指定颜色列表，第一个参数可以指明方向，值为：left, right, bottom, top

```css
background:-moz-linear-gradient(blue, yellow, green, pink, white);
background: radial-gradient(position, shape or size, color-stops);
```

### 动画

transaction

```css
div {
   width: 50px;
   height: 50px;
   background: #32CD32;
   transition: width 3s;
}
div:hover {
   width: 250px;
}
```

rotate

```css
transform: rotate(10deg);
```

translate

```css
transform:translate(100px, 50px);
```

skew

```css
transform: skew(30deg);
```

scale

```css
transform: scale(0.7, 0.7);
```

### 伪类

伪类选择器

:first-child
:last-child

伪类元素

::first-line  第一行文本
::first-letter
::selection
::before
::after
