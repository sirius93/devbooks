# Jackson

## 添加依赖

```xml
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-databind</artifactId>
    <version>2.8.8</version>
</dependency>
```

## 使用

### 基本对象

- ObjectMapper 用于控制对象的各种序列化属性和配置
- JsonFactory 用于以 Json 作为序列化格式
- JsonGenerator 用于控制 Json 输入输出流

### 创建 ObjectMapper

```java
ObjectMapper mapper = new ObjectMapper();
```

### 创建 Generator

```java
JsonGenerator generator = mapper.getFactory().createGenerator(System.out, JsonEncoding.UTF8);
generator.writeObject(user);
```

### 定制对象

定制时间格式

```java
DateFormat format = new SimpleDateFormat(
        "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
format.setTimeZone(new SimpleTimeZone(0, "GMT"));
mapper.setDateFormat(format);
```

## 序列化与反序列化

### Bean

```java
Person person = new Person();
String json = mapper.writeValueAsString(person);

Person personAgain = mapper.readValue(json, Person.class);
```

### 基本类型

```java
generator.writeBoolean(true);
generator.writeString(str);
generator.writeRaw(str);
```

### Object

```java
generator.writeStartObject();

generator.writeObjectFieldStart("user");
generator.writeStringField("name", "Jack");
generator.writeNumberField("age", 12);
generator.writeEndObject();

generator.writeStringField("address", "London");
generator.writeEndObject();
```

### List

```java
List<_User> users = objectMapper.readValue(json, new TypeReference<List<_User>>() {});
```


## 定制

### ObjectMapper

```java
//只序列化不是null的属性
objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
//反序列化时忽略 JSON 中存在 POJO 没有的属性
objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//设置时间格式
objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA));
//Pretty Print
objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
```

### Pretty Print

```java
objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
```

#### 字段名与 JSON 的 Key 不一致

```java
@JsonProperty(value = "auth_data")
public Map<String, Map<String, String>> getAuthData() {
    return authData;
}
```

### 只序列化不是 null 的属性

代码方式

```java
objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
```

注解方式

```java
@JsonInclude(JsonInclude.Include.NON_NULL)
private String email;
```

#### 设置序列化时间格式

```java
DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
format.setTimeZone(new SimpleTimeZone(0, "GMT"));
objectMapper.setDateFormat(format);
```

##### 忽略 JSON 中包含 Object 不存在的字段

代码方式

```java
objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
```

注解方式

```java
@JsonIgnoreProperties(value = {"extra", "age"})
public class _User{}
```

##### 不参与序列化属性

```java
@JsonIgnore
private Date createdAt;
```


## 参考资料

- [Jackson使い方メモ](http://qiita.com/opengl-8080/items/b613b9b3bc5d796c840c)

---

可运行实例代码位于 `spring/first_jackson/src/test` 目录中
