[TOC]

# JAXB

JAXB(Java Architecture for XML Binding)是一个标准，其本身提供两种主要特性：将一个Java对象序列化为XML，以及反向操作，将XML解析成Java对象。

## 添加依赖

```xml
<dependency>
    <groupId>javax.xml.bind</groupId>
    <artifactId>jaxb-api</artifactId>
    <version>2.2.11</version>
</dependency>
<dependency>
    <groupId>com.sun.xml.bind</groupId>
    <artifactId>jaxb-impl</artifactId>
    <version>2.2.11</version>
</dependency>
```

## 使用

### 定义 POJO

以下实例中部分元素省略

```java
//根节点的名字
@XmlRootElement(name = "book")
//指定节点子元素的出现顺序
@XmlType(propOrder = { "author", "name", "publisher", "isbn"})
public class Book {

    //指定子元素的名字，默认为字段名
    @XmlElement(name = "title")
    public String getName() {
        return name;
    }

    //指定节点的属性
    @XmlAttribute
    public int getId() {
        return id;
    }
}

@XmlRootElement(name = "com.mrseasons.first.corejava.jaxb")
public class BookStore {

    //指定列表子元素的名字和包裹列表的节点名
    @XmlElementWrapper(name = "bookList")
    @XmlElement(name = "book")
    private ArrayList<Book> bookList;

    public ArrayList<Book> getBooksList() {
        return bookList;
    }
}
```

### POJO -> XML

```java
//do something to init bookstore...
BookStore bookstore = new BookStore();

JAXBContext context = JAXBContext.newInstance(BookStore.class);
Marshaller marshaller = context.createMarshaller();
//Pretty Print
marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//输出到控制台
marshaller.marshal(bookstore, System.out);
//输出到文件
marshaller.marshal(bookstore, new File("targetXmlPath.xml"));
```

输出的 xml 内容为

```xml
<com.mrseasons.first.corejava.jaxb>
  <bookList>
      <book id="1">
          <author>Neil Strauss</author>
          <title>The Game</title>
          <publisher>Harpercollins</publisher>
          <isbn>978-0060554736</isbn>
      </book>
      <book id="2">
          <author>Charlotte Roche</author>
          <title>Feuchtgebiete</title>
          <publisher>Dumont Buchverlag</publisher>
          <isbn>978-3832180577</isbn>
      </book>
  </bookList>
  <location>Frankfurt Airport</location>
  <name>Fraport Bookstore</name>
</com.mrseasons.first.corejava.jaxb>
```

### XML -> POJO

```java
JAXBContext context = JAXBContext.newInstance(BookStore.class);
Unmarshaller unmarshaller = context.createUnmarshaller();
BookStore bookstore2 = (BookStore) unmarshaller.unmarshal(new FileReader(BOOKSTORE_XML));
ArrayList<Book> list = bookstore2.getBooksList();
```
