[TOC]

# ProtoBuf

- ProtoBuf(https://github.com/google/protobuf)

## 简介

Protocol Buffers 是 Google 提供的一种将结构化数据进行序列化和反序列化的方法，其优点是语言中立，平台中立，可扩展性好。

## proto3 与 proto2 区别

- 默认编译器使用 proto2。
- proto2 支持 `required` 和 `optional` 来指定字段是否可选，proto3 删除了这些关键字，所有字段都是可选的。
- proto3 去除了显式默认值的指定。

## 安装

### 安装编译器

1. 跳转到 [https://github.com/google/protobuf/releases](https://github.com/google/protobuf/releases) 下载对应的 Release 包。
2. 解压后执行以下命令
```bash
./configure
make
make check
make install
```
3. 安装完后执行 `protoc --version` 验证安装是否成功。

### 项目配置

```xml
<dependency>
    <groupId>com.google.protobuf</groupId>
    <artifactId>protobuf-java</artifactId>
    <version>3.0.0-beta-2</version>
</dependency>
```

## 使用

### 编写 proto 文件

test/resources/protobut/foo.proto

```
syntax = "proto3";//指定使用 proto3
option java_package = "com.mrseasons.first.corejava.protocolbuffer.bean"; //指定包名

message Person {
  string name = 1;
  int32 id = 2;     //相当于 Java 的 Integer 类型
  string email = 3;

  enum PhoneType {
    MOBILE = 0;
    HOME = 1;
    WORK = 2;
  }

  message PhoneNumber {
    string number = 1;
    PhoneType type = 2;
  }

  repeated PhoneNumber phone = 4; //repeated 相当于 List
}

message AddressBook {
  repeated Person person = 1;
}
```

### 编译 proto 文件

执行该命令后会自动生成包名

```bash
protoc --java_out=src/test/java src/test/resources/protobuf/foo.proto   
```

### 使用 proto

创建对象

```java
Foo.Person.PhoneNumber homeNumber = Foo.Person.PhoneNumber.newBuilder()
        .setNumber("123456")
        .setType(Foo.Person.PhoneType.HOME)
        .build();
Foo.Person.PhoneNumber workNumber = Foo.Person.PhoneNumber.newBuilder()
        .setNumber("112233")
        .setType(Foo.Person.PhoneType.WORK)
        .build();
Foo.Person person = Foo.Person.newBuilder()
        .setEmail("peter@foo.com")
        .setId(1)
        .setName("Peter")
        .addPhone(homeNumber)
        .addPhone(workNumber)
        .build();
```

序列化

```java
ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
person.writeTo(outputStream);
```

反序列化

```java
Foo.Person personAgain = Foo.Person.parseFrom(outputStream.toByteArray());
assertThat(personAgain.getEmail()).isEqualTo("peter@foo.com");
```

## 参考资料

- [proto3 Guide](https://developers.google.com/protocol-buffers/docs/proto3)

---

可运行实例代码位于 `spring/first_core/test` 的 `protobuf` 目录下
