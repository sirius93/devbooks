[TOC]

# 代码压缩工具

## 实现

- [YUI Compressor](https://github.com/yui/yuicompressor)

## YUI Compressor

Yahoo 公司提供的 JavaScript 和 CSS 的压缩工具，使用 Java 编写，不支持同时压缩 Html。

### 安装

直接下载 [Jar 文件](https://github.com/yui/yuicompressor/releases)。

### 使用

```bash
java -jar yuicompressor-2.4.8.jar js/test.js -o js/test-min.js
```

