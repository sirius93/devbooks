# Immutable

用于每次返回新对象以及对象间的深层比较

官方文档 [https://facebook.github.io/immutable-js/docs/#/](https://facebook.github.io/immutable-js/docs/#/)

## 创建对象

### Map() 与 fromJS()

- Immutable.Map(json) 浅转换，转换后内部还是普通 json 对象
- Immutable.fromJS(json)  深层转换，内部如果是对象的话会转为 Record

## Record

用于创建继承 Record 的类的实例

## Map

set(k, v)   -   设置键值对
setIn([kPath, v])   -   设置键值对路径，如： setIn(['list', '0', 'name'], "new") 表示设置 list[0].name=new
get(k) -   获取值
getIn([kPath])

例：

```js
let info = Map({
            title: 'dummyTitle01',
            description: 'dummpyDesc01',
            completed: false
        });
info.get('title');
```



