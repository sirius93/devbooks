# Browserify

Browserify 可以以 CommonJS 方式为浏览器端的 js 添加 require 特性。

## 安装

```bash
npm install -g browserify
```

## 使用

1. 编写需要进行预编译的普通 js 文件。
2. 编译为普通 js，语法：`browserify core.js > bundle.js`
3. 在页面导入生成后的普通 js 文件。
