babel使用
安装
npm install babel-cli -g
会新增两个命令
babel 和 babel-node.

babel-node <file>

babel 类

以前

```js
var Character = function(x, y) {
  this.x = x;
  this.y = y;
  this.health_ = 100;
};

Character.prototype.damage = function() {
  this.health_ = this.health_ - 10;
};

Character.prototype.getHealth = function() {
  return this.health_;
};

Character.prototype.toString = function() {
  return "x: " + this.x + " y: " + this.y + " health: " + this.getHealth();
};
```

es6

```js
class Character {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.health_ = 100;
  }
  damage() {
    this.health_ -= 10;
  }
  getHealth() {
    return this.health_;
  }
  toString() {
    return "x: " + this.x + " y: " + this.y + " health: " + this.health_;
  }
}

var x = process.argv[2];
var y = process.argv[3];
var character = new Character(+x, +y);
character.damage();
console.log(character.toString());
```

Module

Message.js

```js
export const message = 'Hello Babel';
```

Other.js

```js
import {message} from './Message';
console.log(message); // Hello Babel
```

commonjs 形式

Message.js

```js
exports.message = 'Hello Babel';
```

```js
var message = require('./Message').message;
console.log(message); // Hello Babel
```

Default Export

```js
const greeting = 'Hello';
const name = 'Babel';
const version = 'v5.0';
export const obj = {
  greeting: greeting,
  name: name,
  version: version
};

import {obj} from './Message';
console.log(obj.greeting + ' ' + obj.name + ' ' + obj.version); // Hello Babel v5.0
```

变为

```js
const greeting = 'Hello';
const name = 'Babel';
const version = 'v5.0';
export default {
  greeting: greeting,
  name: name,
  version: version
};

import Message from './Message';
console.log(Message.greeting + ' ' + Message.name + ' ' + Message.version); //Hello Babel v5.0
```

Computed Property

原来

```js
var prop = "foo";

var obj = {};
obj[prop] = "bar";
```

ES 6

键也可以是方法

```js
var prop = "foo";
var obj = {
  [prop]: "bar",
  [(()=>"bar" + "baz")()]: "foo"
};


```
