# Vue

## 概念

Vue.js（读音 /vjuː/, 类似于 view） 是一套构建用户界面的 渐进式框架。与其他重量级框架不同的是，Vue 采用自底向上增量开发的设计。Vue 的核心库只关注视图层。

声明式渲染


## 安装与运行

直接使用

添加 `<script src="https://unpkg.com/vue/dist/vue.js"></script>` 标签

安装  Cli 工具

```bash
npm install --global vue-cli
```

创建基于 Webpack 的项目

```bash
vue init webpack my-project
```

启动服务

```bash
npm run dev
```

编译

```bash
npm run build
```

## 基本语法

### 字符串插值

语法：`{{msg}}`

除了普通字符串还支持如 `{{ ok ? 'YES' : 'NO' }}` 这样的表达式，但是不支持完整语句

### 过滤器

过滤器用来进行字符串操作，由 `|` 指示，且只能用在 `{{}}` 或 `v-bind` 中
过滤器本质是一个函数,且第一个参数为表达式的值

```js
new Vue({
  // ...
  filters: {
    capitalize: function (value) {
      if (!value) return ''
      value = value.toString()
      return value.charAt(0).toUpperCase() + value.slice(1)
    }
  }
})
```

```html
{{ message | capitalize('arg1', 'arg2') }}
```

以上 capitalize 中 arg1 为函数的第二个参数

### 计算属性

计算属性用以处理需要进行计算的属性值，其内部 `this` 指向 vm 实例

```js
var vm = new Vue({
  el: '#example',
  data: {
    message: 'Hello'
  },
  computed: {
    // a computed getter
    reversedMessage: function () {
      // `this` points to the vm instance
      return this.message.split('').reverse().join('')
    }
  }
});
```

### 计算属性 vs 方法

计算属性是基于它的依赖缓存。计算属性只有在它的相关依赖发生改变时才会重新取值

所以使用以下计算属性时则不会进行重新取值，而如果使用 methods 则会重新获取

```js
computed: {
  now: function () {
    return Date.now()
  }
}
```

计算属性默认只提供 getter，但也可以使用 setter

```js
computed: {
  fullName: {
    // getter
    get: function () {
      return this.firstName + ' ' + this.lastName
    },
    // setter
    set: function (newValue) {
      var names = newValue.split(' ')
      this.firstName = names[0]
      this.lastName = names[names.length - 1]
    }
  }
}
```

### 指令

格式为 `v-xxx`

#### 直接绑定

输入数据

```js
data: {
  message: 'Hello Vue!'
}
```

绑定数据

```js
<div id="app">
  {{ message }}
</div>
```

#### v-once

用于只绑定一次，不需要进行更新时

```js
<span v-once>This will never change: {{ msg }}</span>
```

#### v-bind

输入数据

```js
data: {
  message: 'You loaded this page on ' + new Date()
}
```

绑定数据

```js
<span v-bind:title="message">
  Hover your mouse over me for a few seconds to see my dynamically bound title!
</span>
```

`v-bind` 也可以用于绑定 html 标签的各种属性，语法为 `v-bind:<attr_name>`

```js
<div v-bind:id="dynamicId"></div>
<button v-bind:disabled="someDynamicCondition">Button</button>
```


#### v-html

`{{}}` 中的 html 会被当成普通文字, 需要使用 v-html 来显示未 html

```html
<div>{{ message }}</div>
<div v-html="message"></div>
```

#### v-if

用于判断条件

输入数据

```js
data: {
  seen: true
}
```

绑定数据

```js
<p v-if="seen">Now you see me</p>
```

#### v-for

用于循环

输入数据

```js
data: {
  todos: [
    { text: 'Learn JavaScript' },
    { text: 'Learn Vue' },
    { text: 'Build something awesome' }
  ]
}
```

绑定数据

```js
<p v-if="seen">Now you see me</p>
```

### 修饰符

以半角句号 . 指明的特殊后缀，用以进行特殊绑定
如: .prevent 修饰符告诉 v-on 指令对于触发的事件调用 event.preventDefault()：
<form v-on:submit.prevent="onSubmit"></form>


### 事件绑定

v-on:<event_type>

```html
<button v-on:click="reverseMessage">Reverse Message</button>
```

```js
methods: {
  reverseMessage: function () {
    this.message = this.message.split('').reverse().join('')
  }
}
```

### 双向绑定

v-model

```html
<input v-model="message">
```

```js
data: {
  message: 'Hello Vue!'
}
```

### Vue 实例

####  构造器

```js
var vm = new Vue({
  // 选项, 所有的选项可以在 https://cn.vuejs.org/v2/api/ 中查看
  el: '#app',
  data: {},
  methods: {},
  filters: {}
})
```

el 指定需要进行加载的父容器的 Id

可以扩展 Vue 构造器，从而用预定义选项创建可复用的组件构造器：

```js
var MyComponent = Vue.extend({
  // 扩展选项
})
var myComponentInstance = new MyComponent()
```

#### 属性与方法

每个 Vue 实例都会代理其 `data` 对象里所有的属性

```js
var data = { a: 1 }
var vm = new Vue({
  data: data
})
vm.a === data.a
```

注意只有这些被代理的属性是响应式的。如果在实例创建之后添加新的属性到实例上，它不会触发视图更新

#### $ 属性与方法

Vue 实例暴露了一些有用的实例属性与方法。这些属性与方法都有前缀 `$`，以便与代理的 `data` 属性区分

```js
vm.$data === data // -> true
vm.$el === document.getElementById('example') // -> true
vm.$watch('a', function (newVal, oldVal) {
  // 这个回调将在 `vm.a`  改变后调用
})
```

#### 生命周期

new Vue() -> beforeCreated -> created -> beforeMount -> mounted -> beforeUpdate -> updated -> beforeDestroy -> destroyed


### Class 绑定

class 绑定可以和普通的 class 共存，名字为定义的 class 名，值为布尔值，表示是否使用

```js
<div class="static" v-bind:class="{ active: isActive, error: hasError }"></div>
```

class 的值也可以是计算属性或数组

```html
<div v-bind:class="classObject"></div>
<div v-bind:class="[activeClass, errorClass]">
```

### 样式

直接绑定样式语法： `v-bind:style`

vue 为自动为特殊样式添加前缀


## 组件

在 Vue 里，一个组件实质上是一个拥有预定义选项的一个 Vue 实例

### 自定义组件

```html
<todo-item v-for="item in groceryList" v-bind:todo="item"></todo-item>
```

```js
Vue.component('todo-item', {
  props: ['todo'],
  template: '<li>{{ todo.text }}</li>'
})

data: {
  groceryList: [
    { text: 'Vegetables' },
    { text: 'Cheese' },
    { text: 'Whatever else humans are supposed to eat' }
  ]
}
```


## 缩写

v-bind:href --> :href
v-on:click  --> @click



## 参考资料

- [官方文档](https://cn.vuejs.org/v2/guide)
