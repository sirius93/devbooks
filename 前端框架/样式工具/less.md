[TOC]

# Less

## 安装

需要先安装 Node

```bash
npm install -g less
```

安装完后可以执行 `lessc -v` 进行检查。

## 使用

less 文件后缀为 `.less`

### 命令行

在命令行中打印生成的 css 文件

```bash
lessc base.less
```

将指定的 Less 生成为 Css 文件

```bash
lessc base.less base.css
```

### 样式

```css
//variable
@color: #23599b;

//import into one css
@import "foo";

.main {
  width: 100px;
  height: 100px;
  color: aqua;
  background-color: @color;

  //support nested
  p {
    font-style: italic;
  }
  a {
    display: block;
    padding: 6px 12px;
    text-decoration: none;
  }
}

//mixin class
.box-sizing(@sizing) {
  -webkit-box-sizing: @sizing;
  -moz-box-sizing: @sizing;
  box-sizing: @sizing;
}

.box-border {
  border: 1px solid #ccc;
  .box-sizing(border-box);
}

//extend
.message {
  border: 1px solid #ccc;
  padding: 12px;
  color: #333;
}

.success:extend(.message) {
  border-color: green;
}

.error:extend(.message) {
  border-color: red;
}

//calc
.container {
  width: 100%;
}

article[role="main"] {
  float: left;
  width: 600px / 960px * 100%;
}
```
