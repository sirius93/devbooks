[TOC]

# 样式工具

## 实现

- [sass](http://sass-lang.com)
- [less](http://lesscss.org)
- [postcss](http://postcss.org/)
- [koala](https://github.com/oklai/koala)
- [Bootstrap](http://getbootstrap.com/)

koala 是一个前端预处理器语言图形编译工具，支持 Less、Sass、Compass、CoffeeScript，帮助 web 开发者更高效地使用它们进行开发。


## PostCSS

使用 JavaScript 来转换 CSS，通过各种插件来支持各种功能，比如说添加后缀，使用未来的样式等等

### 安装

```bash
npm install postcss-cli
```


## Bootstrap

### 安装

```html
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="//cdn.bootcss.com/jquery/1.11.3/jquery.min.js"></script>
<script src="//cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
```


## 参考资料

- [Bootstrap 中文站](http://www.bootcss.com/)
- [Bootstrap 教程](http://www.runoob.com/bootstrap/bootstrap-tutorial.html)
