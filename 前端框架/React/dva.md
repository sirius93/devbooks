# Dva

## 使用 Cli

### 安装 Cli

```bash
npm i dva-cli -g
```

### 创建项目

```bash
dva new <project_name>
```

### 配置 Antd 和 babel-plugin-import

```bash
npm i antd --save
npm i babel-plugin-import --save-dev
```

## 使用 CLI

### 生成路由

```bash
dva g route <plural>
```

### 生成 Module

```bash
dva g model <plural>
```

### 生成 Component

```bash
dva g component <plural_folder>/<plural>
```


## 基本概念

### 结构

- app
  - dva 的实例，代表一个应用
- Plugins
- Model
  - 定义全局 state 名，reducer, effect 
- Router
  - 路由
- service
  - 负责调用远程 api 等操作，只是普通的 js
- route
  - 相当于 Container


### 数据流向

- 当一个行为会改变数据的时候可以通过 dispatch 发起一个 action
- 如果该行为是同步的话则会直接通过 Reducers 改变 State
- 如果该行为是异步的话（副作用）会先触发 Effects 然后流向 Reducers 最终改变 State


### State

在 dva 中可以通过 dva 的实例属性 `_store` 看到顶部的 state 数据

### Action

通过 dispatch 函数可以发起一个 action，dispatch 是在组件 connect Models 之后，通过 props 传入的

```js
dispatch({
  type: 'add',
});
```

### Reducer

Reducer（也称为 reducing function）函数接受两个参数：之前已经累积运算的结果和当前要被累积的值，返回的是一个新的累积结果。
该函数把一个集合归并成一个单值。
Reducer 的概念来自于是函数式编程

### Effect

dva 为了控制副作用的操作，底层引入了 `redux-sagas` 做异步流程控制

### Subscription

Subscriptions 是一种从源获取数据的方法，它来自于 elm
数据源可以是当前的时间、服务器的 websocket 连接、keyboard 输入、geolocation 变化、history 路由变化等等。

```js
import key from 'keymaster';
...
app.model({
  namespace: 'count',
  subscriptions: {
    keyEvent(dispatch) {
      key('⌘+up, ctrl+up', () => { dispatch({type:'add'}) });
    },
  }
});
```


## 全局事件

### 全局错误处理

在创建时通过 `onError` 方法来指定

```js
const app = dva({
  onError(e) {
    message.error(e.message, ERROR_MSG_DURATION);
  },
});
```

## 组件


## 参考资料

- [dva-knowledgemap](https://github.com/dvajs/dva-knowledgemap)
