# Redux Sagas

## Saga

Saga 本质就是一个 generator 函数，例：`function* playLevelOne() { ... }`

yield promise 可以阻塞 saga 的执行，直到 promise.resolve() 被调用

## 常用操作

### yield

用于执行对应的单个 task 或多个 task

```js
yield [call(fun), call(fun)]
```

### select

类似 getState，用于从 state 中获取值

```js
const {foo} = yield select();
```

### put

类似 dispatch，用于分发 Action

注意参数用的是括号，语法：`yield put(action)`

例：

```js
yield put(StoryActions.listSuccess(activeType, items))
```


### call

用于阻塞调用

注意参数使用的是逗号，语法：`yield call(promise, args...)`

例：

```js
const ids = yield call(api.listItems, activeType)
```

### fork

用于无阻塞调用。它的出发点是把副作用 (Side effect，异步行为就是典型的副作用) 看成 "线程"，可以通过普通的 action 去触发它，当副作用完成时也会触发 action 作为输出。

### take

用于等待 redux dispatch 直到匹配到了某个 action

```js
while (true) {
  yield take('CLICK_BUTTON');
  yield fork(clickButtonSaga);
}
```

以上语法糖

```js
yield takeEvery('*', function* logger(action) {
  const newState = yield select();
  console.log('received action:', action);
  console.log('state become:', newState);
});
```

### takeEvery/takeLatest

takeEvery 每个 action 触发时调用方法
takeLatest  不允许并发，只会执行最后一个

### race

用于对优先完成的 task 进行处理，比如用在设置超时处理上

```js
function* fetchPostsWithTimeout() {
  const {posts, timeout} = yield race({
    posts: call(fetchApi, '/posts'),
    timeout: call(delay, 1000)
  })

  if (posts)
    put({type: 'POSTS_RECEIVED', posts})
  else
    put({type: 'TIMEOUT_ERROR'})
}
```

### yield*

用于组合多个saga

```js
function* playLevelOne() { ... }
function* playLevelTwo() { ... }
function* playLevelThree() { ... }
function* game() {
  const score1 = yield* playLevelOne()
  put(showScore(score1))

  const score2 = yield* playLevelTwo()
  put(showScore(score2))

  const score3 = yield* playLevelThree()
  put(showScore(score3))
}
```


