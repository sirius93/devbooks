# React 相关知识

## 依赖库

### React Router

通过 `Link` 标签直接调用对应的 History Api，而不是刷新整个页面

### Redux Thunk

thunkMiddleware 用于异步 action,使 actionCreator 可以同时返回 action 和函数

### Immutable

### Fetch

安装

```bash

```

使用

```js
import fetch from 'isomorphic-fetch'
fetch(url);
```

## Redux 使用

整个应用只有一个 Store，一个 Reducer，Reducer 就是一个函数

### createStore

接收一个代表 reducer 的函数,并将 reducer 函数注册到 store 上

### bindActionCreators 和 connect

- `bindActionCreators()` 用于将 `dispatch` 对象绑定到 ActionCreator 上，并且在调用 ActionCreator 时实际调用 `dispatch(actionCreator.apply(args))`

- `connect()` 绑定 store 到其参数对象上


### State

Store 里保存一个全局的 State, 每个 Component 也可以由自己的局部 State，通过 `this.state={}` 初始化局部 State，通过 `this.setState({})` 进行重新绘制

默认画面是否刷新是判断 State 里的 preState 的值和当前的 nextState 是否是同一引用来决定是否刷新， 对于对象，数组等元素如果只是添加，删除元素的话实际改变的还是 preState，引用相同，所以也不会触发重新绘制
可以使用 Immutable 每次返回新数据

### Action Type

```js
export const FETCH_USER_START = "FETCH_USER_START";
```

### Action Creator

#### 同步 ActionCreator

```js
export function echo(msg) {
    return {
        type: ECHO,
        data: msg
    }
}
```

#### 异步 ActionCreator

```js
export function fetchData(userId) {
   return function (dispatch) {
       dispatch({type: FETCH_USER_START});
       return fetchUser(dispatch, userId);
   };
}

function fetchUser(dispatch, userId) {
   return fetch(`${API_URL}/${userId}`)
       .then(resp=> {
           if(resp.status!=200){
               throw new Error(`error occurs, status code is ${resp.status}`)
           }
           return resp.json()
       })
       .then(json=>{
           dispatch({type:FETCH_USER_SUCCESS, data: json})
       }).catch(e=>{
           dispatch({type:FETCH_USER_FAILURE, data:e})
       })
}
```

使用了 ES7 后

```js
export function fetchData(userId) {
    return (dispatch) => {
        dispatch({type: FETCH_USER_START});
        return fetchRemoteData(dispatch, userId);
    };
}

async function fetchRemoteData(dispatch, userId) {
    try {
        let resp = await fetch(`${API_URL}/${userId}`);
        if (resp.status != 200) {
            throw new Error(`error occurs, status code is ${resp.status}`)
        }
        let json = await resp.json();
        dispatch({type: FETCH_USER_SUCCESS, data: json})
    } catch (e) {
        dispatch({type: FETCH_USER_FAILURE, data: e})
    }
}
```
