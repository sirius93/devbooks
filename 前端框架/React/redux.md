# Redux

## Flux

Flux 数据流动图

![](http://cdn4.infoqstatic.com/statics_s1_20160105-0313u5/resource/news/2014/05/facebook-mvc-flux/zh/resources/0519001.png)

数据单向流动

Action -> Dispatcher -> Store -> View -> Action...

- Action 发送给 Dispatcher
- Store 向 Dispatcher 注册自己
- Dispatcher 接收到 Action 后发送给所有注册在上面的 Store
- Store 更新状态
- View 从 Store 中获得最新状态更新视图

## Redux

Redux 是 js 的状态容器，提供可预测化的状态管理。由 Flux 演变而来，简化了 Flux 的流程。

### 数据处理流程

Reducer 处理数据，接收不同的 Action Type 和旧的 state，返回新的 state。Reducer 表示了如何更新 state。Reducer 应该没有副作用，只要传入的参数一致，必须返回一致的结果，所以不能使用 `Date.now()` 等非纯函数。当遇到未知的 action 时，reducer 应该返回旧的 state。
Action 发送请求（服务器响应，用户输入）到 store，是 store 的唯一数据源。Action 表示了一件事情的发生。
Store 存储 state，一个应用只应有一个 store 存在。


### 常用文件结构

```
app/
  actions/
  components/
  containers/
    App.js         应用基本界面
    xxx.js         应用具体界面
  reducers/
index.js   注册程序
```

### 组件

Redux 中组件分为容器组件和展示组件。
容器组件工作在路由层，从 Redux 获取 state，向 Redux 派发 actions。
展示组件属于中间层，只是接受数据并展示，不知道数据的来源也不知道 Redux 的存在。

### Action

特别注意设计 Action 的属性时千万不要使用 `type` 作为属性的名字

#### Action Type

```js
export const FETCH_USER_START = "FETCH_USER_START";
```

#### Action Creator

##### 同步 ActionCreator

```js
export function echo(msg) {
    return {
        type: ECHO,
        data: msg
    }
}
```

##### 异步 

一般情况下，每个 API 请求都至少需要 dispatch 三个不同的 action：
一个通知 reducer 请求开始的 action。
一个通知 reducer 请求成功结束的 action。
一个通知 reducer 请求失败的 action。  

```js
export function fetchData(userId) {
   return function (dispatch) {
       dispatch({type: FETCH_USER_START});
       return fetchUser(dispatch, userId);
   };
}

function fetchUser(dispatch, userId) {
   return fetch(`${API_URL}/${userId}`)
       .then(resp=> {
           if(resp.status!=200){
               throw new Error(`error occurs, status code is ${resp.status}`)
           }
           return resp.json()
       })
       .then(json=>{
           dispatch({type:FETCH_USER_SUCCESS, data: json})
       }).catch(e=>{
           dispatch({type:FETCH_USER_FAILURE, data:e})
       })
}
```

使用了 ES7 后

```js
export function fetchData(userId) {
    return (dispatch) => {
        dispatch({type: FETCH_USER_START});
        return fetchRemoteData(dispatch, userId);
    };
}

async function fetchRemoteData(dispatch, userId) {
    try {
        let resp = await fetch(`${API_URL}/${userId}`);
        if (resp.status != 200) {
            throw new Error(`error occurs, status code is ${resp.status}`)
        }
        let json = await resp.json();
        dispatch({type: FETCH_USER_SUCCESS, data: json})
    } catch (e) {
        dispatch({type: FETCH_USER_FAILURE, data: e})
    }
}
```

### State

Store 里保存一个全局的 State, 每个 Component 也可以由自己的局部 State，通过 `this.state={}` 初始化局部 State，通过 `this.setState({})` 进行重新绘制

默认画面是否刷新是判断 State 里的 preState 的值和当前的 nextState 是否是同一引用来决定是否刷新， 对于对象，数组等元素如果只是添加，删除元素的话实际改变的还是 preState，引用相同，所以也不会触发重新绘制
可以使用 Immutable 每次返回新数据

### 方法

#### createStore

接收一个代表 reducer 的函数,并将 reducer 函数注册到 store 上

#### bindActionCreators 和 connect

- `bindActionCreators()` 用于将 `dispatch` 对象绑定到 ActionCreator 上，并且在调用 ActionCreator 时实际调用 `dispatch(actionCreator.apply(args))`
- `connect()` 绑定 store 到其参数对象上



## 相关中间件

### react-redux

提供了在 React 项目中使用 Redux 架构的功能
react-redux 提供两个关键模块：Provider 和 connect

#### provider

Provider 这个模块是作为整个 App 的容器，在你原有的 App Container 的基础上再包上一层，它的工作很简单，就是接受 Redux 的 store 作为 props。

#### connect

connect([mapStateToProps], [mapDispatchToProps], [mergeProps], [options])
connect 就是将 store 中的必要数据(不是整个 state)作为 props 传递给 React 组件来 render，并包装 action creator 用于在响应用户操作时 dispatch 一个 action。connect 返回一个函数，它接受一个 React 组件的构造函数作为连接对象，最终返回连接好的组件构造函数。

#### mapDispathToProps 和 mapStateToProps

mapDispatchToProps（）
注意左边和右边的名字需要不一样

例：

```js
const mapStateToProps = state => {
  return {
    todos: getVisibleTodos(state.todos, state.visibilityFilter)
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onTodoClick: id => {
      dispatch(toggleTodo(id))
    }
  }
}

const VisibleTodoList = connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoList)

export default VisibleTodoList
```

### redux-thunk

一个 thunk 就是一个封装表达式的函数，封装的目的是延迟执行表达式
Redux thunk 会检测函数是否需要 dispatch, 会自行绑定，不使用的话就需要手动传入 store.dispatch
Redux thunk 除了会传入 dispatch 参数外, 还会把 getState 作为第二个参数传入

ActionCreator 创建 Action 是同步的，redux-thunk 可以将 ActionCreator 转换为 Thunk ActionCreator 来实现异步操作，即

- 同步情况：dispatch(action)
- 异步情况：dispatch(thunk)

例：

```js
//同步 Creator
function receivePosts(subreddit, json) {
  return {
    type: RECEIVE_POSTS,
    subreddit,
    posts: json.data.children.map(child => child.data),
    receivedAt: Date.now()
  }
}

//异步 Creator
export function fetchPosts(subreddit) {
  return function (dispatch) {
    // 首次 dispatch：更新应用的 state 来通知 API 请求发起了。
    dispatch(requestPosts(subreddit))

    // thunk middleware 调用的函数可以有返回值，
    return fetch(`http://www.subreddit.com/r/${subreddit}.json`)
      .then(response => response.json())
      .then(json =>
        // 这里，使用 API 请求结果来更新应用的 state。
        dispatch(receivePosts(subreddit, json))
      )
  }
}
```

### redux-logger

用于在控制台上输出对应的 state 内容

### react-router-redux

用于实现路由控制

#### Link

通过 `Link` 标签直接调用对应的 History Api，而不是刷新整个页面
