# React

官方网站 [https://facebook.github.io/react/](https://facebook.github.io/react/)

## JSX

JSX = JavaScript XML

### 元素

JSX 中小写的字符串是 HTML 标签，大写开头的变量是 React 组件。

```js
var myDivElement = <div/>;
var myElement = <MyComponent/>;
```

### 属性

#### Dom 属性

元素属性使用 `{}` 定义，而不是 `""`。
而对于 DOM 属性，由于存在关键字的问题，所以会有以下两种别名的存在：

- `htmlFor` - html 的别名
- `className` - class 的别名

```js
var myDivElement = <div className="foo" />;
var myElement = <MyComponent someProperty={true} />;
```

#### 非 Dom 属性

非 DOM 属性有三种：dangerouslySetInnerHTML、ref、key

- dangerouslySetInnerHTML：在 JSX 中直接插入 HTML 代码，比如说动态生成的 Html，因为会存在 XSS 问题所以是这个名字
- ref：父组件引用子组件，因为 react 提倡模块化，所以子组件是无法访问父组件，控制是单向的。注意 ref 获得的只是虚拟节点，无法直接操作 DOM 节点
- key：提高渲染性能，给节点添加标识，在 diff 算法中提高效率，常用于列表

### 表达式

`{}` 中只能使用求值表达式，不能使用语句

### 注解

类似 JavaScript，但是子元素位置的注解需要使用 `{}` 包裹。

```js
<Node>
  {/* child comment, put {} around */}
  <SubNode>
```

### 展开属性 spread attributes

`...` 展开操作符，可以将属性直接扩散到元素上，无需直接指定。扩散时也可以在元素上指定某一个进行覆盖。

```js
var props = {};
props.foo = x;
props.bar = y;
var component = <Component {...props} foo={'override'}/>;
```



## Component

### 定义 Component

```js
class MyToast extends Component {}
```

### 生命周期

组件本质上是状态机，输入确定，输出一定确定。
状态发生转换时会触发不同的钩子函数，从而让开发者有机会做出响应。

生命周期流程：初始化->运行->销毁

#### 分类

初始化

- getDefaultProps 只调用一次，实例之间共享引用
- getInitialState 初始化每个实例特有的状态
- componentWillMount    只会在装载之前调用一次，在 render 之前触发，是 render 前最后一次修改状态的机会
- componentDidMount 只会在装载完成之后调用一次，在 render 之后并渲染完成真实 Dom 之后触发，可以修改 Dom
- render  只能访问 this.props 和 this.state，只有一个顶层组件，不允许修改状态和 DOM 输出

运行中

以下不会在首次 render 组件的周期调用

- componentWillReceiveProps 父组件修改属性触发，可以修改新属性、修改状态
- shouldComponentUpdate 返回 false 会阻止 render() 被调用
- componentWillUpdate 不能修改属性和状态
- componentDidUpdate  	可以修改 DOM

销毁

- componentWillUnmount  在删除组件之前进行清理操作，比如计时器和事件监听器



#### 使用生命周期

例：

```js
componentDidMount() {
  this.fetchData();
}
constructor(props) {
 super(props);
}
```


### 导出，导入 Component

```js
//导出
export default class MyToast extends Component {
}

//导入
import MyToast from './_plugin_toast';
```

### props

一般用于定义不可变属性以及父组件向子组件传递的属性

#### 定义 props

第一种方式，定义在类中

```js
class MyToast extends Component {
  static defaultProps = {
    maxLoops: 10
  };
  static propTypes = {
    text: React.PropTypes.string.isRequired,
    maxLoops: React.PropTypes.number.isRequired
  };
}
```

第二种方式，定义在类外

```js
MyToast.defaultProps = {
  maxLoops: 10
};
MyToast.propTypes = {
  text: React.PropTypes.string.isRequired,
  maxLoops: React.PropTypes.number
};
```

#### 父组件向子组件传递

```js
<Child prop1={xxx} prop2={xxx} />
```

### state

一般用于定义可变状态

第一种方式，定义在构造方法中

```js
constructor(props) {
  super(props);
  this.state = {loopsRemaining: this.props.maxLoops};
}
```

第二种方式，定义为静态变量

```js
state = {
    loopsRemaining: this.props.maxLoops
};
```

### 事件处理


#### 绑定事件

```js
class LikeButton extends Component {
  getInitialState() {
    return { liked: false };
  }

  handleClick(e) {
    this.setState({ liked: !this.state.liked });
  }

  render() {
    const text = this.state.liked ? 'like' : 'haven\'t liked';
    return (
      <p onClick={this.handleClick.bind(this)}>
          You {text} this. Click to toggle.
      </p>
    );
  }
}
```

如果启用了 esLint 的话上面的方式会报错，因为 In Class Property 并不安全

有以下两种解决方式：

第一种

```js
constructor(props) {
   super(props);
   this._handleItemClick = this._handleItemClick.bind(this);
 }

 _handleItemClick() {
   this.setState({activeItem: name});
 }
```

第二种

安装 `transform-class-properties`

```js
_handleItemClick = (e, {name}) => {
  this.setState({activeItem: name});
};
```


### 高阶组件

推荐使用高阶组件替代 Mixins
高阶组件可以在不修改原组件代码的情况下增强元组件的功能
高阶组件接收一个组件作为参数，类似装饰器模式

例：

```js
import React from 'react';
import { IntervalEnhance } from "./interval-enhance.jsx"; // 1. 导入包裹组件

class CartItem extends React.Component {
  // component code here
}

export default IntervalEnhance(CartItem); // 2. 包裹原有的 CartItem 组件
```

以上 CartItem 为需要增强的组件，IntervalEnhance 为高阶组件

```js
export let IntervalEnhance = ComponsedComponent => class extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      seconds: 0 // 2
    };
  }
  render() {
    return <ComponsedComponent {...this.props} {...this.state} />;
  }
}
```

以上实际上是以下方式的简写

```js
export function foobar(){
  return function(ComponsedComponent){
    return class Foobar extends Component{
    }
  }
}
```

##  常用组件

### 表单

表单中的组件可以分为不可控组件和可控组件

#### 不可控组件

组件中的数据和 state 不一定是同步的

```js
<input type="text" defaultValue="hello"/>
```

获得数据

```js
ReactDOM.findDOMNode(this.refs.helloTo).value
```

#### 可控组件

```js
<input type="text" defaultValue={this.state.value}/>
```

获得数据

```js
this.state.value
```




## Class 与 Style

### className

### 多个 className

### 定义 Style

### 内联 Style



## 与 ES5 对比

### 类

Before

```js
import React from 'react';

class Hello extends React.Component {
  render() {
    return <h1>Hello</h1>
  }
}
```

Without ES6

```js
var React = require('react');

var Hello = React.createClass({displayName: 'Hello',
  render: function() {
    return React.createElement("h1", null, "Hello ");
  }
});
```



## 参考资料


### 示例

- react-server-example [https://github.com/mhart/react-server-example](https://github.com/mhart/react-server-example)


