# React Router

## 使用

### 跳转

根据不同的路由实现(hash, history)，一般使用 `<Link>` 标签，否则就要手动指定具体的跳转方式

### 路由参数

路由中携带参数

```js
<Route path="/repos/:userName/:repoName">
```

获取路由中的参数

```js
this.props.params.repoName
```
