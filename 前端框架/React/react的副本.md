Nabu



http://nativebase.io/docs/v2.0.0/



mapDispatchToProps（）
注意左边和右边的名字需要不一样

call() arguments use comma
const ids = yield call(api.listItems, activeType)

put() arguments use bracket
yield put(StoryActions.listSuccess(activeType, items))

action， argument name
注意不要使用 type，千万注意


takeEvery 每个action 触发时调用方法
// takeLatest 不允许并发，只会执行最后一个
// Fork 执行一个非阻塞操作
// take 暂停并等待 action 到达


Ignite 使用




Redux
官方文档
http://redux.js.org/docs/introduction/Motivation.html




-----

React

声明式，不直接操作DOM

环境搭建
工具 CodeKit

JSX
JSX = JavaScript XML

首字母大写为React组件
小写为Dom组件

因为关键字问题，DOM属性
htmlFor 为 html 别名
className 为class 别名


非DOM属性：dangerouslySetInnerHTML、ref、key
dangerouslySetInnerHTML：在JSX中直接插入HTML代码，比如说动态生成的Html，因为有XSS问题所以起这个名字
ref：父组件引用子组件，因为react提倡模块化，所以子组件是无法访问父组件，控制是单向的
key：提高渲染性能，给节点添加标识，在diff算法中提高效率，常用于列表

ref获得的只是虚拟节点，无法直接操作DOM节点

{}中只能使用求值表达式，不能使用语句


节点内注释 {} ，注释需要使用 {}包裹



组件生命周期

组件本质上是状态机，输入确定，输出一定确定。
状态发生转换时会触发不同的钩子函数，从而让开发者有机会做出响应。

初始化->运行->销毁

初始化
getDefaultProps	只调用一次，实例之间共享引用
getInitialState		初始化每个实例特有的状态
componentWillMount	render之前最后一次修改状态的机会
render	只能访问this.props和this.state，只有一个顶层组件，不允许修改状态和DOM输出
componentDidMount	成功render并渲染完成真实DOM之后触发，可以修改DOM

运行中
componentWillReceiveProps	父组件修改属性触发，可以修改新属性、修改状态
shouldComponentUpdate	返回false会阻止render调用
componentWillUpdate	不能修改属性和状态
render
componentDidUpdate	可以修改DOM

销毁
componentWillUnmount	在删除组件之前进行清理操作，比如计时器和事件监听器


参数必须为ReactDom.render装载的参数的节点
 if (event.target.value == "123") {
                    React.unmountComponentAtNode(document.getElementsByTagName("body")[0]);
                    return;
                }


事件处理函数
绑定事件
没有括号，否则等于方法调用
onChange={this.handleChange}


组件协同
组件嵌套
Mixin
Mixin的目的是横向抽离出组件的相似代码，类似面向切面


属性是由父组件传递过来的，不能由组件自身修改
<Hello name=""/>
{}为js求值表达式

状态
setState更新组件状态，会使用diff算法


表单
不可控组件和可控组件

不可控组件
组件中的数据和state不一定是同步的
<input type="text" defaultValue="hello"/>
获得数据
ReactDOM.findDOMNode(this.refs.helloTo).value

可控组件
<input type="text" defaultValue={this.state.value}/>
获得数据
this.state.value


高阶组件
使用高阶组件替代Mixins
高阶组件可以在不修改原组件代码的情况下增强元组件的功能
高阶组件接收一个组件作为参数，类似装饰器模式

```js
import React from 'react';
import { IntervalEnhance } from "./interval-enhance.jsx"; // 1. 导入包裹组件

class CartItem extends React.Component {
  // component code here
}

export default IntervalEnhance(CartItem); // 2. 包裹原有的CartItem组件
```

以上 CartItem 为需要增强的组件，IntervalEnhance 为高阶组件

```js
export let IntervalEnhance = ComponsedComponent => class extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      seconds: 0 // 2
    };
  }
  render() {
    return <ComponsedComponent {...this.props} {...this.state} />;
  }
}
```

以上实际上是以下方式的简写

```js
export function foobar(){
  return function(ComponsedComponent){
    return class Foobar extends Component{
    }
  }
}
```
