[TOC]

## JQuery Validation Plugin

## 添加依赖

导入 `jquery.js` 和 `jquery.validate.js`

```js
<script src="js/jquery-1.9.1.js"></script>
<script src="js/jquery.validate.js"></script>
```

## 使用

#### 使用标签定义规则

##### 表单

通过 `minlength` 和 `required` 等侵入式的标签属性来定义验证的规则

```html
<form id="loginForm" method="get">
    <fieldset>
        <legend>Please provide your name, email address (won't be published) and a comment</legend>
        <p>
            <label for="cname">Name (required, at least 2 characters)</label>
            <input id="cname" name="name" minlength="2" type="text" required>
        </p>
        <p>
            <label for="cpassword">Password (required)</label>
            <input id="cpassword" type="password" name="password" required>
        </p>
        <p>
            <label for="email">E-Mail (optional)</label>
            <input id="email" type="email" name="email">
        </p>
        <p>
            <input class="submit" type="submit" value="Submit">
        </p>
    </fieldset>
</form>
```

##### 提交验证

```js
$.validator.setDefaults({
    submitHandler: function () {
        alert("submitted!");
    }
});
$().ready(function () {
    $("#loginForm").validate();
});
```

### 使用 JS 定义规则

##### 表单

```html
<form class="cmxform" id="signupForm" method="get" action="">
    <fieldset>
        <legend>Validating a complete form</legend>
        <p>
            <label for="firstname">Firstname</label>
            <input id="firstname" name="firstname" type="text">
        </p>
        <p>
            <label for="lastname">Lastname</label>
            <input id="lastname" name="lastname" type="text">
        </p>
        <p>
            <label for="username">Username</label>
            <input id="username" name="username" type="text">
        </p>
        <p>
            <label for="password">Password</label>
            <input id="password" name="password" type="password">
        </p>
        <p>
            <label for="confirm_password">Confirm password</label>
            <input id="confirm_password" name="confirm_password" type="password">
        </p>
        <p>
            <label for="agree">Please agree to our policy</label>
            <input type="checkbox" class="checkbox" id="agree" name="agree">
        </p>
        <p>
            <input class="submit" type="submit" value="Submit">
        </p>
    </fieldset>
</form>
```

##### 提交验证

```js
$.validator.setDefaults({
    submitHandler: function () {
        alert("submitted!");
    }
});
$().ready(function () {
    $("#signupForm").validate({
        rules: {
            firstname: "required",
            lastname: "required",
            username: {
                required: true,
                minlength: 2
            },
            password: {
                required: true,
                minlength: 5
            },
            confirm_password: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            email: {
                required: true,
                email: true
            },
            topic: {
                required: "#newsletter:checked",
                minlength: 2
            },
            agree: "required"
        },
        messages: {
            firstname: "Please enter your firstname",
            lastname: "Please enter your lastname",
            username: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 2 characters"
            },
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            confirm_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            },
            email: "Please enter a valid email address",
            agree: "Please accept our policy",
            topic: "Please select at least 2 topics"
        }
    });
});
```

