[TOC]

# Restful

## 实现

- [Jersey](https://jersey.java.net/)

Jersey 框架实现了 JAX-RS 2.0 API。

JAX-RS 即 Java API for RESTful Services。根据 REST 架构，一个 RESTful Web 服务不应该在服务器上保持客户端状态。这种约束被称为无状态。
