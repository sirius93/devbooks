[TOC]

# Netty

官网 [http://netty.io/](http://netty.io/)

## Overview

Netty 是一个基于 JAVA NIO 类库的异步通信框架，它的架构特点是：异步非阻塞、基于事件驱动、高性能、高可靠性和高可定制性。
Netty 是基于 Reactor 模式构建的。
Vert.x 就是基于 Netty 开发的。最新版为 Netty 5，但是稳定版为 Netty 4。

## 添加依赖

```xml
<dependency>
    <groupId>io.netty</groupId>
    <artifactId>netty-all</artifactId>
    <version>5.0.0.Alpha2</version>
</dependency>
```

## 使用

### 传递 Object

创建作为消息的 JavaBean

```java
public class Command implements Serializable {

    private String action;
    private String data;
}
```

创建服务端的消息处理类，本例中用于接收并打印客户端发送过来的 Command 对象

```java
public class CommandServerHandler extends ChannelHandlerAdapter {

    //发送数据
    @Override
    public void channelActive(final ChannelHandlerContext ctx) {
      System.out.println("Server send data:");

      final ByteBuf data = ctx.alloc().buffer(4);
      data.writeInt(65535);

      final ChannelFuture future = ctx.writeAndFlush(data);
      future.addListener(new ChannelFutureListener() {

         public void operationComplete(ChannelFuture future) {
             System.out.println("operationComplete");
         }
      });
    }

    //获取数据
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.out.println("Server receive data:");

        Command command = (Command) msg;
        System.out.println(command);
        ctx.close();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }
}
```

创建服务器

```java
// bossGroup 线程池用来接受客户端的连接请求
EventLoopGroup bossGroup = new NioEventLoopGroup();
// workerGroup 线程池用来处理 boss 线程池里面的连接的数据
EventLoopGroup workerGroup = new NioEventLoopGroup();
try {
    ServerBootstrap serverBootstrap = new ServerBootstrap();
    serverBootstrap.group(bossGroup, workerGroup)
            .channel(NioServerSocketChannel.class)
            .childHandler(new ChannelInitializer<SocketChannel>() {
                @Override
                public void initChannel(SocketChannel ch) throws Exception {
                    ch.pipeline()
                            //Netty 内置的处理对象序列化的工具
                            .addLast(
                                    new ObjectDecoder(ClassResolvers.cacheDisabled(getClass().getClassLoader()))
                            ).addLast(new CommandServerHandler());
                }
            })
            .option(ChannelOption.SO_BACKLOG, 128)
            .childOption(ChannelOption.SO_KEEPALIVE, true);

    // Bind and start to accept incoming connections.
    ChannelFuture future = serverBootstrap.bind(port).sync();

    // Wait until the server socket is closed.
    future.channel().closeFuture().sync();
} finally {
    workerGroup.shutdownGracefully();
    bossGroup.shutdownGracefully();
}
}
```

创建客户端消息处理类，本例中用于建立连接时向服务器发送 Command 对象。

```java
public class CommandClientHandler extends ChannelHandlerAdapter {

    // 获取数据
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        System.out.println("Client receive data:");

        ByteBuf byteBuf = (ByteBuf) msg;
        try {
            System.out.println(byteBuf.readInt());
            ctx.close();
        } finally {
            byteBuf.release();
        }
    }

    // 发送数据
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("Client send data:");

        Command command = new Command();
        command.setAction("insert");
        command.setData("{\"x\":1}");

        ctx.write(command);
        ctx.flush();
        //处理完成,关闭服务
        ctx.close();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }
}
```

创建客户端

```java
String host = "localhost";
int port = 8080;
EventLoopGroup workerGroup = new NioEventLoopGroup();

try {
    Bootstrap bootstrap = new Bootstrap();
    bootstrap.group(workerGroup)
            .channel(NioSocketChannel.class)
            .option(ChannelOption.SO_KEEPALIVE, true)
            .handler(new ChannelInitializer<SocketChannel>() {
                @Override
                public void initChannel(SocketChannel ch) throws Exception {
                    ch.pipeline()
                            //Netty 内置的处理对象序列化的工具
                            .addLast(new ObjectEncoder())
                            .addLast(new CommandClientHandler());
                }
            });

    // Start the client.
    ChannelFuture future = bootstrap.connect(host, port).sync();

    // Wait until the connection is closed.
    future.channel().closeFuture().sync();
} finally {
    workerGroup.shutdownGracefully();
}
```

---

可运行实例代码位于 `spring/first_netty` 目录下

## 参考资料

- [Netty系列之Netty高性能之道](http://www.infoq.com/cn/articles/netty-high-performance/)
- [Netty 教程](http://blog.csdn.net/kobejayandy/article/details/11493717)
