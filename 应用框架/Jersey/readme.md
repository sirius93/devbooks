# Restful

## 实现

- [Jersey](https://jersey.java.net/)

Jersey 框架实现了 JAX-RS 2.0 API。

JAX-RS 即 Java API for RESTful Services。根据 REST 架构，一个 RESTful Web 服务不应该在服务器上保持客户端状态。这种约束被称为无状态。

## Jersey

### 安装

Http Api

```xml
<dependency>
    <groupId>org.glassfish.jersey.containers</groupId>
    <artifactId>jersey-container-grizzly2-http</artifactId>
    <version>2.22.1</version>
</dependency>
```

J2EE (Servlet Support)

```xml
<dependency>
    <groupId>org.glassfish.jersey.containers</groupId>
    <artifactId>jersey-container-grizzly2-servlet</artifactId>
    <version>2.22.1</version>
</dependency>
```

Jersey 单元测试框架

```xml
<dependency>
    <groupId>org.glassfish.jersey.test-framework</groupId>
    <artifactId>jersey-test-framework-util</artifactId>
    <scope>test</scope>
    <version>2.22.1</version>
</dependency>
```

测试框架使用的容器

```xml
<dependency>
    <groupId>org.glassfish.jersey.test-framework.providers</groupId>
    <artifactId>jersey-test-framework-provider-bundle</artifactId>
    <type>pom</type>
    <scope>test</scope>
    <version>2.22.1</version>
</dependency>
```

### 使用

#### Resource

```java
@Path(value = "/todoservice")
public class TodoService {
    //同步 resource
    @GET
    @Produces(value = MediaType.TEXT_XML)
    public Todo getXml() {
        Todo todo = new Todo();
        todo.setSummary("This is my first todo");
        todo.setDescription("This is my first todo");
        return todo;
    }

     public static final ExecutorService service = Executors.newFixedThreadPool(1);

    //异步 resource
    @GET
    @Produces(value = MediaType.TEXT_XML)
    public void getXml(@Suspended final AsyncResponse response) {
        service.execute(new Runnable() {
            @Override
            public void run() {
                Todo todo = new Todo();
                todo.setSummary("This is async todo");
                todo.setDescription("This is async todo");
                response.resume(todo);
            }
        });
    }
}

@XmlRootElement
public class Todo {
    private String summary;
    private String description;
}
```

#### HttpServer

开启 Server

```java
//指定 Resource 所在的包名
ResourceConfig resourceConfig = new ResourceConfig().packages(Main.class.getPackage().getName());
HttpServer server = GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URL), resourceConfig);
Client client = ClientBuilder.newClient();
WebTarget target = client.target(BASE_URL);
//...
server.shutdown();
```

#### JUnit 测试 HttpServer

```java
//同步
String xml = target.path("/helloworld")
                .request()
                .accept(MediaType.TEXT_HTML)
                .get(String.class);
System.out.println(xml);

//异步
Future<Response> future = target.path("/async/todo")
                .request()
                .accept(MediaType.TEXT_XML)
                .async()
                .get();
Response responseFuture = future.get();
System.out.println(responseFuture.getEntity());
```

#### 使用Jersey 单元测试框架 进行测试

```java
public class JersyTodoServiceTest extends JerseyTest {

    @Override
    protected Application configure() {
        return new ResourceConfig().packages(Main.class.getPackage().getName());
    }

    @Test
    public void testCreateUser() throws Exception {
        Form form = new Form();
        form.param("summary", "foo");
        form.param("desc", "bar");

        String result = target("todoservice/todo")
                .request(MediaType.APPLICATION_XML)
                .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE), String.class);
        System.out.println("result is " + result);
    }
}
```

#### J2EE 支持

web.xml

```xml
<servlet>
    <servlet-name>Jersey Web Application</servlet-name>
    <servlet-class>org.glassfish.jersey.servlet.ServletContainer</servlet-class>
    <init-param>
        <param-name>jersey.config.server.provider.packages</param-name>
        <param-value>com.mrseasons.spring.first.jersey</param-value>
    </init-param>
    <load-on-startup>1</load-on-startup>
</servlet>
<servlet-mapping>
    <servlet-name>Jersey Web Application</servlet-name>
    <url-pattern>/foobar/*</url-pattern>
</servlet-mapping>
```

Form 表单提交的 Resource 处理

```java
@POST
@Path("/todo")
@Produces(MediaType.APPLICATION_XML)
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
public Todo createUser(@FormParam("summary") String summary,
                       @FormParam("desc") String desc,
                       @Context HttpServletResponse servletResponse) throws Exception {
    Todo todo = new Todo();
    todo.setSummary(summary);
    todo.setDescription(desc);

    if ("tiger".equals(summary)) {
        throw new MyException("tiger error");
    }
    if ("tiger".equals(desc)) {
        int i = 1 / 0;
    }

    return todo;
}
```

JSON 的 Resource

```java
@POST
@Produces(value = MediaType.APPLICATION_JSON)
public String getJsonObject() throws IOException {
    ByteOutputStream outputStream = new ByteOutputStream();
    ObjectMapper mapper = new ObjectMapper();
    JsonGenerator generator =
            mapper.getFactory().createGenerator(outputStream, JsonEncoding.UTF8);
    generator.useDefaultPrettyPrinter();
    generator.writeStartObject();
    generator.writeObjectFieldStart("user");
    generator.writeStringField("name", "Jack");
    generator.writeNumberField("age", 12);
    generator.writeEndObject();
    generator.writeStringField("address", "London");
    generator.writeEndObject();
    generator.flush();
    return new String(outputStream.getBytes());
}
```

#### Exception Provider

用于处理异常

自定义的异常

```java
public class MyException extends WebApplicationException {

    public MyException() {
        super(Response.status(Response.Status.NOT_FOUND).build());
    }

    public MyException(String message) {
        super(Response.status(Response.Status.NOT_FOUND).
                entity(message).type("text/plain").build());
    }
}
```

内置异常

```java
@Provider
public class ArithmeticExceptionMapper implements ExceptionMapper<ArithmeticException> {
    @Override
    public Response toResponse(ArithmeticException ex) {
        return Response.status(400).
                entity(ex.getMessage()).
                type("text/plain").
                build();
    }
}
```
