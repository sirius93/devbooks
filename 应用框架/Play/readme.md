[TOC]

# Play Framework

[官网](https://www.playframework.com/)

## 安装

1. 下载 [Typesafe Activator](https://typesafe.com/get-started)
2. 解压后执行 `./activator [-Dhttp.address=0.0.0.0 -Dhttp.port=8888]`
3. 访问上述地址
4. 选择任一模板，如 "play-scala-intro" 点击 `Create App` 创建应用。

通过命令行创建应用

```bash
activator new my-first-app play-scala-intro
cd my-first-app
activator run
```

然后访问 `http://localhost:9000`。

## 使用


## 参考资料

- [Play Framework 中文文档(2.3.x)](http://xring.info/play-for-scala-developers/index.html)
- [Play Document](https://www.playframework.com/documentation/2.5.x/ScalaActions)
