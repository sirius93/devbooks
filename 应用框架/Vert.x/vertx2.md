# 基本概念

## 组成

### Verticle

1. 能够被 Vert.x 执行的代码包被称为 Verticle。也就是说 Verticle 是Vert.x 的可执行单元。
2. Verticles 可以使用多种语言编写。
3. 一个 Vert.x 实例可以同时运行多个 Verticle。但是一个 Verticle 实例总是运行在单一线程中。
4. 一个 Verticle 可以被直接运行，但是通常都是被打包成 Module。

### Module

1. 一个 Vert.x 应用通常由多个 Module 组成。
2. 一个 Module 可以包含多个 Verticle。
3. Modules 可以被安装到 Maven 仓库。

### Vert.x Instances

1. Verticles 运行在 Vert.x 实例中。
2. 一个 Vert.x 实例运行在其自己的 JVM 中。
3. Vert.x 集群可以通过 eventbus 来使内部的 verticles 之间进行通信。

### Event Loops

1. 一个 Vert.x 实例在其内部管理着一组线程。通常这线程数和服务器的内核数相等。这些线程就被称作为 event loops。
2. 一个 Verticle 实例被部署时，服务器会分配一个 event loop 给这个Verticle。
3. 一个 Event Loop 可能被分配给多个 Verticle。

记住，绝对不要阻塞 Event Loop。

### Worker Verticles

1. 在 Worker 中可以执行 Block 线程的代码。
2. 一个Worker Verticle不是运行在 Event Loop 中，而是运行在内部的 Worker Pool 中。

### Shared data

1. Vert.x 提供的用于实现共享数据的简单 map 和 set。

2. 用于解决在单个 Vert.x 实例中的多个 Verticles 间的数据共享。

3. Shared data 通过不可变的特性来解决并发访问的问题。


# 在命令行中使用

## 运行 Verticle

```shell
vertx run <verticle> [parameters]
```

参数
- `-conf <config_file>` config_file 是一个 json 文件，用于指定运行时的配置。通常

- `-cp <path>` 指定其余 Verticle 和资源文件的位置。默认值为"."代表当前路径。多个 path 间用":"进行分隔。
  例如：`-cp classes:lib/otherscripts:jars/myjar.jar:jars/otherjar.jar`

- `-instances <instances>` 指定一个 Vert.x Server 运行的 Verticle 的实例数。

- `includes <mod_list>` 指定包含到此 Verticle 的 Module 列表，多个 Module 间用","分隔。

- `-worker` 指定此 Verticle 是否为一个 Worker。

- `-cluster` 指定此 Verticle 是否与其它网络上的 Verticle 构成集群。集群的 Vert.x 实例可以通过分布式的 Event Bus 与其它节点间进行通信。默认值为 false。

- `-cluster-port` 指定集群通信的端口号。默认为0，意味着随机挑选一个空闲的临时端口。通常不用自己指定。

- `-cluster-host` 指定集群通信的地址。默认会挑选一个可用的接口。

例子

运行10个实例

`vertx run com.acme.MyVerticle -cp "classes:lib/myjar.jar" -instances 10`

在一台机器上运行两个 Verticle 构成集群

```
vertx run handler.js -cluster
vertx run sender.js -cluster
```

## 运行 Module

```shell
vertx runmod <module name>
```

参数

- `-conf <config_file>`	config_file 为 json文件，包含运行时的各种参数
- `-instances <instances>`
- `-cluster`
- `-cluster-host`
- `-cp` Vert.x 会使用 mod.json 作为配置文件来读取其余 Verticle 和资源文件。

例子

`vertx runmod com.acme~my-mod~2.1`

### 运行 Module Zip

```shell
vertx runzip <zip_file_name>
```

### 运行 Module Fat Jar

```shell
vertx fatjar <module_name>
```

## High Availability (HA) Support

### 故障恢复

```shell
vertx runmod com.acme~my-mod~2.1 -ha
```
指定"-ha"时隐含指定"-cluster"。
当一个 Verticle 运行在 HA 状态时，一旦遇到故障导致 Verticle 被关闭，此 Verticle 会自动在集群的其它节点进行重启。但是如"ctrl+c" 等正常退出不会造成这种情况。

### HA Groups

只有在同一个 HA Groups的各节点间会执行故障恢复。开启 HA 但不指定 HA Group 时默认都属于__DEFAULT__。

```shell
vertx runmod <module name> -ha -hagroup <group name>
```

例子

```shell
In console 1:
vertx runmod com.mycompany~my-mod1~1.0 -ha -hagroup g1

In console 2:
vertx runmod com.mycompany~my-mod2~1.0 -ha -hagroup g1

In console 3:
vertx runmod com.mycompany~my-mod3~1.0 -ha -hagroup g2
```

如果 kill console1，那么 mod1 会在console2中被重启。
如果 kill console3，那么 mod3不会被重启。

### Network Partitions
Quorum是集群中的特殊 group，指定了当集群中节点数至少为多少时才执行故障恢复。

```shell
vertx runmod <module name>  -ha -quorum <num>
```

#  Verticle

## 成员变量
- container
container对象代表着当前 Verticle 运行于哪一个容器内。其可以用于控制 Verticle 和 Module 的部署及环境变量，Log 等的访问。

- vertx
vertx 对象用于访问 Vert.x 的 Core API。

## 成员方法

- start()
- start(future)

## 建立并启动一个 Verticle

继承org.vertx.java.platform.Verticle类，实现其中的 start()方法
start()方法为一个 Verticle 启动的入口

## 异步启动

有时需要在一个 Verticle 中启动多个 Verticle，且只有这些 Verticle 都启动成功后才认为程序被启动成功了。此时可以实现start()的变种方法： start(futrue)方法，通过 future 的判断来执行此逻辑。

## 关闭 Container
```shell
container.exit()
```

## Log

```shell
Logger logger = container.logger();
logger.info("I am logging something");
```

## 通过程序部署其它Verticle

### 部署 Verticle

```shell
container.deployVerticle(otherVerticleInstance);
container.deployVerticle(verticlePath);
```

在代码中指定 config

```shell
JsonObject jsonConfig = new JsonObject();
config.putString("foo", "wibble");
container.deployVerticle(verticlePath, jsonConfig);
```

### 部署 Worker

使用deployWorkerVerticle代替 deployVerticle

### 部署 Module

```shell
container.deployModule("io.vertx~mod-mailer~2.0.0-beta1", config);
```

### 同时启动多个Verticle实例

```shell
container.deployVerticle(verticlePath, jsonConfig, numberOfInstances);
```

### 部署成功的回调方法

```shell
container.deployVerticle("foo.ChildVerticle", new AsyncResultHandler<String>() {
    public void handle(AsyncResult<String> asyncResult) {
        if (asyncResult.succeeded()) {
            System.out.println("The verticle has been deployed, deployment ID is " + asyncResult.result());
        } else {
            asyncResult.cause().printStackTrace();
        }
    }
});
```

### 卸载 Verticle
```java
container.undeployVerticle(deploymentID);
```

# Event Bus

##  基本概念

Event Bus 实现了不同Verticles 之间的通信。从而无需关心 Verticle 是用什么语言编写的，也不用关心它们是否在同一个 Vert.x 实例中。

Event Bus 构成的点对点分布式消息系统可以横跨多个服务器节点和多个浏览器。

基本的 Event Bus API 包括了registering handlers，unregistering handlers 和 sending 及 publishing messages.

使用 Event Bus进行通信时注意多个 Verticle 要由一个统计的 Verticle 进行部署，或者多个 Verticle 部署在用一个集群上。

## 原理

### Addressing

Event Bus 上的消息会被发送到一个 Address上。
Address 只是一个普通的字符串。

### Handlers

Handler 负责从 Event Bus 上接收消息。
必须先在一个 Address 上注册一个 Handler。
来自不同的 Verticles的 Handlers 可以被注册在同一个 Address 上。
一个 Handler 也可以被注册在多个 Address 上。

### Publish / subscribe messaging

Event Bus 支持 publish 消息。
Message 被 published 到一个 address 上。
Publish 的意思是发送消息到所有注册到某一个 address 上的所有 handlers。

### Point to point and Request-Response messaging

Event Bus 支持点对点消息。
Message 被发送到一个 Address 上。
Vert.x会 route 其到指定 address的某一个 handler 上。
如果 Address 上有多个 Handler 的话，Vert.x 会使用非严格的轮询策略。

在发送消息时也可以指定某一个 Reply Handler 负责接收消息。
当消息由recipient接收并已经被处理过后，recipient会调用 Reply Handler 来执行。
This is a common messaging pattern called the Request-Response pattern.

### Transient

Event Bus 上的消息都是Transient的。这意味着消息可能被丢失。所以如果你关系消息是否被丢失，需要使你Handler 时幂等的，并且有重发机制。或者使用一个队列来存储消息。

### Types of messages

string, number, boolean ,json

## Event Bus API

### Registering and Unregistering Handlers

#### Register Handler

```java
EventBus eventBus = vertx.eventBus();

Handler<Message> handler = new Handler<Message>() {
    @Override
    public void handle(Message message) {
        System.out.println("receive message " + message.body());
    }
};
```

#### Unregister Handler

```java
eventBus.unregisterHandler("test.address", handler);
```

#### Register/Unregister with  ResultHandler

当你 register/unregister 一个 handler 时，可能需要一段时间集群上的所有节点才能获得 register 和 unreigster 的信息。可以通过设置第三个参数，当集群上的所有节点都完成操作后获得通知。

```java
eventBus.registerHandler("test.address", handler, new AsyncResultHandler<Void>() {
    @Override
    public void handle(AsyncResult<Void> asyncResult) {
        System.out.println("The handler has been registered across the cluster ok? " + asyncResult.succeeded());
    }
});
```

#### Publishing messages

Message 会被发送到指定 Address 的所有 Handler 上。

```java
eventBus.publish("test.address", "hello world publish");
```

#### Sending messages

Message 会被发送到指定 Address 的某一个 Handler 上。

```java
eventBus.send("test.address", "hello world p2p");
```

#### Replying to messages

在 Receiver 的 Handler 上调用 message.reply(...)可以实现消息的回答。
Reply 的参数可以为空，也可以为 null。

例如

The Receiver

```java
EventBus eventBus = vertx.eventBus();

Handler<Message> handler = new Handler<Message>() {
    @Override
    public void handle(Message message) {

     if (messageAsyncResult.succeeded()) {
        System.out.println("I received a reply to the reply" + messageAsyncResult.result().body());
    } else {
        ReplyException replyException = (ReplyException) messageAsyncResult.cause();
        System.err.println("No reply to the reply was received before the 1 second timeout!");
        System.err.println(replyException.getMessage());
    }

		//reply
       message.reply("This is a reply.");
    }
};

eventBus.registerHandler("test.address", handler);
```

The Sender

```java
EventBus eventBus = vertx.eventBus();

eventBus.send("test.address", "This is a message", new Handler<Message<Object>>() {
    @Override
    public void handle(Message<Object> objectMessage) {
        if (message.succeeded()) {
            System.out.println("I received a reply " + message.result().body());
        } else {
            System.err.println("No reply was received before the 1 second timeout!");
            ReplyException replyException = (ReplyException) message.cause();
            System.err.println(replyException.getMessage());
        }
    }
});
```

##### Specifying timeouts for replies

The Sender

使用 eventbus.sendWithTimeout()代替 eventbus.send()

The Receiver

使用 message.replyWithTimeout()代替 message.reply()


##### Default timeouts

```java
eventBus.setDefaultReplyTimeout(5000);
```

#### Message Types

Vert.x Buffer，JSON 和数组在同一个 JVM 中发送前会被拷贝，以此来避免竞争状态。

# Shared Data

Vert.x 使用java.util.concurrent.ConcurrentMap 和 java.util.Set来传送数据。

Share Data

```java
ConcurrentMap<String, Integer> map = vertx.sharedData().getMap("demo.mymap");
```

Share Map

```java
Set<String> set = vertx.sharedData().getSet("demo.myset");
```

在其它 Verticle 中也可以通过此方法来获得 data。

# Buffers

类似 byte array

##  Creating Buffers

```shell
Buffer emptyBuffer = new Buffer();

Buffer strBuffer = new Buffer("string");

Buffer strEncodeBuffer = new Buffer("你好", "utf-8");

byte[] bytes = new byte[]{1, 2, 3};
Buffer byteBuffer = new Buffer(bytes);
```

### 预先指定 Size

预分配空间有助于提高效率
这些预分配的空间值为 empty，不是0
当超过预分配的空间时 buffer 会自动扩容

```shell
//buffer with specified size
//the buffer is empty, not filled with zero
Buffer bufferWithSize = new Buffer(1000);
```

### 设置内容

```shell
//append buffer
Buffer buffer = new Buffer();
buffer.appendInt(1).appendString("hello");

//rand buffer
Buffer randomeBuffer = new Buffer();
buffer.setInt(0, 1);
buffer.setString(3, "world");
```

### 发送 Buffer

```shell
vertx.createNetServer().connectHandler(new Handler<NetSocket>() {
    @Override
    public void handle(NetSocket socket) {
        socket.write(buffer);
    }
}).listen(1234);
```

### 获得 Buffer

```shell
vertx.createNetClient().connect(1234, new Handler<AsyncResult<NetSocket>>() {
    @Override
    public void handle(AsyncResult<NetSocket> event) {
        NetSocket socket = event.result();
        socket.dataHandler(new Handler<Buffer>() {
            @Override
            public void handle(Buffer buffer) {
                // 注意 int 和 byte 的字节数导致 i 变为 i+=4
                for (int i = 0; i < buffer.length(); i+=4) {
                    System.out.println(buffer.getInt(i));
                }
            }
        });
    }
```

# Delayed and Periodic Tasks

## One-short Timers

```shell
long timerID = vertx.setTimer(1000, new Handler<Long>() {
    @Override
    public void handle(Long event) {
        System.out.println("And one second later this is printed");
    }
});
```

返回的 timerID 可以用于取消任务

## Periodic Timers

```shell
long timerID = vertx.setPeriodic(1000, new Handler<Long>() {

    private int time;

    @Override
    public void handle(Long timerID) {
        time++;
        System.out.println("run " + time);

        if (time > 5) {
            vertx.cancelTimer(timerID);
        }
    }
});
```

## Cancelling timers

```shell
vertx.cancelTimer(timerID);
```

# Writing TCP Servers and Clients

## Net Server

### Creating a Net Server

```shell
NetServer netServer= vertx.createNetServer();
netServer.connectHandler(new Handler<NetSocket>() {
    @Override
    public void handle(NetSocket netSocket) {
        System.out.println(netSocket.localAddress().getPort());
    }
}).listen(1234, "127.0.0.1");
```

如果 port 设为"0"的话，系统会随机挑选一个空闲的端口。

### Closing a Net Server

```shell
netServer.close()
```

### Closing with Notification

```shell
netServer.close(new AsyncResultHandler<Void>() {
    public void handle(AsyncResult<Void> asyncResult) {
        log.info("Close succeeded? " + asyncResult.succeeded());
    }
});
```

### Socket 数据传输

见 Buffer

### Closing a Socket

```shell
socket.close()
```

### Socket Closing Handler

```shell
NetServer server = vertx.createNetServer();

server.connectHandler(new Handler<NetSocket>() {
    public void handle(final NetSocket sock) {
        sock.closedHandler(new VoidHandler() {
            public void handle() {
                log.info("The socket is now closed");
            }
        });
    }
}).listen(1234, "localhost");
```

### Socket Exception Handler

```shell
NetServer server = vertx.createNetServer();

server.connectHandler(new Handler<NetSocket>() {
    public void handle(final NetSocket sock) {
        sock.exceptionHandler(new Handler<Throwable>() {
            public void handle(Throwable t) {
                log.info("Oops, something went wrong", t);
            }
        });
    }
}).listen(1234, "localhost");
```

### Event Bus Write Handler

每一个NetSocket 都会自动在 Event Bus 上注册一个默认的 handler用以接收 buffer 数据（必须为 Buffer）。这个默认的handler 绑定在 writeHandlerId()这个地址上。

Receiver

```java
netSocket.dataHandler(new Handler<Buffer>() {
    @Override
    public void handle(Buffer event) {
        System.out.println(event.getString(0, event.length()));
    }
});
```

Writer

```java
NetSocket netsocket = asyncResult.result();
String writeHandleId = netsocket.writeHandlerID();
Buffer buffer = new Buffer();
buffer.appendString("write message");

// 第二个参数必须为 buffer
vertx.eventBus().send(writeHandleId, buffer);
```

### Scaling TCP Servers

开启多个实例

### NetClient

NetClient被用来进行 TCP 通信。

#### Config Reconnection

```java
client.setReconnectAttempts(1000);  //how many times, -1 means infinite, default is 0
client.setReconnectInterval(500); //default is 1000
```

setReconnectAttempts()控制尝试连接多少次，默认值是0，-1表示一直尝试直到连通。
setReconnectInterval()控制尝试重新连接间隔的间隔，默认为1000

### SSL

#### SSL Server

只使用服务器证书
```java
NetServer server = vertx.createNetServer()
    .setSSL(true)
    .setKeyStorePath("/path/to/your/keystore/server-keystore.jks")
    .setKeyStorePassword("password");
```

服务器，客户端双向认证
```java
server = vertx.createNetServer()
    .setSSL(true)
    .setKeyStorePath("/path/to/your/keystore/server-keystore.jks")
    .setKeyStorePassword("password")
    .setTrustStorePath("/path/to/your/truststore/server-truststore.jks")
    .setTrustStorePassword("password")
    .setClientAuthRequired(true);
```
server-truststore.jks必须任何一个务器信任的客户端证书

#### SSL Clients

信任所有证书
无法确定建立连接的对象的身份，无法防御中间人攻击

```java
//        trust all server certificates (dangerous):
        NetClient client = vertx.createNetClient()
                .setSSL(true)
                .setTrustAll(true);
```

信任 trust store 中包括的证书
```java
//        only trust those certificates it has in its trust store:
        client = vertx.createNetClient()
                .setSSL(true)
                .setTrustStorePath("/path/to/your/client/truststore/client-truststore.jks")
                .setTrustStorePassword("password");
                ```

信任 trust store 中包括的证书，同时提供客户端证书

```java
//        only trust those certificates it has in its trust store, and also to supply a client certificate:
        client = vertx.createNetClient()
                .setSSL(true)
                .setTrustStorePath("/path/to/your/client/truststore/client-truststore.jks")
                .setTrustStorePassword("password")
                .setKeyStorePath("/path/to/keystore/holding/client/cert/client-keystore.jks")
                .setKeyStorePassword("password");
```

### User Datagram Protocol (UDP)

#### Sender

```java
DatagramSocket socket=vertx.createDatagramSocket(InternetProtocolFamily.IPv4);

Buffer buffer=new Buffer("content");

//send a buffer
socket.send(buffer, "127.0.0.1", 1234, new AsyncResultHandler<DatagramSocket>() {
    @Override
    public void handle(AsyncResult<DatagramSocket> asyncResult) {
        System.out.println("buffer:"+asyncResult.succeeded());
    }
});

//send a string
socket.send("a string used as content", "127.0.0.1", 1234, new AsyncResultHandler<DatagramSocket>() {
    @Override
    public void handle(AsyncResult<DatagramSocket> asyncResult) {
        System.out.println("content:"+asyncResult.succeeded());
    }
        });
```

#### Receiver

```java
DatagramSocket socket=vertx.createDatagramSocket(InternetProtocolFamily.IPv4);

socket.listen("127.0.0.1", 1234, new AsyncResultHandler<DatagramSocket>() {
    @Override
    public void handle(AsyncResult<DatagramSocket> asyncResult) {
        if(asyncResult.succeeded()){
            socket.dataHandler(new Handler<DatagramPacket>() {
                @Override
                public void handle(DatagramPacket datagramPacket) {
                    System.out.println(datagramPacket.sender().getHostString());
                    System.out.println(datagramPacket.data());
                }
            });
        }else{
            System.out.println(asyncResult.cause());
        }
    }
});
```

#### Multicast


Sender

与普通 UDP Sender 一样

Receiver

监听特定地址和端口并且接收来自指定 group的数据包。

```java
DatagramSocket socket=vertx.createDatagramSocket(InternetProtocolFamily.IPv4);
        socket.listen( "0.0.0.1", 1234,new AsyncResultHandler<DatagramSocket>() {
            @Override
            public void handle(AsyncResult<DatagramSocket> asyncResult) {
                if(asyncResult.succeeded()) {
                    socket.dataHandler(new Handler<DatagramPacket>() {
                        @Override
                        public void handle(DatagramPacket packet) {
                            //Do something with the packet
                        }
                    });

                    //join the multicast group
                    socket.listenMulticastGroup("127.0.0.1", new AsyncResultHandler<DatagramSocket>() {
                        @Override
                        public void handle(AsyncResult<DatagramSocket> asyncResult1) {
                            System.out.println("Listen succeed? " + asyncResult1.succeeded());
                        }
                    });
                }else {
                    System.out.println("Listen failed? "+asyncResult.cause());
                }
            }
        });
```

#### Unlisten / leave a Multicast group

There are sometimes situations where you want to receive packets for a Multicast group for a limited time.

In this situations you can first start to listen for them and then later unlisten.

This is shown here:

final DatagramSocket socket = vertx.createDatagramSocket(InternetProtocolFamily.IPV4);
socket.listen("0.0.0.0", 1234, new AsyncResultHandler<DatagramSocket>() {
    public void handle(AsyncResult<DatagramSocket> asyncResult) {
        if (asyncResult.succeeded()) {
            socket.dataHandler(new Handler<DatagramPacket>() {
                public void handle(DatagramPacket packet) {
                    // Do something with the packet
                }
            });

            // join the multicast group
            socket.listenMulticastGroup("230.0.0.1", new AsyncResultHandler<DatagramSocket>() {
                public void handle(AsyncResult<DatagramSocket> asyncResult) {
                    if (asyncResult.successed()) {

                        // will now receive packets for group
                        ...
                        ...
                        // do some work
                        ..
                        ..
                        socket.unlisten("230.0.0.1", new AsyncResultHandler<DatagramSocket>() {
                            public void handle(AsyncResult<DatagramSocket> asyncResult) {
                                log.info("Unlisten succeeded? " + asyncResult.succeeded());
                            }
                        });

                    } else {
                       log.warn("Listen failed", asyncResult.cause());
                    }

                }
            });
        } else {
            log.warn("Listen failed", asyncResult.cause());
        }
    }
});


#### Blocking multicast

Beside unlisten a Multicast address it's also possible to just block multicast for a specific sender address.

Be aware this only work on some Operating Systems and kernel versions. So please check the Operating System documentation if it's supported.

This an expert feature.

To block multicast from a specic address you can call blockMulticastGroup(...) on the DatagramSocket like shown here:

final DatagramSocket socket = vertx.createDatagramSocket(InternetProtocolFamily.IPV4);
...
...
// This would block packets which are send from 10.0.0.2
socket.blockMulticastGroup("230.0.0.1", "10.0.0.2", new AsyncResultHandler<DatagramSocket>() {
    public void handle(AsyncResult<DatagramSocket> asyncResult) {
        log.info("block succeeded? " + asyncResult.succeeded());
    }
});


# Flow Control - Streams and Pumps

在 Vert.x 中，任何写操作都会立即返回，但是写操作本身依然在队列中。
不难看出，当向一个对象进行写操作的速度超过其写入底层资源的速度时，写入队列会增长到超出限制，最后导致资源耗尽。

所以需要一种流程控制能力来解决这个问题。

任何流程控制对象都实现ReadStream或 WriteStream 接口。
常见的NetSocket同时实现了这两个接口。

```java
NetServer server = vertx.createNetServer();

server.connectHandler(new Handler<NetSocket>() {
    public void handle(final NetSocket sock) {

        sock.dataHandler(new Handler<Buffer>() {
            public void handle(Buffer buffer) {
                // Write the data straight back
                sock.write(buffer);
            }
        });

    }
}).listen(1234, "localhost");
```

There's a problem with the above example: If data is read from the socket faster than it can be written back to the socket, it will build up in the write queue of the NetSocket, eventually running out of RAM. This might happen, for example if the client at the other end of the socket wasn't reading very fast, effectively putting back-pressure on the connection.

```java
NetServer server = vertx.createNetServer();
server.connectHandler(new Handler<NetSocket>() {
    public void handle(final NetSocket sock) {

        sock.dataHandler(new Handler<Buffer>() {
            public void handle(Buffer buffer) {
                // Write the data straight back
                sock.write(buffer);
                if (sock.writeQueueFull()) {
                    sock.pause();
                    sock.drainHandler(new VoidHandler() {
                        public void handle() {
                            sock.resume();
                        }
                    });
                }
            }
        });

    }
}).listen(1234, "localhost");
```

drainHandler 会在 socket 准备好接收更多数据时执行
这种操作非常常见，所以 Vert.x 提供了帮助类 Pump

```java
Pump.createPump(sock, sock).start();
```

## ReadStream

ReadStream is implemented by HttpClientResponse, HttpServerRequest, WebSocket, NetSocket, SockJSSocket and AsyncFile.

Functions:

dataHandler(handler): set a handler which will receive data from the ReadStream. As data arrives the handler will be passed a Buffer.
pause(): pause the handler. When paused no data will be received in the dataHandler.
resume(): resume the handler. The handler will be called if any data arrives.
exceptionHandler(handler): Will be called if an exception occurs on the ReadStream.
endHandler(handler): Will be called when end of stream is reached. This might be when EOF is reached if the ReadStream represents a file, or when end of request is reached if it's an HTTP request, or when the connection is closed if it's a TCP socket.
## WriteStream

WriteStream is implemented by , HttpClientRequest, HttpServerResponse, WebSocket, NetSocket, SockJSSocket and AsyncFile.

Functions:

write(buffer): write a Buffer to the WriteStream. This method will never block. Writes are queued internally and asynchronously written to the underlying resource.
setWriteQueueMaxSize(size): set the number of bytes at which the write queue is considered full, and the method writeQueueFull() returns true. Note that, even if the write queue is considered full, if write is called the data will still be accepted and queued.
writeQueueFull(): returns true if the write queue is considered full.
exceptionHandler(handler): Will be called if an exception occurs on the WriteStream.
drainHandler(handler): The handler will be called if the WriteStream is considered no longer full.
## Pump

Instances of Pump have the following methods:

start(): Start the pump.
stop(): Stops the pump. When the pump starts it is in stopped mode.
setWriteQueueMaxSize(): This has the same meaning as setWriteQueueMaxSize on the WriteStream.
bytesPumped(): Returns total number of bytes pumped.
A pump can be started and stopped multiple times.

When a pump is first created it is not started. You need to call the start() method to start it.


# Writing HTTP Servers and Clients

## Writing HTTP servers

### Creating an HTTP Server

```java
HttpServer server = vertx.createHttpServer();
server.listen(8080, "myhost");
```

### 接收请求

```java
HttpServer server=vertx.createHttpServer();
server.requestHandler(new Handler<HttpServerRequest>() {
    @Override
    public void handle(HttpServerRequest request) {
        System.out.println("a request has arrived");
        request.response().end();
    }
}).listen(8080, "localhost");
```

注意，requestHandler 在请求头到达时就会执行，这时 request 中还没有 body

### 处理请求

#### Request 方法

发起如下请求

[http://localhost:8080/a/b/c?name=peter&age=18](http://)

```java
request.method()	// GET
request.version()	// HTTP_1_1
request.uri()	// /a/b/c?name=peter&age=18
request.path()	// /a/b/c
request.query()	// name=peter&age=18
request.headers()	// org.vertx.java.core.http.impl.HttpHeadersAdapter@66770347
request.params()	// org.vertx.java.core.http.CaseInsensitiveMultiMap@4b5a208
request.absoluteURI()	// http://localhost:8080/a/b/c?name=peter&age=18
```

#### Reading Data from the Request Body

requestHandler 在收到请求头时就响应
dataHandler 在收到请求体后才响应。根据请求体的大小，dataHandler 可能被多次调用。


The request object implements the ReadStream interface so you can pump the request body to a WriteStream. See the chapter on streams and pumps for a detailed explanation.

```java
//接收到数据时
request.dataHandler(new Handler<Buffer>() {
    @Override
    public void handle(Buffer buffer) {
        System.out.println("receive "+buffer.length() +" bytes");
    }
});

//接收到整个 body 时
request.bodyHandler(new Handler<Buffer>() {
    @Override
    public void handle(Buffer body) {

    }
});

//request 结束时
request.endHandler(new Handler<Void>() {
    @Override
    public void handle(Void event) {

    }
});
```

#### 文件上传

streamToFileSystem()可以方便的将流转为本地文件

```java
request.expectMultiPart(true);
request.uploadHandler(new Handler<HttpServerFileUpload>() {
    @Override
    public void handle(HttpServerFileUpload httpServerFileUpload) {
        httpServerFileUpload.streamToFileSystem("uploads/"+httpServerFileUpload.filename());
    }
});
```

#### 处理表单

```java
request.endHandler(new Handler<Void>() {
    @Override
    public void handle(Void event) {
        // The request has been all ready so now we can look at the form attributes
        MultiMap attrs = request.formAttributes();
        // Do something with them
    }
});
```

### HTTP Server Responses

#### Setting Status Code and Message

```java
request.response().setStatusCode(739).setStatusMessage("Too many gerbils").end();
```

- setStatusCode()
	默认 statusCode 为200
- setStatusMessage()

#### Writing HTTP responses

```java
request.response().write(myBuffer);

request.response().write("hello", "UTF-16");
```

write()方法是异步的，将 write放入队列后就会立即返回

如果没有使用 Http chunking 就必须设置Content-Length

#### Ending HTTP responses

必须在 response 结束前调用

```java
request.response().end();
```

同 write("That's all folks") 后再 end()

```java
request.response().end("That's all folks");
```

#### Closing the underlying connection

You can close the underlying TCP connection of the request by calling the close method.

```java
request.response().close();
```

#### Response headers

```java
request.response().headers().set("Cheese", "Stilton");
request.response().putHeader("Some-Header", "elephants").putHeader("Pants", "Absent");
```

#### Chunked HTTP Responses and Trailers

Vert.x supports HTTP Chunked Transfer Encoding. This allows the HTTP response body to be written in chunks, and is normally used when a large response body is being streamed to a client, whose size is not known in advance.

DDefault is non-chunked. When in chunked mode, each call to response.write(...) will result in a new HTTP chunk being written out.

```java
req.response().setChunked(true);
```

When in chunked mode you can also write HTTP response trailers to the response. These are actually written in the final chunk of the response.

To add trailers to the response, add them to the multimap returned from the trailers() method:

```java
request.response().trailers().add("Philosophy", "Solipsism");
request.response().putTrailer("Cat-Food", "Whiskas").putTrailer("Eye-Wear", "Monocle");
```

### Serving files directly from disk

```java
req.response().sendFile("web/" + file);
req.response().sendFile("web/" + file, "handler_404.html");
```

通常来说 sendFile()在大文件的使用上速度较快。如果文件较小，那么可能不如手动读取文件再发送快。

Note: If you use sendFile while using HTTPS it will copy through userspace, since if the kernel is copying data directly from disk to socket it doesn't give us an opportunity to apply any encryption.

If you're going to write web servers using Vert.x be careful that users cannot exploit the path to access files outside the directory from which you want to serve them.

### Pumping Responses

```java
Pump.createPump(req, req.response()).start();
```

### HTTP Compression

```java
HttpServer server = vertx.createHttpServer();
server.setCompressionSupported(true);
```

## Writing HTTP Clients

### Creating an HTTP Client

```java
HttpClient client = vertx.createHttpClient();
client.setPort(1234);
client.setHost("localhost");
```

默认监听 localhost 的80端口

### Pooling and Keep Alive

默认 httpclient 使用http connections pool。当 response end 后，当前使用的连接会被重新放回池内。如果不想使用连接池需要调用如下代码

```java
client.setKeepAlive(false);
```

这样每次Http请求都会建立一个新的连接，并且在 response end 时被关闭

修改请求池大小

```java
client.setMaxPoolSize(10)
```

默认大小为1

### Closing the client

```java
client.close();
```

### Making Requests

```java
HttpClientRequest request = client.post("/some-path", new Handler<HttpClientResponse>() {
    @Override
    public void handle(HttpClientResponse response) {
        System.out.println(response.statusCode());
    }
});
request.end();
```

建立请求时可以使用 post(),put()等方法，也可以直接使用 request("POST")
第一个参数为 uri，是相对路径
建立请求结束时必须调用 end()方法

getNow()同 get()方法，但是可以自动end()，可以用于建立简单的 Get 请求

### Other

设置请求头，请求体,timeout,chunck等方法与 Server 端相同

### HTTP Client Responses

获得返回的 Http 状态码和状态信息

```java
client.getNow("/some-path/", new Handler<HttpClientResponse>() {
    public void handle(HttpClientResponse resp) {
        log.info('server returned status code: ' + resp.statusCode());
        log.info('server returned status message: ' + resp.statusMessage());
    }
});
```

获得响应体

```java
client.getNow("/some-path/", new Handler<HttpClientResponse>() {
    public void handle(HttpClientResponse resp) {
        resp.dataHandler(new Handler<Buffer>() {
            public void handle(Buffer data) {
                log.info('I received ' + buffer.length() + ' bytes');
            }
        });
    }
});
```

基本同 Server 端编程

### Reading cookies

```java
response.cookies()
```

### 100-Continue Handling

According to the HTTP 1.1 specification a client can set a header Expect: 100-Continue and send the request header before sending the rest of the request body.

The server can then respond with an interim response status Status: 100 (Continue) to signify the client is ok to send the rest of the body.

The idea here is it allows the server to authorise and accept/reject the request before large amounts of data is sent. Sending large amounts of data if the request might not be accepted is a waste of bandwidth and ties up the server in reading data that it will just discard.

Vert.x allows you to set a continueHandler on the client request object. This will be called if the server sends back a Status: 100 (Continue) response to signify it is ok to send the rest of the request.

This is used in conjunction with the sendHead function to send the head of the request.

An example will illustrate this:

HttpClient client = vertx.createHttpClient().setHost("foo.com");

final HttpClientRequest request = client.put("/some-path/", new Handler<HttpClientResponse>() {
    public void handle(HttpClientResponse resp) {
        log.info("Got a response " + resp.statusCode());
    }
});

request.putHeader("Expect", "100-Continue");

request.continueHandler(new VoidHandler() {
    public void handle() {

        // OK to send rest of body
        request.write("Some data").end();
    }
});

request.sendHead();


### HTTP Compression

```java
HttpClient client = vertx.createHttpClient();
client.setTryUseCompression(true);
```

### Routing HTTP requests with Pattern Matching

```java
HttpServer server = vertx.createHttpServer();

// 建立路由
RouteMatcher routeMatcher = new RouteMatcher();

routeMatcher.get("/animals/dogs", new Handler<HttpServerRequest>() {
    @Override
    public void handle(HttpServerRequest httpServerRequest) {
        httpServerRequest.response().end("You request dogs");
    }
}).get("/animals/cats", new Handler<HttpServerRequest>() {
    @Override
    public void handle(HttpServerRequest httpServerRequest) {
        httpServerRequest.response().end("You request cats");
    }
}).all("/animals/other", new Handler<HttpServerRequest>() {
    @Override
    public void handle(HttpServerRequest httpServerRequest) {
        httpServerRequest.response().end("You request other");
    }
});

server.requestHandler(routeMatcher).listen(1234);
```

matcher 相关方法除了get 之外还有 post,put,connect,head 等常用 http 方法。除此之外还有 all 用于接收任何类型的请求。

### Extracting parameters from the path

":"可以用于接收参数

```java
routeMatcher.put("/:blogname/:post", new Handler<HttpServerRequest>() {
    public void handle(HttpServerRequest req) {
        String blogName = req.params().get("blogname");
        String post = req.params().get("post");
        req.response().end("blogname is " + blogName + ", post is " + post);
    }
});
```

### Extracting params using Regular Expressions

```java
routeMatcher.allWithRegEx("\\/([^\\/]+)\\/([^\\/]+)", new Handler<HttpServerRequest>() {
    public void handle(HttpServerRequest req) {
        String first = req.params().get("param0");
        String second = req.params().get("param1");
        req.response.end("first is " + first + " and second is " + second);
    }
});
```

同样也有 get, post 等版本

### Handling requests where nothing matches

当没有路径匹配时调用，可以用于处理404页面

```java
routeMatcher.noMatch(new Handler<HttpServerRequest>() {
    public void handle(HttpServerRequest req) {
        req.response().end("Nothing matched");'
    }
});
```

## WebSockets

### Creating

```java
HttpServer server = vertx.createHttpServer();

server.websocketHandler(new Handler<ServerWebSocket>() {
    public void handle(ServerWebSocket ws) {
        // A WebSocket has connected!
    }
}).listen(8080, "localhost");
```

### Reading and Writing

```java
Pump.createPump(ws, ws).start();
```

or

```java
websocket.write(new Buffer("some-string"));
```

### Rejecting WebSockets

只接收指定路径的 socket 请求

```java
if (ws.path().equals("/services/echo")) {
    Pump.createPump(ws, ws).start();
} else {
    ws.reject();
}
```

### WebSockets on the HTTP client

```java
HttpClient client = vertx.createHttpClient().setHost("foo.com");

client.connectWebsocket("/some-uri", new Handler<WebSocket>() {
    public void handle(WebSocket ws) {
        // Connected!
    }
});
```

### WebSockets in the browser

## SockJS

# File System

文件操作都是异步的

## copy

copy(source, destination, recursive, handler)
copy(source, destination, handler)

```java
vertx.fileSystem().copy("foo.dat", "bar.dat", new AsyncResultHandler<Void>() {
    public void handle(AsyncResult ar) {
        if (ar.succeeded()) {
            log.info("Copy was successful");
        } else {
            log.error("Failed to copy", ar.cause());
        }
    }
});
```

## move

move(source, destination, handler)

## truncate

truncate(file, len, handler)

len is the length in bytes to truncate it to

## chmod

chmod(file, perms, handler)
chmod(file, perms, dirPerms, handler)

rw-r--r--

## props

properties file

props(file, handler)

## delete

delete(file, handler)
delete(file, recursive, handler)

## mkdir

mkdir(dirname, handler)
mkdir(dirname, createParents, handler)

## readDir

readDir(dirName)
readDir(dirName, filter)

## open

open(file, handler)
open(file, perms, handler)
open(file, perms, createNew, handler)

handler 中返回AsyncFile，可以进行异步访问

# DNS Client

## Create

```java
DnsClient client = vertx.createDnsClient(new InetSocketAddress("10.0.0.1", 53), new InetSocketAddress("10.0.0.2", 53));
```

## lookup

Try to lookup the A (ipv4) or AAAA (ipv6) record for a given name.

```java
DnsClient client = vertx.createDnsClient(new InetSocketAddress("10.0.0.1", 53));
client.lookup("vertx.io", new AsyncResultHandler<InetAddress>() {
    public void handle(AsyncResult<InetAddress> ar) {
        if (ar.succeeded()) {
            System.out.println(ar.result());
        } else {
            log.error("Failed to resolve entry", ar.cause());
        }
    }
});
```

## lookup4

Try to lookup the A (ipv4) record for a given name.

```java
DnsClient client = vertx.createDnsClient(new InetSocketAddress("10.0.0.1", 53));
client.lookup4("vertx.io", new AsyncResultHandler<Inet4Address>() {
    public void handle(AsyncResult<Inet4Address> ar) {
        if (ar.succeeded()) {
            System.out.println(ar.result());
        } else {
            log.error("Failed to resolve entry", ar.cause());
        }
    }
});
```

## resolveA

Try to resolve all A (ipv4) records for a given name. 

```java
DnsClient client = vertx.createDnsClient(new InetSocketAddress("10.0.0.1", 53));
client.resolveA("vertx.io", new AsyncResultHandler<List<Inet4Address>>() {
    public void handle(AsyncResult<List<Inet4Address>> ar) {
        if (ar.succeeded()) {
            List<Inet4Address> records = ar.result());
            for (Inet4Address record: records) {
                System.out.println(record);
            }
        } else {
            log.error("Failed to resolve entry", ar.cause());
        }
    }
});
```

## resolveCNAME

```java
DnsClient client = vertx.createDnsClient(new InetSocketAddress("10.0.0.1", 53));
client.resolveCNAME("vertx.io", new AsyncResultHandler<List<String>>() {
    public void handle(AsyncResult<List<String>> ar) {
        if (ar.succeeded()) {
            List<String> records = ar.result());
            for (String record: records) {
                System.out.println(record);
            }
        } else {
            log.error("Failed to resolve entry", ar.cause());
        }
    }
});
```


# Modules

## 分类

### Runnable
在 mod.json 中配置了main的描述符

### Non-runnable
没有配置 main 的描述符，可以用于引入其他资源

## 运行

- 命令行
`vertx runmod modname`

`vertx runzip modname.zip`

- 程序中
`container.deployModule(modname)`

## Module Identifier

每个 module 都由一个唯一的标示符来识别。
标示符由三部分组成：

- Owner	表示 Module 的拥有者，通常为倒过来的域名
- Name	表示 Module 的名字
- Version	表示 Module 的版本

这三部分由分隔符 "~" 连接。

示例

`io.vertx~mod-mongo-persistor~2.0.0-beta1`

## Module 的结构

一个 Module文件后缀为.zip，其本身类似.jar，但是因为 Vert.x 包括 Java以外的实现，所以没有起名为.jar。

### Module 描述文件

每个 Module 必须在 root 目录包含一个名为 mod.json 的描述文件。一个描述文件的组成类似 json 文件。

#### 描述文件中的各个属性

##### main

表示用于启动 module 的 verticle，值为Verticle 的完整限定名。

示例

```
"main": "org.mycompany.mymod.MyMain"
"main": "app.js"
```

**语言限定**

如果遇到多个不同语言写的 module 名字相同的情况，需要使用语言限定符来指定使用的语言。所有语言限定符列在 Vert.x 安装目录的 conf 文件夹下的 langs.properties 文件中。

`"main": "groovy:org.mycompany.mymod.MyCompiledGroovyClass"`

##### worker

worker运行于来自线程池的线程中，允许有阻塞操作

示例
`"worker": true`

##### multi-threaded

用于将 worker 运行在多线程中，默认值为 false

##### includes

include其它 modules，include 即被包含的 modules 使用当前 module 的 class loader。
include 使用分隔符 "," 来连接多个 modules

示例
`"includes": "io.vertx~some-module~1.1,org.aardvarks~foo-mod~3.21-beta1"`

##### preserve-cwd

Module 默认访问的文件系统以 Module 目录为默认路径。
如果不希望访问 module 路径，需要设置为 true,默认为 false

`"preserve-cwd": true`

##### auto-redeploy

`"auto-redeploy": true`

##### resident

Vert.x 默认为先载入 Module 到内存中，当长时间不使用时会从内存中移出。
设为 true 后 Vert.x 不会主动从内存中移除 Module，知道 Vert.x 被关闭。

`"resident": true`

##### system

一个 Module 通常被安装在运行 Vert.x根目录的 mods 目录下。
设置为 true，并且安装在 Vert.x 安装目录的sys-mods目录下会使该 Module 可以被其他应用引用。

`"system": true`

##### deploys

用逗号分隔，表示该 Module 运行时部署的其它 Module 列表

##### description

##### licenses

##### author

##### keywords

##### developers

##### homepage


## 第三方包

放在 Module 根目录的lib 文件夹下
如果第三方包需要被多个 Module 引用，可以建立一个 Module 负责管理第三方包，其余 Module 直接 include 此 Module。

## Module classloaders

每个 Module 都使用各自的 classloader，即每个不相干的 Module 间都是互相独立的。同一 Module 的不同实例可以互相进行通信。
Raw Verticle也拥有其独立的 classloader。


# Installing modules manually from the repository

`vertx install io.vertx~mod-mongo-persistor~2.0.0-beta1`

# Uninstalling modules

`vertx uninstall io.vertx~mod-mongo-persistor~2.0.0-beta1`

# Publishing to Maven repositories

For Maven projects this is accomplished by executing mvn deploy as is normal for any Maven project.

For Gradle projects you just run ./gradlew uploadArchives as normal for any Gradle project.

# Developing Vert.x Modules with Maven

## 建立工程

```shell
mvn archetype:generate -Dfilter=io.vertx:
```

## Setup your IDE

```shell
mvn idea:idea

mvn eclipse:eclipse
```

## 目录

### Module

mod.json 放在src/main/resources下

source 放在src/main/java下

unit test 放在src/test/unit下，unit test不运行在 Vert.x 容器内

### Integration test

运行在 Vert.x 容器内
Java 项目放在src/test/java下
具体间官方 example

## 运行

1.依赖 build 后的 target 目录

先 build 工程

```shell
mvn package
```

在进入 target 目录

```shell
cd target
```

运行

```shell
vertx runmod org.example~vertx-maven~1.0-SNAPSHOT
```

或者直接runzip

2.使用 maven 命令

```shell
mvn clean package vertx:runMod
```


Vert.x Module 运行时会先寻找本地 mods 文件夹，再寻找Vert.x 安装目录的sys-mods 文件夹，在寻找本地 maven 仓库

强烈建议建立自己的 Vert.x 专用仓库

3.在 IDEA 中运行

选择 Edit Configuration
Add New Configuration->Application
Main class:选择 org.vertx.java.platform.impl.cli.Starter
Use classpath of moduel: 选择需要运行的模块
Program arguments: runmod org.example~vertx-maven~1.0-SNAPSHOT -cp target
pom.xml change io.vertx 相关 scope 从 provided 到 compile

```shell
export VERTX_MODS=$HOME/Dev/vertx-mods
```

运行 runmod 后Module 会被自动复制到这里，runzip不会

## Debug

确保 mod.json 中设置 `"auto-redeploy": true`
IDEA 设定的Compiler - 勾选 Make project automatically
执行 mvn idea:idea
执行 mvn clean compile 重新编译
执行 mvn vertx:runMod，之后代码改变后就会自动部署




# Developing Vert.x Modules with Gradle

## 建立工程

获取模板
```shell
git clone https://github.com/vert-x/vertx-gradle-template.git my-vertx-module
```

修改 repo
```shell
git remote rm origin
git remote add origin <path to your repo>
```

## Setup your IDE

```shell
./gradlew idea
```
Or
```shell
./gradlew eclipse
```


# vrm

下载测试数据
http://media.mongodb.org/zips.json

导入到数据库
mongoimport --db vertx --collection zips --file ./zips.json

配置mongo module
<dependency>
    <groupId>io.vertx</groupId>
    <artifactId>mod-mongo-persistor</artifactId>
    <version>2.1.0</version>
    <scope>compile</scope>
</dependency>


runmod org.smartjava~vertx-demo-1~1.0-SNAPSHOT -conf src/main/resources/config.js


-----



