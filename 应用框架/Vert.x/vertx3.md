[TOC]

# Vert.x 3

## Docker 镜像

pom.xml

```xml
<version>3.2.1</version>

<dependencies>
  <dependency>
    <groupId>io.vertx</groupId>
    <artifactId>vertx-core</artifactId>
    <version>${project.version}</version>
  </dependency>
</dependencies>

<build>
 <plugins>
   <plugin>
     <artifactId>maven-compiler-plugin</artifactId>
     <version>3.1</version>
     <configuration>
       <source>1.8</source>
       <target>1.8</target>
     </configuration>
   </plugin>

   <groupId>com.spotify</groupId>
    <artifactId>docker-maven-plugin</artifactId>
    <version>0.2.8</version>
    <executions>
      <execution>
        <id>docker</id>
        <phase>package</phase>
        <goals>
          <goal>build</goal>
        </goals>
      </execution>
    </executions>
    <configuration>
      <dockerDirectory>${project.basedir}/src/main/docker</dockerDirectory>
      <!-- Configure the image name -->
      <imageName>sample/vertx-hello</imageName>
      <resources>
        <resource>
          <targetPath>/verticles</targetPath>
          <directory>${project.build.directory}</directory>
          <includes>
            <include>${project.artifactId}-${project.version}.jar</include>
          </includes>
        </resource>
        <!-- don't forget to also add all the dependencies required by your application -->
      </resources>
    </configuration>
    </plugin>
 </plugins>
</build>
```

Java

```java
public class HelloVerticle extends AbstractVerticle {

  @Override
  public void start() throws Exception {
    vertx.createHttpServer().requestHandler(request -> {
      request.response().end("Hello Java world !");
    }).listen(8080);
  }
}
```

src/main/docker/Dockerfile

```
###
# vert.x docker example using a Java verticle
# To build:
#  docker build -t sample/vertx-java .
# To run:
#   docker run -t -i -p 8080:8080 sample/vertx-java
###

# Extend vert.x image
FROM vertx/vertx3

ENV VERTICLE_NAME io.vertx.sample.hello.HelloVerticle
ENV VERTICLE_FILE target/hello-verticle-3.2.1.jar

# Set the location of the verticles
ENV VERTICLE_HOME /usr/verticles

EXPOSE 8080

# Copy your verticle to the container
COPY $VERTICLE_FILE $VERTICLE_HOME/

# Launch the verticle
WORKDIR $VERTICLE_HOME
ENTRYPOINT ["sh", "-c"]
CMD ["vertx run $VERTICLE_NAME -cp $VERTICLE_HOME/*"]
```

运行

```bash
mvn package
docker build -t sample/vertx-java .
docker run -t -i -p 8080:8080 sample/vertx-java
```
