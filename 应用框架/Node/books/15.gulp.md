[TOC]

# Gulp

Gulp 是自动化工具。

## 安装

```bash
npm install --global gulp gulp-cli
npm install --save-dev gulp
```

## 使用

### 创建任务

创建配置文件 `gulpfile.js`

```js
var gulp = require('gulp');

// 默认任务
gulp.task('default', function() {
    // 将你的默认的任务代码放在这, 运行 gulp
    console.log('start gulp')
});

// 自定义任务
gulp.task("foobar", function () {
    console.log('start custom task')

    gulp.src("./*.js")
        .pipe(gulp.dest("./dist"));
});
```

### 执行任务

执行默认命令 `default`

```bash
gulp
```

执行指定命令

```bash
gulp <tasks...>
```

### 常用操作

#### ES 6,7 及 React 转换

安装 `babel-preset-es2015` 和 `babel-preset-stage-0` 和 `gulp-react`

```js
var gulp = require('gulp');
var babel = require('gulp-babel');
var react = require('gulp-react');

const es6 = {
    src: 'es6/*.js',
    dest: 'es5_dir'
};

// ES6 and React
gulp.task('babel', function () {
    return gulp.src(es6.src)
        .pipe(react())
        .pipe(babel())
        .pipe(gulp.dest(es6.dest))
});
```

#### Watch 操作

监听文件夹改动后执行特定任务

```js
gulp.task('watch', function () {
    gulp.watch(es6.src, ['babel']);
});
```

#### 组合多个任务

```js
gulp.task('one', function () {
   console.log('one');
});
gulp.task('two', function () {
    console.log('two');
});
gulp.task('mytask', ['one', 'two'], function () {
    console.log('end mytask');
});
```

#### 编译 Less 和 Sass

需要安装 `gulp-less` 和 `gulp-sass`

```js
var less = require('gulp-less');
var sass = require('gulp-sass');

gulp.task('less', function () {
    return gulp.src('style/*.less')
        .pipe(less())
        .pipe(gulp.dest(es6.dest))
});
gulp.task('sass', function () {
    return gulp.src('style/*.scss')
        .pipe(sass())
        .pipe(gulp.dest(es6.dest))
});
gulp.task('style', ['less', 'sass']);
```

#### Mocha

```js
var mocha = require('gulp-mocha');

gulp.task('mocha-test', function () {
    return gulp.src('test/*.js')
        .pipe(mocha());
});
```

#### browserify 和 uglify

browserify 用于模块化，可以将多个 js 文件合并为一个
uglify 用于进行代码压缩

需要安装 `gulp-browserify` 和 `gulp-uglify`

```js
var browserify = require('gulp-browserify');
var uglify = require('gulp-uglify');

gulp.task('browserify', function () {
    return gulp.src('main.js')
        .pipe(browserify())
        .pipe(uglify())
        .pipe(gulp.dest('./outputs'));
});

```

## 参考资料

- [Gulp 中文网](http://www.gulpjs.com.cn/)
