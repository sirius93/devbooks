[TOC]

# 数据类型

## 类型转换

### 字符串 -> 数字

可以在字符串前面加上 `+` 转换为数字或者使用以下几种方法：

```js
var one = '1'
var two = '2'
console.log(typeof one, one + two)    //  string 12
console.log(typeof +one, +one + (+two))    //  number 3
console.log(Number(one) + Number(two))
console.log(parseInt(one, 10) + parseInt(two, 10))
```

## 精确计算

```js
console.log(2-1.8)  //  0.19999999999999996
console.log((2-1.8).toFixed(2)) //  0.20
```
