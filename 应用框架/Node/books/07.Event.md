[TOC]

# Event

## 定义监听器

通常监听器第一个参数设定为 error，第二个开始为结果

```js
var events = require('events')
var emitter = new events.EventEmitter()
emitter.on('connect', function () {
    console.log('connected')
})
```

## 触发监听器

```js
emitter.emit('connect')
```

## 监听器种类

一次性监听器

```js
emitter.once('close', function () {
    console.log('close')
})
```

监听器删除

```js
emitter.removeListener('connect', function () {
    console.log('remove')
})
```
