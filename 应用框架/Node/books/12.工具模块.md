[TOC]

# 工具模块

## os

提供运行平台的信息

```js
var os = require('os')
console.log('os type', os.type())
console.log('platform', os.platform())
console.log('memory', os.totalmem())
```

## path

提供对路径的各种工具类

```js
var path = require('path')
console.log('normalization', path.normalize('/one//two/three/..'))
console.log('absolute', path.resolve('filter.js'))
console.log('ext', path.extname('filter.js'))
console.log('dir', path.dirname(path.resolve('filter.js')))
```

## dns

提供 DNS 服务

```js
var dns = require('dns')
dns.lookup('www.github.com', function (err, addr, family) {
    console.log('addr', addr)
    dns.reverse(addr, function (err, hostnames) {
        if (err) {
            console.log(err.stack)
            return
        }
        console.log(JSON.stringify(hostnames))
    })
})
```

## domain

提供对 io 中异常的统一管理，不会使程序直接崩溃。按使用方式分为显示绑定和隐式绑定两种功能。
显示绑定需要使用代码将 emitter 绑定到 domain 上，当 emitter 没有 error 的处理事件时，domain 会处理异常。
隐式绑定在 domain 的 `run()` 方法中声明的 emitter 会被自动绑定到该 domain 上。 

```js
var domain = require("domain")
var EventEmitter = require("events").EventEmitter
var emitter1 = new EventEmitter()

var domain1 = domain.create()
domain1.on('error', function (err) {
    console.log("domain1 处理这个错误 (" + err.message + ")")
})

// 显式绑定
domain1.add(emitter1)

emitter1.on('error', function (err) {
    console.log("监听器处理此错误 (" + err.message + ")")
})
emitter1.emit('error', new Error('通过监听器来处理'))
emitter1.removeAllListeners('error')
emitter1.emit('error', new Error('通过 domain1 处理'))

var domain2 = domain.create()
domain2.on('error', function (err) {
    console.log("domain2 处理这个错误 (" + err.message + ")")
})

// 隐式绑定
domain2.run(function () {
    var emitter2 = new EventEmitter()
    emitter2.emit('error', new Error('通过 domain2 处理'))
})

domain1.remove(emitter1)
emitter1.emit('error', new Error('转换为异常，系统将崩溃!'))
```
