[TOC]

# Http

## Client

### Request

`get` 是 `request` 的快捷操作，固定了方法为 `get` 及自动调用了 `req.end()`。

```js
var http = require('http')
var bl = require('bl')

http.get('http://www.baidu.com', function (resp) {
    resp.setEncoding('utf-8')
    //handle two events
    resp.on('error', console.error)
    resp.on('data', function (data) {
        console.log('------>start\n', data, '\n<------end\n')

        //all evernts
        http.get('http://www.baidu.com', function (resp) {
            resp.pipe(bl(function (err, d) {
                if (err)
                    return console.error(err)

                d = d.toString()
                console.log(d.length)
                console.log(d)
            }))
        })
    })
})
```

详细设置，必须注意 host 一定不能加 http 前缀。可以使用 `request` 代替 `get`。

```js
var options ={
    host:'www.baidu.com',
    port:80
};
http.get(options, function (resp) {
        resp.on('error', console.error);
        resp.on('data', function (data) {
            console.log('------>start\n', data, '\n<------end\n')
        });
    });
```

## Server

### 创建 Server

```js
/**
 * Created by mrseasons on 16/2/19.
 */
var http = require('http')
var fs = require('fs')
var map = require('through2-map')
var url = require('url')

//curl localhost:1234
http.createServer(function (req, resp) {
    if (req.method == 'PUT') {
        req.pipe(map(function (chunk) {
            return chunk.toString().toUpperCase()
        })).pipe(resp)
        return
    } else if (req.method == 'POST') {
        resp.writeHead(200, {'Content-Type': 'application/json'})
        resp.end(JSON.stringify({
            "x": 1,
            "y": 2
        }))
        return
    }

    var parsedUrl = url.parse(req.url, true)
    var parseName = parsedUrl.pathname
    console.log(parsedUrl)
    resp.writeHead(200, {'content-type': 'text-plain'})
    fs.createReadStream('../res/students.csv')
        .pipe(resp)
}).listen(1234)
```
