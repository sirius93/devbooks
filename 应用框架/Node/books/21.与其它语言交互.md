[TOC]

# 与其它语言交互

## 与 C++ 交互

### 前提

先安装 node-gyp [https://github.com/nodejs/node-gyp](https://github.com/nodejs/node-gyp)

### 使用

#### 编写 C++ 文件

hello.cc

```c++
#include <node.h>
#include <v8.h>

using namespace v8;

// 传入了两个参数，args[0] 字符串，args[1] 回调函数
void hello(const FunctionCallbackInfo<Value>& args) {
  // 使用 HandleScope 来管理生命周期
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  // 判断参数格式和格式
  if (args.Length() < 2 || !args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(
      String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  // callback, 使用Cast方法来转换
  Local<Function> callback = Local<Function>::Cast(args[1]);
  Local<Value> argv[1] = {
    // 拼接String
    String::Concat(Local<String>::Cast(args[0]), String::NewFromUtf8(isolate, " world"))
  };
  // 调用回调, 参数: 当前上下文，参数个数，参数列表
  callback->Call(isolate->GetCurrentContext()->Global(), 1, argv);
}

// 相当于在 exports 对象中添加 { hello: hello }
void init(Handle<Object> exports) {
  NODE_SET_METHOD(exports, "hello", hello);
}

// 将 export 对象暴露出去
// 原型 `NODE_MODULE(module_name, Initialize)`
NODE_MODULE(test, init);
```

#### 编写 gyp 文件

用于定义 build 后的产物

binding.gyp

```json
{
  "targets": [
    {
      "target_name": "hello",
      "sources": [ "hello.cc" ]
    }
  ]
}
```

#### 编写 JavaScript 文件

```js
var addon = require('./build/Release/hello');
addon.hello('test', function(data){
    console.log(data);  // test world
})
```

#### 编译 C++ 代码

```bash
node-gyp configure
node-gyp build
```

编译后的产物会生成在当前目录下的 `build//Release` 目录下，文件名为 gyp 文件中定义的 `target_name`。



## 与 Java 交互

### 前提

安装 node-java 模块 [https://github.com/joeferner/node-java](https://github.com/joeferner/node-java)

```bash
npm install java
```

如果需要支持 Java 8 还需要执行以下命令

```bash
./compile-java-code.sh
./compile-java8-code.sh
node-gyp configure build
```

### 使用

```js
var java = require("java");
java.classpath.push('./src/libs/foo.jar')

var list1 = java.newInstanceSync("java.util.ArrayList");
console.log(list1.sizeSync()); // 0
list1.addSync('item1');
console.log(list1.sizeSync()); // 1

var Calculator = java.import('Calculator')
var cal = new Calculator()
console.log(cal.addSync(1,20))
```
