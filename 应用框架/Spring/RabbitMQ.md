# RabbitMQ

## 配置

依赖配置

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-amqp</artifactId>
</dependency>
```

环境配置

```yml 
spring:
  rabbitmq:
    host: localhost
    port: 5672
    username: spring
    password: 123456
    listener:
      simple:
        concurrency: 3
```


## 使用

实现 MessageListener 接口，通过 Jackson2JsonMessageConverter 或 SimpleMessageConverter 获得消息体

```java
@Component
public class RentMessageListener extends AbstractPersistableMessageListener {

    // 配置的消息队列名
    @RabbitListener(queues = "${amqp.rent.queue}")
    @Override
    public void onMessage(Message message) {
        Object payload = messageConverter.fromMessage(message);
        Map<String, Object> headers = message.getMessageProperties().getHeaders();
        // 通过 ObjectMapper 将payload转为实体
    }
}
```