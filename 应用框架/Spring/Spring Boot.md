[TOC]

# SpringBoot

## 基本使用

### 应用入口

```java
@SpringBootApplication
public class Bootstrap {
    public static void main(String[] args) {
        SpringApplication.run(Bootstrap.class, args);
    }
}
```

## 注解

### @SpringBootApplication

以上注解实际包含三个注解

- @Configuration
- @EnableAutoConfiguration
- @ComponentScan

### @Configuration

IOC 容器的配置类，用于配置需要注册到 Spring 容器中的 Bean

例：

```java
@Configuration
public class HogeProvider {
    @Bean
    public Hoge getHoge() {
        return new Hoge(1, "hoge provider");
    }
}
```

### @EnableAutoConfiguration

自动将所有符合条件的 @Configuration 加载进来并创建对应 Ioc 容器
可以借助 @Import 收集和注册特定场景的 bean 定义

### @ComponentScan

自动扫描 Component


#### @PropertySource 与 @PropertySources

用于加载 properties 文件

Java 8 可以并行使用多个 @PropertySource，Java 8 以下版本则需要使用 @PropertySources 声明多个

@PropertySource("classpath:1.properties")

@PropertySources({@PropertySource("classpath:1.properties")})


#### @import 与 @ImportResource

@import 用于将多个基于 JavaConfig 的配置组合到一起

@Import(MockConfiguration.class)

@ImportResource 用于将多个基于 xml 的配置组合到一起

#### @AutoConfigurationAfter 与 @AutoConfigurationBefore

用于限制在某个配置完成之前/之后才加载

## 自动配置模块

命名均以 spring-boot-starter-xxx 开头
只要添加依赖默认就会执行

### 日志

默认使用 Logback 作为日志

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-logging</artifactId>
</dependency>
```

自定义配置

- 根据 Logback 约定,使用 logback.xml
- 在 `application.properties` 使用 `logging.config` 作为键指向配置文件地址



### 持久层

#### JDBC

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-jdbc</artifactId>
</dependency>
```

配置

```
spring:
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://127.0.0.1:3306/test?useSSL=false
    username: root
```

容器会自动配置好 JDBCTemplate， DataSourceTransactionManager 等类，可以直接使用

如果配置了多个数据源，可以在一个上面添加 `@Primary` 注解来防止冲突，或者在启动源时排除 DataSource 的自动配置原项


## 配置文件

默认配置文件 `application.yml` 或 `application.properties`

例：

```
home.province=ZheJiang
home.city=WenLing
home.desc=dev: I'm living in ${home.province} ${home.city}.
```

在配置文件中可以通过占位符引用其它定义的配置

### 配置类

可以通过 `@ConfigurationProperties` 注解将配置文件中相同前缀的属性直接绑定到对应的实体上

```java
@Component
@ConfigurationProperties(prefix = "home") //绑定了 home.a, home.b 等属性
public class HomeProperties{}
```

### 随机数

SpringBoot 可以通过 randome 属性在配置文件中生成随机值

```
user:
  id: ${random.long}
  age: ${random.int[1,200]}
  desc: 泥瓦匠叫做${random.value}
  uuid: ${random.uuid}
```

### 多环境配置

application-dev.properties：开发环境
application-prod.properties：生产环境

指定使用何环境

```
spring.profiles.active=dev
```

也可以在启动时进行指定

```Shell
java -jar -Dspring.profiles.active=prod springboot-properties-0.0.1-SNAPSHOT.jar
```

### 注意事项

父工程是无法读取子模块的配置文件的，只有子模块读取父模块配置文件的值

## 高级使用

### spring.factories

META-INF/spring.factories

用于加载自定义的 Jar 包时告诉宿主应用如何加载 SpringBoot 的自动配置，其内容为

org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
com.xxx.FoobarConfiguration
