# Swagger

## Maven 配置

```xml
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger2</artifactId>
    <version>${springfox-version}</version>
</dependency>
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger-ui</artifactId>
    <version>${springfox-version}</version>
</dependency>
```

最新版本为 2.7.0，而 2.4.0 有 bug，无法正常生成文档

## 基本使用

### 修改访问路径

Swagger 文档的访问路径如下：

http://<server>:<port>/swagger-ui.html

通常为了能简单地进行访问，都会自定义一个 Controller 来重定向到该地址

```java
@Controller
public class HomeController {
	@RequestMapping(value = "/")
	public String index() {
		return "redirect:swagger-ui.html";
	}
}
```

可以通过修改以下地址来修改访问 json 格式的配置文件

```yml
springfox.documentation.swagger.v2.path=/api-docs
```

### 配置 Swagger

```java
@Configuration
public class SwaggerDocumentationConfig {

    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("统一账号系统")
            .description("Mock 服务器")
//            .license("")
//            .licenseUrl("")
//            .termsOfServiceUrl("")
            .version("0.0.1")
//            .contact(new Contact("","", ""))
            .build();
    }

    @Bean
    public Docket customImplementation(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                    .apis(RequestHandlerSelectors.basePackage("io.swagger.api"))
                    .build()
                .directModelSubstitute(org.joda.time.LocalDate.class, java.sql.Date.class)
                .directModelSubstitute(org.joda.time.DateTime.class, java.util.Date.class)
                .apiInfo(apiInfo());
    }

}
```

### 使用

在应用上添加 `@EnableSwagger2` 注解

#### 路径映射

使用注解 `@Api`

例：

```java
@Api(value = "users", description = "账号系统")
public interface UserApi{}
```

#### 方法映射

使用注解 `@ApiOperation`

例：

```java
@ApiOperation(value = "获取用户信息", notes = "根据请求头部的 SessionToken 来指定用户")
ResponseEntity<RespMsg<User>> me(){}
```

#### 参数映射

通过 `@ApiParam` 来标明使用的参数

```java
public ResponseEntity<RespMsg> update(@ApiParam(value = "用户信息", required = true)
                                          @RequestBody User user,
                                          @ApiParam(value = "应用ID", required = true)
                                          @RequestHeader(value = "X-Eju-AppId") String appId,
                                          @ApiParam(value = "用户凭证", required = false)
                                          @RequestHeader(value = "X-Eju-SessionToken", required = false) String sessionToken) throws ApiException {}
```

#### 标注 Bean

使用 `@ApiModel` 标注模型，使用 `@ApiModelProperty` 标注属性

```java
@ApiModel(value="user对象",description="用户对象user")
public class User implements Serializable{
    private static final long serialVersionUID = 1L;
     @ApiModelProperty(value="用户名",name="username",example="xingguo")
     private String username;
     @ApiModelProperty(value="状态",name="state",required=true)
      private Integer state;
      private String password;
      private String nickName;
      private Integer isDeleted;

      @ApiModelProperty(value="id数组",hidden=true)
}
```

`@ApiModelProperty` 有以下几种配置

- value–字段说明
- name–重写属性名字
- dataType–重写属性类型
- required–是否必填
- example–举例说明
- hidden–隐藏
