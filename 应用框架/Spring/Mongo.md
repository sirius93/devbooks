## MongoDB

### 安装

添加以下依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-mongodb</artifactId>
</dependency>
```

### 配置 MongoDB

修改配置文件

```
spring.data.mongodb.host=133.130.52.73
spring.data.mongodb.database=test
spring.data.mongodb.port=32770
```

配置 MongoDB

添加以下配置，注意其会自动扫描其下所有包，所以要保证把该注解放在最外面

```java
@EnableMongoRepositories
```

### 使用代码配置

通常来说无需在代码中进行配置

```java
@Configuration
public class MongoConfiguration extends AbstractMongoConfiguration {

    @Autowired
    private Environment environment;

    @Override
    protected String getDatabaseName() {
        return environment.getRequiredProperty("spring.data.mongodb.database");
    }

    @Bean
    @Override
    public Mongo mongo() throws Exception {
        ServerAddress serverAddress = new ServerAddress(environment.getRequiredProperty("spring.data.mongodb.host"),
                Integer.valueOf(environment.getRequiredProperty("spring.data.mongodb.port")));
        List<MongoCredential> credentialList = new ArrayList<>();
        return new MongoClient(serverAddress, credentialList);
    }
}
```


### 使用

#### 创建模型

可以使用 Spring Mongo Data 的注解也可以使用 JPA 的注解

```java
@Document(collection = "user")
public class MongoUser {
    @Id
    private String userId;

    @NotNull
    @Indexed(unique = true)
    private String username;

    @NotNull
    private String password;

    // 序列化使用的构造方法
    @PersistenceConstructor
    public MongoUser(String userId, String username, String password) {
        this.userId = userId;
        this.username = username;
        this.password = password;
    }
}
```

#### 创建 MongoRepository

泛型参数为模型和 ID 类型，默认 Repository 已经包含了很多通用的方法

```java
@Repository
public interface MongoUserRepo extends MongoRepository<MongoUser,String>{

    MongoUser findByUsername(String username);
}
```

#### 使用 MongoTemplate

默认 Repository 已经包含了很多通用的方法，如果这些还不满足可以使用 MongoTemplate

```java
@Autowired
private MongoTemplate mongoTemplate

val query = Query.query(
            Criteria.where("appName")
                    .`is`(appName)
                    .and("os")
                    .`is`(os))
            .with(Sort(Sort.Order(Sort.Direction.DESC, "appVersion")))

template.upsert(query,
                    Update()
                            .set("appName", appName)
                            .set("appVersion", newVersion)
                            .set("forceUpdate", forceUpdate)
                            .set("os", os)
                            .set("md5", hash)
                            .set("fileId", fileId),
                    NativeResource::class.java)
```

#### 使用 GridFsService

使用 `GridFsService` 可以实现分布式文件存储

```java
@Resource
lateinit var gridFsService: GridFsService

String fileId = gridFsService.store(input!!.inputStream, fileName)
String hash = gridFsService.selectById(fileId).get("md5")

GridFSDBFile dbFile = gridFsService.selectById(fileId)
```
