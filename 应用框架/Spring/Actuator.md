## Actuator

Actuator 用于进行健康检查

### 安装

添加以下依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```

配置 url 和端口

```
management.address=localhost
management.port=9001
management.trace.include=REQUEST_HEADERS,RESPONSE_HEADERS,ERRORS,PATH_INFO,\
PATH_TRANSLATED,CONTEXT_PATH,USER_PRINCIPAL,PARAMETERS,QUERY_STRING,AUTH_TYPE,\
REMOTE_ADDRESS,SESSION_ID,REMOTE_USER
```

### 使用

访问 `http://localhost:9001/health` 可以获得健康检查的对应信息

Endpoint 一栏

autoconfig  提供 SpringBoot 的自动配置报告
beans  给出所有 bean 的信息
- info   给出任意自定义信息
- health 健康检查
- metrics 当前应用的 metrics 信息
- dump  dump 线程
- env 当前配置信息

应用配置类:
查看自动化配置信息：/<context>/autoconfig
查看所有的spring beans：/<context>/beans
查看配置属性：/<context>/configprops
查看系统环境信息/<context>/env
查看请求映射信息：/<context>/mappings
查看应用自定义的信息：/<context>/info

度量指标类：
内存、CPU等信息：/<context>/metrics
健康指标：/<context>/health
线程信息：/<context>/dump
请求调用链：/<context>/trace

操作控制类：
关闭监控端点：/<context>/shutdown
修改日志级别：/<context>/loggers


生产环境下建议关闭所有，然后再按需开启

```
endpoints.enabled=false
endpoints.info.enabled=true
```

配置文件

```
# endpoint 的上下文路径，默认为根路径
management.context-path=
# 配置单独的监听地址，以下则为只允许本地访问
management.address=127.0.0.1
```

### 自定义健康信息

Actuator 允许在返回的默认的健康信息中携带自定义的信息

默认包含了 DataSourceHealthIndicator,DiskSpaceHealthIndicator,RedisHealthIndicator,MongoHealthIndicator 等，也可以自定义健康检查信息

自定义健康检查

只要注册到 IOC 容器中，SpringBoot 会自动发现并启用

可以实现 HealthIndicator 接口或者 AbstractHealthIndicator 抽象类

```java
public class MyHealthIndicator implements HealthIndicator {

    @Override
    public Health health() {
        try {
            long count = 10;
            if (count >= 0) {
                return Health.up().withDetail("count", count).build();
            } else {
                return Health.unknown().withDetail("count", count).build();
            }
        } catch (Exception e) {
            return Health.down(e).build();
        }
    }
}
```

或者

```java
public class MyIndicator extends AbstractHealthIndicator {
    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception {
        builder.withDetail("key","value");
        builder.up();
    }
}
```

添加加配置中

```java
@Configuration
public class HealthConfiguration {

    @Autowired
    private HealthAggregator aggregator;

    @Bean
    public HealthIndicator healthIndicator() {
        CompositeHealthIndicator compositeHealthIndicator = new CompositeHealthIndicator(aggregator);
        compositeHealthIndicator.addHealthIndicator("name", new MyHealthIndicator());
        return compositeHealthIndicator;
    }
}
```

以上代码会在健康信息中添加如下信息

```json
{
  "healthIndicator": {
    "status": "UP",
    "name": {
      "status": "UP",
      "count": 10
    }
  }
  ...
}
```

### 自定义 Info 信息

```java
@Component
public class ExampleInfoContributor implements InfoContributor {

    @Override
    public void contribute(Info.Builder builder) {
        builder.withDetail("example", Collections.singletonMap("someKey", "someValue"));
    }
}
```

访问 `xxx/info` 会返回如下信息

```json
{
  "example": {
    "someKey": "someValue"
  }
}
```
