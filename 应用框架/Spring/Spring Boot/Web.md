# Web 项目

## Maven 配置

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
```

默认运行 `mvn springboot:run` 会执行内嵌的 tomcat 服务，也可以指定 `spring-boot-starter-jetty` 启用 Jetty

## 工程目录

静态资源

SpringBoot 推荐使用 Jar 而不是 War 部署应用,所以与传统 Java Web 不一样,静态文件不在 `src/main/webapps` 而是 `resources` 目录

`resources/static` 存放静态文件
`resources/templates` 存放模板文件

如果使用 War 的话则可以继续原来的目录结构

## 基本配置

```
server:
  port: 10086 #修改端口
```

## Interceptor

### 基本使用

定义 Interceptor 类

```java
public class UserInterceptor extends HandlerInterceptorAdapter {

    @Resource
    private UserFacadeService userFacadeService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        super.afterCompletion(request, response, handler, ex);
    }
}
```

设置启动时加载 Interceptor

```java
@Configuration
public class InterceptorConfig extends WebMvcConfigurerAdapter {

    @Resource
    private UserInterceptor userInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(userInterceptor).addPathPatterns("/**");
        super.addInterceptors(registry);
    }

    @Bean
    public UserInterceptor getUserInterceptor(){
        return new UserInterceptor();
    }
}
```

>如果添加 Interceptor 时直接使用 new 的话则该拦截器不受 Spring 容器管理，其中就无法使用自动注入 Service 等功能。



## CommandLineRunner

`CommandLineRunner` 用于在应用启动时执行部分代码，可以通过 `@Order` 来标示启动的顺序

```java
@Component
@Order(value = 1)
public class EchoCommandLineRunner implements CommandLineRunner {

    @Override
    public void run(String... strings) throws Exception {
        System.out.println("Do something when application is launched.");
    }
}
```

### SpringEL

为@Value注入null为默认值

```java
@Value("${stuff.value:#{null}}")
```
