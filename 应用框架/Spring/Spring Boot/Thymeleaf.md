# Thymeleaf

## Maven 配置

添加以下依赖

```xml
<dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-thymeleaf</artifactId>
</dependency>
```

## 应用配置

修改配置文件

```
# prefix 配置模板路径，默认为 classpath:/templates
#spring.thymeleaf.prefix=/WEB-INF/views/templates/

# suffix 配置后缀，这样 controller 中只需返回名字，无需添加后缀名，默认后缀名为 `.html`
spring.thymeleaf.suffix=.html

spring.thymeleaf.mode=HTML5
spring.thymeleaf.encoding=UTF-8
spring.thymeleaf.content-type=text/html

# Hotload
spring.thymeleaf.cache=false
```

## 使用

### 返回模板

在代码中返回 ModelAndView 实例

```java
return new ModelAndView(path, key, value);
```

或者基于 Spring 的自动注入功能

```java
public String foo(Model model){
  model.addAttribute('foo', 'bar');
  return "hello"; // 模板相对路径
}
```

### 模板语法

取值

```html
<dd th:text="${data.id}"></dd>  //变量值
<img th:src="@{/images/logo.png}"/> //字符串
```

遍历

```html
<dl th:each="hoge : ${hogeList}">
    <dd th:text="${hoge.id}"></dd>
</dl>
```

日期格式化

```HTML
th:value="${createDate}?${#dates.format(createDate, 'yyyy-MM-dd')}:''"
```
