## Jolokia

使用http方式获取JMX信息的工具类

### 配置

```xml
<dependency>
   <groupId>org.jolokia</groupId>
   <artifactId>jolokia-core</artifactId>
</dependency>
```

### 使用

#### jconsole形式访问

1. 打开jconsole，选择对应进程
2. 选择Tomcat节点下的ThreadPool
3. 选择http-nio-8080节点，可以查看maxThreads等属性

#### http方式访问

http://localhost:<management.port=>/jolokia/read/Tomcat:type=Connector,port=<server.port>

对应的配置选项为

server.port=8080
management.port=7002
