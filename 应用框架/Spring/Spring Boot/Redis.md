## Redis

### 安装

添加以下依赖

```xml
<dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```

### 配置 Redis

修改配置文件

```
spring.redis.database=0
spring.redis.host=133.130.52.73
spring.redis.port=6379
spring.redis.pool.max-idle=8
spring.redis.pool.max-active=8
spring.redis.pool.max-wait=-1
spring.redis.timeout=100000
```


添加以下代码

以下代码使用了 Spring Cache 注解

```java
@Configuration
@EnableCaching
public class RedisConfiguration extends CachingConfigurerSupport {

    // key 的生成策略
    // 这里为 完整类名+方法名
    @Bean
    public KeyGenerator wiselyKeyGenerator() {
        return new KeyGenerator() {
            @Override
            public Object generate(Object target, Method method, Object... params) {

                StringBuilder sb = new StringBuilder();
                sb.append(target.getClass().getName());
                sb.append(method.getName());
                for (Object obj : params) {
                    sb.append(obj.toString());
                }
                return sb.toString();
            }
        };
    }

    @Bean
    public CacheManager cacheManager(RedisTemplate redisTemplate) {
        return new RedisCacheManager(redisTemplate);
    }

    @Bean
    public RedisTemplate<String, String> redisTemplate(
            RedisConnectionFactory factory) {
        StringRedisTemplate template = new StringRedisTemplate(factory);
        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);
        template.setValueSerializer(jackson2JsonRedisSerializer);
        template.afterPropertiesSet();
        return template;
    }
}
```

### 使用

#### 使用注解进行缓存

支持三种注解，每种注解参数中的 `value` 为 redis 的 `key` 的前缀

- `@Cacheable` 存取缓存
- `@CachePut`  修改缓存,不存在则创建
- `@CacheEvict` 删除缓存

```java
@Cacheable（keyGenerator = "wiselyKeyGenerator")
public String cache() {
   System.out.println("没有缓存");
   return "" + new Random().nextInt(10000);
}
```

#### 使用 RedisTemplate 缓存对象

使用 `redisTemplate` 对象

```java
@Autowired
private RedisTemplate<String, String> redisTemplate;

redisTemplate.opsForValue().set(key, json, ttl);    

String json = redisTemplate.opsForValue().get(key);
```
