## JPA

### 配置

pom 文件

```xml
<dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>
<dependency>
   <groupId>mysql</groupId>
   <artifactId>mysql-connector-java</artifactId>
   <scope>runtime</scope>
</dependency>
```

yml 文件

```yml
spring:
  datasource:
    url: jdbc:mysql://localhost:3306/test?characterEncoding=utf8
    username: root
    password: 12345678
  jpa:
    database: MYSQL
    show-sql: true
    hibernate:
      ddl-auto: update
      naming:
        strategy: org.hibernate.cfg.ImprovedNamingStrategy
    properties:
      hibernate:
        dialec
```

### 实体注解

#### @Entity, @Table, @Id, @GeneratedValue

@Entity 表示是实体类
@Table 表示表名
@Id 表示唯一标识
@GeneratedValue 表示主键生成方式

例：

```java
@Entity
@Table(name = "department")
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
}
```

#### @ManyToOne, @ManyToMany

用于表示各自关系

例：

```java
public class User {
    @ManyToOne
    @JoinColumn(name = "did")
    @JsonBackReference
    private Department deparment;

    @ManyToMany(cascade = {}, fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "roles_id")})
    private List<Role> roles;
}
```

#### @DateTimeFormat

用于格式化时间

例:

```java
@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
private Date createdate;
```


#### @JsonBackReference

用于防止对象的递归访问

例：

```java
@JsonBackReference
private Department deparment;
```

### Repository

#### @Repository

用于标识为 Repository，需继承自 JpaRepository 接口

例：

```java
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
  User findByNameLike(String name);
  User readByName(String name);
}
```

然后可以使用 and, or 等关键字声明接口方法，jpa 会自动实现它们

### 使用

JPA 代码配置

```java
@Order(Ordered.HIGHEST_PRECEDENCE)
@Configuration
@EnableTransactionManagement(proxyTargetClass = true)
@EnableJpaRepositories(basePackages = "com.**.repos")
@EntityScan(basePackages = "dbdemo.**.entity")
public class JpaConfiguration {

    @Bean
    PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor(){
        return new PersistenceExceptionTranslationPostProcessor();
    }

}
```
