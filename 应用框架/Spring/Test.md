## Spring Test


### 单元测试

例

```java
@RunWith(SpringRunner.class)
@SpringBootTest
public class CityServiceUnitTest {

    @SpringBootApplication(scanBasePackages = "com.shouzheng.demo.web")
    static class InnerConfig { }

    @Autowired
    private CityService cityService;

    @MockBean
    private CityMapper cityMapper;

    @Test
    public void testInsert() {
        City city = new City();
        cityMapper.insert(city);
        Mockito.verify(cityMapper).insert(city);
    }
}
```

@RunWith注解声明测试是在spring环境下运行的，这样就可以启用Spring的相关支持。
@SpringBootTest注解负责扫描配置来构建测试用的Spring上下文环境。它默认搜索@SpringBootConfiguration类，除非我们通过classes属性指定配置类，或者通过自定义内嵌的@Configuration类来指定配置。如上面的代码，就是通过内嵌类来自定义配置。
@SpringBootApplication扩展自@Configuration，其scanBasePackages属性指定了扫描的根路径。确保测试目标类在这个路径下，而且需要明白这个路径下的所有bean都会被实例化。虽然我们已经尽可能的缩小了实例化的范围，但是我们没有避免其他无关类的实例化开销。

### 测试Controller

```java
@RunWith(SpringRunner.class)
@WebMvcTest(CityController.class)
public class CityControllerUnitTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CityService service;

    @Test
    public void getAllCities() throws Exception {

        City city = new City();
        city.setId(1L);
        city.setName("杭州");
        city.setState("浙江");
        city.setCountry("中国");

        Mockito.when(service.getAllCities()).
                thenReturn(Collections.singletonList(city));

        mvc.perform(MockMvcRequestBuilders.get("/cities"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("杭州")));
    }
}
```

@WebMvcTest默认只实例化Controller组件，解决方法为自定义内部类配置

```java
@RunWith(SpringRunner.class)
@WebMvcTest(CityController.class)
public class CityControllerWebLayer {

    @SpringBootApplication(scanBasePackages = {"com.shouzheng.demo.web"})
    static class InnerConfig {}

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CityService service;
}
```

### 持久层

```java
@RunWith(SpringRunner.class)
@MybatisTest
// 不使用NONE则使用内部数据库
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CityMapperUnitTest {

    @SpringBootApplication(scanBasePackages = {"com.shouzheng.demo.mapper"})
    static class InnerConfig {}

    private static Logger LOG = LoggerFactory.getLogger(CityMapperUnitTest.class);

    @Autowired
    private CityMapper cityMapper;

    @Before
    @After
    public void printAllCities() {
        List<City> cities = cityMapper.selectAllCities();
        LOG.info("{}", cities);
    }

    @Test
//    @Rollback(false) // 禁止回滚
    public void test1_insert() throws Exception {
        City city = new City();
        city.setName("杭州");
        city.setState("浙江");
        city.setCountry("CN");
        cityMapper.insert(city);
        LOG.info("insert a city {}", city);
    }

    @Test
    public void test2_doNothing() {
    }
}
```

@MybatisTest搜索配置类的逻辑和@SpringBootTest、@WebMvcTest相同，为了避免Spring环境问题（上文在测试Controller一节中介绍过），这里直接使用内部类进行配置。

@FixMethodOrder(MethodSorters.NAME_ASCENDING)用来指定测试方法的执行顺序，这是为了观察事务回滚的效果。

如果将test1_insert方法上的@Rollback(false)注释放开，事务不会回滚，test2_doNothing方法之后打印输出的内容会包含test1_insert方法里插入的数据。
