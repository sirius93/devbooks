## Zuul

### API Gateway

Spring Cloud 中使用 Zuul 作为 API Gateway。Zuul 提供了动态路由、监控、回退、安全等功能。

### 配置

pom 文件

```xml
<dependency>
  <groupId>org.springframework.cloud</groupId>
  <artifactId>spring-cloud-starter-zuul</artifactId>
</dependency>

<dependency>
  <groupId>org.springframework.cloud</groupId>
  <artifactId>spring-cloud-starter-eureka</artifactId>
</dependency>
```

yml 文件

```yml
spring:
  application:
    name: microservice-api-gateway
server:
  port: 8050
eureka:
  instance:
    hostname: gateway
  client:
    serviceUrl:
      defaultZone: http://discovery:8761/eureka/
```

#### 使用

在应用上添加 `@EnableZuulProxy` 注解

之后访问 `http://<gateway_host>:<gateway_port>/<service_id_on_eureka>` 将会访问到 Eureka 上对应的服务。要想自定义访问路径需要对 yml 文件进行修改，追加以下代码

```yml
zuul:
  ignored-services: microservice-provider-user          # 需要忽视的服务(配置后将不会被路由)
  routes:
    user:                                               # 路径唯一标识名
      path: /user/**                                    # 映射到的路径
      service-id: microservice-provider-user            # Eureka 中的 serviceId
```
