## Hystrix

### Overview

在 Spring Cloud 中使用了 Netflix 开发的 Hystrix 来实现熔断器。

##### 雪崩效应

在微服务架构中通常会有多个服务层调用，基础服务的故障可能会导致级联故障，进而造成整个系统不可用的情况，这种现象被称为服务雪崩效应。服务雪崩效应是一种因“服务提供者”的不可用导致“服务消费者”的不可用,并将不可用逐渐放大的过程。

##### 熔断器

熔断器 (CircuitBreaker) 的原理很简单，如同电力过载保护器。它可以实现快速失败，如果它在一段时间内侦测到许多类似的错误，会强迫其以后的多个调用快速失败。

##### 原理

Hystrix 提供四个机制解决雪崩问题。
线程池和信号量隔离：只能二选一来限制资源使用。
  - 线程池隔离：使用线程池来存储当前请求，上下文切换有消耗，支持异步调用，可以应对突发流量
  - 信号量隔离：使用信号量记录当前运行的线程数量，超过一定数量进行丢弃，无线程切换，不支持异步
降级：超时降级，资源不足降级；降级或返回静态值或备选服务
熔断：到达阀值自动降级；开关默认关闭，到达阀值开启，一段时间后进入半熔断，按请求结果进入关闭或熔断状态
缓存：请求缓存、请求合并；一般不建议使用，对问题排查会造成问题

原理

Hystrix 使用命令模式 HystrixCommand(Command) 包装依赖调用逻辑，每个命令在单独线程中的信号授权下执行

### 使用

#### Hystrix with Ribbon

##### 配置

pom 文件

```xml
<dependency>
  <groupId>org.springframework.cloud</groupId>
  <artifactId>spring-cloud-starter-hystrix</artifactId>
</dependency>
```

##### 使用

应用上添加 `@EnableCircuitBreaker` 注解

通过 `HystrixCommand` 注解表示失败时调用的方法

例：

```java
@HystrixCommand(fallbackMethod = "fallback")
public User findById(Long id) {
  return this.restTemplate.getForObject("http://microservice-provider-user/" + id, User.class);
}
public User fallback(Long id) {
  return new User();
}
```

以上示例中可以通过关闭 `microservice-provider-user` 服务来进行测试。

#### Hystrix with Feign

在 Feign中 使用 Hystrix 是非常简单的事情，因为 Feign 已经集成了 Hystrix。但是若想通过 Dashboard 访问，还需添加 `spring-cloud-starter-hystrix` 依赖和 `@EnableCircuitBreaker` 注解。

例：

由于 Feign 是声明式接口，所以通常将 fallback 作为其内部类存在

```java
@FeignClient(name = "microservice-provider-user", fallback = UserFeignHystrixClient.HystrixClientFallback.class)
public interface UserFeignHystrixClient {
    @RequestMapping("/{id}")
    public User findByIdFeign(@RequestParam("id") Long id);

    @Component
    static class HystrixClientFallback implements UserFeignHystrixClient {

        @Override
        public User findByIdFeign(Long id) {
            return new User();
        }
    }
}
```



#### Hystrix Dashboard

Dashboard 用于实时查看应用的调用情况。

##### 配置

pom 文件

```xml
<dependency>
  <groupId>org.springframework.cloud</groupId>
  <artifactId>spring-cloud-starter-hystrix-dashboard</artifactId>
</dependency>
```

##### 使用

1. 在应用上添加 `@EnableHystrixDashboard` 注解
2. 访问 `http://<host>:<port>/hystrix.stream` 查看仪表盘，在查看前必须先保证至少调用服务一次

#### Turbine

Turbine 用于整合一堆 hystrix.stream 数据源来供 Dashboard 展示。

##### 配置

xml 文件

```xml
<dependency>
  <groupId>org.springframework.cloud</groupId>
  <artifactId>spring-cloud-starter-turbine</artifactId>
</dependency>
<dependency>
  <groupId>org.springframework.cloud</groupId>
  <artifactId>spring-cloud-netflix-turbine</artifactId>
</dependency>
```

yml 文件

```yml
spring:
  application.name: microservice-hystrix-turbine
server:
  port: 8031
security.basic.enabled: false
turbine:
  aggregator:
    clusterConfig: default   # 指定聚合哪些集群，多个使用","分割，默认为default
  appConfig: consumer-movie-feign,consumer-movie-ribbon  # 配置服务列表
  clusterNameExpression: new String("default")
  # 1. clusterNameExpression指定集群名称，默认表达式appName；此时：turbine.aggregator.clusterConfig需要配置想要监控的应用名称
  # 2. 当clusterNameExpression: default时，turbine.aggregator.clusterConfig可以不写，因为默认就是default
  # 3. 当clusterNameExpression: metadata['cluster']时，假设想要监控的应用配置了eureka.instance.metadata-map.cluster: ABC，则需要配置，同时turbine.aggregator.clusterConfig: ABC
eureka:
  client:
    serviceUrl:
      defaultZone: http://discovery:8761/eureka/
```

1. 不同服务需要配置不同的主机名，并将 preferIpAddress 设为 false 或者不设置，否则将会造成在单个主机上测试，Turbine只显示一个图表的情况。
2. 配置项：`turbine.clusterNameExpression` 与 `turbine.aggregator.clusterConfig`的关系：

| turbine.clusterNameExpression取值          | turbine.aggregator.clusterConfig 取值      |
| ---------------------------------------- | ---------------------------------------- |
| 默认（appName）                              | 配置想要聚合的项目，此时使用turbine.stream?cluster=项目名称大写访问监控数据 |
| new String("default") 或者"'default'"      | 不配置，或者配置default，因为默认就是default            |
| metadata['cluster']；同时待监控的项目配置了类似：eureka.instance.metadata-map.cluster: ABC | 也设成ABC，需要和待监控的项目配置的eureka.instance.metadata-map.cluster一致。 |

##### 使用

1. 在应用上添加 `@EnableTurbine` 注解
2. 通过 Dashboard 访问 `http://<host>:<port>/turbine.stream` 即可
