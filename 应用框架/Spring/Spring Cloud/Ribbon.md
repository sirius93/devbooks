## Ribbon

### 消费者

消费者可以简单的通过各种客户端实现，但是当生产者是多个时，需要通过特定的消费者来实现负载均衡的消费机制。

### Ribbon

Ribbon 是一个客户端负载均衡器，Ribbon 会自动的帮助你基于某种规则（如简单轮询，随机连接等）去连接这些机器。

Ribbon 工作时分为两步：第一步先选择 Eureka Server, 它优先选择在同一个 Zone 且负载较少的 Server；第二步再根据用户指定的策略，在从 Server 取到的服务注册列表中选择一个地址。其中 Ribbon 提供了多种策略，例如轮询、随机、根据响应时间加权等。

#### 配置

pom 文件

```xml
<dependency>
  <groupId>org.springframework.cloud</groupId>
  <artifactId>spring-cloud-starter-eureka</artifactId>
</dependency>
<dependency>
  <groupId>org.springframework.cloud</groupId>
  <artifactId>spring-cloud-starter-ribbon</artifactId>
</dependency>
```



yml 文件

```yml
server:
  port: 8010
spring:
  application:
    name: microservice-consumer-movie-ribbon
eureka:
  client:
    serviceUrl:
      defaultZone: http://discovery:8761/eureka/
  instance:
    preferIpAddress: true
```



#### 使用

在应用上添加 `@EnableDiscoveryClient` 注解，应用可以通过 `@LoadBalanced` 注解来开启负载均衡的能力

例：

```java
@Bean
@LoadBalanced
public RestTemplate restTemplate() {
  return new RestTemplate();
}
```

然后通过消费者注册到 Eureka 的服务名进行访问

```java
restTemplate.getForObject("http://microservice-provider-user/" + id, User.class);
```
