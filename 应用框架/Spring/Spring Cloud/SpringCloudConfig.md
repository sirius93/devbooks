## Spring Cloud Config

### 配置

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-config</artifactId>
</dependency>
```

### 实例

建立多个 bootstrap 文件

bootstrap.yml

```yml
spring:
  application:
    name: 'testApp'
  jmx:
    default-domain: "${spring.application.name}"
```

bootstrap-local.yml，用于本地使用

```yml
eureka:
  client:
    enabled: false
    registerWithEureka: false
    fetchRegistry: false
spring:
  cloud:
    config:
      enabled: false
```

bootstrap-dev.yml，用于服务器上使用

```yml
spring:
  cloud:
    config:
      uri: http://${config.server:127.0.0.1}:8021
      name: testApp
      label: label01
      profile: dev
```

启动时指定参数 -Dspring.profiles.active=dev