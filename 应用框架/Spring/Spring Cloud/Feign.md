## Feign

Feign 是一个声明式的 web service 客户端，它使得编写 web service 客户端更为容易。创建接口，为接口添加注解，即可使用 Feign。Feign 可以使用 Feign 注解或者 JAX-RS 注解，还支持热插拔的编码器和解码器。Spring Cloud为 Feign 添加了Spring MVC 的注解支持，并整合了 Ribbon 和 Eureka 来为使用 Feign 时提供负载均衡。

### 配置

pom 文件

```xml
<dependency>
  <groupId>org.springframework.cloud</groupId>
  <artifactId>spring-cloud-starter-eureka</artifactId>
</dependency>

<dependency>
  <groupId>org.springframework.cloud</groupId>
  <artifactId>spring-cloud-starter-feign</artifactId>
</dependency>
```

yml 文件

```yml
server:
  port: 8020
spring:
  application:
    name: microservice-consumer-movie-feign
eureka:
  client:
    serviceUrl:
      defaultZone: http://discovery:8761/eureka/
  instance:
    preferIpAddress: true
ribbon:
  eureka:
    enabled: true         # 默认为true。如果设置为false，Ribbon将不会从Eureka中获得服务列表，而是使用静态配置的服务列表。静态服务列表可使用：<client>.ribbon.listOfServers来指定。参考：http://projects.spring.io/spring-cloud/docs/1.0.3/spring-cloud.html#spring-cloud-ribbon-without-eureka
```

### 使用

在应用上添加 `@EnableDiscoveryClient` 和 `@EnableFeignClients` 注解，然后通过 `@FeignClient` 注解编写应用

例：

```java
@FeignClient(name = "microservice-provider-user")
public interface UserFeignClient {
    @RequestMapping("/{id}")
    User findByIdFeign(@RequestParam("id") Long id);
}
```
