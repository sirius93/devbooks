# SpringCloud

## Overview

Spring Cloud 是在 Spring Boot 的基础上构建的，用于简化分布式系统构建的工具集，为开发人员提供快速建立分布式系统中的一些常见的模式。

例如：配置管理（configuration management），服务发现（service discovery），断路器（circuit breakers），智能路由（ intelligent routing），微代理（micro-proxy），控制总线（control bus），一次性令牌（ one-time tokens），全局锁（global locks），领导选举（leadership election），分布式会话（distributed sessions），集群状态（cluster state）。

Spring Cloud 项目主页：http://projects.spring.io/spring-cloud/

## 基本使用

pom 文件配置

```xml
<parent>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-parent</artifactId>
  <version>1.5.4.RELEASE</version>
  <relativePath/> <!-- lookup parent from repository -->
</parent>
<build>
  <plugins>
    <plugin>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-maven-plugin</artifactId>
    </plugin>
  </plugins>
</build>
```

## 配置文件

SpringBoot 拥有 application 和 bootstrap 两种配置文件，其中 bootstrap 在 application 前加载，主要用于应用上下文的引导。最常见的用法是通过 Spring Cloud Config Server 从服务端来取真实配置，然后才通过 application 文件进行上下文的初始化。




## 配置中心

Spring Cloud Config 提供了一种在分布式系统中外部化配置服务器和客户端的支持。Config Server 存储后端默认使用git存储配置信息，因此可以很容易支持标记配置环境的版本，同时可以使用一个使用广泛的工具管理配置内容。

### 服务端

#### 配置

pom 文件

```xml
<dependency>
  <groupId>org.springframework.cloud</groupId>
  <artifactId>spring-cloud-config-server</artifactId>
</dependency>
```

yml 文件

```yml
server:
  port: 8040
spring:
  application:
    name: microservice-config-server
  cloud:
    config:
      server:
        git:
          uri: https://github.com/eacdy/spring-cloud-study/     # 配置git仓库的地址
          search-paths: config-repo                             # git仓库地址下的相对地址，可以配置多个，用,分割。
          username:                                             # git仓库的账号
          password:                                             # git仓库的密码
```



#### 使用

1. 在应用上添加 `@EnableConfigServer` 注解
2. 将各种 Properties 配置文件上传到 git 仓库中

| 获取git上的资源信息遵循如下规则                        |
| ---------------------------------------- |
| /{application}/{profile}[/{label}]       |
| /{application}-{profile}.yml             |
| /{label}/{application}-{profile}.yml     |
| /{application}-{profile}.properties      |
| /{label}/{application}-{profile}.properties |

例：访问 `https://github.com/eacdy/spring-cloud-study/microservice-config-client-dev.properties`  配置文件可以按照 [http://localhost:8040/microservice-config-client-dev.properties](http://localhost:8040/microservice-config-client-dev.properties) [http://localhost:8040/microservice-config-client/dev](http://localhost:8040/microservice-config-client/dev)



### 客户端

#### 配置

pom 文件

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-web</artifactId>
</dependency>

<dependency>
  <groupId>org.springframework.cloud</groupId>
  <artifactId>spring-cloud-starter-config</artifactId>
</dependency>

<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```

yml 文件

application.yml 文件中设置端口号

bootstrap.yml 文件内容如下，会比 application.yml 更先加载

```yml
spring:
  application:
    name: microservice-config-client    # 对应服务端的{application}
  cloud:
    config:
      uri: http://config-server:8040/
      profile: dev                      # 指定要获取的 profile
      label: master                     # 指定git仓库的分支
```

#### 使用

1. 通过 `@RefreshScope` 进行热更新的支持，通过 `@Value` 获取配置文件中的属性
2. 通过 `curl  -X POST http://<host>:<port>/refresh` 进行配置文件的重新加载

例：

```java
@RestController
@RefreshScope
public class ConfigClientController {
  @Value("${profile}")
  private String profile;

  @GetMapping("/hello")
  public String hello() {
    return this.profile;
  }
}
```

### 配置中心 + 注册中心

#### 服务端

##### 配置

pom 文件

```xml
<dependency>
  <groupId>org.springframework.cloud</groupId>
  <artifactId>spring-cloud-config-server</artifactId>
</dependency>
```

yml 文件

```yml
server:
  port: 8050
spring:
  application:
    name: microservice-config-server-eureka
  cloud:
    config:
      server:
        git:
          uri: https://github.com/eacdy/spring-cloud-study/
          search-paths: config-repo
          username: 
          password: 
eureka:
  client:
    serviceUrl:
      defaultZone: http://discovery:8761/eureka/
```

##### 使用

在应用上添加 `@EnableConfigServer` 和 `@EnableDiscoveryClient` 两个注解

#### 客户端

##### 配置

pom 文件

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-web</artifactId>
</dependency>
<dependency>
  <groupId>org.springframework.cloud</groupId>
  <artifactId>spring-cloud-starter-config</artifactId>
</dependency>
<dependency>
  <groupId>org.springframework.cloud</groupId>
  <artifactId>spring-cloud-starter-eureka</artifactId>
</dependency>
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```

yml 文件

```yml
spring:
  application:
    name: microservice-config-client-eureka
  cloud:
    config:
      profile: dev
      label: master
      discovery:
        enabled: true                                 # 默认false，设为true表示使用注册中心中的configserver 配置而不自己配置 configserver 的 uri
        serviceId: microservice-config-server-eureka  # 指定 config server 在服务发现中的serviceId，默认为：configserver
eureka:
  client:
    serviceUrl:
      defaultZone: http://discovery:8761/eureka/
```



##### 使用

添加 `@EnableDiscoveryClient` 注解，其它通普通客户端

> 想要将Config Server 与 注册中心联合使用，只需要在客户端侧配置`spring.cloud.config.discovery.enabled=true` 和 `spring.cloud.config.discovery.serviceId` 两个配置项即可



## Docker

###  SpringBoot 应用示例

1. 创建 dockerfile 文件

```
# 基于哪个镜像
FROM java:8

# 将本地文件夹挂载到当前容器
VOLUME /tmp

# 拷贝文件到容器，也可以直接写成ADD microservice-discovery-eureka-0.0.1-SNAPSHOT.jar /app.jar
ADD microservice-discovery-eureka-0.0.1-SNAPSHOT.jar app.jar
RUN bash -c 'touch /app.jar'

# 开放8761端口
EXPOSE 8761

# 配置容器启动后执行的命令
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
```

2. 构建 docker 镜像

```
docker build -t eacdy/test1 .		# 格式：docker build -t 标签名称 Dockerfile的相对位置
```

3. 启动镜像

```
docker run -p 8761:8761 eacdy/test1
```

### docker-compose

docker-compose 用于同时管理多个 Docker 镜像文件

## 参考资料

- [spring-cloud-book](https://github.com/eacdy/spring-cloud-book)















