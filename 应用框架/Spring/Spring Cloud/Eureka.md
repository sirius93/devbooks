## Eureka

Eureka 用于实现服务发现，Spring Cloud 支持得最好的是 Eureka，其次是 Consul，最次是 Zookeeper。

### 原理

Eureka Server 之间会做注册服务的同步，从而保证状态一致；
Eureka Client 会注册到 Eureka Server 上
Eureka Client 可以既是消费者也是生产者

Service Provider 会向 Eureka Server 做 Register（服务注册）、Renew（服务续约）、Cancel（服务下线）等操作；
Service Consumer 会向 Eureka Server 获取注册服务列表，并消费服务。
Service Consumer 会优先访问同处一个 Zone 中的 Service Provider

#### 配置

pom 文件

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-eureka-server</artifactId>
</dependency>
```

 yml 文件

单机模式

```yml
server:
  port: 8761                    # 指定该 Eureka 实例的端口

eureka:
  instance:
    hostname: discovery         # 指定该 Eureka 实例的主机名，则该实例地址为 http://discovery:8761/
  client:
    enabled: false
    registerWithEureka: false
    fetchRegistry: false
    serviceUrl:
      defaultZone: http://${eureka.instance.hostname}:${server.port}/eureka/
```

默认情况下，Eureka 服务端会将自己也作为客户端尝试注册，在单机模式下需要关闭

HA 模式

需要启动多个服务端实例，多个服务端实例通过互相注册来实现 HA，客户端可以通过 `defaultZone` 中指定多个地址（使用逗号分割）来注册到多个实例中实现 HA

```yml
spring:
  profiles: peer1                                 # 指定 profile=peer1
server:
  port: 8761
eureka:
  instance:
    hostname: peer1                               # 指定当 profile=peer1 时的主机名
  client:
    serviceUrl:
      defaultZone: http://peer2:8762/eureka/      # 将自己注册到另一个 Eureka 实例 peer2 上
```

以上建立了一个配置文件名为 `peer1`，可以同时建立多个配置文件，然后运行时指定不同配置文件来启动不同实例 `java -jar microservice-discovery-eureka-0.0.1-SNAPSHOT.jar --spring.profiles.active=peer1`



#### 基本使用

1. 在应用上添加 `@EnableEurekaServer` 注解
2. 访问默认管理界面，端口号及域名配置在 yml 文件中

## 生产者

#### Eureka

#### 配置

pom 文件

```xml
<dependency>
  <groupId>org.springframework.cloud</groupId>
  <artifactId>spring-cloud-starter-eureka</artifactId>
</dependency>
```

yml 文件

```yml
server:
  port: 8000
spring:
  application:
    name: microservice-provider-user    # 项目名称尽量用小写，也是注册到 Eureka 的服务名
eureka:
  client:
    serviceUrl:
      defaultZone: http://discovery:8761/eureka/    # 指定注册中心的地址，高可用时为多个
  instance:
    preferIpAddress: true
```



#### 使用

在应用上添加 `@EnableDiscoveryClient` 注解，应用可以通过依赖注入 `DiscoveryClient`实例结合 actuator 来获得当前应用相关的健康信息等。
