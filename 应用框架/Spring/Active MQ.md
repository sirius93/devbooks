## Active MQ

### 安装

添加以下依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-activemq</artifactId>
</dependency>
```

修改配置文件

```
spring.activemq.broker-url=tcp://133.130.52.73:32778
spring.activemq.in-memory=true
spring.activemq.pool.enabled=false
```

配置队列

```java
@Configuration
public class JmsConfiguration {

    @Bean
    public Queue queue() {
        return new ActiveMQQueue("sample.queue");
    }
}
```

添加注解

```java
@EnableJms
```

### 使用

访问 Web 管理界面

http://<host>:8161

### 配置队列信息

```java
@Configuration
public class JmsConfiguration {

    @Bean
    public Queue queue() {
        return new ActiveMQQueue("sample.queue");
    }
}
```

#### 生产者

```java
@Service
public class ProducerService {

    @Autowired
    private JmsMessagingTemplate messagingTemplate;

    @Autowired
    private Queue queue;

    public void send(String msg){
        messagingTemplate.convertAndSend(queue, msg);
    }
}
```

#### 消费者

```java
@Service
public class ConsumeService {

    @JmsListener(destination = "sample.queue")
    public void receive(String text) {
        System.out.println("receive a message is: " + text);
    }
}
```
