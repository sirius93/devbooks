## Batch

batch 用于处理批处理程序

### 安装

加入以下依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-batch</artifactId>
</dependency>
```

此外还要确保在程序入口添加了 `@EnableBatchProcessing` 注解

### 特点

- 一个 Job 代表一个持续任务
- 每个 Job 都可以由多个 Step
- 每个 Step 都是一个 Task
- Job 执行完毕后除非参数不同否则不能再执行


### 创建 Job

```java
@Configuration
public class CountJob {

    @Autowired
    private JobBuilderFactory jobs;

    @Autowired
    private StepBuilderFactory steps;

    // 创建 Task
    protected Tasklet tasklet(String message) {

        return new Tasklet() {
            @Override
            public RepeatStatus execute(StepContribution contribution,
                                        ChunkContext context) {
                System.out.println("invoke task " + message);
                return RepeatStatus.FINISHED;
            }
        };

    }

    // 一个job 可以由多个 step, 每个step 是一个task
    @Bean
    public Job job() throws Exception {
        return this.jobs.get("job" + System.currentTimeMillis())
            .start(step1())
            .next(step2())
            .build();
    }

    @Bean
    protected Step step1() throws Exception {
        return this.steps.get("step1")
            .tasklet(tasklet("hello"))
            .build();
    }

    @Bean
    protected Step step2() throws Exception {
        return this.steps.get("step2")
            .tasklet(tasklet("world"))
            .build();
    }
}
```
