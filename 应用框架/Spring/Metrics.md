# Metrics

## Maven 配置

添加以下依赖以及 Spring AOP

```xml
<dependency>
    <groupId>io.dropwizard.metrics</groupId>
    <artifactId>metrics-core</artifactId>
    <version>3.1.2</version>
</dependency>
<dependency>
    <groupId>io.dropwizard.metrics</groupId>
    <artifactId>metrics-annotation</artifactId>
    <version>3.1.2</version>
</dependency>
```

## 使用

接口可以通过访问 `/metrics` 访问统计信息

### Counter

在需要进行计数的方法上添加 @Counter 注解

```java
@Counted
 @RequestMapping(path = "/2", method=RequestMethod.GET)
 public String hello2(Model model) {
     return "hello";
 }
```

监视代码并计数

```java
@Component
@Aspect
public class AutoMetricsAspect {

    protected ConcurrentMap<String, Counter> counters = new ConcurrentHashMap<>();

    @Autowired
    MetricRegistry metricRegistry;

    @Pointcut(value = "execution(public * *(..))")
    public void publicMethods() {
    }

    @Before("publicMethods() && @annotation(countedAnnotation)")
    public void instrumentCounted(JoinPoint jp, Counted countedAnnotation) {
        String name =
                StringUtils.hasLength(countedAnnotation.name()) ?
                        countedAnnotation.name() :
                        jp.getSignature().getName();  // 调用的方法名
        Counter counter=counters.computeIfAbsent(name, key->metricRegistry.counter(name));
        counter.inc();
    }
}
```
