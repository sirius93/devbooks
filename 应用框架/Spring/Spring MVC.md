[TOC]

# Spring MVC

## 基础知识

### 内部结构

调用顺序

http->DispatcherServlet->HandlerMapping
->Handler->xxxService
->ViewResolver->View

## 使用

### 环境配置

### 常用注解

#### @RequestMapping

基本概念

- `DispatcherServlet` 截获请求后通过 `@RequestMapping` 注解来处理请求
- `@RequestMapping` 用于开发 Controller 类，Controller 类使用 `@Controller` 标注，在配置文件中使用 `<context:component-scan/>` 进行扫描

使用

- 在类或方法定义处使用 `@RequestMapping` 标注
  - 类级别决定了该类所有方法的父路径，非必须
  - 方法级别决定了处理哪个请求，必须

属性

- `value` 代表请求路径
- `method` 指定请求类型，不指定时表示处理任何类型的请求。可以使用 `{m1,m2}` 数组表示接收多种请求类型。
- `consumes` 指定请求类型，即 `Content-Type` (如 `text/html,application/json`)，也可以用数组表示多种类型。
- `produces` 指定返回的内容类型，如 `text/plain`，也可以用数组表示。需要注意的是由于默认返回的是页面，所以要想使 `produces` 生效需要同时设置 `@ResponseBody` 注解。
- `params` 指定请求必须包含某些参数（如：表示相等 foo=bar, 表示不等 foo!=bar，表示必须存在 foo， 表示必须不存在 !foo）
- `headers` 指定必须包含的请求头 (如：content-type=text/* ）

URL 占位符

- `/user/*/login` 匹配任意字符
- `/user/**/login` 匹配任意字符，也可以省略
- `/user/login??` 匹配两个任意字符
- `/user/{userid}`


#### @Component, @Controller, @Service, @Repository

基本概念

这四个注解都是类级别的注解，可以带一个名字作为参数。`@Component` 是其中的通用注解，`@Controller` 和 `@Service` 实际都是标注了 `@Component` 的注解，主要的目的是作为区分，使代码逻辑更清晰。

使用

- `@Component` 通用标注
- `@Controller` 标注 Controller 层，`@RestController` 标注只提供 API 功能的 Controller 层
- `@Service` 标志 Service 层
- `@Repository` 标注 DAO 层

配置

指定包名进行扫描

使用注解时会默认从声明 @ComponentScan 的类所在包进行扫描

```xml
<context:component-scan base-package="com.mrseasons.spring.first.springmvc"/>
```

指定扫描规则

```xml
<context:component-scan base-package="com.mrseasons.spring.first.springmvc" use-default-filters="false">
    <context:include-filter type="annotation" expression="org.springframework.stereotype.Controller"/>
    <context:include-filter type="annotation" expression="org.springframework.stereotype.Component"/>
</context:component-scan>
```

#### @Resource 和 @Autowire

这两个注解用于实现依赖注入功能。

`@Autowire` 默认按类型注入，标注在方法和构造方法上。默认要求被注入的 bean 必须存在。如果需要允许为 null，则需要设置属性 `required=false`。`@Autowire` 结合 `@Qualifier` 可以实现按名称注入。
`@Resource` 默认按名称注入，放在字段上表示按字段名注入，放在 setter 上表示按属性名注入。如果找不到对应名称时则按类型注入，但是如果指定了 `@Resource` 的 `name` 属性则不会按类型注入。
`@Resource` 属于 J2EE 注解，推荐使用，可以减少和 Spring 之间的耦合。

#### @PathVariable 和 @RequestParam

`@PathVariable` 用于在使用 `@RequestMapping` 注解中使用占位符的情况下指定该占位符指定的参数。（例：`@RequestMapping(path = "/{id}", method = RequestMethod.GET) public String getById(@PathVariable int id, Model model)`）

`@RequestParam` 用于在控制层获取参数，也可以使用原生的 `httpServletRequest.getParameter("name")` 达到同样功能。`@RequestParam` 有三个属性:`value`,`required`,`defaultValue`。（例：`public String updateBook(@RequestParam String name, @RequestParam int price, HttpServletRequest request)`）

#### @CookieValue 和 @SessionAttributes

`@CookieValue` 用于读取 Cookies 中的值并且赋值给变量，有三个属性:`value`,`required`,`defaultValue`。（例：`String login2(@CookieValue String name)`）
`@SessionAttributes` 用于类级别，表示标识哪些属性需要存放在 Session 中。使用时 Spring 会扫描 `ModelMap`，`ModelAndView` 中是否存放有该属性，存在的话则放入 Session 中。除此之外还可以直接用原生态的`request.getSession()` 来处理 session 数据，使用起来更加方便。（例：`@Controller @SessionAttributes("user") public class UserController{ public String register(HttpServletRequest request, ModelMap modelMap){modelMap.put("user", user);}}`）
使用 `@SessionAttributes` 后必须使用 `SessionStatus` 手动清除 Session 中的数据。（例：`void logout(SessionStatus status){status.setComplete()}`）


#### @ResponseBody 和 @RequestHeader

`@RequestHeader` 把 Request 请求的 header 部分的值绑定到方法的参数上。（例：`void info(@RequestHeader("Keep-Alive") long keepAlive)`）
`@RequestBody` 作用在方法上。默认 Controller 返回的是页面，而 `@RequestBody` 可以将 Controller 的方法返回的对象，通过适当的 HttpMessageConverter（转换器）转换为指定格式后，写入到 Response 对象的 body 数据区。默认如果返回的是对象的话则使用 Jackson-Databind 返回 json 数据。

SpringMVC 内置有 7 个转换器

配置转换器

```xml
<mvc:annotation-driven>
    <mvc:message-converters>
        <bean class="org.springframework.http.converter.StringHttpMessageConverter">
            <property name="supportedMediaTypes" value="text/plain;charset=UTF-8"/>
        </bean>
        <bean class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter">
            <property name="supportedMediaTypes">
                <list>
                    <value>application/json</value>
                </list>
            </property>
        </bean>
    </mvc:message-converters>
</mvc:annotation-driven>
```

#### @ConditionalOnMissingBean

用于当指定的Bean不存在于Spring容器中使用，


### 校验框架

#### JSR303 校验框架

直接作用在属性上还不是 setter 与 getter，也不会作用在静态成员上。

@NotNull		  注解元素必须是非空
@Null				  注解元素必须是空
@Digits			  验证数字构成是否合法
@Future			  验证是否在当前系统时间之后
@Past				  验证是否在当前系统时间之前
@Max				  验证值是否小于等于最大指定整数值
@Min				  验证值是否大于等于最小指定整数值
@Pattern		  验证字符串是否匹配指定的正则表达式
@Size				  验证元素大小是否在指定范围内
@DecimalMax	  验证值是否小于等于最大指定小数值
@DecimalMin	  验证值是否大于等于最小指定小数值
@AssertTrue		被注释的元素必须为true
@AssertFalse	被注释的元素必须为false

#### Hibernate Validator

Hibernate Validator 是 JSR303 的实现，此外还扩展了以下几个注解。

@Email			被注释的元素必须是电子邮箱地址
@Length			被注释的字符串的大小必须在指定的范围内
@NotEmpty		被注释的字符串的必须非空
@Range			被注释的元素必须在合适的范围内

#### SpringMVC 校验框架

##### 配置

mvc.xml

```xml
<mvc:annotation-driven validator="validator"/>
```

##### 使用

1. 在属性上标注注解

例

```java
@Email(message = "address is invalid")
private String email;
```

2. 在 Controller 层使用 `@Valid` 注解标注需要验证的 Bean。
```java
public String doRegister(@Valid User user)
```

3. 校验结果保存在 BindingResult 或 Errors 对象中。BindingResult 接口扩展了 Errors 接口，以便可以使用 Spring 的org.springframeword.validation.Validator 对数据进行校验，同时获取数据绑定结果对象的信息。


##### 自定义验证规则

定义一个类实现 `ConstraintValidator` 接口来制定规则，然后再定义一个注解使用该类。

##### 使用 i18n

在 resources 目标下创建属性文件，例子以下例子中为 `resources/i18n/message.properties`

```xml
<bean id="messageSource" class="org.springframework.context.support.ResourceBundleMessageSource">
    <property name="defaultEncoding" value="utf-8"/>
    <property name="basename" value="i18n.message"/>
    <!--刷新周期，以秒为单位-->
    <property name="cacheSeconds" value="10"/>
</bean>
```

### 文件上下传

#### 文件上传

##### 配置

添加以下依赖

```xml
<dependency>
   <groupId>commons-fileupload</groupId>
   <artifactId>commons-fileupload</artifactId>
   <version>1.3.1</version>
</dependency>
<dependency>
   <groupId>commons-io</groupId>
   <artifactId>commons-io</artifactId>
   <version>2.4</version>
</dependency>
```

修改 SpringMVC 配置文件

```xml
<bean id="multipartResolver"
      class="org.springframework.web.multipart.commons.CommonsMultipartResolver">
    <property name="defaultEncoding" value="utf-8"/>
    <property name="maxUploadSize" value="10485760000"/>
    <property name="maxInMemorySize" value="4096"/>
</bean>
```
以上 id 必须为 `multipartResolver`，因为 Spring 内部的 `DispatcherServlet` 指明了这个常量。
- `maxUploadSize`			文件最大限制，单位是 byte
- `maxInMemorySize`		低于这个大小的文件暂存内存中
- `defaultEncoding`			默认编码

修改 Form 表单，添加 `method="post" enctype="multipart/form-data"`。

然后在对应的方法中添加类型为 `MultipartFile` 的参数


#### 文件下载

设置编码格式为 `text/html;charset=utf-8`
设置 `header` 中 `Content-disposition` 属性值为 `attachment; filename=文件名`。
设置 `header` 中 `Content-Length` 属性，值为文件的大小。

然后在对应方法中添加类型为 `HttpServletResponse` 的参数，通过其的 OutputStream 输出下载的文件

#### 国际化

Spring 通过实现 `MessageSource` 接口，来支持国际化，其中 `ResourceBundleMessageSource` 是一个常用的实现

#### 通过内置方法指定区域

Spring 使用 `LocaleResolver` 接口来解析用户的区域
`LocaleResolver` 有三个常用的实现类：
- `AcceptHeaderLocaleResolver`  默认，基于用户请求头中的 `accept-language`。
- `CookieLocaleResolver`
- `SessionLocaleResolver` 基于 Session，不存在的话取指定的默认值，没有指定的话也按照请求头。

#### 动态修改用户区域

直接调用 `LocaleResolver.setLocale()`
使用拦截器，Spring 使用 `LocaleChangeInterceptor` 来修改用户区域。实现本地化信息的监听


使用 `<spring:message code="resource.key.name" />` 在 JSP 页面中实现国际化。



## MyBatis + SpringMVC

### SqlSession

SqlSessionTemplate 是 MyBatis-Spring 的核心。 这个类负责管理 MyBatis 的 SqlSession, 调用 MyBatis 的 SQL 方法, 翻译异常。 SqlSessionTemplate 是线程安全的, 可以被多个 DAO 所共享使用。它管理 session 的生命 周期,包含必要的关闭,提交或回滚操作。

注入 SqlSessionTemplate

```java
public class UserDaoImpl implements UserDao {

  private SqlSession sqlSession;

  public void setSqlSession(SqlSession sqlSession) {
    this.sqlSession = sqlSession;
  }
}
```

```xml
<bean id="userDao" class="org.mybatis.spring.sample.dao.UserDaoImpl">
  <property name="sqlSession" ref="sqlSession" />
</bean>
```

SqlSessionDaoSupport 是 一 个 抽象 的支 持 类, 用来 为你 提供 SqlSession 。 调 用 getSqlSession()方法你会得到一个 SqlSessionTemplate,之后可以用于执行 SQL 方法。

```java
public class UserDaoImpl extends SqlSessionDaoSupport implements UserDao {}
```

### 映射器 Mapper

映射器可以不用继承 SqlSessionTemplate 或者 SqlSessionDaoSupport 就自动获得 sqlSession 的 Dao。映射器实际是动态代理，所以 Dao 的映射器需要使用接口而不是类。

定义一个映射器

```xml
<bean id="userDaoMapper" class="org.mybatis.spring.mapper.MapperFactoryBean">
  <property name="mapperInterface" value="org.mybatis.spring.sample.mapper.UserDaoMapper" />
  <property name="sqlSessionFactory" ref="sqlSessionFactory" />
</bean>
```

MapperScannerConfigurer 可以自动扫描并注册映射器从而避免一个个手动注册。

```xml
<bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
   <!-- 映射器接口文件的包路径， -->
   <property name="basePackage" value="com.mrseasons.spring.first.springmvc.dao" />
</bean>
```

### JDBCTemplate

使用 JDBCTemplate 连接数据库可以直接编写 Sql 语句。

RowMapper 接口，这个接口的实现类的功能是将结果集中的每一行数据封装成用户定义的结构。
ColumnMapRowMapper：返回一个 List 对象，对象中的每一个元素都是一个以列名为 key 的 Map 对象
BeanPropertyRowMapper：当 POJO 对象和数据库表字段完全对应或者驼峰式与下划线式对应时，该类会根据构造函数中传递的 class 来自动填充数据
SingleColumnRowMapper：也返回一个 List 对象，对象中的每个元素是数据库中的某列的值

mybatis存储过程
mybatis+spring 事务
@Transaction标签
<foreach><choose><if>标签

KeyHolder返回插入的id

### MyBatis 缓存策略

#### 使用

1. 修改 MyBatis 配置文件
  ```
  <setting name="cacheEnabled" value="true"/>
  ```
2. 修改需要使用缓存的 Mapper 文件
  ```
  <cache readOnly="true" size="500" flushInterval="120000" eviction="LRU"/>
  ```
  - `readOnly` true 则返回的是同一对象，false 则返回的是 cache 对象的副本
  - `size` 默认为 1024
  - `flushIneterval` 默认为空，即容量足够时永远不会过期
  - `evication` 缓存算法：LRU(默认),FIFO,SOFT,WEAK
3. 在 SQL 语句指定
  ```
  <select useCache="true">
  ```

### 定时任务

#### 配置

Spring 配置文件

```xml
<task:annotation-driven/>
<context:component-scan base-package="com.mrseasons.spring.first.springmvc.task"/>
```

Java 代码

用 `@Component` 注解标注任务类
定义没有返回值的方法并在方法上使用 `@Scheduled` 注解。
其中`@Scheduled` 注解可以使用 `fixedRate` 或 cron 表达式指定执行周期。


#### Cron 表达式

一个 cron 表达式有至少 6 个（也可能 7 个）有空格分隔的时间元素：

- 秒（0~59）
- 分钟（0~59）
- 小时（0~23）
- 天（月）（0~31，但是你需要考虑你月的天数）
- 月（0~11）
- 天（星期）（1~7 1=SUN 或 SUN，MON，TUE，WED，THU，FRI，SAT）
- 年份（1970－2099）

其中可以使用 `-` 表示范围，使用 `/` 表示每隔。

CRON 表达式实例：

 - 0 0 12 * * ? 		每天12点触发
 - 0 15 10 ? * * 		每天10点15分触发
 - 0 15 10 * * ? 		2015 2015年每天10点15分触发
 - 0 * 14 * * ? 		每天下午的 2点到2点59分每分触发
 - 0 0/5 14,18 * * ? 	在每天下午2点到2:55期间和下午6点到6:55期间的每5分钟触发
 - 0 0-5 14 * * ? 		每天下午的 2点到2点05分每分触发


### 访问静态资源

通过开放 tomcat 的 defaultServlet，修改默认的 url-parttern
通过 SpringMVC 的 resources 标签配置



## 安全

### XSS 跨站脚本 与 SQL 注入
SQL命令插入到表单或页面请求达到欺骗服务器执行恶意SQL命令
select * from user where userId=${id}
http://xxx/test/userId=1 or 1=1
XSS跨站脚本
如页面为test.jsp 内容为 <body><%=request.getParameter("content")%></body>
使用链接访问
http://xxxx/test.jsp?content=<script>alert("xss注入")</script>


解决方法
定义Filter，使用HttpServletRequestWrapper截获特殊字符处理
进行校验




---
