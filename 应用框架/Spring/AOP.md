## AOP

AOP 用于进行面向切面编程，可以作为监视器

### 安装

添加以下依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-aop</artifactId>
</dependency>
```

### 使用 AOP

```java
@Aspect
@Component
public class FooServiceMonitor {

    @AfterReturning("execution(* com.bookislife.calpico.todo.service..*Service.get(..))")
    public void monitorFoo(JoinPoint joinPoint) {
        System.out.println("complete " + joinPoint);
    }
}
```
