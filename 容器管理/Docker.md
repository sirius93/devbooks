[TOC]

## 基本概念

Docker 包括三个基本概念

- 镜像（Image）
- 容器（Container）
- 仓库（Repository）

**镜像**

Docker 镜像就是一个只读的模板。

一个镜像可以包含一个完整的 ubuntu 操作系统环境，里面仅安装了 Apache 或用户需要的其它应用程序。

镜像可以用来创建 Docker 容器。

**容器**

Docker 利用容器来运行应用。容器是镜像的实例。

**仓库**

仓库是集中存放镜像文件的场所。有时候会把仓库和仓库注册服务器（Registry）混为一谈，并不严格区分。实际上，仓库注册服务器上往往存放着多个仓库，每个仓库中又包含了多个镜像，每个镜像有不同的标签（tag）。

仓库分为公开仓库（Public）和私有仓库（Private）两种形式。

最大的公共仓库是 [Docker Hub](https://hub.docker.com/)。

## 安装

``` bash
sudo yum install docker
```

启动服务

``` bash
sudo systemctl start docker.service
```

随系统启动加载

``` bash
sudo systemctl enable docker.service
```

## 使用

### 镜像

Docker 运行容器前需要本地存在对应的镜像，如果镜像不存在本地，Docker 会从镜像仓库下载。默认所有本地镜像都会保存在 `/var/lib/docker` 目录中。

#### 获取镜像

``` bash
# 从官方仓库下载
sudo docker pull ubuntu:12.04
# 从指定仓库中下载
sudo docker pull dl.dockerpool.com:5000/ubuntu:12.04
```

参数格式为 "bookislife.com/mybox:0.0.1"，即仓库名/镜像名:版本。

其中 `12.04` 这样的版本信息如果不指明的话默认使用 `latest`。

#### 查看镜像列表

``` bash
sudo docker images [过滤名]
```

#### 添加 TAG

``` bash
sudo docker tag <镜像ID> <标签>
```

建立新标签，不会删除原来的标签

#### 提交镜像

只能提交正在运行的容器，即通过 `docker ps` 能看到的容器

``` bash
sudo docker commit [-m 提交消息] 容器ID 镜像名 [目标镜像仓库:TAG]
```

例

``` bash
sudo docker commit 63264170dfee busybox-1
```

#### Dockerfile

Dockerfile 文件用于创建新的镜像。其中每一步都会创建一个新容器，在容器中执行该命令后再提交。

语法

- `#` 表示注释
- `FROM` 表示基础镜像
- 紧接着是维护者的信息
- `RUN` 开头的指令会在创建时运行

基本指令

- FROM <image> 或 <image>:<tag> 可以使用多次，但是必须作为第一条指令
- MAINTAINER <name> 维护者信息
- RUN <command> 或 ["executable", "params"...]，指令较长可以用 `\` 换行。前者在 shell 中运行，后者使用 `exec` 运行。
- CMD ["executable", "params"...]，使用 `exec` 运行，指令容器运行时的命令，每个容器只能有一条，指定多条时后面会覆盖前面。也可以用于提供 ENTRYPOINT  的参数。可以被 `RUN` 指定的命令所覆盖。
- EXPOSE <port>... 容器使用的端口，但是 Docker 不会自动打开该端口。启动时需要通过 `-p` (手动指定端口映射)或 `-P`（自动建立端口映射） 指定。
- ENV <key> <value> 指定环境变量
- ADD <src> <dest> 复制src到 dest，src为相对路径，结尾为`/`会被当做为文件夹，且如果源文件为压缩包时会被自动解压
- COPY <src> <dest> 类似 ADD，但不会自动解压，源目录为本地目录时推荐使用
- ENTRYPOINT ["executable", "params"...]，容器运行后的命令，只能有一个。除非加了 `—entrypoint` 标志否则不会被 `RUN` 指定的命令覆盖。
- VOLUME ["/data"] 创建挂载点
- WORKDIR /path 	指定后续的RUN，CMD，ENTRYPOINT 的工作目录，多个时后面会基于之前的相对路径

例

```
FROM ubuntu:14.04
MAINTAINER Docker Newbee <newbee@docker.com>
RUN apt-get -qq update
RUN apt-get -qqy install ruby ruby-dev
RUN mkdir test
# 添加本地文件到镜像的对应路径
ADD app /var/www
# 开放端口
EXPOSE 80
# 描述容器启动后执行的命令
CMD ["/usr/sbin/apachectl", "-D", "FOREGROUND"]
```

编写完成后通过以下指令创建镜像

``` bash
sudo docker build -t=<TAG> <PATH> .
```

- **TAG** 标记一般格式为 "bookislife.com/mybox:0.0.1"，即仓库名/镜像名:版本

#### 上传镜像

上传前需要确保 tag 已被正常设置，一般格式为 "username/imageName:version"

``` bash
sudo docker push ouruser/sinatra
```

#### 导出镜像到本地

``` bash
sudo docker save -o <file>
```

#### 载入本地文件到本地镜像库

``` bash
sudo docker load --input <file>
```

#### 删除镜像

``` bash
sudo docker rmi <镜像名>
```

在删除之前需要删除所有依赖该镜像的容器，多个镜像以空格分隔

#### 镜像构建历史

``` bash
sudo docker history <镜像名>
```



### 容器

#### 新建并启动

``` bash
sudo docker run -t -i ubuntu:12.04 /bin/bash
```

- **-t** 使用伪终端
- **-i** 使容器输入保持打开
- **-d** 后台运行
- **-P** 随机开放端口
- **-p ip:<hostport>:<容器端口>** 本地端口映射到容器端口
- **-volumes-from <容器ID>** 挂载指定容器的数据卷
- **-v <本地目录>:<容器内目录>** 挂载本地数据卷到容器内，只支持目录
- **-rm** 清理容器运行时产生的文件，后台运行无效
- **—restart=<次数>** 容器挂掉后的重启次数
  - always 表示总是重启
  - on-failure:5 表示失败后重启 5 次
- **-u** 指定容器运行时的用户
  - `-u root` 可以用于运行那些容器内部需要 Root 用户的指令

#### 启动已终止容器

``` bash
sudo docker start -i <容器ID>
```

- **restart** 用于重启

#### 进入容器

主要用于需要连接到后台运行的容器

``` bash
sudo docker attach <容器ID>
```

- **-sig-proxy=false**  退出容器时不终止前台进程

#### 退出容器

`CTRL + Q`

或

``` bash
exit
```

#### 终止容器

``` bash
sudo docker stop <容器ID>
```

#### 查看容器列表

``` bash
sudo docker ps -a
```

- **-a** 查看已终止的容器列表

#### 查看容器 Log 信息

``` bash
sudo docker logs <容器ID>
```

- **--tail** 指定最新日志的行数

#### 查看容器详细信息

``` bash
sudo docker inspect
```

#### 查看容器中运行的进程

``` bash
sudo docker top <容器ID>
```

#### 修改容器名

``` bash
sudo docker rename <old_name> <new_name>
```

#### 在运行中的容器内运行进程

``` bash
sudo docker exec [-d|-t|-i] <容器ID> <CMD>
```

- 基本使用同 `run` 命令，例如：`docker exec -it 5e0aed82a588 /bin/bash`

#### 查看容器端口情况

``` bash
sudo docker port <容器ID> [端口号]
```

#### 删除容器

``` bash
sudo docker rm <容器ID>
```

- **-f** 强制删除 容器

#### 导出容器

``` bash
sudo docker export <容器ID> > <文件>
```

#### 导入容器

``` bash
cat <文件> | sudo docker import - <镜像名>
```

#### 复制本地文件到容器

Docker 1.8 以上开始支持

``` bash
docker cp /var/local/docker/redis/cluster/redis.conf redis100:/opt/redis/redis.conf
```

其它方式

```bash
tar -cv <source> | docker exec -i <container_id> tar x -C <target_path>
```

### 仓库

[Docker Hub](https://hub.docker.com/)

#### 注册，登录

``` bash
sudo docker login -u <username> -p <password> -e <email>
```

#### 搜索

``` bash
sudo docker search <镜像名>
```

#### 私有仓库

???

##### 运行 docker-registry

``` bash
sudo docker run -d -p 5000:5000 -v <目录> registry
```

## Docker 管理工具

### DockerUI

[项目地址](https://github.com/crosbymichael/dockerui)

安装

``` bash
docker run -d -p 9000:9000 --privileged -v /var/run/docker.sock:/var/run/docker.sock uifd/ui-for-docker
```

运行

``` bash
http://<dockerd host ip>:9000
```

截图

![dockerui](images/8/dockerui.png)

### Shipyard

[项目地址](https://github.com/shipyard/shipyard)

安装

``` bash
curl -s https://shipyard-project.com/deploy | bash -s
```

运行

``` bash
http://<dockerd host ip>:8080
```

默认 Username: admin Password: shipyard

截图

![shipyard](images/8/shipyard.png)


### Docker Compose

可以用于指定每个容器间的关系，而不用手动管理

安装 Docker Compose

```bash
curl -L https://github.com/docker/compose/releases/download/1.8.1/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```

验证安装

```bash
docker-compose --version
```

编写 `docker-compose.yml` 文件

```yml
version: '2'
  services:
    web:
      build: .
      ports:
       - "5000:5000"
      volumes:
       - .:/code
      depends_on:
       - redis
    redis:
      image: redis
```

- build 使用当前目录的 Dockerfile 构建 web 镜像
- volumes 将宿主机当前目录映射到容器的 code 目录
- depends_on link web 容器到 redis 容器

创建应用

```bash
docker-compose up [-d]
```

- `-d` 后台运行

其它命令

```bash
docker-compose run <container_id> env  查看环境变量
docker-compose ps
```


## 其它功能

### 免 sudo 使用 Docker 命令

创建 Docker Group

``` bash
sudo groupadd docker
```

加入当前用户到该 Group 中

``` bash
sudo gpasswd -a ${USER} docker
```

重启 Docker 服务（Centos 7 例子）

``` bash
systemctl restart docker.service
```

切换当前会话到新 Group 中

``` bash
newgrp - docker
```

### 退出容器而不中止容器中的前台进程

组合键：Ctrl+P+Q

### 中止容器

当退出容器时报 `There are stopped jobs.`，直接执行以下命令杀死进程。

``` bash
kill -9 $(jobs -p)
```

### 连接容器

有三种方法

- `--link` 有启动顺序的问题
- **设置固定 IP** 比较复杂
- **使用内网 IP** 容器端口映射到宿主机的端口上，然后其它容器使用内网 IP 进行连接

## Docker 实践

### Git Clone

使用 dockerfile 在镜像中 clone 代码会发生两个问题：Repo 过大以及由于镜像缓存问题除了第一次，以后再 build 不会 pull 新的远端代码。解决方式是在 pull 之间使用 ADD 等方法强制不使用缓存。或者更简单的是设置环境变量为当前时间 `ENV date`

```
ADD dummyfile /data/
RUN git clone --depth 1 <your_repo>
```



### Supervisor

Supervisor 为进程管理工具

Dockerfile

安装 ssh, apache, supervisor

``` bash
FROM ubuntu:14.04
MAINTAINER Docker Newbee <newbee@docker.com>
RUN echo "deb http://archive.ubuntu.com/ubuntu precise main universe" > /etc/apt/sources.list
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y openssh-server apache2 supervisor
RUN mkdir -p /var/run/sshd
RUN mkdir -p /var/log/supervisor
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
EXPOSE 22 80
CMD ["/usr/bin/supervisord"]
```

使用方法

``` bash
sudo docker build -t test/supervisord .

```

启动容器

``` bash
sudo docker run -p 22 -p 80 -t -i test/supervisords
```




### 跨主机连接

#### 使用网桥实现

Docker 默认使用 docker0，通过ifconfig可以看到，修改为自定义的网桥
安装网桥管理工具
apt-get install bridge-utils
两台虚拟机
ip1:10.211.55.3
ip2:10.211.55.5
修改配置 /etc/network/interfaces
auto br0
iface br0 inet static
address 10.211.55.3
netmask 255.255.255.0
gateway 10.211.55.1
bridge_ports eth0
Docker设置
修改/etc/default/docker 文件
DOCKER_OPTS="-b=br0 --fixed-cidr='xxxxxx'"
-b=br0 指定使用的自定义网桥
--fixed-cidr 限制ip地址分配范围

#### 使用 Open vSwitch 实现（通常使用）

Open vSwitch是虚拟交换机
通过GRE隧道协议进行容器间连接
双网卡 host-only 和 nat
安装Open vSwitch
apt-get install openvswitch-switch
安装网桥管理工具
apt-get install bridge-utils
两台虚拟机
ip1:192.168.59.103
ip2:192.168.59.104

查看状态
ovs-vsctl show

步骤
1.建立ovs网桥
ovs-vsctl add-br obr0
2.添加gre连接
ovs-vsctl add-port obr0 gre0
ovs-vsctl set interface gre0 type=gre options:remote_ip=其它ip
3.配置docker容器虚拟网桥
brctl addbr br0
ifconfig br0 192.168.1.1 netmask 255.255.255.0
4.为虚拟网桥添加ovs接口
brctl addif br0 obr0
5.添加不同Docker容器网段路由
DOCKER_OPTS="-b=br0"
重启docker
ip route add 192.168.2.0/24 via 192.168.59.104 dev eth0


#### 使用 weave 实现

安装weave



参考资料

- [配置Docker多台宿主机间的容器互联](http://ylw6006.blog.51cto.com/470441/1606239/)
- [Docker学习总结之跨主机进行link](http://www.tuicool.com/articles/RfQRny)
- [Docker容器的跨主机连接](http://www.mamicode.com/info-detail-1168129.html)
