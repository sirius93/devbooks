# K8S

大数据所关注的计算密集型离线业务，并不像常规 Web 服务那样适合用容器进行托管和扩容，也没有对应用打包的强烈需求，所以 Hadoop、Spark 等项目到现在也没在容器技术上投下更大的赌注。

## 容器底层实现

对于 Docker 等大多数 Linux 容器来说，Cgroups 技术是用来制造约束的主要手段，而 Namespace 技术则是用来修改进程视图的主要方法。
即，Namespace的作用是隔离，Cgroups的作用是限制。

一个“容器”，实际上是一个由 Linux Namespace、Linux Cgroups 和 rootfs 三种技术构建出来的进程的隔离环境。  
一个正在运行的 Linux 容器，其实可以被“一分为二”地看待：
- 一组联合挂载在 /var/lib/docker/aufs/mnt 上的 rootfs，这一部分我们称为“容器镜像”（Container Image），是容器的静态视图；
- 一个由 Namespace+Cgroups 构成的隔离环境，这一部分我们称为“容器运行时”（Container Runtime），是容器的动态视图。

### Namespace

#### 特点

Namespace用于让被隔离的进程只看到一个全新的进程空间。PID Namespace只看到当前Namespace里的进程，Mount Namespace只看到当前Namespace里的挂载点配置，Network Namespace只能访问自己的网络配置。

Docker在使用Linux的clone()方法启动容器进程时会指定一系列Namespace，这样在容器内部看进程PID就是从1开始，也看不到宿主机的相关信息。

一个进程，可以选择加入到某个进程已有的 Namespace 当中，从而达到“进入”这个进程所在容器的目的，这正是 docker exec 的实现原理。

#### 缺点

在 Linux 内核中，有很多资源和对象是不能被 Namespace 化的，最典型的例子就是：时间。这就意味着，如果你的容器中的程序使用 settimeofday(2) 系统调用修改了时间，整个宿主机的时间都会被随之修改，所以相比于在虚拟机里面可以随便折腾的自由度，在容器里部署应用的时候，“什么能做，什么不能做”，就是用户必须考虑的一个问题。

### Cgroups

#### 特点

如果没有限制，任何一个容器都可以将宿主机上的资源消耗干净。而Linux Cgroups 就是 Linux 内核中用来为进程设置资源限制的一个重要功能。Linux Cgroups 的全称是 Linux Control Group。它最主要的作用，就是限制一个进程组能够使用的资源上限，包括 CPU、内存、磁盘、网络带宽等等。

在 Linux 中，Cgroups 给用户暴露出来的操作接口是文件系统，即它以文件和目录的方式组织在操作系统的 /sys/fs/cgroup 路径下。
mount -t cgroup可以输出系统当前的一系列挂载了cgroup的目录（也叫子系统，如cpu,memory等），目录里有对应的配置文件信息。

#### 缺点

Cgroups也有不完善的地方，你如果在容器里执行 top 指令，就会发现，它显示的信息居然是宿主机的 CPU 和内存数据，而不是当前容器的数据。

### rootfs

一个系统由操作系统文件和操作系统内核组成。

rootfs（根文件系统）就是所谓的容器镜像，其挂载了操作系统文件，这样进入容器后就能看到类似实际使用该操作系统的体验，而应用以及它运行所需要的所有依赖，也都被封装在了一起。
而操作系统内核，同一台机器上的所有容器，都共享宿主机操作系统的内核。

## 容器基础

### 设计理念

- 容器是单进程可控的（启动时指定的进程），进入容器内部运行的进程是不受docker的控制。
- 容器的“单进程模型”，并不是指容器里只能运行“一个”进程，而是指容器没有管理多个进程的能力。这是因为容器里 PID=1 的进程就是应用本身，其他的进程都是这个 PID=1 进程的子进程。
- 容器本身的设计，就是希望容器和应用能够同生命周期，否则，一旦出现类似于“容器是正常运行的，但是里面的应用早已经挂了”的情况，编排系统处理起来就非常麻烦了。

### Layer

用户制作镜像的每一步操作，都会生成一个层，也就是一个增量 rootfs。

上面的读写层通常也称为容器层，下面的只读层称为镜像层，所有的增删查改操作都只会作用在容器层，相同的文件上层会覆盖掉下层。知道这一点，就不难理解镜像文件的修改，比如修改一个文件的时候，首先会从上到下查找有没有这个文件，找到，就复制到容器层中，修改，修改的结果就会作用到下层的文件，这种方式也被称为copy-on-write。

docker commit，实际上就是在容器运行起来后，把最上层的“可读写层”，加上原先容器镜像的只读层，打包组成了一个新的镜像。

### ENTRYPOINT

Dockerfile 中的 ENTRYPOINT 和 CMD 都是 Docker 容器进程启动所必需的参数，完整执行格式是：“ENTRYPOINT CMD”。但是，默认情况下，Docker 会为你提供一个隐含的 ENTRYPOINT，即：/bin/sh -c。所以，在不指定 ENTRYPOINT 时，比如在我们这个例子里，实际上运行在容器里的完整进程是：/bin/sh -c "python app.py"，即 CMD 的内容就是 ENTRYPOINT 的参数。

### Volume

Volume 机制，允许你将宿主机上指定的目录或者文件，挂载到容器里面进行读取和修改操作。

容器的镜像操作，比如docker commit发生在宿主机上，而由于Mount Namespace的隔离作用，宿主机是不知道这个挂载存在的，所以在容器内部创建文件或目录时宿主机上虽然也会创建，但是如果进行写操作时，宿主机上只会得到空文件或空目录，因此docker commit等操作不会将数据卷也提交到新创建的镜像中。

## K8S基础

### 基础架构

Kubernetes 项目的架构，跟它的原型项目 Borg 非常类似，都由 Master 和 Node 两种节点组成，而这两种角色分别对应着控制节点和计算节点。

- 控制节点，即 Master 节点，由三个紧密协作的独立组件组合而成，它们分别是负责 API 服务的 kube-apiserver、负责调度的 kube-scheduler，以及负责容器编排的 kube-controller-manager。整个集群的持久化数据，则由 kube-apiserver 处理后保存在 Etcd 中。
- 而计算节点上最核心的部分，则是一个叫作 kubelet 的组件。

Kubernetes 并不是基于Docker创建的项目，而只是将Docker视为一种容器运行时的实现，只要能够运行标准镜像就能使用k8s来管理。

### kubelet

kubelet 主要负责同容器运行时（比如 Docker 项目）打交道。而这个交互所依赖的，是一个称作 CRI（Container Runtime Interface）的远程调用接口，这个接口定义了容器运行时的各项核心操作，比如：启动一个容器需要的所有参数。这也是为何，Kubernetes 项目并不关心你部署的是什么容器运行时、使用的什么技术实现，只要你的这个容器运行时能够运行标准的容器镜像，它就可以通过实现 CRI 接入到 Kubernetes 项目当中。

此外，kubelet 还通过 gRPC 协议同一个叫作 Device Plugin 的插件进行交互。这个插件，是 Kubernetes 项目用来管理 GPU 等宿主机物理设备的主要组件，也是基于 Kubernetes 项目进行机器学习训练、高性能作业支持等工作必须关注的功能。

而 kubelet 的另一个重要功能，则是调用网络插件和存储插件为容器配置网络和持久化存储。

### Pod、Service 与 Development

- Pod 里的容器紧密相连，视为一个整体，共享同一个 Network Namespace、同一组数据卷，从而达到高效率交换信息的目的。
- Service为Pod提供一个代理入口，对外暴露一个固定的地址。这样像Web应用只需关注数据库Pod的Service信息即可，而不用关心数据库的IP是否变化。
- Development可以一次启动多个Pod实例，并完成容器的滚动更新。

除了应用与应用之间的关系外，应用运行的形态是影响“如何容器化这个应用”的第二个重要因素。为此，Kubernetes 定义了新的、基于 Pod 改进后的对象。比如 Job，用来描述一次性运行的 Pod（比如，大数据任务）；再比如 DaemonSet，用来描述每个宿主机上必须且只能运行一个副本的守护进程服务；又比如 CronJob，则用于描述定时任务等等。

在 Kubernetes 项目中，我们所推崇的使用方法是：
1、通过一个“编排对象”，比如 Pod、Job、CronJob 等，来描述你试图管理的应用；
2、为它定义一些“服务对象”，比如 Service、Secret、Horizontal Pod Autoscaler（自动水平扩展器）等。这些对象，会负责具体的平台级功能。

这种使用方法，就是所谓的“声明式 API”。这种 API 对应的“编排对象”和“服务对象”，都是 Kubernetes 项目中的 API 对象（API Object）。这就是 Kubernetes 最核心的设计理念。

## 集群安装

### kubeadm

https://github.com/kubernetes/kubeadm
使用简单的k8s部署工具，但不能用于生产环境
kubeadm把 kubelet 直接运行在宿主机上，然后使用容器部署其他的 Kubernetes 组件。

安装
$ apt-get install kubeadm
但是如果你有部署规模化生产环境的需求，我推荐使用kops或者 SaltStack 这样更复杂的部署工具。但，在本专栏接下来的讲解中，我都会以 kubeadm 为依据进行讲述。一

对于只关心学习 Kubernetes 本身知识点、不太关注如何手工部署 Kubernetes 集群的同学，可以略过本节，直接使用 MiniKube 或者 Kind，来在本地启动简单的 Kubernetes 集群进行后面的学习即可。如果是使用 MiniKube 的话，阿里云还维护了一个国内版的 MiniKube（https://github.com/AliyunContainerService/minikube），这对于在国内的同学来说会比较友好。

### 手动安装k8s集群

https://time.geekbang.org/column/article/39724

https://www.datayang.com/article/45

## Pod

Pod，是 Kubernetes 项目中最小的 API 对象，即原子调度单位，而不是容器。这就意味着，Kubernetes 项目的调度器，是统一按照 Pod 而非容器的资源需求进行计算的。
这样就不会出现Swam里三个应用都需要在一台服务器上启动，2个启动完后第3个没有资源了的问题。只有将三个应用都放在一个pod中就可以了。

Pod 里的所有容器，共享的是同一个 Network Namespace，并且可以声明共享同一个 Volume。原生Docker启动多个容器共享同一Namespace的话必须有先后启动顺序，这样关系就不是对等，而是拓扑。k8s则是先创建中间容器Infra，然后其它容器通过Join Network Namespace与其关联形成对等关系，pod的生命周期和Infra容器保持一致。

## Development

所有控制器都放在k8s安装目录的pkg/controller下。实际上，这些控制器之所以被统一放在 pkg/controller 目录下，就是因为它们都遵循 Kubernetes 项目中的一个通用编排模式，即：控制循环（control loop）。
控制过程伪代码如下：

```
for {
  实际状态 := 获取集群中对象X的实际状态（Actual State）
  期望状态 := 获取集群中对象X的期望状态（Desired State）
  if 实际状态 == 期望状态{                    
    什么都不做
  } else {
    执行编排动作，将实际状态调整为期望状态
  }
}
```

Development控制器
Development管理ReplicaSet，ReplicaSet管理多个Pod。
Development修改ReplicaSet控制的值，来实现Pod的数量扩展。
ReplicaSet根据希望的值，来增加，删除Pod数。

Deployment 实际上是一个两层控制器。首先，它通过 ReplicaSet 的个数来描述应用的版本；然后，它再通过 ReplicaSet 的属性（比如 replicas 的值），来保证 Pod 的副本数量。备注：Deployment 控制 ReplicaSet（版本），ReplicaSet 控制 Pod（副本数）。
