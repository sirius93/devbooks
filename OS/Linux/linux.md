[TOC]

# Linux

## 基本概念

Linux 本质来源不是 Unix，但是它借鉴了 Unix 的设计思想，所以在系统业界上把这种和 Unix 是一致设计思想的系统归为：类 Unix 系统。
类 Unix 系统，除了我们今天要讲的 Linux，还有Mac OS X、FreeBSD（这两个是直接从 Unix 系发展过来的，所以相对 Linux 是比较地道的类 Unix 系统）

通常情况下，Linux 被打包成供个人计算机和服务器使用的 Linux 发行版，一些流行的主流 Linux 发布版，包括 Debian（及其派生版本 Ubuntu、Linux Mint）、Fedora（及其相关版本 Red Hat Enterprise Linux、CentOS, RHEL）和 openSUSE。


## 常用指令

shell 脚本的第一行通常是 `#! /bin/sh` 是告诉内核使用 bash。

### 算术指令

`$((...))` 的算术展开提供完整的算术运算能力,且使用与 C 相同的运算符与优先级.


### 变量声明

语法：`变量名=值`，中间不能有任何的空格，在变量前加上 `$` 表示引用变量。
当所赋予的值包含空格的时候，需要将值用单引号或者双引号包起来，用单引号包起来的后果是单引号里面的所有特殊符号都不具备特殊含义，用双引号包起来代表特殊符号有特殊含义。

例：

```bash
foo=100
echo $foo
```

- `readonly` 可以用于将变量修饰为只读变量。（例：`readonly foo`）
- `export` 用于导出变量，这样其它 shell 也能直接使用。
- `unset` 用于删除变量或函数。

取值

- 在变量前加上 `$` 表示引用变量的值
- `${name:=default_value}` 当不存在时返回默认值
- `${name?message}` 当不存在时显示消息并中断当前命令
- `${name:+value}` 不存在时返回空，存在时返回 `value` 指定的值

```bash
${bar:=100}
```

数值变量

Shell 中变量默认为字符串，可以使用 `declare` 声明为数值类型。

```bash
declare -i n=10
```

命令替换

命令替换用于存储命令的执行结果，在适当的地方再输出。
命令替换是使用反引号包裹的命令。

```bash
DATE=`date`  
echo "Date is $DATE"  

files=`ls -l`
echo "Tree is $files"
```

### 输出指令

- echo <字符串> - 回显并换行
- echo <字符串> < <文件名>	- 输出到文件
	- << 为追加到文件
- echo $? - 显示上一条指令的执行结果，`0` 表示正常执行完毕
- printf <字符串> <参数列表> - 输出
- eval <字符串> - 将字符串当做命令进行执行，多个参数时用空格进行分隔

### 输入指令

- cat -n <file>		- 显示文件内容，-n 表示显示行号
- more <file>		- 以分页形式显示文件内容
- head -n [n] <file>	- 显示文件前 n 行
- tail -n [n] [-f] <file> - 显示后 n 行，f 表示交互环境，可以用于监视文件，直到 ctrl+c 选项后才停止
- clear			  - 清屏
- cal			  - 显示日历
- read <varname> - 读取输入到变量中
- grep <字符串> <文件列表>	- 使用正则表达式进行检索
 - -i 忽略大小写
 - -n 显示行号
 - -R 遍历目录
 - -E 使用正则
 - -c <n> 同时打印上下 n 行
 - -A <n> 显示匹配到的这行和它后面的n行
 - -v 排除匹配的行。

### 帮助

- help	-	查看所有命令
- man <cmd>	- 查看指定命令的详细信息，q 退出查看

### 历史命令

- history     查看历史命令

### 文件夹指令

- pwd	- 显示当前路径

- ls	- 显示当前文件夹下所有可见文件
	- -l	- 查看文件详细信息
		- 详细信息中 `d` 表示文件夹，`-` 表示文件，`l` 表示链接
	- -a	- 显示隐藏文件
	- 文件名	- 显示特定文件，文件名可用通配符 `*`

- cd	- 跳转到指定路径
	- ..	- 跳转到上一级路径
	- ~		- 跳转到 Home
	- $HOME - 跳转到指定的环境路径

- mkdir 文件夹名	- 创建文件夹
	- p		- 创建多层文件夹

- 删除匹配的所有文件夹 `find . -name build -type d -exec rm -rf {} \;`

### 文件命令

- touch 文件名		- 创建文件

- file 文件名		- 显示文件类型

- rm 文件名		- 删除文件
	- i		- 删除前确认
	- f		- 强制删除
	- R 	- 删除文件夹

- cp 源 目标		- 复制文件
	- i		- 复制前确认
	- R		- 复制文件夹

- scp 源 目录	- 复制文件到远程目录
  scp ././target/swagger-spring-1.0.0.jar root@10.0.60.115:/var/local/mockServer/sso

- mv 源 目标		- 移动文件
- ln 源文件名 链接名	- 创建硬链接（实际为拷贝）
	- -s	- 创建软链接（源文件删除后就无法访问）

### 权限命令

文件权限

- chmod	权限 路径	- 设置权限
	- 权限如 1777，其中第一个参数为是否只有自己可用删除
	- chmod a+r path - All is read.

应用权限

- chown -R www.www /alidata/www/phpwind
	- 可将目录/alidata/www/phpwind下的所有文件和目录的拥有者拥有组都修改为www账户

不知道应用使用的是何种账户访问文件时，可以通过以下指令查询

- ps aux|grep nginx

### 登录/登出

- login		- 登录
- id		- 查看当前登录的用户
- who		- 查看所有用户
- hostname 查看登录的完整用户名

### 任务命令

- <cmd>&	在后台执行命令
- jobs 查看所有任务

### 其它命令

- top   - 查看机器负载情况
- df		- 查看分区
- |			- 管道，用于同时执行多个命令，如 `ls -l | grep ^d | more`
- wc		- 计数，需要通过管道与其它命令同时使用
	- -l 统计行数
	- -w 次数
	- -c 统计字数
- sort -n   - 按照数值大小排序
- uniq -c   - 显示不重复的数据，并在每行前追加出现次数
- tee 文件名	- 输出控制台信息到文件，需要通过管道与其它命令结合
	- `ls -l | tee fil1`
- env		- 查看环境变量
- source	- 执行脚本文件
- ps		- 查看进程
- ifconfig	- 查看 ip 地址
- zip <target> <source> 	- 压缩文件
- tar -cvf 目标文件名 要压缩的目标（可多个）	- 压缩文件
	- -xvf	- 解压缩到当前路径

### 链接

链接分软链接和硬链接，其中软链接又叫符号链接，本质只是包含了指向的文件的地址，相当于快捷方式。硬链接是文件的不同命名，与原文件是等同的，所以不能再不同文件系统上为一个文件创建硬链接。

建立软链接

```bash
ln -s <source> <dist>
```

建立硬链接

```bash
ln <source> <dist>
```

使用 ls 查看创建情况

```
lrwxr-xr-x  1 sidneyy  staff  10 Apr 29 10:01 ky -> ../foo.txt
-rw-r--r--  2 sidneyy  staff  12 Apr 26 16:22 lky
```

软链接类型为 `l`

### 磁盘命令

- df -sh  查看磁盘空间

### 定时任务

```
crontab <minute> <hour> <dayofmonth> <mon> <dayofweek> <cmd>
```

参数没有的话使用 `*` 占位。可以用 `,` 区别多种条件，用 `-` 表示范围。

crnotab -e  进行 vi 编辑并注册
crontab -l  列出所有注册



### 控制语句

空格非常重要！！！

#### 条件语句

##### If

判断条件写在 `[]` 中且有一个空格

```bash
if [ 1 -eq 1 ]; then
    echo '1==1'
elif [ 1 -ne 1 ]; then
    echo '1<>1'
else
    echo 'other'
fi
```

条件语句中常用的运算符:  

字符串判断  
str1 = str2　　　　　　当两个串有相同内容、长度时为真  
str1 != str2　　　　　 当串 str1 和 str2 不等时为真  
-n str1　　　　　　　 当串的长度大于 0 时为真(串非空)  
-z str1　　　　　　　 当串的长度为 0 时为真(空串)  
str1   当串 str1 为非空时为真

数字的判断  
int1 -eq int2　　　　两数相等为真  
int1 -ne int2　　　　两数不等为真  
int1 -gt int2int1 大于 int2 为真  
int1 -ge int2int1 大于等于 int2 为真  
int1 -lt int2int1 小于 int2 为真  
int1 -le int2int1 小于等于 int2 为真

文件的判断  
-r file　　　　　用户可读为真  
-w file　　　　　用户可写为真  
-x file　　　　　用户可执行为真  
-f file　　　　　文件为正规文件为真  
-d file　　　　　文件为目录为真  
-c file　　　　　文件为字符特殊文件为真  
-b file　　　　　文件为块特殊文件为真  
-s file　　　　　文件大小非 0 时为真  
-t file　　　　　当文件描述符(默认为 1 )指定的设备为终端时为真   

复杂逻辑判断  
-a 　 　　　　　 与  
-o　　　　　　　 或  
!　　　　　　　　非  


##### case

每个条件都可以有多个语句，以 `;;` 表示一个条件的结束。

```bash
echo 'input number:\n'
read num
case $num in
    1) echo 'number 1'
        echo 'the second sentence';;
    2) echo 'number 2';;
    *) echo 'default';;
esac
```

#### 循环语句

循环语句支持 `break` 和 `continue`。

##### for

```bash
for i in $(seq 1 10)
do
    echo ${i}
done
```

操作文件时可以这样 `for file in $(ls $dirname)`

另一种 for 循环

```bash
for((k=5; $k<10; k=$k+1))
do
    echo $k;
done
```

##### while

```bash
declare -i k=1
while [ $k -le 5 ]
do
    echo $k
    k=$k+1
done
```

##### Until

```bash
declare -i k=5
until (($k>10))
do
    echo $k
    k=$k+1
done
```

### 函数

函数声明时可以省略 `function` 关键字，也可以省略 `return` 关键字。

$0：是脚本本身的名字；
$#：是传给脚本的参数个数；

```bash
# 定义函数，省略 function add()
add(){
 echo 'call a function'
 # 函数参数 $1, $2
 return $(($1+$2))
}
# 调用函数
add 2 3
# 获得函数返回值
ret=$?
echo "sum = $ret"
```

### 通用 Source Build 步骤

1. wget http://目的のソース.tar.gz
2. tar -xvf 目的のソース.tar.gz
3. cd 目的のソース
4. ./configure 或 ./configure --prefix 指定安装路径
5. make
6. sudo make install

### 环境变量

#### export

export 表示使某个变量生效，仅对当前终端有效

export 多个变量时使用空格进行分隔

可以使用 export PATH = ... 直接进行赋值

#### 配置文件

`/etc/profile`	系统环境配置（所有用户）
`$HOME/.bash_profile`	当前用户环境配置

## 包管理

### 分类

|os|name|sample|
|---|---|---
|RedHat|yum|yum update
|Mac|brew|brew upgrade
|Debian|apt-get|apt-get upgrade

### 安装路径

|os|location
|---|---
|ubuntu|dpkg -L packageName
|redhat|rpm -ql packageName
|mac|安装在 /usr/local/Cellar, link 在/usr/bin

### 包管理器指令

|type|os|cmd
|---|---|---
|安装|ubuntu|apt-get install ...
||centos|yum -y install ...
|检索|ubutn|apt-get search ...
||centos|yum search...

### 检索指令



## Curl

标准 Get 请求输出到控制台

```bash
curl 'http://packagist.jp/packages.json'
```

- --verbose(-v) 详细表示请求头信息
- -k 忽略证书

指定 Get 请求参数

```bash
curl 'http://example.com/search' \
--verbose \
--get \
--data-urlencode 'q=ウェッブサービス' \
--data 'page=25'
```

以上构建的 url 等同于 "http://example.com/search?q=webapi&page=25"

`data` 和 `data-urlencode` 可以多次出现

指定 Post 请求参数

```bash
curl 'http://example.com/entries' \
--verbose \
--request POST \
--data-urlencode 'title=特報記事' \
--data-urlencode 'description=ブログ記事です'
```

`--request` 的缩写形式为 `-x`

以上 Post 请求的形式为 "application/x-www-form-urlencoded"

其它类型二进制数据的 Post 请求

```bash
curl 'http://example.com/entries' \
--verbose \
--request POST \
--header 'Content-Type: application/json' \
--data-binary '{"title":"hoge","description":"fuga"}'
```

- --data-binary 用于指定数据
- --header 用于指定请求头

请求参数

- --header(-H) 指定请求头，可以复数使用
- --cookie(-b) 指定cookie，字符串(a=x;b=y;...)或者本地cookie路径
- --cookie-jar(-c) 保存cookie到指定文件
- --user-agent (-A)
- --referer (-e)
- --resolve 指定特定ip对于特定hostname (--resolve 'example.com:80:127.0.0.1')
- -d 指定文件

使用配置文件

新建文件 api.conf

```bash
url = http://example.com/search
data-urlencode = "q=検索API"
data = "page=1"
```

命令行输入

```bash
curl --config api.conf
```

## CentOS

### 检测端口是否有打开

TCP 端口

```bash
nc -w 1 127.0.0.1 53 && echo true || echo false
```

UDP 端口

```bash
nc -w 1 -u 127.0.0.1 53 && echo true || echo false
```

### 查看端口占用情况

```bash
netstat -tnlp
```

### 关闭端口对应进程

```bash
kill -9 16085
```

### 打开端口

CentOS 7.0

```bash
firewall-cmd --zone=public --add-port=80/tcp --permanent
```

打开后重启防火墙

```bash
firewall-cmd --reload
```

### 修复英文路径

如果安装了中文版的 CentOS 之后，root目录及home目录下会出现中文到路径名，如 “桌面”、“文档”，“图片 、公共的” 、“下载”、 “音乐”、“ 视频”等目录，此时使用控制台会非常不方便。可以使用以下命令进行恢复：

```bash
export LANG=en_US
xdg-user-dirs-gtk-update
```

### 查找应用路径

where 查找所有路径

```bash
where java
```

which 查找当前命令使用的路径

```bash
which java
```
