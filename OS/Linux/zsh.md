[TOC]

# Zsh

## plugin

- git
- autojump 需要先安装 `brew install autojump`，然后通过 `j <dir_name>` 可以跳转到任意历史记录的目录。

## 特性

杀应用

传统 `ps -Af | grep ruby`，然后 `kill -9 <id>`

zsh

```bash
kill ruby<TAB>
```

查找

```bash
find <from_path> -name <name>
```

例：

```
find . -name *.rb
```
