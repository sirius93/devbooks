素材
非 Retina 素材为1x，通常直接使用文件名
Retina 素材为2x，通常为文件名@2x.png
iPhone 6+Retina 素材为3x，文件名为@3x.png

非 Retina 屏幕上一个 pt 是一个像素

代码自动完成 Tab，也可以用于在参数间跳转

cmd+t 打开选项卡

Product 菜单用于编译和运行项目
cmd+r 直接运行

模拟器安装 option，可以执行多点触摸


Swift 语法

String template
var sentence = "your name is \(name)"

数组
var message:[String]=["a", "b"]
message[1]
message.append("c")
message.count

字典
var message:[String:String]=["x":"a","y":"b"]
message["x"]
message.count

对象
var label:UILabel = UILabel()

方便方法
class A{
	func initWithName(str:String)->String{}
}
var a:A = A(name:"abc")

类型转换
String(a)
a as String

常量
let x = 1

可选值
即在可能不返回返回值或者无返回值时必须声明
var str:NSString ? ="John"
即如果 str 没有初始化，则值为 nil
fun methodA()->NSDate?{}
var date= d as? NSDate

可选值的拆包
var x=str!
隐式拆包
var x:String!=str

可选绑定
if let s:String=str{

}

防止空值
?.
a.m()?.name.m()

循环
for var count=0;count<50;count=count+1{}
for msg in messages{}
for (k,v) in messages{}
while expres{}
do{}while expres


内存管理
大部分对象在方法执行完后就被自动释放
weak 声明的变量也如此
值为 nil 也回被释放

## Cocoa Touch

Cocoa Touch 是一系列软件框架和运行环境，其前身类名都以 NS 打头

iOS 技术层
核心os 层-核心服务层-多媒体层-Cocoa Touch 层

iOS 生命周期

main()-UIApplicationMain()-willFinishLaunchingWithOptions
-didFinishLaunchingWithOptions-handle events
-退出前台-applicationWillResignActive-进入后台-applicationDidEnterBackground

applicationDidEnterBackground 用于进入后台前序列化内容

 Cocoa 类都以 NS 开头，部分 Swift 类已经和 Cocoa 类做了桥接，但是还是有 NSDate,NSURL 等没有做桥接，只能使用 Cocoa 类。

 帮助
 按住 option 点击类名或方法名

 ## Interface Builder

 XML称为 storyboard
 组成特定屏幕的一系列称为场景，通过切换将场景关联起来

 预览界面
 助手编辑器-Automatic-Preview

 创建输出口
 按住场景的 View Controller 拖到控件上

 创建事件连接
 打开Connection Inspect，选择 Send Events 中的一项拖到场景的 View Controller 上
 右键视图上任意对象可以快速查看其配置的事件连接

 self.abc 类似 this.abc

 MVC
 视图控制器
 @IBAction
 @IBOutlet

 @IBOutlet weak var flowerView: UIWebView!
 @IBAction func getFlower(sender: AnyObject?) {}
AnyObject 是通用类型，编写与特定类无关联的代码

IB到代码
安装 ctrl 键拖到 class 中

图像切片
第一二条决定了重复区域，第二第三条间决定了被替换区域



模拟器键盘不弹出
选择hardware，然后选择keyboards，有三个选项， 依次都勾选就行了

//隐藏键盘
thePlace.resignFirstResponder()

替换文字
theStory.text.stringByReplacingOccurrencesOfString("<place>", withString: thePlace.text!)

调整视图叠放次序
Editor-Arrange-Send to back

生成随机数
arc4random_uniform(10)生成1-10随机数

类型转换
Double()


在 Playground 中测试 WebView
import UIKit
import XCPlayground


var appleView:UIWebView=UIWebView(frame: CGRectMake(0,0,400,400))
var appleURL:NSURL
appleURL=NSURL(string: "http://www.apple.com")!
appleView.loadRequest(NSURLRequest(URL: appleURL))

XCPlaygroundPage.currentPage.liveView=appleView

然后 View-Assistant Editor-Show Assistant Editor 就可以看到实时效果


plist 运行访问非暗号化网页
修改 info.list
 <key>NSAppTransportSecurity</key>
    <dict>
        <key>NSAllowsArbitraryLoads</key>
        <true/>
    </dict>


设置 ScrollView 可滚动区域
 theScroller.contentSize=CGSizeMake(300, 1000)


 使用音乐
 import AudioToolbox

 系统音和提示音的区别是提示音静音时会自动震动


 ## 使用工具栏和选择器
