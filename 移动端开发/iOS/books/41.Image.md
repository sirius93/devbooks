[TOC]

# Image

## 触摸事件

UIImage 默认没有开启交互功能，无法响应触摸功能，需要先进行开启

```oc
self.userInteractionEnabled = YES;
```
