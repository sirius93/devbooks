[TOC]

# Flutter

## 开始使用

### 创建工程

```
flutter create my_app
```

### 运行工程

运行前需要保证模拟器已开启，开启iPhone模拟器的命令为 `open -a Simulator`

```
flutter run
```
## 添加依赖

解决被墙问题，添加以下地址

```
export PUB_HOSTED_URL=https://pub.flutter-io.cn
export FLUTTER_STORAGE_BASE_URL=https://storage.flutter-io.cn
```

1. 修改依赖文件，工程下的`pubspec.yaml`。
2. 执行 `flutter packages get`。

如果出现“Waiting for another flutter command to release the startup lock...”，则可以删除Flutter安装目录下的`/bin/cache/lockfile`。


## 基本使用

### 设置启动界面

```
void main() => runApp(MyApp());
```

### 添加无状态组件

```
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Startup Name Generator',
      home: RandomWords(),
    );
  }
}
```

### 添加有状态组件

```
class RandomWords extends StatefulWidget {
  @override
  RandomWordsState createState() => new RandomWordsState();
}

class RandomWordsState extends State<RandomWords> {
  final _suggestions = <WordPair>[];
  final _biggerFont = const TextStyle(fontSize: 18.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Startup Name Generator'),
      ),
      body: _buildSuggestions(),
    );
  }

  Widget _buildSuggestions() {
    return ListView.builder(
        padding: const EdgeInsets.all(16.0),
        itemBuilder: (context, i) {
          if (i.isOdd) return Divider();

          final index = i ~/ 2;
          if (index >= _suggestions.length) {
            o.addAll(generateWordPairs().take(10));
          }
          return _buildRow(_suggestions[index]);
        });
  }

  Widget _buildRow(WordPair pair) {
    return ListTile(
      title: Text(
        pair.asPascalCase,
        style: _biggerFont,
      ),
    );
  }

}
```

## 语法

无状态组件不能被修改，有状态组件可以被修改。


## 官方网站

https://github.com/flutter/flutter

https://flutter-io.cn/docs/get-started/install/macos
