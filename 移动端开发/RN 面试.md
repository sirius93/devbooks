- 设计模式
广播使用了观察者模式
AIDL 使用了代理模式
View 和 ViewGroup 使用了组合模式
Media 使用了门面模式
AlertDialog 使用了建造者模式
Service 使用了单例模式
Adapter 使用了适配器模式

- View 绘制过程
先调用 onMeasure() 测量控件的大小
再调用 onLayout() 测量布局的位置
然后调用 onDraw() 绘制界面

- 事件分发
系统传递事件给 Activity 对应的 phoneWindow 上的 decorView
然后调用 decorView 的 dispatchTouchEvent() 进行事件分发
触发 dispatchTouchEvent() 的如果是 view 的话则分发给自己
如果是 ViewGroup 的话可以通过 onIterceptorTouchEvent() 阻止事件传递给其子 View
如果一个 View 设置了 OnTouchListener 的话，则优先处理 OnTouchListener 的 onTouch 事件
没有消费的话才交给 onTouchEvent 处理

- Fragment 生命周期
onAttach - onCreate - onCreateView - onActvitiyCreated - onStart - onResume

- LauncherMode
Standard   每次新开一个 Activity
SingleTop 如果在栈顶则复用，并且调用其的 onNewIntent() 方法
SingleTask ??新开一个栈，由其打开的 act 总在其栈中 栈中独享，即栈中只能有一个该实例，如果已经打开则复用，并且弹出其上面的所有 Activity
SingleInstance 新开一个栈，其打开的 Activity 还是在原来的栈中

- 布局优化
减少层级
多使用相对布局
使用 merge 合并布局
使用 ViewStub 延迟实例化，可见时才加载

- listview滑动时右侧滚动条长度一直在改变是因为什么？
track

- 广播分类
有序广播  -  被广播接收器接收后，可被终止，无法往下继续传达。         典型代表：短信广播
普通广播  -  发送至每一个已经注册（订阅）的广播接收器，无法被终止。 典型代表：开机启动广播

- 广播顺序
当广播为有序广播时
优先级高的先接收
动态优先于静态
静态：先扫描的大于后扫描的，动态：先注册的大于后注册的

广播为普通广播时
无视优先级，动态广播接收器优先于静态广播接收器
静态：先扫描的大于后扫描的，动态：先注册的大于后注册的

- AMS, PMS
AMS Activity Manager Service
AMS 负责创建，管理 Activity
使用startActivity有两种形式：
直接调用Context类的startActivity方法；这种方式启动的Activity没有Activity栈，因此不能以standard方式启动，必须加上FLAG_ACTIVITY_NEW_TASK这个Flag。
调用被Activity类重载过的startActivity方法，通常在我们的Activity中直接调用这个方法就是这种形式；

真正的startActivity使用了Instrumentation类的execStartActivity方法
其实startActivity最终通过ActivityManagerNative这个方法远程调用了AMS的startActivity方法

PMS Package Manager Service
PMS 掌管包信息
主要负责各种APK的安装，卸载，优化和查询

- 强引用，弱引用和软引用
强引用，内存满了后 oom
软引用，内存满了后，gc 后回收
弱引用，gc 后回收

- Binder
实现 IBinder 的类
实现跨进程通信
客户端和服务端通信的媒介
Android 系统的各种 Manager 就是对应的服务端，应用层则是客户端

- Redux
Store 应用全局唯一，存储所有数据
ActionCreator 创建 Action
Action 触发的一个事件
Dispatch 注册在 Store 中，将 Action 发送到 Store 上
Reducer 注册在 Store 中，处理 Store 接收到的 Action 将结果应用到 View 上

- Growth hacking
既懂技术又懂营销

- 组件化开发
一个应用分成多个 Module 进行开发，每部分都是一个 lib
一个共同模块，负责工具类
其它模块按照业务组装

- 插件化开发
一个应用分成一个宿主 APK 和多个插件，每部分都是一个 apk
主要动态加载 apk，获得其中的 class 文件，然后通过反射实例化

- JSX
在 JS 代码中直接加入 Html

- React Router
路由控制

- Redux thunk
一个 thunk 就是一个封装表达式的函数，封装的目的是延迟执行表达式
Redux thunk 会检测函数是否需要 dispatch, 会自行绑定，否则就需要手动传 store.dispatch
除了传入 dispatch 参数, 还会把  getState 作为第二个参数传入
redux-thunk 是一个通用的解决方案，其核心思想是让 action 可以变为一个 thunk ，这样的话:
同步情况：dispatch(action)
异步情况：dispatch(thunk)

- Immutable
用于每次返回新对象，对象间的深层比较

- flow
代码静态检查

- RN Style 特点
不用单位
不支持百分比

- Flexbox 布局
justifyContent
flexDirection，默认 column
alignItems
alignSelf

- RN 和 Weex 区别
RN 单位采用原生 dp/pt，Weex 则假设宽度为 750px

- RN timeout
```js
function timer(t){
  return new Promise(resolve=>setTimeout(resolve, t));
}

await Promise.race([ apiGet('/api/foo'), timer(1000)]);
```

- 应对 OOM
Detect Memory Leaks
发现内存过高，比如说100m，点击 Dump Java Heap，点击 Analyser Task分析 activity，比如说有一个分析的framework，持有了当前 activity，导致不会被释放
或者使用 Leak Canary

- 性能优化工具
Android Studio
Android Monitor Tab，可以查看内存，cpu, network
比如说列表画面，进入后来内存16m，来回滑动后，network 没有变化，证明缓存起了作用，退出列表画面，内存下降，证明没有泄露

Frame Rate
https://github.com/wasabeef/Takt
测试帧率是否在60s

Hugo
https://github.com/JakeWharton/hugo
方法执行时间测试工具

开发者模式
Show Layout Bounds
显示布局

Debug GPU Overdraw
显示过渡绘制

UIAutomatorViewer
UIAutomatorViewer在 sdk 中，可以查看 view 布局，实时修改布局样式

Animations Testing
animation scale 可以放慢动画来查看动画哪里出错了
Animations Testing (another style)
adb shell screencast /sdcard/demo.mp4
adb pull /sdcard/demo.mp4
然后通过 vlc 打开逐帧查看

Stetho
通过浏览器查看布局

- RN 坑
1. ToolBar 无法显示 —> 必须设置高度
2. TextInput 无法显示 —> 设置 flexDirection为 Row 后会丢失宽度，必须设置宽度或 flex:1
3. 添加某些库后 iOS 编译报在用户名下找不到模块（issue #4968）
3. xhr 无法发起请求，返回 unexpected </html> 错误 —> http method 需严格为 GET 而不是 get
4. socket.io 报 userAgent undefined —> 修改 userAgent  为任意值
5. socket.io 无法发起请求 —> 修改 io config.jsonp=false
6. promise.resolve() 不接受 JSONArray 等自定义对象以及 Map，而只接受基本类型和 ReadableMap 等 RN 对象，而 iOS 支持原生 Dictornary
7. 默认不支持 timeout，只能改原生或靠 js 模拟
8. react-native-router-flux 快速切换路由时，生命周期函数没有执行，动画也没显示，切换画面时加延时
9. require 的文件名必须是静态的，不能是字符串拼接，否则只会找完全一致，不会匹配解析度
10. 不支持 mipmap 文件夹
11. 只能返回单个节点，因此页面上或多或少会产生一些冗余的节点，过多嵌套容易造成 crash



---

- 提高效率做的工具
1. 集成一个 debug 画面
2. Log 重定向到 View, 文件， console
3. 原生，h5 路由
4. 可配置文件
5. SonarQube
6. Mock Server




---


介绍项目
各模块设计
介绍项目中用到的技术
如何分析ANR异常
APP架构设计
