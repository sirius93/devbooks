# iOS vs Android

## 基本功能

### Controller ~~ Activity

iOS

初始化 Controller

oc

```oc
HypnosisViewController *controller=[[HypnosisViewController alloc]init];
```

swift

```swift
 let hvc=HypnosisViewController()
```

Android

没有办法获得 Activity 实例

### 初始画面和背景色

iOS

在 `AppDelegate.m` 中设置 `root controller`

oc

```oc
self.window.rootViewController=myController;
self.window.backgroundColor=[UIColor whiteColor];
[self.window makeKeyAndVisible];
```

swift

```swift
self.window?.rootViewController=myController
self.window?.backgroundColor=UIColor.white
self.window?.makeKeyAndVisible()
```

Android

在 `AndroidMainifest.xml` 中设置 `intent-filter` 和 `theme`

### View 作为成员变量

iOS

需声明为 `IBOutlet`，且除非是根布局，否则通常都声明为弱引用

oc

声明在类的扩展中

```oc
@interface ReminderViewController()
@property (nonatomic, weak) IBOutlet UIDatePicker *datePicker;
@end
```

swift

```swift
 @IBOutlet weak var datePicker:UIDatePicker?
```

### 静态变量

iOS

oc

直接在方法内部声明

```oc
static NSDateFormatter *dateFormatter = nil;
if(!dateFormatter){
    dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    dateFormatter.timeStyle=NSDateFormatterNoStyle;
}
```

swift

```swift
static let dateFormatter:DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .medium
    formatter.timeStyle = .none
    return formatter
}()
```

Android

```java

```

### 控件回调方法

iOS

点击事件需要声明为 `IBAction`，其它方法需要实现对应的 delegate

自定义方法

oc

```oc
-(IBAction)addReminder:(id)sender{
}
```

swift

```oc
@IBAction func addReminder(_ sender: Any) {
}
```

其它方法

oc

```oc
@interface HypnosisViewController()<UITextFieldDelegate>
@end

@implementation HypnosisViewController
- (void)loadView{
  textField.delegate = self;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
}
@end
```

swift

```swift
class HypnosisViewController: UIViewController,UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
  }
}
```

Android

两种没有区别


## 生命周期

### 布局加载完

iOS

一般只有第一次加载时应用的配置在这里加载

oc

```oc
-(void)viewDidLoad{
    [super viewDidLoad];
}
```

swift

```swift
override func viewDidLoad() {
    super.viewDidLoad()
}
```

Android

```java
 protected void onCreate(Bundle savedInstanceState) {
   super.onCreate(savedInstanceState);
 }
```

### 布局可见/不可见

iOS

一般每次都需要应用的配置在这里进行设置

oc

```oc
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}
```

swift

```swift
override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
}
override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
}
```

Android

```java
@Override
protected void onResume() {
    super.onResume();
}
@Override
protected void onPause() {
    super.onPause();
}
```

### 重绘画面

iOS

oc

```oc
[self setNeedsDisplay];
```

swift

```swift
self.setNeedsDisplay()
```

Android

```java
this.invalidate();
```

### 设置画面布局

iOS

重写 `loadView()` 方法

oc

```oc
-(void)loadView{
    self.view=backgroundView;
}
```

swift

```swift
override func loadView() {
}
```

Android

```java
setContentView(view);
```

### 画面间传递参数

iOS

直接修改掌握的 `ViewController` 的成员变量


### 格式化日期

iOS

oc

```oc
NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
dateFormatter.dateStyle = NSDateFormatterMediumStyle;
dateFormatter.timeStyle=NSDateFormatterNoStyle;
self.dateLabel.text=[dateFormatter stringFromDate:item.dateCreated];
```

swift

```swift
let formatter = DateFormatter()
formatter.dateStyle = .medium
formatter.timeStyle = .none
dateLabel.text=formatter.string(from: item!.dateCreated)
```

Android

```java

```


## 布局

### UINavigationController ~~ ToolBar

#### 显示在画面上

iOS

在 `AppDelegate` 中创建 `UINavigationController` 的实例并作为根控制器
`UINavigationController` 中保存着画面栈，且初始化时需传入最为栈底的控制器，只有栈顶的控制器所对应的画面可见

oc

```oc
UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:ivc];
self.window.rootViewController=navController;
```

swift

```swift
let navController = UINavigationController(rootViewController: itemsVC)
window?.rootViewController=navController
```

Android

```java

```

#### 设置标题文字

iOS

在每个 `ViewController` 中都可以通过成员变量 `navigationItem` 修改 `UINavigationController` 上的显示效果

oc

```oc
navigationItem.title=_item.itemName;
```

swift

```swift
navigationItem.title = it?.itemName
```

Android

```java

```

#### 跳转画面

iOS

通过每个 `ViewController` 的 `navigationController` 变量可以控制画面跳转

oc

```oc
[navigationController pushViewController:detailViewController animated:YES];
```

swift

```swift
navigationController?.pushViewController(detailViewController, animated: true)
```

Android

```java

```

#### UINavigationItem 和 UIBarButtonItem ~~ View

iOS

`UINavigationItem` 是 `UINavigationController` 上的显示项，可以通过它修改标题文字，添加左右按钮

oc

```oc
UINavigationItem *navItem = self.navigationItem;

// 设置标题文字
navItem.title=@"Homepwner";

// 设置右按钮为自定义方法，方法名为 addNewItem
UIBarButtonItem *addButtonItem=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewItem:)];
navItem.rightBarButtonItem=addButtonItem;

// 设置左按钮为系统消息，消息为 editButtonItem
navItem.leftBarButtonItem=self.editButtonItem;
```

swift

```swift
let navItem=navigationItem

// 设置标题文字
navItem.title="Homepwer - Swift"

// 设置右按钮为自定义方法，方法名为 addNewItem
let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: "addNewItem:")
navItem.rightBarButtonItem = addButton
navItem.leftBarButtonItem = editButtonItem

// 设置左按钮为系统消息，消息为 editButtonItem
navItem.leftBarButtonItem = editButtonItem
```

Android

```java

```

## 组件

### UITextField ~~ TextView

#### 设置文字

iOS  

oc

```oc
textField.text=item.itemName;
```

swift

```swift
textField.text=item.itemName
```

Android

```java

```

#### 设置 placeHolder

iOS

oc

```oc
textField.placeholder=@"Hypnotize me";
```

swift

```swift
textField.placeholder="Hypno me"
```

Android

```java
textView.setHint("Hypno me");
```


#### 设置 return key type ~~ action type

iOS

oc

```oc
textField.returnKeyType=UIReturnKeyDone;
```

swift

```swift
textField.returnKeyType=UIReturnKeyType.done
```

Android

一般直接在布局文件中设置

```java
textView.setImeOptions(EditorInfo.IME_ACTION_DONE);
```

#### 响应点击 return 事件

iOS

实现 `UITextFieldDelegate` 委托

oc

```oc
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
}
```

swift

```swift
func textFieldShouldReturn(_ textField: UITextField) -> Bool {
}
```

Android

```java
textView.addTextChangedListener(new TextWatcher() {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
});
```

#### 关闭键盘

iOS

oc

```oc
[textField resignFirstResponder];
```

swift

```swift
textField.resignFirstResponder()
```

Android

```java
InputMethodManager imManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);  
imManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
```

### UITableViewController ~~ ListView

#### 设置基本样式

iOS

oc

```oc
-(instancetype)init{
    self=[super initWithStyle:UITableViewStylePlain];
    return self;
}
```

swift

```swift
init() {
    super.init(style: UITableViewStyle.plain)
}
```

Android

```java

```

#### 设置显示行数

iOS

oc

```oc
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[[ItemStore sharedStore]allItems]count];
}
```

swift

```swift
override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return ItemStore.sharedStore().allItems.count
}
```

Android

```java

```

#### 设置单元格内容

iOS

oc

```oc
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    // 每次创建
//    UITableViewCell *cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UITableViewCell"];
    // 从池中重用布局
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"UITableViewCell" forIndexPath:indexPath];
    NSArray *items=[[ItemStore sharedStore]allItems];
    BNRItem *item=items[indexPath.row];
    cell.textLabel.text=[item description];
    return cell;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    //告诉系统如何创建对象池内的对象
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
}
```

swift

```swift
override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    // 重用池内对象
    let cell=tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for:indexPath)
    let item=ItemStore.sharedStore().allItems[indexPath.row]
    cell.textLabel?.text=item.description
    return cell
}

override func viewDidLoad() {
    super.viewDidLoad()    
    //告诉系统如何创建对象池内的对象
    tableView.register(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
}
```

Android

```java

```

#### 添加新的一行

iOS

oc

```oc
// 第一段中的最后一行
NSIndexPath *indexPath=[NSIndexPath indexPathForRow:lastRow inSection:0];
[self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
```

swift

```swift
tableView.insertRows(at: [indexPath], with: UITableViewRowAnimation.top)
```

Android

```java

```

#### 编辑模式

iOS

oc

```oc
if(self.isEditing){
    [sender setTitle:@"Edit" forState:UIControlStateNormal];
    //关闭编辑模式
    [self setEditing:NO animated:YES];
}else{
    [sender setTitle:@"Done" forState:UIControlStateNormal];
    [self setEditing:YES animated:YES];
}

// 删除
- (void)   tableView:(UITableView *)tableView
  commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
   forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // If the table view is asking to commit a delete command...
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [tableView deleteRowsAtIndexPaths:@[indexPath]
                         withRowAnimation:UITableViewRowAnimationFade];
    }
}

// 排序
- (void)   tableView:(UITableView *)tableView
  moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath
         toIndexPath:(NSIndexPath *)destinationIndexPath
{
    [[ItemStore sharedStore] moveItemAtIndex:sourceIndexPath.row
                                        toIndex:destinationIndexPath.row];
}
```

swift

```swift
if isEditing {
    sender.setTitle("Edit", for: UIControlState.normal)
    // 关闭编辑模式
    setEditing(false, animated: true)
}else{
    sender.setTitle("Done", for: UIControlState.normal)
    setEditing(true, animated: true)
}

// 删除
override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle==UITableViewCellEditingStyle.delete{
        let items = ItemStore.sharedStore().allItems
        let item=items[indexPath.row]
        ItemStore.sharedStore().removeItem(item: item)

        tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
    }
}

// 排序
override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
    ItemStore.sharedStore().moveItemAtIndex(fromIndex: sourceIndexPath.row, toIndex: destinationIndexPath.row)
}
```

Android

```java

```


#### 刷新数据

OS

oc

```oc
[self.tableView reloadData];
```

swift

```swift
tableView.reloadData()
```

Android

```java

```

#### 选中一行

OS

oc

```oc
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}

```

swift

```swift
override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 }
```

Android

```java

```










---

iOS

oc

```oc

```

swift

```swift

```

Android

```java

```
