[TOC]

# Moco

## 安装

直接前往 github 仓库下载压缩包 [https://github.com/dreamhead/moco](https://github.com/dreamhead/moco)

## 使用

### 命令行

在命令行中运行以下命令

格式

```bash
java -jar <jar_file> http -p <port> -c <config_file>
```

- `-p` 指定端口号
- `-c` 指定配置文件

例：

```
java -jar moco-runner-0.10.2-standalone.jar http -p 12306 -c foo.json
```

### 配置文件

例：

```js
[
  {
    "description": "post text => text",
    "request": {
      "text": "foo"
    },
    "response": {
      "text": "bar"
    }
  },
  {
    "description": "post from file => text",
    "request": {
      "file": "foo.request"
    },
    "response": {
      "text": "pang!!!"
    }
  },
  {
    "description": "get uri => json",
    "request": {
      "uri": "/user/1"
    },
    "response": {
      "json": {
        "name": "Peter",
        "age": 20
      }
    }
  },
  {
    "description": "get+header uri+param => text",
    "request": {
      "uri": "/user",
      "method": "get",
      "headers": {
        "content-type": "application/json"
      },
      "queries": {
        "name": "Jane"
      }
    },
    "response": {
      "text": "can not found"
    }
  }
]
```

- `description` 请求的描述信息
- `request` 请求体
  - `text` body 为文字
  - `json` body 为 json
  - `file` body 为文件
- `response` 响应体
