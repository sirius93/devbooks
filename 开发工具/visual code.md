# Visual Code

## 快捷键

ctrl + g  跳转到指定行号
ctrl + shift + p  输入指令
cmd + p 跳转到指定文件
cmd + p 然后 : 跳转到指定行号
cmd + p 然后 @ 跳转到指定函数
ctrl + tab 文件间切换
shift + option + f 格式化
ctrl + \ 拆分窗口
shift +alt + f  格式化