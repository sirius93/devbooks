[TOC]

## Centos 7 集群搭建

### 集群搭建步骤

总共搭建三台机器，分别为 spark1, spark2 和 spark3

#### 在虚拟机上安装 Centos 7

安装时选择桥接网络

#### 设置临时 IP 地址

```bash
ifconfig eno16777736 192.168.1.101
```

设置完后本机可以使用临时 IP 连接到虚拟机上

#### 设置 Host

```bash
vi /etc/hosts
```

添加以下 Host

```
192.168.1.101 spark1
192.168.1.102 spark2
192.168.1.103 spark3
```

最后将以上 Host 也添加到本机 Host 中。

#### 设置永久 IP 地址

```bash
vi /etc/sysconfig/network-scripts/ifcfg-eno16777736
```

修改以下内容

```
ONBOOT=yes
BOOTPROTO=static
IPADDR=192.168.1.101
NETMASK=255.255.255.0
GATEWAY=192.168.1.1
```

修改完后重启网卡

```bash
service network restart
```


#### 关闭防火墙

```bash
systemctl stop firewalld.service
systemctl disable firewalld.service
```

#### 设置 DNS 服务器

```bash
vi /etc/resolv.conf
```

追加阿里 DNS

```
nameserver 223.5.5.5
nameserver 223.6.6.6
```

测试 DNS 解析

```bash
dig www.taobao.com +short
ping www.taobao.com
```

#### 替换 yum 源

对原来的 yum 源进行备份

```bash
mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
```

下载网易 yum 源

```bash
curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.163.com/.help/CentOS7-Base-163.repo
```

生成缓存

```bash
yum clean all
yum makecache
```

在三台机器上执行以上所有步骤

### 配置 Java 环境

```bash
curl -o /usr/local/java.tar.gz http://download.oracle.com/otn-pub/java/jdk/8u65-b17/jdk-8u65-linux-x64.tar.gz
tar -xvf java.tar.gz
mv jdk1.8.0_65 java8
vi ~/.bashrc
```

添加环境变量

```bash
export JAVA_HOME=/usr/local/java8
export PATH=$PATH:$JAVA_HOME/bin
```

应用环境变量

```bash
source ~/.bashrc
```

在三台机器上都配置好 Java 环境

### 配置 SSH 免密码登陆

在一台机器上生成密钥然后拷贝到另两台服务器上。

1. 生成密钥

    ```bash
    ssh-keygen -t rsa
    ```

2. 复制密钥到指定的地址

    ```bash
    ssh-copy-id -i /root/.ssh/id_rsa.pub spark2
    ```


### 搭建中碰到的问题

#### 无法找到 `ifconfig` 等命令

如果 CentOS 是最小安装可能无法执行 `ifconfig` 等命令，此时需要安装 `net-tools`

```bash
yum install -y net-tools
```

#### 修改了 IP，DNS 后还是无法联网

先取消静态 IP 设置，改为自动获取 IP 地址

```bash
vi /etc/sysconfig/network-scripts/ifcfg-eno16777736
```

修改以下内容

先去掉 IPADDR、NETMASK、GATEWAY，将 BOOTPROTO 由 `static` 改为 `dhcp`

```
BOOTPROTO=dhcp
# IPADDR=192.168.1.101
# NETMASK=255.255.255.0
# GATEWAY=192.168.1.1
```

重启网卡

```bash
service network restart
```

使用 `ifconfig` 查看分配的 IP 地址，切换回静态 IP 并按照 `ifconfig` 显示的信息进行配置。


