[TOC]

## Spark 集群搭建

### 集群搭建步骤

基于 Centos 7 集群，搭建在三台机器上


#### 配置 Spark 环境

[Spark 下载地址](http://spark.apache.org/downloads.html)

1. 解压 Spark 压缩包

2. 设置环境变量


    ```bash
    vi ~/.bashrc
    ```

    添加环境变量

    ```
    export SPARK_HOME=/usr/local/spark
    export PATH=$PATH:$SPARK_HOME/bin
    export CLASSPATH=.:$CLASSPATH:$JAVA_HOME/lib:$JAVA_HOME/jre/lib
    ```

    应用环境变量

    ```bash
    source ~/.bashrc
    ```

3. 修改 Spark 配置

	**修改 `spark-env.sh`**

    ```bash
    cp /usr/local/spark/conf/spark-env.sh.template /usr/local/spark/conf/spark-env.sh
    vi /usr/local/spark/conf/spark-env.sh
    ```

	修改内容

    ```
    export JAVA_HOME=/usr/local/java8
    export SCALA_HOME=/usr/local/scala
    export SPARK_MASTER_IP=spark1
    export SPARK_WORKER_MEMORY=1g
    export HADOOP_CONF_DIR=/usr/local/hadoop/etc/hadoop
    ```

4 修改 slaves 文件

    ```bash
    cp /usr/local/spark/conf/slaves.template /usr/local/spark/conf/slaves
    vi /usr/local/spark/conf/slaves
    ```

    修改内容

    ```
    spark2
    spark3
    ```

5. 复制 Spark 目录到其它节点

    ```bash
    scp -r /usr/local/spark root@spark2:/usr/local/
    ```

6. 在主节点启动 Spark 集群

    ```bash
    /usr/local/spark/sbin/start-all.sh
    ```

7. 验证安装

    在三台机器上分别执行 `jps`，查看是否有以下进程

    - spark1: master
    - spark2: worker
    - spark3: worker

	全部确认后使用浏览器尝试访问： http://spark1:8080
