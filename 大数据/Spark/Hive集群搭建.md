[TOC]

## Hive 集群搭建

### 集群搭建步骤

基于 Centos 7 集群，只需在 Spark 1 上执行

#### 配置 Hive 环境

[Hive 下载地址](http://hive.apache.org/downloads.html)

1. 解压 Hive 压缩包

2. 配置环境变量

    ```bash
    vi ~/.bashrc
    ```

    添加环境变量

    ```
    export HIVE_HOME=/usr/local/hive
    export PATH=$PATH:$HIVE_HOME/bin
    ```

    应用环境变量

    ```bash
    source ~/.bashrc
    ```

3. 修改 Hive 配置

    **修改 `hive-default.xml`**

    ```bash
    mv /usr/local/hive/conf/hive-default.xml.template /usr/local/hive/conf/hive-default.xml
    vi /usr/local/hive/conf/hive-default.xml
    ```

    修改内容

    ```
    <property>
      <name>javax.jdo.option.ConnectionURL</name>
      <value>jdbc:mysql://spark1:3306/hive_metadata?createDatabaseIfNotExist=true</value>
    </property>
    <property>
      <name>javax.jdo.option.ConnectionDriverName</name>
      <value>com.mysql.jdbc.Driver</value>
    </property>
    <property>
      <name>javax.jdo.option.ConnectionUserName</name>
      <value>hive</value>
    </property>
    <property>
      <name>javax.jdo.option.ConnectionPassword</name>
      <value>hive</value>
    </property>
    <property>
      <name>hive.metastore.warehouse.dir</name>
      <value>/user/hive/warehouse</value>
    </property>
    ```

    **修改 `hive-env.sh`**

    ```bash
    cp /usr/local/hive/conf/hive-env.sh.template /usr/local/hive/conf/hive-env.sh
    vi /usr/local/hive/bin/hive-config.sh
    ```

    修改内容

    ```
    export JAVA_HOME=/usr/local/java8
    export HIVE_HOME=/usr/local/hive
    export HADOOP_HOME=/usr/local/hadoop
    ```

4. 验证安装

    ```bash
    hive
    create table test(id int);
    select * from test;
    drop table test;
    ```

    如果报 `[ERROR] Terminal initialization failed; falling back to unsupported` 说明 `Hive has upgraded to Jline2 but jline 0.94 exists in the Hadoop lib.`。

    解决办法是删除 Hadoop 下的 jline 包

    ```bash
    rm /usr/local/hadoop/share/hadoop/yarn/lib/jline-0.9.94.jar
    ```


#### 配置 MySql 环境

1. 安装 MySql Server

    Centos 7 源使用了 MariaDB 替代了 MySql，所以不能直接使用 yum 来安装，需要先添加 MySql 源

    ```bash
    rpm -ivh http://dev.mysql.com/get/mysql-community-release-el7-5.noarch.rpm
    yum install -y mysql-server
    ```

2. 启动 MySql Server

    ```bash
    systemctl start mysqld.service
    systemctl enable mysqld.service
    ```

3. 安装 MySql Java Connector

    ```bash
    yum install -y mysql-connector-java
    ```

4. 将 `mysql-connector-java.jar` 拷贝到 Hive 的 lib 包中

    ```bash
    cp /usr/share/java/mysql-connector-java.jar /usr/local/hive/lib
    ```

5. 在 MySql 上创建 Hive 元数据库，并对 Hive 进行授权

    ```bash
    mysql
    create database if not exists hive_metadata;
    grant all privileges on hive_metadata.* to 'hive'@'%' identified by 'hive';
    grant all privileges on hive_metadata.* to 'hive'@'localhost' identified by 'hive';
    grant all privileges on hive_metadata.* to 'hive'@'spark1' identified by 'hive';
    flush privileges;
    use hive_metadata;
    ```

