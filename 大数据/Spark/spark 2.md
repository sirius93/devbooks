[TOC]

## 概述

### 分类

- Spark Core 离线计算
- Spark SQL 交互式查询
- Spark Streaming 实时流计算(实际由于是批处理，所以是准实时)
- Spark MLlib 机器学习
- Spark GraphX 图形计算


### 其它概念

- HDFS 大数据的存储系统
- MapReduce 计算框架
- Hive 通过SQL查询HDFS数据
- HBase 通过NOSQL进行实时或非实时查询
- Spark 针对HDFS进行离线计算，针对Hive进行查询


### Spark 与 Hadoop

Spark Streaming是准实时，batch处理
Storm来一条处理一条，实时，所以吞吐量不大

Spark与MapReduce最大的不同为迭代式计算
MapReduce在map和reduce后就结束了，所以在一个job里处理有限

趋势

Spark进行计算
Hadoop存储数据(HDFS,Hive,HBase)及资源调度(YARN)


## 使用 Spark

### 运行 Spark 程序

#### 运行在本地

编写代码后直接运行

#### 运行在 Spark 集群

1. 将待分析的文件上传到 HDFS 上
  ```bash
  hadoop fs -put /opt/tmp/spark.txt /spark.txt
  ```
  提交后的文件可以通过 [Web 界面](http://spark1:50070/) 的 `Utilities` - `Browse the file system` 进行查看
2. 打包应用程序
  Java 版本需要使用 Maven 提交包括 Scala 环境，Scala 直接导出 Java 就可以了。
  对于 Java 应用程序
  使用 Maven 插件，对 Spark 工程进行打包 `mvn clean package`
3. 将打包后的 Spark 工程的 jar 包上传到机器
4. 编写 spark-submit 脚本
  ```bash
  /usr/local/spark/bin/spark-submit \
  --class com.bookislfe.spark_sample.core.WordCountCluster \
  --num-executors 3 \
  --driver-memory 100m \
  --executor-memory 100m \
  --executor-cores 3 \
  /opt/tmp/spark-study/java/spark-sample-0.0.1-SNAPSHOT-jar-with-dependencies.jar
  ```
5. 执行 spark-submit 脚本，提交 Spark 应用到集群执行


spark-submit
不指定master的话默认使用的也是本地程序
使用集群需要添加
--master spark://192.168.1.107:7077
因为集群太耗内存，所以以后都使用local测试

rdd内存迭代式不像mapreduce可以不用跨节点进行计算

reduceByKey会在reduce之前先在本地进行一次聚合计算，然后根据同一个Key
shuffle到同一个节点上

### RDD

#### 概念

- RDD 是 Spark 提供的核心抽象，全称为 Resilient Distributed Dataset，即弹性分布式数据集。
- RDD 在抽象上来说是一种元素集合，包含了数据。
- 一个 RDD 会被分为多个分区，每个分区分布在集群中的不同节点上，从而让 RDD 中的数据可以被并行操作。
- RDD 数据通常从 HDFS 或 Hive 获得。

### 创建RDD

三种方式

- 使用集合，常用于测试
- 使用本地文件，常用于临时文件
- 使用 HDFS，最常用，针对 HDFS 存储的大数据进行离线批处理操作

### 并行化集合

集合中的一部分数据到一个节点，另一部分到另一个节点
`parallelize()` 可以指定将集合切分为多少个 partition，每个 partition 会有一个 task 来进行处理，官方建议每个 CPU 创建 2-4 个 partition
Spark 默认会根据集群的情况来设置

### 文件系统

Spark 支持任何 Hadoop 支持的存储系统来创建 RDD，HDFS，Cassandra，HBase，本地文件
使用 `context` 的 `textFile()`，支持文件，目录，压缩文件，通配符

如果是本地文件，windows 进行本地测试只要有一份文件，如果是在集群上针对 linux 本地文件，则需要拷贝到所有 worker 上
Spark 默认会为 HDFS 文件的每一个 block 创建一个partition，但是也可以通过 `textFile()`的第二个参数指定分区，但是必须比 block 数量多


### RDD 操作

分类

RDD 操作分为 Transformation 和 Action
- Transformation 会针对已有的 RDD 创建一个新的 RDD
- Action 主要对 RDD 进行最后的操作，如遍历，reduce，保存到文件等，并可以返回结果给 Driver 程序

map() 就是 transformation 操作
transformation 的特点就是 lazy 特性，不会触发 Spark 执行，避免产生过多中间结果
action 执行后会触发一个 Spark job 的运行，从而触发这个 action 之前的所有 transformation

Action

- reduce
- collect
- count
- take
- saveAsTextFile
- countByKey
- foreach

### RDD 持久化

#### 使用持久化

调用 RDD 的 `cache()` 或 `persist()` 方法

其中
`cache()` 调用的是 `persist(MEMORY_ONLY)`
`persist()` 支持持久化策略

清除缓存则使用 `unpersist()``
使用 `cache()` 必须是链式操作即
lines = cx.textFile().cache()
不能是
lines = cx.textFile()
lines.cache()

#### 持久化策略

- MEMORY_ONLY
- MEMORY_AND_DISK
- MEMORY_ONLY_SER     同 MEMORY_ONLY，但是会使用 JAVA API 进行序列化
- MEMORY_AND_DSK_SER
- DISK_ONLY
- MEMORY_ONLY_2 保存到第二个节点作为备份

如何选择策略

1. MEMORY_ONLY 优先
2. MEMORY_ONLY_SER,需要 CPU 进行反序列化
3. 后缀_2，快速的错误中恢复
4. 尽量不使用 DISK 相关策略


### 共享变量

调用算子函数时，默认每个 task 保留一份变量的副本，且只能操作自己的变量。但是有两种共享变量：

- Broadcast Variable 广播变量
  只读，广播变量仅仅为每个节点拷贝一份，可以优化性能
  `context.broadcast(variable)`
  `variable.value()`

- Accumulator 累加变量
  可写，累加变量，多个task操作一份，主要用于累加，task只能进行累加，driver才能进行读取
  `context.accumulator(0)`



## Spark 架构原理

### Spark 基本组件

1. Driver （进程）
  负责执行提交的 Spark 应用程序
  通常 Driver 就是 Spark 集群的节点之一或者提交 Spark 程序的机器
2. Master （进程）
  负责资源调度和分配，还有集群的监控等
3. Worker （进程）
  存储 RDD 的 partition，启动其它进程和线程对 RDD 上的 partition 进行计算
4. Executor (进程）和 Task （线程）
  执行计算过程

### Spark 初始化过程

Driver 注册到 Master，Master 发送给 Worker，Worker 为应用启动 Executor
Executor 会注册给 Driver，Driver 就知道哪些 Executor 是为其服务的

### Spark 内核架构

- Application
- spark-submit
- Driver
  进程，执行应用
- SparkContext
  初始化 DAGScheduler 和 TaskScheduler
- Master
  使用资源调度算法通知集群的 Worker 上为应用启动多个 Executor
- Worker
 为应用启动 Executor
- Executor
  进程，反向注册到 TaskScheduler 上
- Job
  每个 action 被执行就会创建一个 Job，Job 会提交给 DAGScheduler
- DAGScheduler
  DAGScheduler的主要任务是基于Stage构建DAG，决定每个任务的最佳位置
  DAGScheduler 会将 Job 分为多个 Stage，为每个 Stage 创建一个 TaskSet 并提交给底层的 TaskScheduler
- TaskScheduler
  通过其自己的后台进程连接 Master，向 Master 注册应用
  将 TaskSet 中的每一个 Task 提交到 Executor 上执行
- Task
  Task是在集群上运行的基本单位。一个Task负责处理RDD的一个partition
- Stage
  RDD的多个patition会分别由不同的Task去处理，这些 Task 组成了一个 Stage。一个 Job 包含多个 Stage，一个 Stage 由一组相同的 Task 组成。
- ShuffleMapTask and ResultTask
  Task 有两种
  ShuffleMapTask根据Task的partitioner将计算结果放到不同的bucket中。
  而ResultTask将计算结果发送回Driver Application，所以只有最后一个 Task 是 ResultTask

## 宽依赖与窄依赖

窄依赖 Narrow Dependency
RDD 的每个 partition 仅仅依赖于父RDD的一个 partition，即一对一关系
如简单的map操作

宽依赖 Shuffle Dependency
本质就是 shuffle，每个父 RDD 的 partition 都可能传输到子 RDD 的多个 partition
如reduce操作

## Spark 应用

- 二次排序：按照第一列排序，如果第一列相同，则按照第二列排序
- topn












---------------------------------------------------


？mapreduce shuffle

21 8606

概述
HDFS大数据的存储系统
MapReduce计算框架
Hive通过SQL查询HDFS数据
HBase通过NOSQL进行实时或非实时查询
Spark针对HDFS进行离线计算，针对Hive进行查询
Spark Core离线计算
Spark SQL交互式查询
Spark Streaming实时流计算
Spark MLlib机器学习
Spark GraphX图形计算

趋势
Spark进行计算
Hadoop存储数据(HDFS,Hive,HBase)及资源调度(YARN)

Spark Streaming是准实时，batch处理
Storm来一条处理一条，实时，所以吞吐量不大


Scala基础

REPL环境
:paste
进入粘贴模式，可以插入多行代码，然后ctrl+d


块表达式最后一句就是返回值

输入
readLine()

Breaks.break实现累break功能
breakable{
 if (n==5) break
}

for推导式  yield 构造集合


参数类型必须，返回值类型如果函数体不包含递归就可以省略
递归最经典的斐波那契数列
1 1 2 3 5 8 13

定义函数
def foo(){1}
def foo()={1}
def foo():Int={1}

sum(1,2,3,4)
sum(1 to 4: _*)


过程，lazy值和异常
过程即没有返回值（即没有等号）或返回值为Unit的函数

lazy只有第一次使用时才会计算

Array,ArrayBuffer
ArrayBuffer类型ArrayList

array.mkString()
array.toString()
Array一般用mkString()


array->array
for(i<-arr if i%2==0) yield i*i
arr.filter(_ % 2==0).map(2*_)

map
tuple
空map
new mutable.HashMap[String,Int]


@BeanProperty 修饰field




调用父类构造函数时
如果父类中定义了参数
则子类定义参数时不要加val或var，否则相当与覆盖


                                                13,56        全部

匿名内部类
val p=new Person(){
 override def hello=""
}


trait直接完成责任链设计模式

trait field初始化
提前定义
在初始化前定义
trait SayHello{
 val msg:String
 //初始化代码
 println(msg.toString)
}
class Person extends SayHello{
  子类调用父类构造方法，此时msg还未被重写
  val msg:String = "init"
}
p=new Person().println
class Person
val p =new {
 val msg:String = "init"
}with Person with Sayhello

或者用lazy value


Scala函数式编程

匿名函数
函数付给变量
高级函数
高阶函数类型推断
def greeting(func:(String)=>Unit, name:String)=func(name)
greeting((name)=>println(name))
def triple(func:(Int)=>Int)=func(3)
triple(3*_)

常用高阶函数
map(2*_)
foreach(println _)
filter(_%2==0)
reduceLeft(_*_)，reduce操作，即先对1，2处理，处理完结果与3处理。。。
sortWith(_<_)

闭包
函数在变量不处于其有效作用域时，还能够对变量进行访问
Scala通过为每个函数创建一个函数对象来实习，局部变量作为函数对象的成员变量来实现


SAM转换
Scala需要调用Java代码，
Java不支持将函数传入方法，通常需要实现某个接口，而这些接口都只有单个抽象方法
SAM 即 single abstract method

SAM需要使用隐式转换
Java
button.addActionListener(new ActionListener{
  override def actionPerformed(event:ActionEvent){
    println("click")
  }
})

Scala
implicit def getActionListener(actionProcessFunc:(ActionEvent)=>Unit)=new ActionListener{
  override def actionPerformed(event:ActionEvent){
    println("click")
  }
}
button.addActionListener((e:ActionEvent)=>println("click"))

Currying函数


ArrayBuffer类似ArrayList

mutable.LinkedList(1,2,3)
有elem和next，类似List head和tail

val lines=List(lines01,lines02)
lines.flatMap(_.split("")).map((_,1)),map(_,2).reduceLeft(_+_)

对Array和List进行模式匹配，匹配指定值和所有元素
Option与模式匹配



泛型
泛型类型
如果类初始化需要泛型参数，则创建时可以自动推断，而不要明确指定类型
泛型函数
view bounds对类型进行隐式转换后再判定是否在范围内
<%
Context Bounds
对于T:类型要求必须存在一个类型为 类型[T]的隐式参数，基于上下文，隐式参数由Scala注入
class Calculator[T:Ordering]{
  def max(implicit order:Orering[T])={}
}
Manifest Context Bounds
数组元素为T，则定义为[T:Manifest]泛型类型就可以实例化Array[T]
class Meat()
class Vegetable()
def packageFood[T:Manifest](food: T*)={}

Existential Type
Array[_]



------------------------------------------------------------------


隐式转换
即手动定义将某种类型的对象转换成其它类型的对象
类似依赖注入，由Scala自动注入
隐式转换最核心的就是定义隐式转换函数
使用隐式转换加强现有类功能，装饰功能
装饰vs代理
代理通常都是在类中直接创建代理类的对象
装饰则是传入对象


隐式参数



Actor
Actor trait
重写act方法
使用start()启动
使用！向actor发送消息
actor使用receive和模式匹配接收消息

class HelloActor extends Actor{
 def act(){
    while(true){
           receive{
        case name:String=>println(name)
}
    }
  }
}
val helloActor=new HelloActor
helloActor.start()

helloActor!"leo"


收发case class

receive{
 case Login(username,password)=>
}

actor!Login("abc","123)


Actor之间互发消息

一个actor向另一个发消息时带上自己的引用，如电话
对方通过引用来回消息

case class Message(content:String, sender:Actor)


不常用
同步获得返回
val reply=act!?message

异步获得返回
val future=act!!message
val reply=future()




-----------------------------------------

Centos 6.5 集群搭建

8606

设临时
hostname
ifconfig eth0 192.168.1.191
vi /etc/hosts
192.168.1.191   spark1
ping spark1

配windows hosts
192.168.1.191 spark1


永久配置虚拟机网络


关闭防火墙
配置dns服务器
vi /etc/resolv.conf
nameserver 61.139.2.69
ping www.baidu.com

---------------------CentOS 7集群-----------------------------------------

安装时选择桥接网络

设置临时 ip 地址

```bash
ifconfig eno16777736 192.168.1.101
```bash

设置 host

```bash
vi /etc/hosts
```

```
192.168.1.101 spark1
192.168.1.102 spark2
192.168.1.103 spark3
```

设置永久 ip 地址

```bash
vi /etc/sysconfig/network-scripts/ifcfg-eno16777736
```

```
ONBOOT=yes
BOOTPROTO=static
IPADDR=192.168.1.101
NETMASK=255.255.255.0
GATEWAY=192.168.1.1
```

重启网卡

```bash
service network restart
```

关闭防火墙

```bash
systemctl stop firewalld.service
systemctl disable firewalld.service
```

设置 DNS 服务器

```bash
vi /etc/resolv.conf
```

阿里 DNS

```
nameserver 223.5.5.5
nameserver 223.6.6.6
```

测试 DNS 解析

```bash
dig www.taobao.com +short
```

换 yum 源为网易

备份

```bash
mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
```

下载网易源

```bash
curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.163.com/.help/CentOS7-Base-163.repo
```

生成缓存

```bash
yum clean all
yum makecache
```

如果CentOS是最小安装可能无法执行 `ifconfig` 等命令，此时需要安装 `net-tools`

```bash
yum install -y net-tools
```

配置Java环境

```bash
curl -o /usr/local/java.tar.gz http://download.oracle.com/otn-pub/java/jdk/8u65-b17/jdk-8u65-linux-x64.tar.gz
tar -xvf java.tar.gz
mv jdk1.8.0_65 java8
vi /etc/bashrc
export JAVA_HOME=/usr/local/java8
export PATH=$PATH:$JAVA_HOME/bin

source /etc/bashrc
```

配置SSH免密码登录

生成密钥

```bash
ssh-keygen -t rsa
```bash

复制密钥到指定Host

```bash
ssh-copy-id -i /root/.ssh/id_rsa.pub spark2
```

---------------------------------------------------------------

Hadoop 集群搭建

Hadoop 下载地址 http://hadoop.apache.org/releases.html

1. 解压 Hadoop 压缩包

2. 配置环境变量

```bash
vi /etc/bashrc
```

内容

```
export HADOOP_HOME=/usr/local/hadoop
export PATH=$PATH:$HADOOP_HOME/bin:$HADOOP_HOME/sbin
```

```bash
source /etc/bashrc
```

3. 修改 Hadoop 配置，三台都是一样的配置

```bash
vi /usr/local/hadoop/etc/hadoop/core-site.xml
```

内容

```
<property>
  <name>fs.default.name</name>
  <value>hdfs://spark1:9000</value>
</property>
```

```bash
vi /usr/local/hadoop/etc/hadoop/hdfs-site.xml
```

内容

```
<property>
  <name>dfs.name.dir</name>
  <value>/usr/local/data/namenode</value>
</property>
<property>
  <name>dfs.data.dir</name>
  <value>/usr/local/data/datanode</value>
</property>
<property>
  <name>dfs.tmp.dir</name>
  <value>/usr/local/data/tmp</value>
</property>
<property>
  <name>dfs.replication</name>
  <value>3</value>
</property>
```

```bash
cp /usr/local/hadoop/etc/hadoop/mapred-site.xml.template /usr/local/hadoop/etc/hadoop/mapred-site.xml
vi /usr/local/hadoop/etc/hadoop/mapred-site.xml
```

内容

```
<property>
  <name>mapreduce.framework.name</name>
  <value>yarn</value>
</property>
```

```bash
vi /usr/local/hadoop/etc/hadoop/yarn-site.xml
```

内容

```
<property>
  <name>yarn.resourcemanager.hostname</name>
  <value>spark1</value>
</property>
<property>
  <name>yarn.nodemanager.aux-services</name>
  <value>mapreduce_shuffle</value>
</property>
```

```bash
vi /usr/local/hadoop/etc/hadoop/slaves
```

内容

```
spark1
spark2
spark3
```

4. 由于其它两台也是同样配置，所以直接将 Hadoop 工作目录复制到另两台服务器上

```bash
scp -r /usr/local/hadoop root@spark2:/usr/local/
```

5. 在三台机器上各自创建工作目录

```bash
mkdir -p /usr/local/data/tmp
mkdir -p /usr/local/data/datanode
mkdir -p /usr/local/data/namenode
```

6. 启动 HDFS 集群

格式化 NameNode

```bash
hdfs namenode -format
```

启动集群

```bash
start-dfs.sh
```

检查集群状态

在三台机器上各自执行 `jps` 命令，检查下列进程是否运行

spark1：NameNode、DataNode、SecondaryNameNode
spark2：DataNode
spark3：DataNode

浏览器尝试访问： http://spark1:50070/

7. 启动 YARN 集群

```bash
start-yarn.sh
```

在三台机器上各自执行 `jps` 命令，检查下列进程是否运行

spark1：ResourceManager、NodeManager
spark2：NodeManager
spark3：NodeManager

浏览器尝试访问：http://spark1:8088/


----------------------------------------------------------------

配置 Hive

Hive搭建

只需在 Spark 1 上执行

安装 MySql

1. 安装 Server

Centos 7 源使用了 MariaDB 替代了 MySql，所以不能直接使用 yum 来安装

```bash
rpm -ivh http://dev.mysql.com/get/mysql-community-release-el7-5.noarch.rpm
yum install -y mysql-server
```

2. 启动 MySql Server

```bash
systemctl start mysqld.service
systemctl enable mysqld.service
```

3. 安装 Connector

```bash
yum install -y mysql-connector-java
```

4. 将mysql connector拷贝到hive的lib包中

```bash
cp /usr/share/java/mysql-connector-java.jar /usr/local/hive/lib
```

5. 在mysql上创建hive元数据库，并对hive进行授权

```bash
mysql
create database if not exists hive_metadata;
grant all privileges on hive_metadata.* to 'hive'@'%' identified by 'hive';
grant all privileges on hive_metadata.* to 'hive'@'localhost' identified by 'hive';
grant all privileges on hive_metadata.* to 'hive'@'spark1' identified by 'hive';
flush privileges;
use hive_metadata;
```


安装 Hive

Hive 下载地址 http://hive.apache.org/downloads.html

1. 解压 Hive 压缩包

2. 配置环境变量

```bash
vi /etc/bashrc
```

内容

```
export HIVE_HOME=/usr/local/hive
export PATH=$PATH:$HIVE_HOME/bin

```

```bash
source /etc/bashrc
```

3. 修改 Hive 配置

```bash
mv /usr/local/hive/conf/hive-default.xml.template /usr/local/hive/conf/hive-default.xml
vi /usr/local/hive/conf/hive-default.xml
```

内容

```
<property>
  <name>javax.jdo.option.ConnectionURL</name>
  <value>jdbc:mysql://spark1:3306/hive_metadata?createDatabaseIfNotExist=true</value>
</property>
<property>
  <name>javax.jdo.option.ConnectionDriverName</name>
  <value>com.mysql.jdbc.Driver</value>
</property>
<property>
  <name>javax.jdo.option.ConnectionUserName</name>
  <value>hive</value>
</property>
<property>
  <name>javax.jdo.option.ConnectionPassword</name>
  <value>hive</value>
</property>
<property>
  <name>hive.metastore.warehouse.dir</name>
  <value>/user/hive/warehouse</value>
</property>
```

```bash
cp /usr/local/hive/conf/hive-env.sh.template /usr/local/hive/conf/hive-env.sh
```

```bash
vi /usr/local/hive/bin/hive-config.sh
```

内容

```
export JAVA_HOME=/usr/local/java8
export HIVE_HOME=/usr/local/hive
export HADOOP_HOME=/usr/local/hadoop
```

4. 验证安装

```bash
hive
create table test(id int);
select * from test;
drop table test;
```

如果报 `[ERROR] Terminal initialization failed; falling back to unsupported` 说明 `Hive has upgraded to Jline2 but jline 0.94 exists in the Hadoop lib.`。

解决办法是删除 Hadoop 下的 jline

```bash
rm /usr/local/hadoop/share/hadoop/yarn/lib/jline-0.9.94.jar
```

如果报 `Exception in thread "main"java.lang.RuntimeException: java.lang.IllegalArgumentException:java.net.URISyntaxException: Relative path in absolute URI:${system:java.io.tmpdir%7D/$%7Bsystem:user.name%7D`

解决办法是修改 hive-site.xml 中所有包含 "system:java.io.tmpdir" 的属性为绝对路径。

-----------------------Zookeeper 集群搭建----------------------------------------------------


安装 Zookeeper

Zookeeper 下载地址 http://zookeeper.apache.org/releases.html

1. 解压 Zookeeper 压缩包

2. 配置环境变量

```bash
vi /etc/bashrc
```

内容

```
export ZOOKEEPER_HOME=/usr/local/zk
export PATH=$PATH:$ZOOKEEPER_HOME/bin
```

```bash
source /etc/bashrc
```

3. 修改 Zookeeper 配置

```bash
mv /usr/local/zk/conf/zoo_sample.cfg /usr/local/zk/conf/zoo.cfg
vi /usr/local/zk/conf/zoo.cfg
```

内容

```
dataDir=/usr/local/zk/data
server.0=spark1:2888:3888
server.1=spark2:2888:3888
server.2=spark3:2888:3888
```

4. 创建目录

```bash
mkdir -p /usr/local/zk/data
```

5. 配置节点标志

```bash
echo 0 > /usr/local/zk/data/myid
```

6. 复制 Zookeeper 目录到另两个节点，并创建data和修改 .bashrc, myid(1 和 2)

```bash
scp -r /usr/local/zk root@spark2:/usr/local/
```

7. 在三台机器上分别启动 Zookeeper

```bash
zkServer.sh start
```

8. 验证安装

```bash
zkServer.sh status
```

坑

报 "Starting zookeeper ... already running as process xxx"

可能是上次没有正常关闭系统造成的，检查 /usr/local/zk/data 下有没有zookeeper_server.pid 文件，有的就删除掉。


-----------------------Kafka 集群搭建----------------------------------------------------

安装 Scala

Scala 下载地址 http://www.scala-lang.org/download/2.11.7.html

```bash
export SCALA_HOME=/usr/local/scala
export PATH=$PATH:$SCALA_HOME/bin
```

安装 Kafka

Kafka 下载地址 http://kafka.apache.org/downloads.html

1. 解压 Kafka 压缩包

2. 修改 Kafka 配置

```bash
vi /usr/local/kafka/config/server.properties
```

内容

```
broker.id=0
zookeeper.connect=spark1:2181,spark2:2181,spark3:2181
```

3. 解压 slf4j 压缩包，将 slf4j-nop 复制到 Kafka 的 libs 目录下

```bash
cp /usr/local/slf4j-1.7.13/slf4j-nop-1.7.13.jar /usr/local/kafka/libs
```

4. 复制 Kafka 目录到其它节点，唯一要改的是 broker.id

```bash
scp -r /usr/local/kafka root@spark2:/usr/local/
```

5. 在三台机器上分别启动 Kafka 服务，启动完成后可通过 `jps` 查看进程

```bash
nohup bin/kafka-server-start.sh config/server.properties &
```

6. 验证安装

在三台机器上分别执行 `jps`，查看是否有 `kafka` 进程。

创建 Topic

```bash
bin/kafka-topics.sh --zookeeper spark1:2181,spark2:2181,spark3:2181 --topic TestTopic --replication-factor 1 --partitions 1 --create
```

创建生产者

```bash
bin/kafka-console-producer.sh --broker-list spark1:9092,spark2:9092,spark3:9092 --topic TestTopic
```

创建消费者

```bash
bin/kafka-console-consumer.sh --zookeeper spark1:2181,spark2:2181,spark3:2181 --topic TestTopic --from-beginning
```

-----------------------Spark 集群搭建----------------------------------------------------

安装 Spark

Spark 下载地址 http://spark.apache.org/downloads.html

1. 解压 Spark 压缩包

2. 设置环境变量

```
export SPARK_HOME=/usr/local/spark
export PATH=$PATH:$SPARK_HOME/bin
export CLASSPATH=.:$CLASSPATH:$JAVA_HOME/lib:$JAVA_HOME/jre/lib
```

3. 修改 Spark 配置

```bash
cp /usr/local/spark/conf/spark-env.sh.template /usr/local/spark/conf/spark-env.sh
vi /usr/local/spark/conf/spark-env.sh
```

内容

```
export JAVA_HOME=/usr/local/java8
export SCALA_HOME=/usr/local/scala
export SPARK_MASTER_IP=spark1
export SPARK_WORKER_MEMORY=1g
export HADOOP_CONF_DIR=/usr/local/hadoop/etc/hadoop
```

4 修改 slaves 文件

```bash
cp /usr/local/spark/conf/slaves.template /usr/local/spark/conf/slaves
vi /usr/local/spark/conf/slaves
```

内容

```
spark2
spark3
```

5. 复制 spark 目录到其它节点

```bash
scp -r /usr/local/spark root@spark2:/usr/local/
```

6. 在主节点启动 spark 集群

```bash
/usr/local/spark/sbin/start-all.sh
```

7. 验证安装

在三台机器上分别执行 `jps`

spark1: master
spark2: worker
spark3: worker

浏览器访问 http://spark1:8080



----------------------------------------------------


主机器启动hdfs
start-dfs.sh
主机器启动yarn
start-yarn.sh
三台机器启动zk
zkServer.sh start
三台启动kfk
cd/usr/local/kafka/
nohup bin/kafka-server-start.sh config/server.properties &
启动spark
/usr/local/spark/sbin/start-all.sh

-------------------------------------------------------



RDD是Spark提供的核心抽象，全称为Resillient Distributed Dataset，即弹性分布式数据集。
RDD在抽象上来说是一种元素集合，包含了数据。它是被分区的，分为多个分区，每个分区分布在集群中的不同节点上，从而让RDD中的数据可以被并行操作。（分布式数据集）
一个RDD会有多个分区，每个分区都在不同的节点上
RDD数据通常从HDFS或Hive获得



Spark与MapReduce最大的不同为迭代式计算
MapReduce在map和reduce后就结束了，所以在一个job里处理有限


-----------------------------------------

第一个Spark应用

### 运行在本地

### 运行在Spark集群
- 将spark.txt文件上传到hdfs上去
	hadoop fs -put /opt/tmp/spark.txt /spark.txt
	提交后的文件可以通过 http://spark1:50070/  Utilities - Browse the file system 查看
- 使用maven插件，对spark工程进行打包 mvn clean package
- 将打包后的spark工程jar包，上传到机器
- 编写spark-submit脚本
```bash
/usr/local/spark/bin/spark-submit \
--class com.bookislfe.spark_sample.core.WordCountCluster \
--num-executors 3 \
--driver-memory 100m \
--executor-memory 100m \
--executor-cores 3 \
/opt/tmp/spark-study/java/spark-sample-0.0.1-SNAPSHOT-jar-with-dependencies.jar
```
- 执行spark-submit脚本，提交spark应用到集群执行


???scala 只需
spark-assembly-1.3.0-hadoop



spark-shell写程序
spark-shell

val lines=sc.textFile("hdfs://spark1:9000/spark.txt")


spark1:4040


spark-submit
不指定master的话默认使用的也是本地程序
使用集群需要添加
--master spark://192.168.1.107:7077
因为集群太耗内存，所以以后都使用local测试


rdd内存迭代式不像mapreduce可以不用跨节点进行计算

reduceByKey会在reduce之前先在本地进行一次聚合计算，然后根据同一个Key
shuffle到同一个节点上


Spark架构原理
1. Driver （进程）
编写的Spark程序就在Driver上，由Driver进程执行
Spark集群节点之一或者提交Spark程序的机器
2. Master （进程）
负责资源调度和分配，还有集群的监控等
3. Worker （进程）
用自己的内存存储RDD的partition，启动其它进程和线程对RDD上的partition进行计算
4. Executor (进程）
Task （线程）
执行计算

Driver 注册到 Master，Master 发送给 Worker，Worker 为应用启动Executor
Executor 会注册给 Driver，Driver就知道哪些Executor是为其服务的



--------------------------------------------------

创建RDD
三种方式
使用集合，常用于测试
使用本地文件，常用于临时文件
使用HDFS，最常用，针对HDFS存储的大数据进行离线批处理操作


并行化集合
集合中的一部分数据到一个节点，另一部分到另一个节点
parallelize()可以指定将集合切分为多少个partition，每个partition会有一个task来进行处理，官方建议每个CPU创建2-4个partiton
Spark默认会根据集群的情况来设置


Spark支持任何Hadoop支持的存储系统来创建RDD，HDFS，Cassandra，HBase，本地文件
使用context的textFile()，支持文件，目录，压缩文件，通配符

如果是本地文件，windows进行本地测试只要有一份文件，如果是在集群上针对
linux本地文件，则需要拷贝到所有worker上
Spark默认会为hdfs文件的每一个block创建一个partition，但是也可以通过textFile()
的第二个参数指定分区，只能比block数量多


java版本需要使用maven提交包括scala环境，scala直接导出java就可以了


----------------------------------


操作RDD
分为transformation和action
transformation会针对已有的RDD创建一个新的RDD
action主要对RDD进行最后的操作，如遍历，reduce，保存到文件等，并可以返回结果给Driver程序

map就是transformation操作
transformation的特点就是lazy特性，不会触发Spark执行，避免产生过多中间结果
action执行后会触发一个Spark job的运行，从而触发这个action之前的所有transformation



action操作开发

reduce
collect
count
take
saveAsTextFile
countByKey
foreach

RDD持久化
调用RDD的cache()或persist（）方法
cache()调用的是persist(MEMORY_ONLY)
persist()支持持久化策略
MEMORY_ONLY
MEMORY_AND_DISK
MEMORY_ONLY_SER	同MEMORY_ONLY，但是会使用JAVA API进行序列化
MEMORY_AND_DSK_SER
DISK_ONLY
MEMORY_ONLY_2 保存到第二个节点作为备份
清除缓存使用unpersist()
使用cache()必须是链式操作即
lines = cx.textFile().cache()
不能是
lines = cx.textFile()
lines.cache()


如何选择策略
1.MEMORY_ONLY优先
2.MEMORY_ONLY_SER,需要CPU进行反序列化
3.后缀_2，快速的错误中恢复
4.尽量不使用DISK相关策略




共享变量
调用算子函数时，默认每个task保留一份变量的副本，且只能操作自己的变量
两种
Broadcast Variable
只读，广播变量仅仅为每个节点拷贝一份，可以优化性能
context.broadcast(variable)
variable.value()

Accumulator
可写，累加变量，多个task操作一份，主要用于累加，task只能进行累加，driver才能进行读取
context.accumulator(0)


基于排序机制的wordcount






二次排序
按照第一列排序，如果第一列相同，则按照第二列排序

topn

极客学院课程下载
-----------------------


源码剖析

Spark内核源码深度剖析
Spark内核架构
1、Application
2、spark-submit
3、Driver
进程，执行应用
4、SparkContext
初始化DAGScheduler和TaskScheduler
5、Master
使用资源调度算法通知集群的Worker上为应用启动多个Executor
6、Worker
为应用启动Executor
7、Executor
进程，反向注册到TaskScheduler上
8、Job
每个action被执行就会创建一个Job，Job会提交给DAGScheduler
9、DAGScheduler
DAGScheduler会将Job分为多个Stage，为每个Stage创建一个TaskSet
10、TaskScheduler
通过其自己的后台进程连接Master，向Master注册应用
将TaskSet中的每一个Task提交到Executor上执行
11、ShuffleMapTask and ResultTask
Task有两种，只有最后一个Task是ResultTask




宽依赖与窄依赖
窄依赖 Narrow Dependency
RDD的每个partition仅仅依赖于父RDD的一个partition，即一对一关系
如简单的map操作
宽依赖 Shuffle Dependency
本质就是shuffle，每个父RDD的partition都可能传输到子RDD的多个partition
如reduce操作


--------------------------------

基于Yarn的两种提交
Spark三种提交模式
1.standalone，基于Spark自己的Master-Worker集群
2.基于Yarn的yarn-cluster模式
3.基于Yarn的yarn-client模式
切换模式即提交时加上--master [yarn-cluster|yarn-client]
yarn-client的driver运行在本地客户端，负责调度application，会与yarn产生大量网络通信，优点是可以看到所有log，可以用于测试
yarn-cluster的driver运行在nodemanager，用于生产环境

修改 spark-env.sh
export HADOOP_HOME=/usr/local/hadoop

/usr/local/spark/bin/spark-submit \
--class com.bookislfe.spark_sample.core.WordCountCluster \
--num-executors 3 \
--driver-memory 100m \
--executor-memory 100m \
--executor-cores 3 \
/opt/tmp/spark-study/java/spark-sample-0.0.1-SNAPSHOT-jar-with-dependencies.jar


-------------------------------------

SparkContext


Spark内核源码深度剖析Master主备切换机制原理剖析与源码分析
Spark内核源码深度剖析Master注册机制原理剖析与源码分析
Spark内核源码深度剖析Master状态改变处理机制原理剖析与源码分析
Spark内核源码深度剖析Master资源调度算法原理剖析与源码分析




master可以配2个，standalone模式支持主备切换。
主备切换有两种，一是基于文件系统，一个是基本ZooKeeper。
前一种需要手动切换，后一种可以自动切换。



-------------------------------------------------


Spark性能优化技术
1、使用高性能序列化类库
2、优化数据结构
3、对多次使用的RDD进行持久化 / Checkpoint
4、使用序列化的持久化级别
5、Java虚拟机垃圾回收调优
6、提高并行度
7、广播共享数据
8、数据本地化
9、reduceByKey和groupByKey的合理使用
10、Shuffle调优（核心中的核心，重中之重



诊断内存的消耗
首先自行指定分区数
再调用cache()进行缓存
查看driver的log的内存信息，类似Added rdd_0_1 in memory
将其乘以分区数就为内存使用数





序列化
RDD中使用了自定义类型，发生shuffle时会使用
Spark默认使用Java的序列化
还可以使用Kryo序列化

Kryo序列化
sparkConf.set("spark.serializer","org.apache.spark.serializer.KryoSerializer")
最好预先注册需要序列化的类来提高性能
这样内部使用时就无需时刻使用完整包名来占用不少内存
sparkConf.registerKryoClasses(Array(classOf[Hello]))
=================================
优化Kryo类库使用
1.优化缓存大小
当自定义类型的字段过多
sparkConf.set
设置spark.kyroserializer.buffer.mb
默认为2m

2.预先注册自定义类型

======================================

优化数据结构
主要是算子函数
1.优先使用数组和字符串而不是集合
比如说用csv格式代替对象
2.避免对象的多层嵌套
3.使用int替代string
使用自增id替代uuid

===========================================

对多次使用的RDD进行持久化或checkpoint

rdd1-rdd2-rdd10-rdd11-rdd12
			   --rdd result
         -rdd20-rdd21
rdd2得出rdd10后可能会被立即丢弃
此时rdd20需要从rdd1开始重新计算

checkpoint会在开始消耗点性能但是即使持久化丢失也可以从checkpoint读取数据



----------------------------------------------------------

使用序列化的持久化级别

==================================
Java虚拟机垃圾回收调优

监测垃圾回收
只要在spark-submit脚本中，增加一个配置即可，--conf "spark.executor.extraJavaOptions=-verbose:gc -XX:+PrintGCDetails -XX:+PrintGCTimeStamps"。

但是要记住，这里虽然会打印出Java虚拟机的垃圾回收的相关信息，但是是输出到了worker上的日志中，而不是driver的日志中。

但是这种方式也只是一种，其实也完全可以通过SparkUI（4040端口）来观察每个stage的垃圾回收的情况。


优化executor内存比例
对于垃圾回收来说，最重要的就是调节RDD缓存占用的内存空间，与算子执行时创建的对象占用的内存空间的比例。默认情况下，Spark使用每个executor 60%的内存空间来缓存RDD，那么在task执行期间创建的对象，只有40%的内存空间来存放
一旦发现40%的内存空间不够用了，就会触发Java虚拟机的垃圾回收操作
使用new SparkConf().set("spark.storage.memoryFraction", "0.5")即可，可以将RDD缓存占用空间的比例降低，从而给更多的空间让task创建的对象进行使用


高级垃圾回收调优
1.Java堆空间被划分成了两块空间，一个是年轻代，一个是老年代。年轻代放的是短时间存活的对象，老年代放的是长时间存活的对象。年轻代又被划分了三块空间，Eden、Survivor1、Survivor2。

首先，Eden区域和Survivor1区域用于存放对象，Survivor2区域备用。创建的对象，首先放入Eden区域和Survivor1区域，如果Eden区域满了，那么就会触发一次Minor GC，进行年轻代的垃圾回收。Eden和Survivor1区域中存活的对象，会被移动到Survivor2区域中。然后Survivor1和Survivor2的角色调换，Survivor1变成了备用。

如果一个对象，在年轻代中，撑过了多次垃圾回收，都没有被回收掉，那么会被认为是长时间存活的，此时就会被移入老年代。此外，如果在将Eden和Survivor1中的存活对象，尝试放入Survivor2中时，发现Survivor2放满了，那么会直接放入老年代。此时就出现了，短时间存活的对象，进入老年代的问题。

如果老年代的空间满了，那么就会触发Full GC，进行老年代的垃圾回收操作


2.Spark中，垃圾回收调优的目标就是，只有真正长时间存活的对象，才能进入老年代，短时间存活的对象，只能呆在年轻代。不能因为某个Survivor区域空间不够，在Minor GC时，就进入了老年代。从而造成短时间存活的对象，长期呆在老年代中占据了空间，而且Full GC时要回收大量的短时间存活的对象，导致Full GC速度缓慢。

如果发现，在task执行期间，大量full gc发生了，那么说明，年轻代的Eden区域，给的空间不够大。此时可以执行一些操作来优化垃圾回收行为：
1、包括降低spark.storage.memoryFraction的比例，给年轻代更多的空间，来存放短时间存活的对象；
2、给Eden区域分配更大的空间，使用-Xmn即可，通常建议给Eden区域，预计大小的4/3；
3、如果使用的是HDFS文件，那么很好估计Eden区域大小，如果每个executor有4个task，然后每个hdfs压缩块解压缩后大小是3倍，此外每个hdfs块的大小是64M，那么Eden区域的预计大小就是：4 * 3 * 64MB，然后呢，再通过-Xmn参数，将Eden区域大小设置为4 * 3 * 64 * 4/3。





对于垃圾回收的调优，尽量就是说，调节executor内存的比例就可以了。因为jvm的调优是非常复杂和敏感的。除非是，真的到了万不得已的地方，然后呢，自己本身又对jvm相关的技术很了解，那么此时进行eden区域的调节，调优，是可以的。

一些高级的参数
-XX:SurvivorRatio=4：如果值为4，那么就是两个Survivor跟Eden的比例是2:4，也就是说每个Survivor占据的年轻代的比例是1/6，所以，你其实也可以尝试调大Survivor区域的大小。
-XX:NewRatio=4：调节新生代和老年代的比例



==================================
提高并行度
设置默认partiton
sparkConf.set("spark.default.parallelism","5")
官方推荐为集群总cpu数量的2到3倍
这样每个cpc core可以分配到2到3个task线程，不太可能出现空闲状态

=====================================
广播共享数据
当算子函数使用的外部数据过大时


=========================================
数据本地化
如果代码和数据是分开的需要移动代码
移动代码通常快于移动数据
数据本地化指数据离计算它的代码的距离

本地化级别：
1、PROCESS_LOCAL：数据和计算它的代码在同一个JVM进程中。
2、NODE_LOCAL：数据和计算它的代码在一个节点上，但是不在一个进程中，比如在不同的executor进程中，或者是数据在HDFS文件的block中。
3、NO_PREF：数据从哪里过来，性能都是一样的。
4、RACK_LOCAL：数据和计算它的代码在一个机架上。
5、ANY：数据可能在任意地方，比如其他网络环境内，或者其他机架上


Spark倾向于使用最好的本地化级别来调度task，但是这是不可能的。如果没有任何未处理的数据在空闲的executor上，那么Spark就会放低本地化级别。这时有两个选择：第一，等待，直到executor上的cpu释放出来，那么就分配task过去；第二，立即在任意一个executor上启动一个task。

Spark默认会等待一会儿，来期望task要处理的数据所在的节点上的executor空闲出一个cpu，从而将task分配过去。只要超过了时间，那么Spark就会将task分配到其他任意一个空闲的executor上。

可以设置参数，spark.locality系列参数，来调节Spark等待task可以进行数据本地化的时间。spark.locality.wait（3000毫秒）、spark.locality.wait.node、spark.locality.wait.process、spark.locality.wait.rack。

==============================================================



reduceByKey和groupByKey
reduceByKey性能高于groupByKey，尽量使用前者
reduceBykey会先在本地组合，大大减少传输到reduce端的数据量

shuffle性能优化
new SparkConf().set("spark.shuffle.consolidateFiles", "true")

spark.shuffle.consolidateFiles：是否开启shuffle block file的合并，默认为false
spark.reducer.maxSizeInFlight：reduce task的拉取缓存，默认48m
spark.shuffle.file.buffer：map task的写磁盘缓存，默认32k
spark.shuffle.io.maxRetries：拉取失败的最大重试次数，默认3次
spark.shuffle.io.retryWait：拉取失败的重试间隔，默认5s
spark.shuffle.memoryFraction：用于reduce端聚合的内存比例，默认0.2，超过比例就会溢出到磁盘上



=================================================

spark 1.5源码编译
下载源码进行解压
针对指定hadoop版本进行编译
./make-distribution.sh --tgz -Phadoop-2.6 -Pyarn -DskipTests -Dhadoop.version=2.6.0 -Phive
得到xxx-bin-tgz

====================================================


Spark SQL

DataFrame的使用
以列形式组织的分布式数据集合

SQLContext的子类HiveContext功能更强
SQLContext dialect只支持SQL
HiveContext默认为hiveql



RDD转为DataFrame
第一种，事先知道RDD的元数据类型，可以使用反射，代码较少
第二种，通过接口动态创建，代码较多，适用于只有程序运行时才知道RDD的元数据类型


使用反射
Java版本：将包含JavaBean的RDD转为DataFrame，不支持嵌套数据和集合等复杂类型
Scala版本：支持case class的RDD自动转换，支持嵌套数据和复杂类型




通用的load和save操作
手动指定数据源类型，可以在不同的数据源间转换
saveMode 决定若数据已存在是覆盖还是抛出异常，默认为异常

Parquet
列式存储格式
与行式存储优势
1.可以跳过不符合条件的数据，降低IO数据量
2.由于同一列数据类型相同，所以可以压缩编码节约空间
3.支持向量运算，有更好的扫描性能


Parquet自动分区推断
不同分区在不同目录
支持自动根据目录名推断出分区信息
分区列的类型也是自动被推断出来的

ParquetPartitionDiscovery例子
建分区目录
hadoop fs -mkdir /spark-study/users
hadoop fs -mkdir /spark-study/users/gender=male
hadoop fs -mkdir /spark-study/users/gender=male/country=US
hadoop fs -put users.parquet /spark-study/users/gender=male/country=US/users.parquet
分区列会被自动追加


Parquet合并元数据
合并元数据是耗时操作，所以1.5开始默认是关闭的
两种方式开启
1.读取Parquet时将mergeScheme设为true
2.context.setConf时将mergeScheme设为true


JSON数据源

Hive数据源，比Parquet和JSON数据源更常用
将hive-site.xml复制到spark/conf，将hive/lib下的mysql connector复制到spark/lib下

进入hive
hive
show tables;



spark

jdbc数据源
可以处理mysql等关系型数据库的数据

准备数据
mysql
show databases;
create database testdb;
use testdb;
create table studnet_infos(name varchar(20),age integer);
create table studnet_scores(name varchar(20),score integer);
insert into studnet_infos values('leo',18),('marry',17),('jack',19);
insert into studnet_scores values('leo',80),('marry',99),('jack',69);
create table good_student_infos(name varchar(20),age integer,score integer);


SQL内置函数
uv的基本含义和业务
每天都有很多用户来访问，但是每个用户可能每天都会访问很多次
所以，uv，指的是，对用户进行去重以后的访问总数


DataFrame函数不会立即返回执行结果，而是返回Column对象，用于在并行作业中求值。
内置函数
聚合函数 countDistinct,sumDistinct,count,avg,max,first,min...
集合函数 size,explode,sort_array...
日期时间函数 to_data,day,current_date...
数学函数
混合函数
字符串函数
窗口函数（对一个组执行函数） lead,rank,rowNumber

开窗函数可以实现分组topn



Spark SQL工作原理及性能优化
1.设置shuffle并行度, conf -> partitions
2.合理设置数据类型
3.明确给出列名而不是*
4.并行处理查询结果
5.缓存表，spark内部是使用内存列格式进行保存
6.广播较小的join表避免网络传输
7.钨丝计划，tungsten.enabled，自动管理内存


Hive on Spark
Hive是大数据领域事实上的SQL标准，底层使用MapReduce
Spark SQL提供了对Hive的查询功能
而Hive on Spark则是Hive的一个项目，底层使用Spark作为查询引擎





Spark Streaming
大数据实时计算，基于Spark Core
实时数据都是发送到消息中间件(kafka)中
这里实际是作为缓冲，直接处理大量数据系统会撑不住

DStream 离散流，代表持续不断的数据流
其内部持续不断生成RDD

Streaming vs Storm
Streaming位于Spark技术栈，可以使用Spark的各种功能
Storm
实时计算模型：实时，来一条处理一条
实时计算延迟度：毫秒级
吞吐量：低
事务机制：完善
健壮性/容错性：ZooKeeper,Acker，非常强
动态调整并行度：支持
Stream
准实时，收集一段时间内再处理
秒级
高
支持，但不完善
Checkpoint,WAL,一般
不支持


金融系统等要求纯实时，可靠的事务机制
或小公司要求根据高峰期动态调整并行度可以用Storm





streaming

安装nc工具
yum install nc


StreamingContext只能start()一次，启动后不能添加任何新的算子操作
一个JVM中只能有一个StreamingContext
stop()会同时停止内部的SparkContext，stop(false)不会




DStream
使用本地模式时必须至少两个线程，一个用于Receiver接收数据，一个用于处理数据
运行在集群上，必须保证cpu>1,Spark Streaming的每个executor分配的core>1


DStream
基础数据源：socket和HDFS文件
基于HDFS文件即监控一个HDFS的目录，处理实时的文件流。这种方式是没有receiver的，不会占用一个cpu。
注意：文件移入目录的时候会被处理，之后即使文件内容改变也不会被处理。目录中的文件格式必须相同。



Kafka数据源
基于Receiver方式
默认配置会丢数据，如果需要高可用需要启用预写日志机制（WAL），该机制会同步地将数据写入到分布式文件系统上

./kafka-topics.sh --zookeeper 192.168.1.67:32771 --topic WordCount --replication-factor 1 --partitions 1 --create

./kafka-console-producer.sh --broker-list 192.168.1.67:32772 --topic WordCount



Spark

transformation规范
map
flatMap
join  对两个DStream进行join操作，每个连接起来的pair，作为新DStream的RDD的一个元素
transform 对数据进行转换操作
updateStateByKey 为每个key维护一份state，并进行更新
window 对滑动窗口数据执行操作

updateStateByKey
要求必须开启Checkpoint机制

transform
transform操作，应用在DStream上时，可以用于执行任意的RDD到RDD的转换操作。它可以用于实现，DStream API中所没有提供的操作。


window滑动窗口操作
比如说一秒一个batch，则dstream一秒生成一个RDD
可以再设置每3s执行一个window，则每个window包含3个RDD，执行完生成一个RDD
window
countByWindow
reduceByWindow
reduceByKeyAndWindow
countByValueAndWindow


Spark
output操作
print()
saveAsTextFile()
saveAsObjectFile()
saveAsHadoopFile9)
foreachRDD()
如果没有执行output操作，不会执行任何计算逻辑

foreachRDD与SQL Connect
1.在for-each外创建connection，由于connection实际不支持序列化，所以无法传给每个task
2.在for-each中创建，相当于为每个RDD的每一条数据创建，消耗性能
3.在for-each的foreachPartition中创建Connection
4.在for-each的foreachPartition中中使用连接池


缓存与持久化机制
对于基于窗口的操作，比如reduceByWindow、reduceByKeyAndWindow，以及基于状态的操作，比如updateStateByKey，默认就隐式开启了持久化机制。即Spark Streaming默认就会将上述操作产生的Dstream中的数据，缓存到内存中，不需要开发人员手动调用persist()方法。

对于通过网络接收数据的输入流，比如socket、Kafka、Flume等，默认的持久化级别，是将数据复制一份，以便于容错。相当于是，用的是类似MEMORY_ONLY_SER_2


Checkpoint机制
使用比较少
有两种数据需要被进行checkpoint
元数据checkpoint
RDD checkpoint
元数据checkpoint主要是为了从driver失败中进行恢复；而RDD checkpoint主要是为了，使用到有状态的transformation操作时，能够在其生产出的数据丢失时，进行快速的失败恢复。

何时启用
使用了有状态的transformation操作——比如updateStateByKey
要保证可以从Driver失败中进行恢复





部署、升级和监控应用程序
部署
启用预写日志机制 WAL
在启用了预写日志机制之后，推荐将复制持久化机制禁用掉，因为所有数据已经保存在容错的文件系统中了，不需要在用复制机制进行持久化，保存一份副本了。只要将输入DStream的持久化机制设置一下即可，persist(StorageLevel.MEMORY_AND_DISK_SER)。（之前讲过，默认是基于复制的持久化策略，_2后缀）

如果集群资源有限,设置Receiver接收速度
如果使用Kafka Direct，Spark可以自动估计Receiver最合理的接收速度，并根据情况动态调整。只要将spark.streaming.backpressure.enabled设置为true即可


升级
使用kafka等支持缓存的数据源时，可以使用context.stop()停止spark后
再部署程序，新程序会从没有消费过的地方继续消费

Spark UI中，以下两个统计指标格外重要：
1、处理时间——每个batch的数据的处理耗时
2、调度延迟——一个batch在队列中阻塞住，等待上一个batch完成处理的时间

Spark

容错机制以及事务语义详解

架构原理深度剖析
StreamingContext初始化与Receiver启动原理剖析与源码分析

Spark

数据接收原理剖析与源码分析

数据处理原理剖析与源码分析（block与batch关系透彻解析）




Spark

性能调优

数据接收并行度调优
如果数据接收称为系统的瓶颈
每一个输入DStream都会在某个Worker的Executor上启动一个Receiver，该Receiver接收一个数据流。因此可以通过创建多个输入DStream，并且配置它们接收数据源不同的分区数据，达到接收多个数据流的效果。比如说，一个接收两个Kafka Topic的输入DStream，可以被拆分为两个输入DStream，每个分别接收一个topic的数据。这样就会创建两个Receiver，从而并行地接收数据，进而提升吞吐量。多个DStream可以使用union算子进行聚合，从而形成一个DStream。然后后续的transformation算子操作都针对该一个聚合后的DStream即可。

int numStreams = 5;
List<JavaPairDStream<String, String>> kafkaStreams = new ArrayList<JavaPairDStream<String, String>>(numStreams);
for (int i = 0; i < numStreams; i++) {
  kafkaStreams.add(KafkaUtils.createStream(...));
}
JavaPairDStream<String, String> unifiedStream = streamingContext.union(kafkaStreams.get(0), kafkaStreams.subList(1, kafkaStreams.size()));
unifiedStream.print();


数据接收并行度调优，除了创建更多输入DStream和Receiver以外，还可以考虑调节block interval。通过参数，spark.streaming.blockInterval，可以设置block interval，默认是200ms。对于大多数Receiver来说，在将接收到的数据保存到Spark的BlockManager之前，都会将数据切分为一个一个的block。而每个batch中的block数量，则决定了该batch对应的RDD的partition的数量，以及针对该RDD执行transformation操作时，创建的task的数量。每个batch对应的task数量是大约估计的，即batch interval / block interval。

例如说，batch interval为2s，block interval为200ms，会创建10个task。如果你认为每个batch的task数量太少，即低于每台机器的cpu core数量，那么就说明batch的task数量是不够的，因为所有的cpu资源无法完全被利用起来。要为batch增加block的数量，那么就减小block interval。然而，推荐的block interval最小值是50ms，如果低于这个数值，那么大量task的启动时间，可能会变成一个性能开销点


除了上述说的两个提升数据接收并行度的方式，还有一种方法，就是显式地对输入数据流进行重分区。使用inputStream.repartition(<number of partitions>)即可。这样就可以将接收到的batch，分布到指定数量的机器上，然后再进行进一步的操作。



任务启动调优


如果每秒钟启动的task过于多，比如每秒钟启动50个，那么发送这些task去Worker节点上的Executor的性能开销，会比较大，而且此时基本就很难达到毫秒级的延迟了。使用下述操作可以减少这方面的性能开销：

1、Task序列化：使用Kryo序列化机制来序列化task，可以减小task的大小，从而减少发送这些task到各个Worker节点上的Executor的时间。
2、执行模式：在Standalone模式下运行Spark，可以达到更少的task启动时间



数据处理并行度调优

如果在计算的任何stage中使用的并行task的数量没有足够多，那么集群资源是无法被充分利用的。举例来说，对于分布式的reduce操作，比如reduceByKey和reduceByKeyAndWindow，默认的并行task的数量是由spark.default.parallelism参数决定的。你可以在reduceByKey等操作中，传入第二个参数，手动指定该操作的并行度，也可以调节全局的spark.default.parallelism参数。



数据序列化调优

数据序列化造成的系统开销可以由序列化格式的优化来减小。在流式计算的场景下，有两种类型的数据需要序列化。

1、输入数据：默认情况下，接收到的输入数据，是存储在Executor的内存中的，使用的持久化级别是StorageLevel.MEMORY_AND_DISK_SER_2。这意味着，数据被序列化为字节从而减小GC开销，并且会复制以进行executor失败的容错。因此，数据首先会存储在内存中，然后在内存不足时会溢写到磁盘上，从而为流式计算来保存所有需要的数据。这里的序列化有明显的性能开销——Receiver必须反序列化从网络接收到的数据，然后再使用Spark的序列化格式序列化数据。

2、流式计算操作生成的持久化RDD：流式计算操作生成的持久化RDD，可能会持久化到内存中。例如，窗口操作默认就会将数据持久化在内存中，因为这些数据后面可能会在多个窗口中被使用，并被处理多次。然而，不像Spark Core的默认持久化级别，StorageLevel.MEMORY_ONLY，流式计算操作生成的RDD的默认持久化级别是StorageLevel.MEMORY_ONLY_SER ，默认就会减小GC开销。


在上述的场景中，使用Kryo序列化类库可以减小CPU和内存的性能开销


batch interval调优（最重要）


如果想让一个运行在集群上的Spark Streaming应用程序可以稳定，它就必须尽可能快地处理接收到的数据。换句话说，batch应该在生成之后，就尽可能快地处理掉。

通过观察Spark UI上的batch处理时间来定。batch处理时间必须小于batch interval时间。

为你的应用计算正确的batch大小的比较好的方法，是在一个很保守的batch interval，比如5~10s，以很慢的数据接收速率进行测试。要检查应用是否跟得上这个数据速率，可以检查每个batch的处理时间的延迟，如果处理时间与batch interval基本吻合，那么应用就是稳定的。否则，如果batch调度的延迟持续增长，那么就意味应用无法跟得上这个速率，也就是不稳定的。因此你要想有一个稳定的配置，可以尝试提升数据处理的速度，或者增加batch interval。记住，由于临时性的数据增长导致的暂时的延迟增长，可以合理的，只要延迟情况可以在短时间内恢复即可。



内存调优
1、DStream的持久化：正如在“数据序列化调优”一节中提到的，输入数据和某些操作生产的中间RDD，默认持久化时都会序列化为字节。与非序列化的方式相比，这会降低内存和GC开销。使用Kryo序列化机制可以进一步减少内存使用和GC开销。进一步降低内存使用率，可以对数据进行压缩，由spark.rdd.compress参数控制（默认false）。

2、清理旧数据：默认情况下，所有输入数据和通过DStream transformation操作生成的持久化RDD，会自动被清理。Spark Streaming会决定何时清理这些数据，取决于transformation操作类型。例如，你在使用窗口长度为10分钟内的window操作，Spark会保持10分钟以内的数据，时间过了以后就会清理旧数据。但是在某些特殊场景下，比如Spark SQL和Spark Streaming整合使用时，在异步开启的线程中，使用Spark SQL针对batch RDD进行执行查询。那么就需要让Spark保存更长时间的数据，直到Spark SQL查询结束。可以使用streamingContext.remember()方法来实现。

3、CMS垃圾回收器：使用并行的mark-sweep垃圾回收机制，被推荐使用，用来保持GC低开销。虽然并行的GC会降低吞吐量，但是还是建议使用它，来减少batch的处理时间（降低处理过程中的gc开销）。如果要使用，那么要在driver端和executor端都开启。在spark-submit中使用--driver-java-options设置；使用spark.executor.extraJavaOptions参数设置。-XX:+UseConcMarkSweepGC。
