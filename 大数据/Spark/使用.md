[TOC]

## 使用




第一个Spark应用

### 运行在本地

### 运行在Spark集群
- 将spark.txt文件上传到hdfs上去
	hadoop fs -put /opt/tmp/spark.txt /spark.txt
	提交后的文件可以通过 http://spark1:50070/  Utilities - Browse the file system 查看
- 使用maven插件，对spark工程进行打包 mvn clean package
- 将打包后的spark工程jar包，上传到机器
- 编写spark-submit脚本
```bash
/usr/local/spark/bin/spark-submit \
--class com.bookislfe.spark_sample.core.WordCountCluster \
--num-executors 3 \
--driver-memory 100m \
--executor-memory 100m \
--executor-cores 3 \
/opt/tmp/spark-study/java/spark-sample-0.0.1-SNAPSHOT-jar-with-dependencies.jar
```
- 执行spark-submit脚本，提交spark应用到集群执行


???scala 只需
spark-assembly-1.3.0-hadoop



spark-shell写程序
spark-shell

val lines=sc.textFile("hdfs://spark1:9000/spark.txt")


spark1:4040


spark-submit
不指定master的话默认使用的也是本地程序
使用集群需要添加
--master spark://192.168.1.107:7077
因为集群太耗内存，所以以后都使用local测试
