[TOC]

## Hadoop 集群搭建

### 集群搭建步骤

基于 Centos 7 集群，搭建在三台机器上

#### 配置 Hadoop 环境

[Hadoop 下载地址](http://hadoop.apache.org/releases.html)

1. 解压 Hadoop 压缩包

2. 配置环境变量

    ```bash
    vi ~/.bashrc
    ```

    添加环境变量

    ```
    export HADOOP_HOME=/usr/local/hadoop
    export PATH=$PATH:$HADOOP_HOME/bin:$HADOOP_HOME/sbin
    ```

    应用环境变量

    ```bash
    source ~/.bashrc
    ```

3. 修改 Hadoop 配置，三台服务器都采用一样的配置

	**修改 `core-site.xml`**

    ```bash
    vi /usr/local/hadoop/etc/hadoop/core-site.xml
    ```

    修改内容

    ```
    <property>
      <name>fs.default.name</name>
      <value>hdfs://spark1:9000</value>
    </property>
    ```

    **修改 `hdfs-site.xml`**

    ```bash
    vi /usr/local/hadoop/etc/hadoop/hdfs-site.xml
    ```

    修改内容

    ```
    <property>
      <name>dfs.name.dir</name>
      <value>/usr/local/data/namenode</value>
    </property>
    <property>
      <name>dfs.data.dir</name>
      <value>/usr/local/data/datanode</value>
    </property>
    <property>
      <name>dfs.tmp.dir</name>
      <value>/usr/local/data/tmp</value>
    </property>
    <property>
      <name>dfs.replication</name>
      <value>3</value>
    </property>
    ```

    **修改 `mapred-site.xml`**

    ```bash
    cp /usr/local/hadoop/etc/hadoop/mapred-site.xml.template /usr/local/hadoop/etc/hadoop/mapred-site.xml
    vi /usr/local/hadoop/etc/hadoop/mapred-site.xml
    ```

    修改内容

    ```
    <property>
      <name>mapreduce.framework.name</name>
      <value>yarn</value>
    </property>
    ```

	**修改 `yarn-site.xml`**

    ```bash
    vi /usr/local/hadoop/etc/hadoop/yarn-site.xml
    ```

    修改内容

    ```
    <property>
      <name>yarn.resourcemanager.hostname</name>
      <value>spark1</value>
    </property>
    <property>
      <name>yarn.nodemanager.aux-services</name>
      <value>mapreduce_shuffle</value>
    </property>
    ```

    **修改 `slaves`**

    ```bash
    vi /usr/local/hadoop/etc/hadoop/slaves
    ```

    添加内容

    ```
    spark1
    spark2
    spark3
    ```

4. 由于其它两台也是同样配置，所以直接将 Hadoop 工作目录复制到另两台服务器上

    ```bash
    scp -r /usr/local/hadoop root@spark2:/usr/local/
    ```

5. 在三台机器上各自创建工作目录

    ```bash
    mkdir -p /usr/local/data/tmp
    mkdir -p /usr/local/data/datanode
    mkdir -p /usr/local/data/namenode
    ```

#### 启动集群

##### 启动 HDFS 集群

1. 格式化 NameNode

    ```bash
    hdfs namenode -format
    ```

2. 启动集群

    ```bash
    start-dfs.sh
    ```

3. 检查集群状态

    在三台机器上各自执行 `jps` 命令，检查下列进程是否运行
    - spark1：NameNode、DataNode、SecondaryNameNode
    - spark2：DataNode
    - spark3：DataNode

全部确认后使用浏览器尝试访问： http://spark1:50070/

##### 启动 YARN 集群

```bash
start-yarn.sh
```

在三台机器上各自执行 `jps` 命令，检查下列进程是否运行
- spark1：ResourceManager、NodeManager
- spark2：NodeManager
- spark3：NodeManager

全部确认后使用浏览器尝试访问：http://spark1:8088/



