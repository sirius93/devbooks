[TOC]

## Kafka 集群搭建

### 集群搭建步骤

基于 Centos 7 集群，搭建在三台机器上


#### 配置 Kafka 环境

[Kafka 下载地址](http://kafka.apache.org/downloads.html)

1. 解压 Kafka 压缩包

2. 修改 Kafka 配置

    **修改 `server.properties`**

    ```bash
    vi /usr/local/kafka/config/server.properties
    ```

    修改内容

    ```
    broker.id=0
    zookeeper.connect=spark1:2181,spark2:2181,spark3:2181
    ```

3. 下载并解压 [slf4j](http://www.slf4j.org/download.html)，将解压后的目录中的 `slf4j-nop` 复制到 Kafka 的 `libs` 目录下

    ```bash
    cp /usr/local/slf4j-1.7.13/slf4j-nop-1.7.13.jar /usr/local/kafka/libs
    ```

4. 复制 Kafka 目录到其它节点，唯一要改变的是 `broker.id`

    ```bash
    scp -r /usr/local/kafka root@spark2:/usr/local/
    ```

5. 进入 Kafka 的安装目录，在三台机器上分别启动 Kafka 服务，启动完成后可通过 `jps` 查看进程

    ```bash
    nohup bin/kafka-server-start.sh config/server.properties &
    ```

6. 验证安装

	- 在三台机器上分别执行 `jps`，查看是否有 `kafka` 进程。

    - 尝试生成并消费消息

    创建 Topic

    ```bash
    bin/kafka-topics.sh --zookeeper spark1:2181,spark2:2181,spark3:2181 --topic TestTopic --replication-factor 1 --partitions 1 --create
    ```

    创建生产者

    ```bash
    bin/kafka-console-producer.sh --broker-list spark1:9092,spark2:9092,spark3:9092 --topic TestTopic
    ```

    创建消费者

    ```bash
    bin/kafka-console-consumer.sh --zookeeper spark1:2181,spark2:2181,spark3:2181 --topic TestTopic --from-beginning
    ```

