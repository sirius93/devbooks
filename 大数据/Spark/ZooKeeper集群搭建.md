[TOC]

## ZooKeeper 集群搭建

### 集群搭建步骤

基于 Centos 7 集群，搭建在三台机器上

#### 配置 ZooKeeper 环境

[ZooKeeper 下载地址](http://zookeeper.apache.org/releases.html)

1. 解压 ZooKeeper 压缩包

2. 配置环境变量

    ```bash
    vi ~/.bashrc
    ```

    添加环境变量

    ```
    export ZOOKEEPER_HOME=/usr/local/zk
    export PATH=$PATH:$ZOOKEEPER_HOME/bin
    ```

    应用环境变量

    ```bash
    source ~/.bashrc
    ```

3. 修改 ZooKeeper 配置

    **修改 `zoo.cfg`**

    ```bash
    mv /usr/local/zk/conf/zoo_sample.cfg /usr/local/zk/conf/zoo.cfg
    vi /usr/local/zk/conf/zoo.cfg
    ```

    修改内容

    ```
    dataDir=/usr/local/zk/data
    server.0=spark1:2888:3888
    server.1=spark2:2888:3888
    server.2=spark3:2888:3888
    ```

4. 创建数据目录

    ```bash
    mkdir -p /usr/local/zk/data
    ```

5. 配置节点 ID

    ```bash
    echo 0 > /usr/local/zk/data/myid
    ```

6. 复制 Zookeeper 目录到另两个节点，并创建数据和修改环境变量和节点 ID(1 和 2)

    ```bash
    scp -r /usr/local/zk root@spark2:/usr/local/
    ```

7. 在三台机器上分别启动 Zookeeper

    ```bash
    zkServer.sh start
    ```

8. 验证安装

    ```bash
    zkServer.sh status
    ```
