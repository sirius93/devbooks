[TOC]

# Hadoop

## 单机部署

### 安装

1. [下载](http://hadoop.apache.org/releases.html)压缩包后解压
2. 修改 Java 环境， `etc/hadoop-env.sh`
  ```
  export JAVA_HOME=xxx
  ```
3. 修改 HDFS 配置，`etc/core-site.xml`
  ```xml
  <configuration>
      <property>
          <name>fs.defaultFS</name>
          <value>hdfs://192.168.1.67:9000</value>
      </property>
  	    <property>
          <name>hadoop.tmp.dir</name>
          <value>/var/local/software/hadoop-2.6.4/data/tmp</value>
      </property>
  </configuration>
  ```
4. 继续修改 `hdfs-site.xml`
  ```xml
  <configuration>
      <property>
          <name>dfs.replication</name>
          <value>1</value>
      </property>
  </configuration>
  ```
5. 修改 Yarn 配置，`mapred-site.xml`
  ```xml
  <configuration>
      <property>
          <name>mapreduce.framework.name</name>
          <value>yarn</value>
      </property>
  </configuration>
  ```
6. 继续修改 `yarn-site.xml`
  ```xml
  <configuration>
      <property>
          <name>yarn.nodemanager.aux-services</name>
          <value>mapreduce_shuffle</value>
      </property>
  </configuration>
  ```

### 运行

1. 格式化文件系统，执行
  ```bash
  bin/hdfs namenode -format
  ```
2. 启动 NameNode 和 DataNode
  ```bash
  sbin/start-dfs.sh
  ```
3. 访问 Hadoop Web 接口，http://localhost:50070/
4. 启动 yarn
  ```bash
  sbin/start-yarn.sh
  ```
5. 访问 Yarn Web 接口，http://localhost:8088/

运行 workcount 测试以验证安装

```bash
bin/hadoop jar share/hadoop/mapreduce/hadoop-mapreduce-examples-2.6.4.jar
```

hdfs 上创建文件
bin/hadoop fs -mkdir -p /user/root



## 参考资料

- [Hadoop 单机部署](http://hadoop.apache.org/docs/r2.6.4/hadoop-project-dist/hadoop-common/SingleCluster.html)
