
配置IP
		vim /etc/sysconfig/network-scripts/ifcfg-eth0
		添加配置：
			IPADDR=“192.168.1.44”       #设置ip			NETMASK=“255.255.255.0”      #设置掩码			GATEWAY=“192.168.1.1”        #设置网关
配置主机名、主机名与IP映射关系
配置DNS
		 echo nameserver 8.8.8.8 > /etc/resolv.conf
配置时区：cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime


防火墙配置
关闭防火墙
		 service iptables status		#查看防火墙状态		service iptables stop		#关闭防火墙		chkconfig iptables --list	#查看防火墙开机启动状态		chkconfig iptables off		#关闭防火墙开机启动
关闭Selinux
		vim /etc/sysconfig/selinux 	#编辑selinux文件
		SELINUX=disabled				#关闭selinux



SSH无密钥配置
生成密钥
		ssh-keygen -t rsa（四个回车）
将公钥拷贝到要免登陆的机器上
		ssh-copy-id hbase01.jkxy.com
		ssh-copy-id hbase02.jkxy.com
		ssh-copy-id hbase03.jkxy.com
