[TOC]

# HBase 集群调优

## 服务器调优

- 内存越大越好
- 交换区设为 0
	关闭操作系统的 swap 或是设置 swappiness 为 0，推荐设置为 0，这样只有在物理内存不够的情况下才会使用交换分区
- GC 回收采用并行增量式
		 -XX:CMSInitiatingOccupancyFraction=70 
- 开启特性 MLAB
		解决 HBase 产生的内存碎片 
- 设置 java 垃圾回收时的 heap，修改 hbase-env.sh
		 HBASE_HEAPSIZE=8000


## 客户端优化

- AutoFlush
		将HTable的setAutoFlush设为false，可以支持客户端批量更新。即当Put填满客户端flush缓存时，才发送到服务端，默认是true。
- Scan Caching
		 scanner一次缓存多少数据来scan（从服务端一次抓多少数据回来scan）
- Scan Attribute Selection
		 scan时建议指定需要的Column Family，减少通信量，否则scan操作默认会返回整个row的所有数据（所有Coulmn Family）


## 配置优化

1. 修改 RegionServer与Zookeeper间的连接超时时间
		 zookeeper.session.timeout=180000
2. 修改 RegionServer的请求处理IO线程数
		<property>
			<name>hbase.regionserver.handler.count</name> 									<value>30</value>
		</property>
3. 调整区域合并大小
		 <property>
			 <name>hbase.hregion.max.filesize</name> 											<value>10734182400</value>
		</property>
hbase.regionserver.global.memstore.upperLimit
hbase.regionserver.global.memstore.lowerLimit
hfile.block.cache.size


hbase.hstore.blockingStoreFiles
hbase.hregion.memstore.block.multiplier
hbase.hregion.memstore.mslab.enabled

4. 压缩调优
		LZO
		SNAPPY
