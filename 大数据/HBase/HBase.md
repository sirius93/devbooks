[TOC]

# HBase

HBase 是一个构建在 HDFS 之上的、分布式的、面向列的开源数据库，是 Google BigTable 的开源实现，它主要用于存储海量数据，是 Hadoop 生态系统中的重要一员。

CAP（一致性，可用性，容错性）
HBase 属于 CP。

## 特点

- 大：一个表可以有数十亿行，上百万列
- 面向列：面向列（族）的存储和权限访问，列（族）独立索引。面向列只会扫描需要的列，而面向行需要扫描整条数据
- 稀疏：对于为空(null)的列，并不占用存储空间，因此，表可以设计的非常稀疏。
- 数据类型单一：HBase 中的数据类型都是字符串（string），在 HBase 内部保存为字节数组。
- 无模式：每行都有一个可排序的主键和任意多的列，列可以根据需要动态增加，同一张表中不同的行可以有截然不同的列
- 自带版本管理功能
- 关注大问题，不善于应付小问题。集群可伸但不能缩小，最小节点为 5 个。
- 除行健外不提供任何索引。所以如果不使用行健就需要自己实现索引或全表扫描。
- 数据都是字节数组，类型没有任何区别。



## 术语

### 宽表与窄表

窄表指列少行多，即每一行尽可能保持唯一
宽表指列多行少

### 主键（Row Key）

主键用于检索记录。为任意字符串，最大长度为 64kb，按字典顺序存储。

### 列族（Column Family）

列族在创建表的时候声明，一个列族可以包含多个列，列中的数据都是以二进制形式存在，没有数据类型。
列族是一些列的集合。
一个列族所有列成员是有着相同的前缀。冒号(:)是列族的分隔符，用来区分前缀和列名。比如，列 courses:history 和 courses:math 都是 列族 courses 的成员。

注意：HBase 对列族和列名都是大小写敏感的

### 时间戳与存储单元（Timestamp and Cell）

HBase 中通过 row 和 columns 确定的一个存贮单元称为 cell。每个 cell 都保存着同一份数据的多个版本，不同版本的数据按照时间的倒序排序。
在写入数据时，时间戳可以由 HBase 自动赋值（当前系统时间精确到毫秒），也可以显式赋值。
{row, column, version} 元组就是一个 HBase 中的一个 cell。

## 数据模型

HBase 存储细节
每个列族存储在 HDFS 上的一个单独文件夹中。
Key 和 Version number 会在每个列族中存储一份。
空值不会保存。

## 基础架构

客户端

- 与 HMaster 通信进行管理类的操作
- 与 HRegionserver 通信进行读写类操作

Zookeeper

- 保证任何时候，集群中只有一个 Master(HA)
- 存贮所有 Region 的寻址入口
- 实时监控 Region server 的上线和下线信息，并实时通知给 Master
- 存储 HBase 的 schema 和 table 元数据

Master 节点

- 管理 HRegionServer 的负载均衡，调整 Region 分布
- 在 Region Split 后，负责新 Region 的分配
- 在 HRegionServer 停机后，负责失效 HRegionServer 上的 Region 迁移
- HMaster 失效仅会导致所有元数据无法被修改，表的数据读写还是可以正常进行


## HDFS 目录

查看目录

```bash
bin/hadoop fs -ls /hbase
```

### WAL

路径：/hbase/WALs

WAL （Write Ahead Log ）是预写日志文件，是 RegionServer 在处理数据插入和删除的过程中用来记录操作内容的一种日志，在 0.94.x 叫做.logs。

HBase 的数据首先会写入到 Write Ahead Log (WAL) 日志中，然后再写入到 region server 的 memstore 中。
在达到一个阀值之后才会写入到磁盘中
	— 阀值的大小参数： hbase.hregion.memstore.flush.size 默认是128M

路径：	/hbase/oldWALs

对应的 0.94.x 版本中.oldlogs 目录。
当 `/hbase/WALs` 目录中的 logs 没有用之后，会将这些 logs 移动到此目录下，HMaster 会定期的进行清理。


### Region

Region 是一组行，由其起始行健（包含）和终止行健（排除）来确定。Region 不会重叠，每个 Region 都会由集群中的一个特定区域的服务器来处理。

Region 分裂
Region 数量大到一定程度后会分裂成两个 Region

Region 存储
一个表有多个 Region
每个 Region 有 memStor 和多个 StoreFile
当 memStor 达到 128m 时会写入到 StoreFile中

### tmp

路径：/hbase/.tmp

临时目录，当对表做创建和删除的时候，会将表 move 到该目录下，然后进行操作

### data

路径：/hbase/data

核心目录，存储 HBase 表的数据

默认情况下，该目录下有两个子目录：
- `/hbase/data/default` 在用户创建表的时候，如果没有指定 namespace 时，表就创建在此目录下
- `/hbase/data/hbase` 系统内部创建的表

### hbase.id

路径：/hbase/hbase.id

存储的是集群的唯一的cluster id（uuid）

### hbase.version

路径：/hbase/hbase.version

集群版本号















--------------------------------------------------------------


=============================




HBase数据的导入

HBase导入方式
1.使用HBase的Put  API
Put导入是最简单的方式，但是不适合快速的导入大量数据

2.使用HBase的bulk load工具
大量数据导入最为有效的方式
使用bulk load最简单的方式是使用importtsv
importtsv是一个内置的工具
importtsv以MapReduce任务的方式，可以将TSV文件中的文本数据直接的导入到HBase的表中，或者导入到HBase已经格式化的文件中

3.自定义MapReduce任务
如果要从其他格式或者数据是动态产生，使用MapReduce是最有效的数据导入方式
虽然使用MapReduce是比较灵活的方式，但是因为MapReduce作业的负载，在数据量大的情况下，如果MapReduce作业设计不当，也可能会造成性能的低下

4.使用开源工具sqoop
将Hadoop和关系型数据库中的数据相互转移的工具
	— 可以将一个关系型数据库（例如 ： MySQL ,Oracle ,Postgres等）中的数据导进到Hadoop的HDFS中，也可以将HDFS的数据导进到关系型数据库中。
	— 通过hadoop的mapreduce把数据从关系型数据库中导入数据到HDFS





HBase Rest/Thrift
HBase Rest
启动rest 服务
	（1）hbase rest start			用默认的方式启动rest服务，端口是8080
	（2）hbase rest start 8585
			以端口8585方式启动
	（3）以daemon方式启动
			hbase-daemon.sh start rest -p 8585
停止rest服务
		bin/hbase-daemon.sh stop rest

Thrift


======

Hbase


HBase的备份与恢复

冷备
	— 需要停掉HBase
	— distcp
热备
	— 不用停掉HBase，部分可实现增量备份
	可能会丢数据
	— 方式一：集群复制
	— 方式二：CopyTable
	— 方式三：Export


HBase冷备

distcp简介
	分布式拷贝数据
	— distcp命令位于hadoop tools包中，主要用于在一个或者两个HDFS集群之间快速拷贝数据。
	— 适用于在停止HBase的情况下冷备份，不适用于热备份
查看帮助
	— hadoop@master:/hadoop/hadoop-2.6.0/bin$ ./hadoop distcp
进行备份，实际上因为HBase建立在HDFS上，所以是备份到HDFS上的另一个目录
	— ./hadoop distcp /hbase /hbasebackup
恢复数据库
	— ./hdfs dfs -mv /hbase /hbase_tmp
	— ./hadoop distcp -overwrite /hbasebackup /hbase


HBase热备
export与import工具
export工具简介
	— 将指定的表导出到HDFS或本地
	— 数据被写到export的输出目录，一个region一个文件
	— 默认为sequencefile
	— 可以选择对输出进行压缩
	— 查看帮助
	— ./hbase org.apache.hadoop.hbase.mapreduce.Export

import工具简介
	— 可以将export导出的数据导入到HBase数据库中
	— 查看帮助
	—./hbase org.apache.hadoop.hbase.mapreduce.Import

CopyTable

CopyTable简介
	— copytable可以将所有的表数据或者部分数据拷贝到同集群或者不同集群的另外一张表中
	— 可以通过指定导入的时间戳实现增量导入
	— 查看帮助
	— hbase org.apache.hadoop.hbase.mapreduce.CopyTable --help

将test表中的数据复制到test3
	— create 'test3', 'cf'
	— ./hbase org.apache.hadoop.hbase.mapreduce.CopyTable --new.name=test3 test



配置 HBase 的监控工具 ganglia

http://ganglia.info/
http://blog.chinaunix.net/uid-11121450-id-3147002.html

ganglia组件
gmond 守护进程，运作在每一个需要检测的节点上，发送和接受同一个组播或单播通道上的统计信息
gmetad 守护进程，定期检查gmond，从那里收集数据存储在RRD存储引擎上，一般一个集群只有一个
ganglia-web 一般安装在gmetad主机上，读取RRD文件

安装
主节点上
yum -y install ganglia-monitor rrdtool gmetad ganglia-webfrontend
其它节点
ganglia-monitor

配置gmond
所有节点上修改 /etc/ganglia/gmond.conf
cluster{
 name="" #集群名
}
udp_send_channel{
 #mcast_join  #注释掉组播
 host=10.10.1.1 #指向gmetad的机器
 port= #监听端口
}
udp_recvchannel{
 #mcast_join  #注释掉组播
 #bind #注释掉
}

配置gmetad.conf
data_source "集群名"

配置hadoop
在所有hadoop上修改hadoop-metrics2.properties

配置其它

启动
每台机器
service ganglia-monitor restart
主节点
service gemtad restart
/etc/init.d/apache2 restart




Hbase
集群复制
集群复制
	— HBase提供了跨集群之间的数据同步复制
	— 通过write-ahead log(WAL)实现
	— 可以复制主集群中的某些表，或者某些表的某些列族
	— 主从集群之间采用的是异步同步的方式
集群复制提供的功能包括：
	— 备份与灾难恢复
	— 数据集成
	— 跨地域的数据分布
	— 读写分离


集群复制采用主集群推送的方式，主集群将数据的修改信息通过WAL日志传送到从集群


集群复制的配置
准备两套HBase的集群，版本都在0.90以上，最好一致
	— ./hbase version
主从集群的机器之间网络是互通的
	— ping
在主集群的的所有hbase-site.xml中配置集群复制
<property>  
  <name>hbase.replication</name>  
  <value>true</value>  
</property>

在主集群中添加复制点，配置主从集群的复制关系
	— ./hbase shell
	— add_peer <ID> <CLUSTER_KEY>
	— ID：唯一的字符串，尽量不要包含特殊字符
	— CLUSTER_KEY: 集群键，格式如下：								hbase.zookeeper.quorum:hbase.zookeeper.property.clientPort:zookeeper.znode.parent（参数可以参考后面的解释）
示例：add_peer '1',"master,slave1,slave2:2181:/hbase"

hbase.zookeeper.quorum
	— Zookeeper 集群的地址列表
	— 默认是 localhost,是给伪分布式用的
	— 可以在hbase-site.xml中找到
	— 如果在hbase-env.sh设置了HBASE_MANAGES_ZK，这些ZooKeeper节点就会和Hbase一起启动。

hbase.zookeeper.property.clientPort
	— zookeeper客户端连接的端口
	— 在ZooKeeper的zoo.conf中的配置
	— 默认: 2181

zookeeper.znode.parent
	— ZooKeeper中的Hbase的根ZNode。
	— 默认: /hbase
如何查看：
	— zkCli.sh -server host:port  2181
	— ls /

测试集群复制
在主集群节点上
	— ./hbase shell
	— create 'rep7',{NAME=>'cf1',REPLICATION_SCOPE=>1}
在从集群节点上
	— create 'rep7',{NAME=>'cf1’}
在主集群节点上
	— put 'rep7', 'row1', 'cf1:v1', 'foo‘
	— scan ‘rep7’
在从集群节点上
	— scan ‘rep7’


集群复制的管理
add_peer
	— 配置集群复制关系
list_peers
	— 查看当前集群的所有复制关系
disable_peer <ID>
	— 暂停集群复制，但是会继续跟踪数据的改变日志
enable_peer <ID>
	— 启用复制关系
remove_peer <ID>
	— 删除集群复制关系，HBase不再跟踪需要复制的数据
enable_table_replication <TABLE_NAME>
	— 打开某张表所有列族的复制
disable_table_replication <TABLE_NAME>
	— 关闭某张表所有列族的复制
append_peer_tableCFs
list_replicated_tables,
 remove_peer_tableCFs
set_peer_tableCFs
show_peer_tableCFs


查看复制状态
监控复制状态
	— status 'replication‘
校验复制
	— ./hbase org.apache.hadoop.hbase.mapreduce.replication.VerifyReplication 1 reptable1


可用于调优的参数
主集群对于同步的数据大小和个数采用默认值会比较大，容易导致备集群内存占用过多，可以考虑配置减少每次同步数据的大小
replication.source.size.capacity：4194304
	— 主集群每次像备集群发送的entry的包的最大值大小
replication.source.nb.capacity：2000
	— 主集群每次像备集群发送的entry最大的个数
