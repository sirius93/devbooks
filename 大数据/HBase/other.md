


========================

进入Shell
bin/hbase shell

创建表
create <table_name>,<colum_family...>
create 'user','info'

查看所有表
list

添加记录
put <table_name>,<row_name>,<column_name:>,<value>
put 'user','rk0001','info:name','peter'
put 'user','rk0002','info:name','jane'
put 'user','rk0002','info:age','20'

查看记录
get <table_name>,<row_name>
get 'user','zk0001'

查看记录数
count <table_name>

删除记录
delete <table_name>,<row_name>,<column_name>
删除全部
deleteall 'user','rk0001'

删除表，先屏蔽表再删除
disable <table_name>
drop <table_name>

查看所有记录
scan <table_name>
scan 'user'

查看某个表某个列中所有数据
scan <table_name>,{columns=>column_family:column_name}

更新记录
没有更新，就是重新写一遍


组命令
general 这些表示分类
cmd:status, table_help,version,whoami

visiblity labels
Security
Quotas
Snapshots
dml
Tools
Replication
ddl

namespace
cmd:alter_namespace,create_namespace,describe_namespace
drop_namespace,list_namespace,list_namespace_tables

list_namespace
create_namespace 'ns1'



===================



HBase架构

客户端
与HMaster通信进行管理类的操作
与HRegionserver通信进行读写类操作

Zookeeper
保证任何时候，集群中只有一个Master(HA)
存贮所有Region的寻址入口
实时监控Region server的上线和下线信息。并实时通知给Master
存储HBase的schema和table元数据

HBase主节点Master
管理HRegionServer的负载均衡，调整Region分布
在Region Split后，负责新Region的分配
在HRegionServer停机后，负责失效HRegionServer上的Region迁移
HMaster失效仅会导致所有元数据无法被修改，表的数据读写还是可以正常进行



bin/hbase zkcli


HBase内部表
-ROOT- 表(新版本已被弃用了？？？？)
		存储.meta.表信息，-root-表中仅有一行数据
		Zookeeper中存储了-ROOT-表的位置信息
.META. 表
		主要存储HRegin的列表和HReginServer 的服务器地址
Namespace表
		存储命名空间


HBase管理命令
flush 刷新数据到本地
split 分隔region


=========================================

HBase集群

在所有的主机上进行操作
创建组hadoop
	— addgroup hadoop
创建用户hadoop
	— adduser --ingroup hadoop hadoop
检查用户
	— id hadoop


配置SSH
注意：如果没有安装ssh，需要提前进行安装
为了允许master node可以以无密码的方式登录到其他主机，需要配置hadoop用户的公钥
	— hadoop@master$ ssh-keygen -t rsa -N ""
在所有的从节点上，添加master节点hadoop用户的公钥信息
	— hadoop@slave1$ mkdir ~/.ssh	— hadoop@slave1$ cat >> ~/.ssh/authorized_keys
检查master节点是否可以以无密码的方式登录到从节点


将安装好的Java同步到其他两个从节点
	— 例子： rsync -avz /hadoop/java7/ slave1:/hadoop/java7/


在主节点上安装Hadoop
修改hadoop-2.6.0/etc/hadoop/hadoop-env.sh，添加JDK支持：
	— export JAVA_HOME=/usr/java/jdk1.8.0_25
修改hadoop-2.6.0/etc/hadoop/core-site.xml

<property>
<name>hadoop.tmp.dir</name>
<value>/tmp</value>
<description>Abase for other temporary directories.</description> ?
</property>

<property>
<name>fs.default.name</name>
<value>hdfs://master:9000</value>
</property>


修改hadoop-2.6.0/etc/hadoop/hdfs-site.xml
<property>   <name>dfs.name.dir</name>
    <value>/home/hadoop/hadoop-2.6.0/dfs/name</value>
    <description>Path on the local filesystem where the NameNode stores the namespace and transactions logs persistently.</description>
</property>
<property>    <name>dfs.data.dir</name>
    <value>/home/hadoop/hadoop-2.6.0/dfs/data</value>
    <description>Comma separated list of paths on the local filesystem of a DataNode where it should store its blocks.</description>
</property>
<property>
    <name>dfs.replication</name>
    <value>1</value>
</property>



修改hadoop-2.6.0/etc/hadoop/mapred-site.xml
<property>
    <name>mapred.job.tracker</name>
    <value>master:9001</value>
    <description>Host or IP and port of JobTracker.</description>
</property>



修改hadoop-2.6.0/etc/hadoop/slaves
slave1
slave2


将Hadoop分发到其他两个从节点
	— 示例： rsync -avz /hadoop/hadoop2.6/ slave1:/hadoop/hadoop2.6/


安装HBase
修改hbase-site.xml

<property>  <name>hbase.rootdir</name>
<value>hdfs://master.Hadoop:9000/hbase</value>
</property>
<property><name>hbase.zookeeper.quorum</name>
<value>master,slave1,slave2</value>
</property>
<property><name>hbase.cluster.distributed</name>
<value>true</value>
</property>
<property>
<name>hbase.zookeeper.property.dataDir</name>
<value>/home/hadoop/data</value>
</property>


修改regionservers
slave1
slave2



=======================================================

hbase(main):001:0> create 'test', 'cf'

hbase(main):002:0> list 'test'

> describe 'test'

插入数据
	hbase(main):003:0> put 'test', 'row1', 'cf:a', 'value1'
	0 row(s) in 0.0850 seconds

	hbase(main):004:0> put 'test', 'row2', 'cf:b', 'value2'
	0 row(s) in 0.0110 seconds

	hbase(main):005:0> put 'test', 'row3', 'cf:c', 'value3'
	0 row(s) in 0.0100 seconds

hbase(main):006:0> scan 'test'

get 'test', 'row1'

disable表
	hbase(main):008:0> disable 'test'
	0 row(s) in 1.1820 seconds
为test表更改版本数，并且添加一个新的列族
	 hbase(main):008:0> alter 'test', {NAME => 'cf', VERSIONS => '2'}, {NAME => 'cf1'}
	hbase(main):009:0> enable 'test'
	0 row(s) in 0.1770 seconds

hbase(main):008:0> disable 'test'
	0 row(s) in 1.1820 seconds

	hbase(main):011:0> drop 'test'

flush某张表的所有region
	— hbase> flush 'hly_temp'
flush某张表的特定region
	— 在Hmaster的Web管理界面中找到表的region name
	— hbase> flush 'hly_temp,31549,1439430417822.28e5ed4d1785d121b04cc5ee15c40bb4.'

> split 'hly_temp,31549,1439430417822.28e5ed4d1785d121b04cc5ee15c40bb4.'

— hbase> compact 'hly_temp'
对表执行major compat操作
	— hbase> major_compact 'hly_temp'

	hbase> balance_switch true
	true（这是balance以前的状态）
使用balancer命令执行集群的数据均衡
	hbase> balancer
	true （true代表一个balance操作已经在后台触发）

move '1591ee7a3fbf1eb02d170ad6418aa94b', 'slave2,16020,1440075843915'

hly_temp,42323,1440076896341.1591ee7a3fbf1eb02d170ad6418aa94b.

count 'hly_temp'
95630 row(s) in 21.4640 seconds

=> 95630
date;./hbase org.apache.hadoop.hbase.mapreduce.RowCounter hly_temp;date


将WAL文件的内容dump出来
	— ./hbase org.apache.hadoop.hbase.regionserver.wal.FSHLog --dump hdfs://master:9000/hbase/WALs/slave1,16020,1440075843511/slave1%2C16020%2C1440075843511.default.1440076900175
将WAL进行分割
	— ./hbase org.apache.hadoop.hbase.regionserver.wal.FSHLog --split hdfs://master:9000/hbase/WALs/slave1,16020,1440075843511/


	— ./hbase wal hdfs://master:9000/hbase/WALs/slave2,16020,1440075843915/slave2%2C16020%2C1440075843915.default.1440076165410
或者命令2：
	— ./hbase hlog hdfs://master:9000/hbase/WALs/slave2,16020,1440075843915/slave2%2C16020%2C1440075843915.default.1440076165410


=============================
HBase管理工具

HBase Web 管理工具
Hmaster的Web接口
	— 端口参数：hbase.master.info.port   默认值 是16010
	— http://hbase_master_server:16010
RegionServer的Web接口
	— 端口参数：hbase.regionserver.info.port  默认值是16030
	— http://hbase_region_server:16030


HBase Shell管理工具
bin/hbase shell
HBase的数据首先会写入到Write Ahead Log (WAL) 日志中
然后再写入到region server 的memstore中
在达到一个阀值之后才会写入到磁盘中
	— 阀值的大小参数： hbase.hregion.memstore.flush.size 默认是128M

flush操作可以将memstore中的内存数据写入到磁盘中
flush某张表的所有region
	— hbase> flush 'hly_temp'
flush某张表的特定region
	— 在Hmaster的Web管理界面中找到表的region name
	— hbase> flush 'hly_temp, ,1324174482248.
e3d9b9952973964f3d8e61e191924698.'

split可以将表的所有region或者某个region进行分割
	— hbase(main):002:0> split 'hly_temp,,1438334725398.da8c833941d5ffcc93a70c454307a8c0.'
在Web管理工具中查看表的region是不是增加了

Compact
	— 把多个小的HFile合并成一个大的文件
	— 减少HFile数量，提升读效率
	— 执行时严重影响HBase性能
Minor Compact
	— 把多个小的HFile合成数量较少的大的Hfile
	— 合并较快，但是会影响磁盘IO
Major Compact
	— 一个store下的所有文件合并
	— 删除过期版本数据
— 删除delete marker数据
	— 指定间隔时间或手动执行


对表执行compat操作
	— hbase> compact 'hly_temp'
对表执行major compat操作
	— hbase> major_compact 'hly_temp'

balancer操作
随着数量的不断增长，集群数据在不同region server上的分布可能会不均匀
虽然HBase会周期性的在后台执行数据平衡的操作
但是当我们需要维护或者重启一个regionserver时，会关闭balancer，这样就使得region在regionserver上的分布不均，这个时候需要手工的开启balance
启用平衡器
	hbase> balance_switch true
	true（这是balance以前的状态）
使用balancer命令执行集群的数据均衡
	hbase> balancer
	true （true代表一个balance操作已经在后台触发）


move操作可以将一个region移动到一个特定的region server
语法：move 'encodeRegionName', 'ServerName'
	— encodeRegionName指的regioName后面的编码
	— ServerName指的是在hmaster web界面中看到的Region Servers列表
示例
hbase(main)>move '3fe79ad9550e5fe69d557143b67d0068', 'slave1,16020,1438766885125'





其他管理工具

RowCounter工具可以查看某张表有多少行
虽然Hbase shell的count命令也可以查找某张表有多少行，但是在数据量大的情况下，效率会非常差
RowCounter会以MapReduce任务的方式来查找某张表有多少行，在大数据量的情况下会更为高效
	— ./hbase org.apache.hadoop.hbase.mapreduce.RowCounter hly_temp

FSHLog工具可以将WAL进行split或者dump
将WAL文件的内容dump出来
	— ./hbase org.apache.hadoop.hbase.regionserver.wal.FSHLog --dump hdfs://master:9000/hbase/WALs/slave1,16020,1439430246468/slave1%2C16020%2C1439430246468.default.1439430419372
将WAL进行分割
	— ./hbase org.apache.hadoop.hbase.regionserver.wal.FSHLog --split hdfs://master:9000/hbase/WALs/slave1,16020,1439430246468/


WAL Pretty Printer工具可以打印WAL中的内容
命令1：
	— ./hbase wal hdfs://master:9000/hbase/WALs/slave1,16020,1439430246468/slave1%2C16020%2C1439430246468.default.1439430419372


HFile工具
打印key-value的数量
	— ./hbase org.apache.hadoop.hbase.io.hfile.HFile -v -f hdfs://master:9000/hbase/data/default/hly_temp/b5779eab9810bb670e8cae59ca512f4e/cf1/d590cfb7d9754f0e8c75cc7905ea6f2c
打印所有的key-value
	— ./hbase org.apache.hadoop.hbase.io.hfile.HFile -p -f hdfs://master:9000/hbase/data/default/hly_temp/b5779eab9810bb670e8cae59ca512f4e/cf1/d590cfb7d9754f0e8c75cc7905ea6f2c


hbck全称是 Health Checker，可以检查HBase集群的一致性状态
最终结果会显示OK或者INCONSISTENCY
如果显示INCONSISTENCY可以添加-details参数，查看详细信息
在集群启动或者region split时，也有可能会显示不一致
如果一致，可以使用-fix参数尝试修复不一致状态
命令示例：
	— $./bin/hbase hbck
