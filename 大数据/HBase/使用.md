[TOC]

## 使用

### Shell 操作

进入 HBase Shell

```bash
bin/hbase shell
```

进入 ZooKeeper Shell

```bash
bin/hbase zkcli
```

### 表操作

#### 创建表

```bash
create <[namespace_name:]table_name>,<column_family...>
```

没有指定命名空间的话创建的表都属于 `default` 命名空间。

例

```bash
create 'user','info'
```

#### 查看所有表

```bash
list
```

#### 删除表

需要先屏蔽表然后再删除

```bash
disable <table_name>
drop <table_name>
```

### 记录操作

#### 添加或更新记录

HBase 中更新记录就是重新添加一遍，即覆盖

```bash
put <table_name>,<row_key>,<column_family:column_name>,<value>
```

例

```bash
put 'user','rk0001','info:name','peter'
put 'user','rk0002','info:name','jane'
put 'user','rk0002','info:age','20'
```

#### 查看记录

```bash
get <table_name>,<row_key>
```

例

```bash
get 'user','zk0001'
```

#### 删除列

```bash
delete <table_name>,<row_key>,<column_family:column_name>
```

#### 删除行

```bash
deleteall <table_name>,<row_key>
```

#### 查看表中所有记录

```bash
scan <table_name>
```

#### 查看指定列中所有数据

```bash
scan <table_name>,{COLUMNS=>column_family:column_name}
```

注意其中 `COLUMNS` 大小写敏感

例

```bash
scan 'test',{COLUMNS=>['blog:title','blog:desc']}
```

### 组命令

#### 查看所有组命令

```bash
help
```

#### general 组

- `status` 查看集群状态
- `whoami` 查看档期标示
- `version`

#### namespace 组

- `list_namespace` 查看所有命名空间
- `create_namespace <name>` 创建命名空间
- `list_namespace_tables <name>` 列出命名空间内所有表

### 管理命令

#### Split

split 可以将表的所有 region 或者某个 region 进行分割

```bash
hbase(main):002:0> split 'hly_temp,,1438334725398.da8c833941d5ffcc93a70c454307a8c0.'
```

执行后可以在 Web 管理工具中查看表的 region 是不是增加了

#### Compact

特点

— 把多个小的 HFile 合并成一个大的文件
— 减少 HFile 数量，提升读效率
— 执行时严重影响 HBase 性能

Minor Compact

— 把多个小的 HFile 合成数量较少的大的 HFFile
— 合并较快，但是会影响磁盘 IO

Major Compact

— 一个 store 下的所有文件合并
— 删除过期版本数据
— 删除 delete marker 数据
	— 指定间隔时间或手动执行

使用

对表执行 compat 操作
	— hbase> compact 'hly_temp'
对表执行 major compat 操作
	— hbase> major_compact 'hly_temp'

#### Flush

flush 操作可以将 memstore 中的内存数据写入到磁盘中

使用

flush 某张表的所有r egion
```bash
hbase> flush 'hly_temp'
```

flush 某张表的特定 region
先在 Hmaster 的 Web 管理界面中找到表的 region name，然后只需
```bash
hbase> flush 'hly_temp, ,1324174482248.
	e3d9b9952973964f3d8e61e191924698.'
```

#### balancer

随着数量的不断增长，集群数据在不同region server 上的分布可能会不均匀
	虽然HBase会周期性的在后台执行数据平衡的操作
	但是当我们需要维护或者重启一个regionserver时，会关闭balancer，这样就使得region在regionserver上的分布不均，这个时候需要手工的开启balance
	启用平衡器
		hbase> balance_switch true
		true（这是balance以前的状态）
	使用balancer命令执行集群的数据均衡
		hbase> balancer
		true （true代表一个balance操作已经在后台触发）

#### move

move 操作可以将一个 region 移动到一个特定的 region server
语法：move 'encodeRegionName', 'ServerName'
	— encodeRegionName指的regioName后面的编码
	— ServerName指的是在hmaster web界面中看到的Region Servers列表
示例
hbase(main)>move '3fe79ad9550e5fe69d557143b67d0068', 'slave1,16020,1438766885125'
