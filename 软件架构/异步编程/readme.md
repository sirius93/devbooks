# 异步编程

## Reactive Stream

### 基本构成

Publisher 发布主题给订阅者
  - subscribe(Subscriber)

Subscriber 订阅
  - onSubscribe(subscription) publisher.subscribe() 后调用
  - onNext(t)
  - onError(e)
  - onComplete()
  
Subscription 主题
  - request(n) Publisher需要接受的元素数量
  - cancel()
  
Processor 继承自 Subscriber, Publisher

一个 Subscription 只能在一个 Subscriber 上工作
一个 Subscriber 只会绑定一次到 Publisher

### 参考资料

- [Reactive Stream](https://github.com/reactive-streams/reactive-streams-jvm)
