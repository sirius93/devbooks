[TOC]

# Logback

## 添加依赖

LogBack + slf4j

添加以下代码后会自动关联其它所有相关依赖

```xml
 <dependency>
    <groupId>ch.qos.logback</groupId>
    <artifactId>logback-classic</artifactId>
    <version>1.1.3</version>
</dependency>
```

## 使用

### 配置文件

LogBack 会先寻找 `logback.groovy`，不存在的话才会寻找 `logback.xml`。官方推荐使用 Groovy 文件作为配置文件。

- rollingPolicy 代表按照一定规则写入文件，如按大小，按日期
- triggeringPolicy 代表按照一定规则分割文件，使用时 FileNamePattern 不能缺少 `%i`
部分 rollingPolicy 同时拥有 triggeringPolicy 功能，此时就不能设置 triggeringPolicy

IDEA 默认不支持颜色显示，需要按照 Grep Console 插件

- root 为默认 Logger，通常用于配置应用中集成的第三方库的 Logger
- logger 为自定义 Logger，通常配置为当前应用的 Logger

### logback.xml 示例

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<configuration>
    <property name="LOG_HOME" value="../log"/>
    <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
        <!--显示颜色,IDEA 不支持,需要额外安装Grep Console插件-->
        <withJansi>true</withJansi>
        <encoder>
            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n</pattern>
        </encoder>
    </appender>
    <appender name="FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <file>flow.log</file>
        <!--SizeAndTimeBasedRollingPolicy 兼具rolling 和trigger 功能,在1.1.7以后追加-->
        <rollingPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy">
            <fileNamePattern>flow.%d{yyyy-MM-dd}.%i.log</fileNamePattern>
            <maxFileSize>100MB</maxFileSize>
        </rollingPolicy>
        <encoder>
            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n</pattern>
        </encoder>
    </appender>
    <logger name="com.bookislife.flow">
        <level value="DEBUG"/>
        <appender-ref ref="FILE"/>
    </logger>
    <root>
        <level value="ERROR"/>
        <appender-ref ref="STDOUT"/>
    </root>
</configuration>
```

### logback.groovy 示例

官方提供了 Xml 到 Groovy 转换的在线工具 `http://logback.qos.ch/translator/asGroovy.html`

使用 groovy 文件进行配置需要添加 groovy 环境到 classpath 中


```groovy
import ch.qos.logback.classic.PatternLayout
import ch.qos.logback.core.ConsoleAppender
import ch.qos.logback.core.rolling.FixedWindowRollingPolicy
import ch.qos.logback.core.rolling.RollingFileAppender
import ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy

import static ch.qos.logback.classic.Level.INFO

appender("STDOUT", ConsoleAppender) {
    layout(PatternLayout) {
        pattern = "%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n"
    }
}
appender("FILE", RollingFileAppender) {
    file = "foobar.log"
    rollingPolicy(FixedWindowRollingPolicy) {
        fileNamePattern = "foobar.log.%i.bak"
        minIndex = 1
        maxIndex = 12
    }
    triggeringPolicy(SizeBasedTriggeringPolicy) {
        maxFileSize = "100MB"
    }
    layout(PatternLayout) {
        pattern = "%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n"
    }
}
logger("com.mrseasons.spring.first.corejava", INFO, ["STDOUT", "FILE"])
```


### Java 应用

```java
Logger logger = LoggerFactory.getLogger(Person.class);
logger.debug("foobar");
logger.info("hello world");
logger.error("something wrong");
//  追加参数
logger.info("Temperature set to {}. Old temperature was {}.", "the first arg", "the second arg");
```
