[TOC]

# 日志

## 实现

- [slf-4j](http://www.slf4j.org/)
- [Log4j](http://logging.apache.org/log4j/2.x/)
- [Logback](http://logback.qos.ch/)

commons-logging Apache 提供的日志接口，避免和具体实现耦合
Log4j   一种日志解决方案
slf-4j  日志接口，避免和具体实现耦合
LogBack  一种日志解决方案，被认为效率高于 Log4j，和 Log4j 是同一作者。可以使用 XMl 和 Groovy 作为配置文件。

## slf-4j

常用的组合为 Log4j+slf4j 或 LogBack+slf4j


---

可运行实例代码位于 `spring/first_core/src/main` 目录的 `LogTest.java` 文件
