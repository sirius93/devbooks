[TOC]

# Java 代码生成

## 实现

- [JavaPoet](https://github.com/square/javapoet)

和 ASM 那种生成 `.class` 文件不同，JavaPoet 用于生成 Java 源文件，即 `.java` 文件。
