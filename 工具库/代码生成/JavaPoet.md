[TOC]

# JavaPoet

官网 [https://github.com/square/javapoet](https://github.com/square/javapoet)

## 添加依赖

```xml
<dependency>
    <groupId>com.squareup</groupId>
    <artifactId>javapoet</artifactId>
    <version>1.5.1</version>
</dependency>
```

## 使用

```java
MethodSpec mainMethodSpec = MethodSpec.methodBuilder("main")
        .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
        .returns(Void.class)
        .addParameter(String[].class, "args")
        .addStatement("$T.out.println($S)", System.class, "Hello, JavaPoet!")
        .build();
TypeSpec helloWorld = TypeSpec.classBuilder("PoetProgrammer")
        .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
        .addMethod(mainMethodSpec)
        .build();

JavaFile javaFile = JavaFile.builder("com.mrseasons.first.corejava.asm01", helloWorld)
        .build();

// 指定输出流
javaFile.writeTo(System.out);

// 生成字符串形式代码
System.out.println(javaFile.toString());
```

以上调用后会生成以下代码

```java
public final class PoetProgrammer {
  public static Void main(String[] args) {
    System.out.println("Hello, JavaPoet!");
  }
}
```



------

可运行实例代码位于 `spring/first_core/src/test` 的 `asm01` 目录下
