# RxJava2

## 安装

```xml
<dependency>
    <groupId>io.reactivex.rxjava2</groupId>
    <artifactId>rxjava</artifactId>
    <version>2.1.1</version>
</dependency>
```


## 使用




[TOC]

# RxJava

## 应用场景

rx 可以用于去抖动，比如说单击变双击的场合，还有搜索框，如果快速输入时每个词都查询会浪费流浪，可以设置一个抖动时长，避免每个字都要进行搜索


## 2.0

Flowable 用于解决背压问题。
所谓背压，即生产者的速度大于消费者的速度带来的问题。比如在Android中常见的点击事件，点击过快则会造成点击两次的效果。

在 RxJava1.x 中的 observeOn， 因为是切换了消费者的线程，因此内部实现用队列存储事件。在 Android 中默认的 buffersize 大小是16，因此当消费比生产慢时， 队列中的数目积累到超过16个，就会抛出MissingBackpressureException， 初学者很难明白为什么会这样，使得学习曲线异常得陡峭。



Observable（被观察者）
Observer/Subscriber（观察者）

建立关联

```java
observable.subscribe(observer);
```

创建 Obsrvable

```
Observable.create(new ObservableOnSubscribe(){});
Observable.just(..);
```

Consumer 是 Obsrvable 的子类，可以用于只需处理 `onNext()` 的情形

创建 Observer

```java
new Observer(){};
```

规则

1．被观察者可以发送无限个 onNext, 观察者也可以接收无限个 onNext.
2．当 Observable 发送了一个 onComplete 后, Observable 的 onComplete 之后的事件将会继续发送, 而 Observer 收到 onComplete 事件之后将不再继续接收事件.
3．当 Observable 发送了一个 onError 后, Observable 中 onError 之后的事件将继续发送, 而 Observer 收到 onError 事件之后将不再继续接收事件.
4．Observable 可以不发送 onComplete 或 onError.
5．最为关键的是 onComplete 和 onError 必须唯一并且互斥, 即不能发多个 onComplete, 也不能发多个 onError, 也不能先发一个 onComplete, 然后再发一个 onError, 反之亦然


线程调度

```java
observable.subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(consumer);
```

## 1.0

RxJava 源自于观察者模式。

它添加了三个缺少的功能：

生产者在没有更多数据可用时能够发出信号通知：onCompleted()事件。
生产者在发生错误时能够发出信号通知：onError()事件。
RxJava Observables 能够组合而不是嵌套，从而避免开发者陷入回调地狱。

观察者模式

基于 subject 这个概念。subject 是一种特殊对象，当它改变时，那些由它保存的一系列对象将会得到通知。而这一系列对象被称作 Observers，它们会对外暴漏了一个通知方法，当 subject 状态发生变化时会调用的这个方法。

在 RxJava 的世界里，我们有四种角色：

Observable
Observer
Subscriber
Subjects

Observables 和 Subjects 是两个“生产”实体，Observers 和 Subscribers 是两个“消费”实体。

Observable 指定可以被观察的事件，然后又 Subscriber 订阅这些可被观察的事件，并且负责处理接收到这些事件后的响应。


Observable

与 Iterable 比较

使用Iterable时，消费者从生产者那里以同步的方式得到值，在这些值得到之前线程处于阻塞状态。相反，使用Observable时，生产者以异步的方式把值推给观察者

| Event     | Iterable(pull)     |  Observable(push)    |
| :------------- | :------------- |:------------- |
| 检索数据      | T next()       |  onNext(T) |
| 发现错误      | throws Exception      |  onError(Throwable)  |
| 完成      | !hasNext()      |  onCompleted() |


创建 Observable

Observable.create(onSubscribe)
Observable.from(collection)
Observable.just(obj)


Subject

subject 可以是一个 Observable 同时也可以是一个 Observer


过滤Observables

过滤序列

Observable.from(apps)
            .filter((appInfo) ->
            appInfo.getName().startsWith("C"))

            .filter(new Func1<AppInfo,Boolean>(){
                @Override
                public Boolean call(AppInfo appInfo){
                    return appInfo != null;
                }
            })


取开头或结尾的几个元素，我们可以用take()或takeLast()


转换Observables

map家族
map函数接收一个指定的Func对象然后将它应用到每一个由Observable发射的值上
我们有一个这样的Observable：它发射一个数据序列，这些数据本身也可以发射Observable。RxJava的flatMap()函数提供一种铺平序列的方式，然后合并这些Observables发射的数据，最后将合并后的结果作为最终的Observable。
记住任何一个Observables发生错误的情况，flatMap()将会触发它自己的onError()函数并放弃整个链。
重要的一点提示是关于合并部分：它允许交叉。正如上图所示，这意味着flatMap()不能够保证在最终生成的Observable中源Observables确切的发射顺序。

concatMap()函数解决了flatMap()的交叉问题，提供了一种能够把发射的值连续在一起的铺平函数，而不是合并它们

GroupBy

Observable<GroupedObservable<String,AppInfo>> groupedItems = Observable.from(apps)
    .groupBy(new Func1<AppInfo,String>(){
        @Override
        public String call(AppInfo appInfo){
            SimpleDateFormat formatter = new SimpleDateFormat("MM/yyyy");
            return formatter.format(new Date(appInfo.getLastUpdateTime()));
        }
    });



组合Observables

merge

private void loadList(List<AppInfo> apps) {
    mRecyclerView.setVisibility(View.VISIBLE);
    List reversedApps = Lists.reverse(apps);
    Observable<AppInfo> observableApps = Observable.from(apps);
    Observable<AppInfo> observableReversedApps = Observable.from(reversedApps);
    Observable<AppInfo> mergedObserbable = Observable.merge(observableApps,observableReversedApps);

    mergedObserbable.subscribe(new Observer<AppInfo>(){
        @Override
        public void onCompleted() {
            mSwipeRefreshLayout.setRefreshing(false);
            Toast.makeText(getActivity(), "Here is the list!", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onError(Throwable e) {
            Toast.makeText(getActivity(), "One of the two Observable threw an error!", Toast.LENGTH_SHORT).show();
            mSwipeRefreshLayout.setRefreshing(false);
        }

        @Override
        public void onNext(AppInfoappInfo) {
            mAddedApps.add(appInfo);
            mAdapter.addApplication(mAddedApps.size() - 1, appInfo);
        }
    });
}


zip()合并两个或者多个Observables发射出的数据项，根据指定的函数Func*变换它们，并发射一个新值。

private void loadList(List<AppInfo> apps) {
    mRecyclerView.setVisibility(View.VISIBLE);
    Observable<AppInfo> observableApp = Observable.from(apps);

    Observable<Long> tictoc = Observable.interval(1, TimeUnit.SECONDS);

    Observable.zip(observableApp, tictoc,
    (AppInfo appInfo, Long time) -> updateTitle(appInfo, time))
    .observeOn(AndroidSchedulers.mainThread())
    .subscribe(new Observer<AppInfo>() {
        @Override
        public void onCompleted() {
            Toast.makeText(getActivity(), "Here is the list!", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onError(Throwable e) {
            mSwipeRefreshLayout.setRefreshing(false);
            Toast.makeText(getActivity(), "Something went wrong!", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNext(AppInfoappInfo) {
            if (mSwipeRefreshLayout.isRefreshing()) {
                mSwipeRefreshLayout.setRefreshing(false);
            }
            mAddedApps.add(appInfo);
            int position = mAddedApps.size() - 1;
            mAdapter.addApplication(position, appInfo);
            mRecyclerView.smoothScrollToPosition(position);
        }
    });
}



And,Then和When

private void loadList(List<AppInfo> apps) {

    mRecyclerView.setVisibility(View.VISIBLE);

    Observable<AppInfo> observableApp = Observable.from(apps);

    Observable<Long> tictoc = Observable.interval(1, TimeUnit.SECONDS);

    Pattern2<AppInfo, Long> pattern = JoinObservable.from(observableApp).and(tictoc);

    Plan0<AppInfo> plan = pattern.then(this::updateTitle);

    JoinObservable
        .when(plan)
        .toObservable()
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<AppInfo>() {

            @Override
            public void onCompleted() {
                Toast.makeText(getActivity(), "Here is the list!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(Throwable e) {
                mSwipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getActivity(), "Something went wrong!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNext(AppInfoappInfo) {
                if (mSwipeRefreshLayout.isRefreshing()) {
                mSwipeRefreshLayout.setRefreshing(false);
                }
                mAddedApps.add(appInfo);
                int position = mAddedApps.size() - 1;
                mAdapter.addApplication(position, appInfo); mRecyclerView.smoothScrollToPosition(position);
            }
        });
}












---

sample

```Java
private Observable<AppInfo> getApps(){
    return Observable.create(subscriber -> {
        List<AppInfoRich> apps = new ArrayList<AppInfoRich>();

        final Intent mainIntent = new Intent(Intent.ACTION_MAIN,null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> infos = getActivity().getPackageManager().queryIntentActivities(mainIntent, 0);

        for(ResolveInfo info : infos){
            apps.add(new AppInfoRich(getActivity(),info));
        }

        for (AppInfoRich appInfo:apps) {
            Bitmap icon = Utils.drawableToBitmap(appInfo.getIcon());
            String name = appInfo.getName();
            String iconPath = mFilesDir + "/" + name;
            Utils.storeBitmap(App.instance, icon,name);

            if (subscriber.isUnsubscribed()){
                return;
            }
            subscriber.onNext(new AppInfo(name,iconPath,appInfo.getLastUpdateTime()));                
        }
        if (!subscriber.isUnsubscribed()){
            subscriber.onCompleted();
        }
    });
}

private void refreshTheList() {
    getApps().toSortedList()
            .subscribe(new Observer<List<AppInfo>>() {

                @Override
                public void onCompleted() {
                    Toast.makeText(getActivity(), "Here is the list!", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onError(Throwable e) {
                    Toast.makeText(getActivity(), "Something went wrong!", Toast.LENGTH_SHORT).show();
                    mSwipeRefreshLayout.setRefreshing(false);
                }

                @Override
                public void onNext(List<AppInfo> appInfos) {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mAdapter.addApplications(appInfos);
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            });
}
```

从列表创建一个Observable

```Java
private void loadList(List<AppInfo> apps) {
    mRecyclerView.setVisibility(View.VISIBLE);
    Observable.from(apps)
            .subscribe(new Observer<AppInfo>() {

                @Override
                public void onCompleted() {
                    mSwipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(getActivity(), "Here is the list!", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onError(Throwable e) {
                    Toast.makeText(getActivity(), "Something went wrong!", Toast.LENGTH_SHORT).show();
                    mSwipeRefreshLayout.setRefreshing(false);
                }

                @Override
                public void onNext(AppInfo appInfo) {
                    mAddedApps.add(appInfo);
                    mAdapter.addApplication(mAddedApps.size() - 1,appInfo);
                }
            });
}
```



http://wiki.jikexueyuan.com/project/rxjava//chapter5/groupby.html
