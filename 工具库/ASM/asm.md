[TOC]

# ASM

ASM 是一款基于 java 字节码层面的代码分析和修改工具。无需提供源代码即可对应用嵌入所需 debug 的代码，用于应用 API 性能分析，代码优化和代码混淆等工作。

## 添加依赖

```xml
<dependency>
    <groupId>org.ow2.asm</groupId>
    <artifactId>asm-all</artifactId>
    <version>5.0.4</version>
</dependency>
```

## 使用

### 自定义 ClassLoader

```java
public class MyClassLoader extends ClassLoader {

    public Class<?> defineMyClass(String name, byte[] b, int off, int len) {
        return super.defineClass(name, b, off, len);
    }
}

FileInputStream fileInputStream = new FileInputStream(classFile);
byte[] buffer = new byte[1024];
int count = fileInputStream.read(buffer);

MyClassLoader loader = new MyClassLoader();
Class clazz = loader.defineMyClass(null, buffer, 0, count);
System.out.println(clazz.getCanonicalName());

Object object = clazz.newInstance();
// 不同 classloader， 真实类型不同
//        Programmer programmer = (Programmer) object;

clazz.getMethod("code", null).invoke(object, null);
```

### 动态创建类

```java
ClassWriter classWriter = new ClassWriter(0);
classWriter.visit(
        Opcodes.V1_8,
        Opcodes.ACC_PUBLIC,
        "DynamicProgrammer",
        null,
        "java/lang/Object",
        null
);

// Define Constructor
MethodVisitor mv = classWriter.visitMethod(Opcodes.ACC_PUBLIC, "<init>", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(Opcodes.ALOAD, 0);
mv.visitMethodInsn(Opcodes.INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
mv.visitInsn(Opcodes.RETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();

//Define Method
MethodVisitor methodVisitor = classWriter.visitMethod(Opcodes.ACC_PUBLIC, "code", "()V",
        null, null);
methodVisitor.visitCode();
methodVisitor.visitFieldInsn(Opcodes.GETSTATIC, "java/lang/System", "out",
        "Ljava/io/PrintStream;");
methodVisitor.visitLdcInsn("I'm a Programmer,Just Coding.....");
methodVisitor.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/io/PrintStream", "println",
        "(Ljava/lang/String;)V", false);
methodVisitor.visitInsn(Opcodes.RETURN);
methodVisitor.visitMaxs(2, 2);
methodVisitor.visitEnd();
classWriter.visitEnd();

byte[] data = classWriter.toByteArray();
System.out.println(new String(data));

MyClassLoader loader = new MyClassLoader();
Class clazz = loader.defineMyClass(null, data, 0, data.length);
System.out.println(clazz.getCanonicalName());

Object object = clazz.newInstance();
clazz.getMethod("code", null).invoke(object, null);
```
