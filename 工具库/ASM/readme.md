[TOC]

# ASM

## 实现

- [ASM](http://asm.ow2.org/)
- [Javassist](http://jboss-javassist.github.io/javassist/)

ASM 需要了解 .class 文件的结构，而 Javassist 则进行了抽象，无需知道这些东西。



## 参考资料

- [Java动态代理机制详解](http://blog.csdn.net/luanlouis/article/details/24589193)



---

可运行实例代码位于 `spring/first_corejava/src/test` 下的 `asm01` 目录下
