[TOC]

# Javassist

## 添加依赖

```xml
<dependency>
    <groupId>org.javassist</groupId>
    <artifactId>javassist</artifactId>
    <version>3.20.0-GA</version>
</dependency>
```

## 使用

### 自定义 ClassLoader

同 asm 一样

### 动态定义类

```java
ClassPool classPool = ClassPool.getDefault();

CtClass ctClass = classPool.makeClass("com.mrseasons.first.corejava.DyProgrammer");
CtMethod ctMethod = CtNewMethod.make("public void code(){}", ctClass);
ctMethod.insertBefore("System.out.println(\"foobar\");");
ctClass.addMethod(ctMethod);
ctClass.writeFile("target/test-classes");

byte[] data = ctClass.toBytecode();
MyClassLoader classLoader = new MyClassLoader();
Class clazz = classLoader.defineMyClass(null, data, 0, data.length);
System.out.println(clazz.getCanonicalName());

Object object = clazz.newInstance();
clazz.getMethod("code", null).invoke(object, null);
```
