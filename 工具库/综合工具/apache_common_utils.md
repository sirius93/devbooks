[TOC]

# Commons BeanUtils

实现对象之间的深复制以及通过 `a.b.c` 这样的方式来访问一个对象持有的子对象的属性。不过考虑到效率问题，通常只使用复制功能。

## 配置文件

```xml
<dependency>
    <groupId>commons-beanutils</groupId>
    <artifactId>commons-beanutils</artifactId>
    <version>1.9.2</version>
</dependency>
```

## 使用

BeanUtils 支持对象之间的深层复制，并且对象的类型可以不一致。第一个参数为目标，第二个参数为源。

```java
Student peter = new Student();
peter.setName("Peter");
peter.setBirthDay(new DateTime(1999, 1, 21, 1, 1).toDate());
peter.setAge(20);

// 对象->对象
// dest->src
Student jack = new Student();
BeanUtils.copyProperties(jack, peter);

// 复制 Map->对象
Map<String, String> map = new HashMap<>();
map.put("fatherName", "Terry");
BeanUtils.copyProperties(jack, map);
```

---

可运行实例代码位于 `spring/first_core/java` 的 `utils` 目录下
