[TOC]

# 其它工具

## 对象复制工具

- [Commons BeanUtils]（http://commons.apache.org/proper/commons-beanutils/）
- [Dozer](http://dozer.sourceforge.net/)

Apache Commons BeanUtils 通过反射实现，支持不同类型间的属性复制。
Dozer 可以通过 XML 进行自定义行为，支持自动类型转换，如页面字符串日期，DTO 为长整形，可定制化强于 Apache Commons BeanUtils。

同类产品还有 Spring BeanUtils

### Commons BeanUtils

实现对象之间的深复制以及通过 `a.b.c` 这样的方式来访问一个对象持有的子对象的属性。不过考虑到效率问题，通常只使用复制功能。

#### 安装

```xml
<dependency>
    <groupId>commons-beanutils</groupId>
    <artifactId>commons-beanutils</artifactId>
    <version>1.9.2</version>
</dependency>
```

#### 使用

BeanUtils 支持对象之间的深层复制，并且对象的类型可以不一致。第一个参数为目标，第二个参数为源。

```java
Student peter = new Student();
peter.setName("Peter");
peter.setBirthDay(new DateTime(1999, 1, 21, 1, 1).toDate());
peter.setAge(20);

// 复制对象->对象
Student jack = new Student();
BeanUtils.copyProperties(jack, peter);

peter.setBirthDay(new DateTime(2001, 1, 10, 3, 5).toDate());

System.out.println(peter);
System.out.println(jack);

// 复制 Map->对象
Map<String, String> map = new HashMap<>();
map.put("fatherName", "Terry");
BeanUtils.copyProperties(jack, map);

System.out.println(jack);
```


## 参考资料

- [Java Bean mapper performance tests](http://www.christianschenk.org/blog/java-bean-mapper-performance-tests/)
