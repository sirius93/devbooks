[TOC]

# 其它工具

## 对象复制工具

- [Commons BeanUtils]（http://commons.apache.org/proper/commons-beanutils/）
- [Dozer](http://dozer.sourceforge.net/)

Apache Commons BeanUtils 通过反射实现，支持不同类型间的属性复制。
Dozer 可以通过 XML 进行自定义行为，支持自动类型转换，如页面字符串日期，DTO 为长整形，可定制化强于 Apache Commons BeanUtils。

同类产品还有 Spring BeanUtils




## 参考资料

- [Java Bean mapper performance tests](http://www.christianschenk.org/blog/java-bean-mapper-performance-tests/)
