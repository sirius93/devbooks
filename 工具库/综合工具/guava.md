[TOC]

# Guava

[Guava](https://github.com/google/guava)

## 配置文件

```xml
<dependency>
    <groupId>com.google.guava</groupId>
    <artifactId>guava</artifactId>
    <version>19.0</version>
</dependency>
```

## 基本使用

Option

```java
Optional<Integer> possible = Optional.of(5);
assert possible.isPresent();
assert 5 == possible.get();
```

Precondition

```java
Preconditions.checkArgument(i < j, "Expected i < j, but %s > %s", i, j);
Preconditions.checkNotNull(str, "str is null");
```

equals/hashcode/toString

```java
Objects.equal("a", "a");
Objects.hashCode(1, 2, 3);
HashFunction function = Hashing.md5();
HashCode hashCode = function.newHasher()
        .putLong(18)
        .putString("Peter", Charset.forName("utf-8"))
        .hash();
MoreObjects.toStringHelper("abc")
                .add("x", 1)
                .toString() //abc{x=1}
```

compareTo

```java
private static class Person implements Comparable {
    private String firstName;
    private String lastName;
    private int age;

    @Override
    public int compareTo(Object o) {
        Person other = (Person) o;
        return ComparisonChain.start()
                .compare(firstName, other.firstName)
                .compare(lastName, other.lastName)
                .compare(age, other.age)
                .result();
    }
}
```

EventBus

```java
class ChangeEvent {
    public int getChange() {
        return new Random().nextInt(1000);
    }
}

class EventBusChangeRecorder {

    @Subscribe
    public void recordCustomerChange(ChangeEvent e) {
        recordChange(e.getChange());
    }

    private void recordChange(int change) {
        System.out.println(Thread.currentThread().getName() + " record:" + change);
    }
}

EventBus eventBus = new EventBus();
eventBus.register(new EventBusChangeRecorder());
ChangeEvent event = new ChangeEvent();
eventBus.post(event);
```

Range

```java
Range<Integer> range = Range.lessThan(5);
range = Range.range(2, BoundType.CLOSED, 7, BoundType.OPEN);
range = Range.openClosed(2, 7);
```

Service

```java
class DefaultService extends AbstractIdleService {

    @Override
    protected void startUp() throws Exception {
        System.out.println("startUp");
    }

    @Override
    protected void shutDown() throws Exception {
        System.out.println("shutDown");
    }
}

final Exception exception = new Exception("deliberate");
AbstractIdleService service = new DefaultService() {
    @Override
    protected void startUp() throws Exception {
        throw exception;
    }
};
try {
    service.startAsync().awaitRunning();
    fail();
} catch (RuntimeException e) {
    assertSame(exception, e.getCause());
}
assertEquals(Service.State.FAILED, service.state());
```

String

```java
Joiner joiner = Joiner.on(",").useForNull("empty");
result = joiner.join(new Person("Peter"), new Person("Anna"), null, new Person("Jack"));    //Peter,Anna,empty,Jack
```


---

可运行实例代码位于 `spring/first_core/java` 的 `guava` 目录下
