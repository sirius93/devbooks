[TOC]

# 代码质量检查

## 实现

- [FindBugs](https://github.com/gleclaire/findbugs-maven-plugin)
- [CheckStyle](https://maven.apache.org/plugins/maven-checkstyle-plugin/)
- [PMD](https://pmd.github.io/)
- [SONAR](http://www.sonarqube.org/)


## FindBugs

FindBugs 是一个静态分析工具，它检查类或者 JAR 文件，将字节码与一组缺陷模式进行对比以发现可能的问题。

### 安装

```xml
<reporting>
    <plugins>
        <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>findbugs-maven-plugin</artifactId>
            <version>3.0.3</version>
        </plugin>
    </plugins>
</reporting>
```

### 使用

生成报告文件，默认文件生成在 `target/findbugsXml.xml`。site 相关所有文件生成在 `target/site` 目录。

```bash
mvn site
```

更改报告文件的位置

```xml
<configuration>
    <xmlOutput>true</xmlOutput>
    <xmlOutputDirectory>target/site</xmlOutputDirectory>
</configuration>
```

显示 GUI 界面

```bash
mvn findbugs:gui
```

完整示例

```xml
<plugin>
    <groupId>org.codehaus.mojo</groupId>
    <artifactId>findbugs-maven-plugin</artifactId>
    <version>3.0.3</version>
    <configuration>
        <xmlOutput>true</xmlOutput>
        <findbugsXmlOutput>true</findbugsXmlOutput>
        <xmlOutputDirectory>target/site/findbugs</xmlOutputDirectory>
        <findbugsXmlOutputDirectory>target/site/findbugs</findbugsXmlOutputDirectory>
        <outputEncoding>UTF-8</outputEncoding>
    </configuration>
</plugin>
```

## CheckStyle

用于检查代码风格

### 安装

```xml
<reporting>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-checkstyle-plugin</artifactId>
            <version>2.17</version>
            <configuration>
                <configLocation>google_checks.xml</configLocation>
                <failOnViolation>false</failOnViolation>
            </configuration>
        </plugin>
    </plugins>
</reporting>
```

默认使用的是 `sun_checks.xml`，也可以切换成 `google_checks.xml`。可以下载这些格式模板后自行修改。Google 的 [checkstyle 配置文件](https://github.com/checkstyle/checkstyle/blob/master/src/main/resources/google_checks.xml)

### 使用

生成报告文件，默认文件生成在 `target/checkstyle-result.xml` 和 `target/site/checkstyle.html`。site 相关所有文件生成在 `target/site` 目录。

```bash
mvn site
```

只执行 checkstyle

```bash
mvn compile
mvn checkstyle:checkstyle
```

## PMD

静态分析工具

### 安装

```xml
 <plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-pmd-plugin</artifactId>
    <version>3.6</version>
</plugin>
```

### 使用

生成报告文件，默认文件生成在 `target/pmd.xml` 和 `target/site/pmd.html`

```bash
mvn pmd:pmd
```

## SONAR

### 安装

#### 压缩包方式

下载 [安装包](http://www.sonarqube.org/downloads/) 并解压

#### 基于 Docker

##### 安装 MySQL

运行 MySQL 容器

```bash
docker run --name mysql01 -p 3306:3306 -e MYSQL_ROOT_PASSWORD=tiger -d mysql
```

连接 MySQL 客户端

```bash
docker run -it --link mysql01 --rm mysql sh -c 'exec mysql -h172.17.0.2 -P3306 -uroot -ptiger'
```

创建 SONAR 用的数据库和用户

```msyql
CREATE DATABASE sonar CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE USER 'sonar' IDENTIFIED BY 'sonar';
GRANT ALL ON sonar.* TO 'sonar'@'%' IDENTIFIED BY 'sonar';
GRANT ALL ON sonar.* TO 'sonar'@'localhost' IDENTIFIED BY 'sonar';
FLUSH PRIVILEGES;
```

##### 安装 SONAR

运行 SONAR 容器

```bash
docker run -d --name sonarqube \
    -p 9000:9000 -p 9092:9092 \
    -e SONARQUBE_JDBC_USERNAME=sonar \
    -e SONARQUBE_JDBC_PASSWORD=sonar \
    -e SONARQUBE_JDBC_URL="jdbc:mysql://192.168.1.67:3306/sonar?useUnicode=true&characterEncoding=utf8" \
    sonarqube:5.1
```

### 使用

#### 访问控制台

URL 为 `http://192.168.1.67:9000/`，默认用户名和密码都是 `admin`。

#### 提交工程到 SONAR

```bash
mvn sonar:sonar -Dsonar.host.url=http://192.168.1.67:9000 -Dsonar.jdbc.url="jdbc:mysql://192.168.1.67:3306/sonar?useUnicode=true&characterEncoding=utf8"
```

#### 安装插件

Settings -> System -> Update Center -> Available Plugin


### 异常

1. 报 `jacoco.exec: Incompatible version 1007` 是因为Jacoco 版本太新，换回0.7.4.201502262128就可以了。




## 参考

- [常用 Java 静态代码分析工具的分析与比较](http://www.oschina.net/question/129540_23043)
