[TOC]

# Apache Shiro

## 使用场景

适用于四种场景
- 认证，用户身份识别
- 授权，访问权限控制
- 密码加密
- 会话管理

## 基本概念

Subject 可以用于表示用户，代表当前连接的身份
SecurityManager 负责调用 Realm 权限认证
Realm 处理权限验证逻辑

## 使用

### Maven 配置

```xml
<!-- 基本服务 -->
<dependency>
     <groupId>org.apache.shiro</groupId>
     <artifactId>shiro-core</artifactId>
     <version>1.2.4</version>
 </dependency>
<!-- Web 服务 -->
<dependency>
   <groupId>org.apache.shiro</groupId>
   <artifactId>shiro-web</artifactId>
   <version>1.2.4</version>
</dependency>
```

需要注意如果使用 Web 时必须把两个包和依赖的 slf4j 和 Apache 的 commmon-beanutils 拷贝到 WEB-INF/lib 目录下。

### 基本使用

配置文件

以上配置文件中定义了两个用户个两个角色，用户的定义格式为 `username=password,roles...`，角色的定义格式为 `rolename=permissions...`。

shiro.ini

```
[users]
zhang=123,admin
wang=123,orgUser

[roles]
admin=*
loginUser=user:update,user:view
appUser=user:update:1,user:view:1
orgUser=user:*
```

代码

```java
Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiro.ini");
SecurityManager securityManager = factory.getInstance();
SecurityUtils.setSecurityManager(securityManager);

Subject currentUser = SecurityUtils.getSubject();
UsernamePasswordToken token = new UsernamePasswordToken("zhang", "123");

// 开启自动登录功能,依靠 cookie
token.setRememberMe(true);
try {
    currentUser.login(token);

    //session
    Session session = currentUser.getSession();
    session.setAttribute("foo", "bar");
    assertThat(currentUser.isAuthenticated());
} catch (AuthenticationException e) {
    assertThat(e).isNull();
}

//sign out
currentUser.logout();
```


### 自定义 Realm

自定义 Realm

```java
public class MyRealm implements Realm {
    @Override
    public String getName() {
        return MyRealm.class.getSimpleName();
    }

    @Override
    public boolean supports(AuthenticationToken authenticationToken) {
        //支持哪些token
        return (authenticationToken instanceof UsernamePasswordToken);
    }

    @Override
    public AuthenticationInfo getAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        String username = token.getUsername();
        String password = new String(token.getPassword());
        if (!username.equals("zhang")) {
            throw new UnknownAccountException("username not found");
        }
        if (!password.equals("123")) {
            throw new IncorrectCredentialsException("username or password is wrong");
        }
        return new SimpleAuthenticationInfo(username, password, getName());
    }
}
```

配置文件

shiro-realm.ini

```
# 声明一个realm
MyRealm=com.mrseasons.spring.first.shiro.MyRealm
MyRealm2=com.mrseasons.spring.first.shiro.MyRealm2
# 指定securityManager的realms实现
securityManager.realms=$MyRealm,$MyRealm2
```

代码同上一节，只是配置文件换了下。

#### 基本 JDBC

配置文件

```
[main]
ds=com.mysql.jdbc.jdbc2.optional.MysqlDataSource
ds.serverName=192.168.1.67
ds.port=3306
ds.user=root
ds.password=tiger
ds.databaseName=shiro

jdbcRealm=org.apache.shiro.realm.jdbc.JdbcRealm
jdbcRealm.dataSource=$ds
jdbcRealm.permissionsLookupEnabled = true
jdbcRealm.authenticationQuery=select password from t_user where name=?
jdbcRealm.userRolesQuery = select role from t_role WHERE name = ?

securityManager.realms=$jdbcRealm
```

代码同上一节。

#### 加密服务

```java
PasswordService passwordService = new DefaultPasswordService();
String encryptPassword = passwordService.encryptPassword("foobar");
System.out.println(encryptPassword);
```

#### Web 服务

Web 服务的配置文件需要放在 `WEB-INF` 目录下。

web.xml

```xml
<filter>
   <filter-name>ShiroFilter</filter-name>
   <filter-class>org.apache.shiro.web.servlet.ShiroFilter</filter-class>
</filter>
<filter-mapping>
   <filter-name>ShiroFilter</filter-name>
   <url-pattern>/ * </url-pattern>
   <dispatcher>REQUEST</dispatcher>
   <dispatcher>FORWARD</dispatcher>
   <dispatcher>INCLUDE</dispatcher>
   <dispatcher>ERROR</dispatcher>
</filter-mapping>
<listener>
   <listener-class>org.apache.shiro.web.env.EnvironmentLoaderListener</listener-class>
</listener>
```

配置文件

```
[main]
# 默认登录页面
authc.loginUrl=/login.jsp

[users]
zhang=123,admin
wang=123,loginUser

[roles]
admin=*
loginUser=user:update,user:view

[urls]
# 指定路径的访问权限,等号右边为内置的Filter,没有权限会调到默认登录页面
/authenticated=authc
/login=anon
/users/**=user
/role=authc,roles[admin]
/permission=authc,perms["user:create"]
```

Servlet

```java
public class LoginServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        String error = null;
        try {
            subject.login(token);
        } catch (UnknownAccountException e) {
            error = "用户不存在";
        } catch (IncorrectCredentialsException e) {
            error = "用户名或密码错误";
        } catch (AuthenticationException e) {
            e.printStackTrace();
            error = e.getMessage();
        }
        if (error != null) {
            req.setAttribute("error", error);
        } else {
            req.setAttribute("success", true);
        }
        req.getRequestDispatcher("/login.jsp").forward(req, resp);
    }
}
```

页面

使用 shiro 的标签来控制页面内容的显示。

```html
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<html>
<head>
    <title>Login</title>
</head>
<body>

Hello Wold

<a href="login.jsp">Login</a>

<!--对于登录用户,显示用户名-->
<shiro:authenticated>
    <h3>Hello, <shiro:principal/></h3>
</shiro:authenticated>

<shiro:notAuthenticated>
    <h3>Hello, Guest</h3>
</shiro:notAuthenticated>

<shiro:hasRole name="admin">
     登录用户为管理员
</shiro:hasRole>

<!--显示当前用户的信息-->
<shiro:user>
    <h3>Welcome back <shiro:principal/> !</h3>
    <p>Not <shiro:principal/> ?</p>
    <p>Please signin <a href="login.jsp">here</a></p>
</shiro:user>
</body>
</html>

```

## 参考资料

- [跟我学 Shiro](http://jinnianshilongnian.iteye.com/category/305053)



---

可运行实例代码位于 `spring/first_shiro/` 下
