# Java Mail

- [Java Mail](http://www.oracle.com/technetwork/java/javamail/index.html)

## 安装

```xml
<dependency>
    <groupId>javax.mail</groupId>
    <artifactId>mail</artifactId>
    <version>1.4.7</version>
</dependency>
```

## 使用

### TLS

```java
Properties prop = new Properties();
prop.setProperty("mail.smtp.host", "smtp.gmail.com");
prop.setProperty("mail.smtp.auth", "true");
prop.setProperty("mail.smtp.port", "587");
prop.setProperty("mail.smtp.starttls.enable", "true");

Session session = Session.getInstance(prop, new Authenticator() {
    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(username, password);
    }
});
session.setDebug(true);

MimeMessage mimeMessage = new MimeMessage(session);
mimeMessage.setFrom(new InternetAddress("siriusgundam@gmail.com"));
mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress("siriusgundam@gmail.com"));
mimeMessage.setSubject("foobar");
mimeMessage.setContent("hello world", "text/html;charset=UTF-8");

Transport.send(mimeMessage);
```

### SSL

```java
Properties prop = new Properties();
prop.setProperty("mail.smtp.host", "smtp.gmail.com");
prop.setProperty("mail.smtp.auth", "true");
prop.put("mail.smtp.port", "465");
prop.put("mail.smtp.socketFactory.port", "465");
prop.put("mail.smtp.socketFactory.class",
        "javax.net.ssl.SSLSocketFactory");

Session session = Session.getInstance(prop, new Authenticator() {
    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(username, password);
    }
});
session.setDebug(true);

MimeMessage mimeMessage = new MimeMessage(session);
mimeMessage.setFrom(new InternetAddress("siriusgundam@gmail.com"));
mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress("siriusgundam@gmail.com"));
mimeMessage.setSubject("foobar2");
mimeMessage.setContent("hello world", "text/html;charset=UTF-8");

Transport.send(mimeMessage);
```

### Mime Message

```java
MimeMessage mimeMessage = new MimeMessage(session);

Multipart multipart = new MimeMultipart("alternative");
BodyPart messageBodyPart = new MimeBodyPart();
messageBodyPart.setText("hello world");
BodyPart htmlBodyPart = new MimeBodyPart();
htmlBodyPart.setContent("<h1>helloworld</h1>", "text/html");
multipart.addBodyPart(messageBodyPart);
multipart.addBodyPart(htmlBodyPart);

mimeMessage.setFrom(new InternetAddress("siriusgundam@gmail.com"));
mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress("siriusgundam@gmail.com"));
mimeMessage.setSubject("foobar3");
mimeMessage.setContent(multipart);

Transport.send(mimeMessage);
```
