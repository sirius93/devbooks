
## Git开发模型

- **集中式协同模型**
n个开发人员 - 共享版本库

- **社交网络式协同模型**
n个初级开发人员 - n个高级开发人员 - 核心开发人员 - 共享版本库
由核心开发人员进行审核，提交

## 安装

设置用户信息

```
git config --global user.name "John Doe"
git config --global user.email johndoe@example.com
```

检查配置信息

```
git config --list
```

## 创建 Git 仓库

1. 创建中央数据仓库（仓库管理员）
	```
	git init --bare <name>	//如 share.git
	```

	命令执行后被创建的目录

	- hooks	定制钩子事件
	- info	其中 exclude 文件定制排除到版本控制的文件
	- objects	git 对象库
	- refs	标识每个分支指向哪个提交
	- config	项目配置
	- description	项目描述
	- HEAD	指向当前分支的一个提交

2. 复制中央仓库到本地（开发人员）

	语法：
    ```
    git clone [from] [to]
    ```

	例

		```
		git clone /f/software/repository/git/shared.git/ .
		```
		 (注意有个点，表明当前目录)

3. 设置个人信息（开发人员）

    ```
    git config user.name "user1"
    git config user.email "user1@163.com"
    ```

    默认操作 `仓库.get/config` 文件
    --global 操作 `~/.gitconfig` 文件
    --system 操作 `/etc/.gitconfig` 文件

4. 跟踪文件，添加到缓存区（版本库的 index 文件）（每次提交都必须执行）（开发人员）

    ```
		git add <file>
		```

5. 提交到本地

    ```
    git commit <file>
    git commit	提交所有文件
    ```

    执行上述命令后会进入提交信息的 vi 界面，之后需要添加提交信息，以“#”开头的都将被忽略
    或者执行下面语句 commit 时直接添加提交信息
		```
    git commit -m 提交信息
		```

    也可以直接使用 "git commit -a" 来实现 add 并 commit 的操作，但是对新增文件无效。

6. Push 到中央仓库

    ```
    git push <path> <branch>
    ```

		或者

		```
	  git push origin master
		```
    - path 为 origin，表示从哪里 clone 就 Push 到哪里
    - head 分支 记录着当前操作的是哪个分支

7. Pull 代码

	```
  git pull
	```

## 移除出版本控制

```
echo "*.txt" > .gitignore
```

## 分支分类

- master 分支
- dev 分支
	dev 分支又可以分为
    - feature 分支
    - release 分支
    - hotifxes 分支

## 分支操作

创建分支

```
git branch <branch>
```

切换分支

```
git checkout <branch>
```

列出所有分支

```
git branch
```

合并分支

```
git merge <branch>
```

删除分支

```
git branch -d <branch>
```

## 远程仓库控制

查看远程仓库

```bash
git remote -v
```

抓取

```bash
git fetch [remote-name]
```

推送

```bash
git push origin master
```

列出标签

```bash
git tag
```

创建标签

```bash
git tag -a <version> -m <message>
```

## 其它操作

查看历史提交记录

```
git log
```

撤销某次提交，仅撤销 index，不修改文件

```
git reset --soft <commit_hash_from_git_log>
```

## Git 实践图

![](git-branch-1.png)

## 冲突实践

```
<<<<HEAD
当前分支
=====
合并的分支名
>>>>branch
```
