[TOC]

# Jade/Pug

Jade 是 Node.js 的模版引擎， 目前更名为 Pug。但是其本身也有多种语言的实现。Java 的实现为 [jade4j](https://github.com/neuland/jade4j)。

## 添加依赖

```xml
<dependency>
    <groupId>de.neuland-bfi</groupId>
    <artifactId>jade4j</artifactId>
    <version>1.1.4</version>
</dependency>
```

## 使用

编写模板，模板文件通常后缀为 `.jade`

src/resources/jade/index.jade

```jade
doctype html
html
  head
    title= pageName
  body
    ol#books
      for book in books
        if book.available
          li #{book.name} for #{book.price} €
```

渲染模版

```java
List<Book> books = new ArrayList<>();
books.add(new Book("The Hitchhiker's Guide to the Galaxy", 5.70, true));
books.add(new Book("Life, the Universe and Everything", 5.60, false));
books.add(new Book("The Restaurant at the End of the Universe", 5.40, true));

Map<String, Object> model = new HashMap<>();
model.put("books", books);
model.put("pageName", "My Bookshelf");

String html = Jade4J.render(JadeTest.class.getResource("/jade/index.jade"), model, true);
System.out.println(html);
```

生成结果

```
<!DOCTYPE html>
<html>
  <head>
    <title>My Bookshelf</title>
  </head>
  <body>
    <ol id="books">
      <li>The Hitchhiker's Guide to the Galaxy for 5.7 €</li>
      <li>The Restaurant at the End of the Universe for 5.4 €</li>
    </ol>
  </body>
</html>
```
