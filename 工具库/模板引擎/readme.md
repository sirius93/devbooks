[TOC]

# 模版引擎

## 实现

- [Jade/Pug](https://github.com/pugjs/pug)
- [Velocity](http://velocity.apache.org/)
- [FreeMarker](http://freemarker.incubator.apache.org/)
- [EJS](https://github.com/tj/ejs)

Velocity 比 FreeMarker 更轻量级，但功能也较弱。在普通页面上 Velocity 高于 JSP 高于 FreeMarker。但在复杂的页面上，FreeMarker 性能更高。

---

可运行实例代码位于 `spring/first_corejava/src/test` 的 `template` 目录下
