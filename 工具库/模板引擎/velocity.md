# Velocity

## 添加依赖

```xml
<dependency>
    <groupId>org.apache.velocity</groupId>
    <artifactId>velocity</artifactId>
    <version>1.7</version>
</dependency>
```

## 使用

编写模板，模板后缀通常为 `.vm`

src/main/resources/templates/helloworld.vm

```vm
<html>
<head>
    <title>$title</title>
</head>
<body>
<h1>$title</h1>

<p>${exampleObject.name} by ${exampleObject.developer}</p>

<ul>
    #set( $count = 1 )
    #foreach($system in $systems)
        <li>$count. ${system.name} from ${system.developer}</li>
        #set( $count = $count + 1 )
    #end
</ul>
</body>
</html>
```

渲染模板

```java
VelocityEngine engine = new VelocityEngine();
engine.init();

Template template = engine.getTemplate("src/main/resources/templates/helloworld.vm");

VelocityContext context = new VelocityContext();
context.put("title", "Vogella example");
context.put("exampleObject", new ValueExampleObject("Java object", "me"));

List<ValueExampleObject> systems = new ArrayList<ValueExampleObject>();
systems.add(new ValueExampleObject("Android", "Google"));
systems.add(new ValueExampleObject("iOS States", "Apple"));
systems.add(new ValueExampleObject("Ubuntu", "Canonical"));
systems.add(new ValueExampleObject("Windows7", "Microsoft"));
context.put("systems", systems);

StringWriter writer = new StringWriter();
template.merge(context, writer);
System.out.println(writer.toString());
```

生成结果

```
<html>
<head>
    <title>Vogella example</title>
</head>
<body>
<h1>Vogella example</h1>

<p>Java object by me</p>

<ul>
                <li>1. Android from Google</li>
                    <li>2. iOS States from Apple</li>
                    <li>3. Ubuntu from Canonical</li>
                    <li>4. Windows7 from Microsoft</li>
            </ul>
</body>
</html>
```
