[TOC]

# FreeMarker

## 添加依赖

```xml
<dependency>
    <groupId>org.freemarker</groupId>
    <artifactId>freemarker</artifactId>
    <version>2.3.23</version>
</dependency>
```

## 使用

编写模板，模板文件通常后缀为 `.ftl`

src/main/resources/templates/helloworld.ftl

```ftl
<html>
<head>
    <title>${title}</title>
</head>
<body>
<h1>${title}</h1>

<p>${exampleObject.name} by ${exampleObject.developer}</p>

<ul>
    <#list systems as system>
        <li>${system_index + 1}. ${system.name} from ${system.developer}</li>
    </#list>
</ul>

</body>
</html>
```

渲染模板

```java
// 1.Configure
Configuration cfg = new Configuration(new Version(2, 3, 23));
cfg.setDefaultEncoding("UTF-8");
cfg.setLocale(Locale.US);
cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
File dir = new File("src/main/resources/templates");
cfg.setDirectoryForTemplateLoading(dir);

// 2.Process Templates
// 2.1 Prepare the template input
Map<String, Object> input = new HashMap<String, Object>();
input.put("title", "Vogella example");
input.put("exampleObject", new ValueExampleObject("Java object", "me"));

List<ValueExampleObject> systems = new ArrayList<ValueExampleObject>();
systems.add(new ValueExampleObject("Android", "Google"));
systems.add(new ValueExampleObject("iOS States", "Apple"));
systems.add(new ValueExampleObject("Ubuntu", "Canonical"));
systems.add(new ValueExampleObject("Windows7", "Microsoft"));
input.put("systems", systems);

// 2.2. Get the template
Template template = cfg.getTemplate("helloworld.ftl");

// 2.3. Generate the output
Writer consoleWriter = new OutputStreamWriter(System.out);
template.process(input, consoleWriter);
```

生成结果

```
<html>
<head>
    <title>Vogella example</title>
</head>
<body>
<h1>Vogella example</h1>

<p>Java object by me</p>

<ul>
        <li>1. Android from Google</li>
        <li>2. iOS States from Apple</li>
        <li>3. Ubuntu from Canonical</li>
        <li>4. Windows7 from Microsoft</li>
</ul>

</body>
</html>
```
