# gRPC

## 安装

```xml
<dependencies>
    <dependency>
        <groupId>io.grpc</groupId>
        <artifactId>grpc-netty</artifactId>
        <version>1.0.3</version>
    </dependency>
    <dependency>
        <groupId>io.grpc</groupId>
        <artifactId>grpc-protobuf</artifactId>
        <version>1.0.3</version>
    </dependency>
    <dependency>
        <groupId>io.grpc</groupId>
        <artifactId>grpc-stub</artifactId>
        <version>1.0.3</version>
    </dependency>
</dependencies>

<build>
    <extensions>
        <extension>
            <groupId>kr.motd.maven</groupId>
            <artifactId>os-maven-plugin</artifactId>
            <version>1.4.1.Final</version>
        </extension>
    </extensions>
    <plugins>
        <plugin>
            <groupId>org.xolstice.maven.plugins</groupId>
            <artifactId>protobuf-maven-plugin</artifactId>
            <version>0.5.0</version>
            <configuration>
                <protocArtifact>com.google.protobuf:protoc:3.1.0:exe:${os.detected.classifier}</protocArtifact>
                <pluginId>grpc-java</pluginId>
                <pluginArtifact>io.grpc:protoc-gen-grpc-java:${grpc.version}:exe:${os.detected.classifier}
                </pluginArtifact>
            </configuration>
            <executions>
                <execution>
                    <goals>
                        <goal>compile</goal>
                        <goal>compile-custom</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```

## 使用

### 步骤

1. 编写 proto 文件，放在 `src/main/proto` 目录下
2. 执行 `mvn compile` 生成对应的接口
3. 编写服务端和客户端代码

### 编写 proto 文件

编写对应的服务接口 `service` 和消息体 `message`

例：

```proto
// 使用 proto3
syntax = "proto3";

// 基本设置
option java_multiple_files = true;
option java_package = "io.grpc.examples.helloworld";
option java_outer_classname = "HelloWorldProto";
option objc_class_prefix = "HLW";

package helloworld;

// 定义一个 service
service Greeter {
  rpc SayHello (HelloRequest) returns (HelloReply) {}
}

// 定义一个消息体
message HelloRequest {
  string name = 1;
}

// 定义一个消息回复体
message HelloReply {
  string message = 1;
}
```

### 服务端

1. 实现 proto 文件中定义的 Service

```java
class GreeterImpl extends GreeterGrpc.GreeterImplBase {

    public void sayHello(HelloRequest req, StreamObserver<HelloReply> responseObserver) {
        //接收参数
        System.out.println("收到的信息:" + req.getName());

        //返回结果
        HelloReply reply = HelloReply.newBuilder().setMessage(("Hello: " + req.getName())).build();
        responseObserver.onNext(reply);
        responseObserver.onCompleted();
    }
}
```

2. 创建 Server

```java
Server server = ServerBuilder.forPort(5000)
                .addService(new GreeterImpl())
                .build()
                .start();

//一直阻塞当前线程直到 server 被关闭                
server.awaitTermination();

//关闭 server
server.shutdown();
```


### 客户端

```java
//gRPC信道
ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port)
//默认 gRPC 为了安全而使用了 tls 连接，plain text 仅用于无需正式时的测试
    .usePlaintext(true)
    .build();

//阻塞/同步 存根
GreeterGrpc.GreeterBlockingStub blockingStub = GreeterGrpc.newBlockingStub(channel);

//创建请求
HelloRequest request = HelloRequest.newBuilder().setName(name).build();
HelloReply response = blockingStub.sayHello(request);

channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
```