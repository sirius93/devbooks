[TOC]

# Joda-Time

## 添加依赖

```xml
 <dependency>
    <groupId>joda-time</groupId>
    <artifactId>joda-time</artifactId>
    <version>2.9.1</version>
</dependency>
```

## 使用

### 基本使用

#### Joda Time

提供了代表当前时间的 `LocalDateTime` 和只包含日期的 `LocalDate` 以及用于与 JDK 对象交互的 `DateTime` 类。

```java
// 当前时间
LocalDate localDate = LocalDate.now();
LocalDateTime localDateTime = LocalDateTime.now();
DateTime dateTime = DateTime.now();
// 指定时间
LocalDateTime localDateTime = new LocalDateTime(2015, 9, 10, 11, 38, 2);
// 时间计算
DateTime dateTime1 = dateTime.plusDays(4);
// 是否为闰年
boolean isLeapYear = dateTime1.year().isLeap();
```

#### Java 8

只提供了 `LocalDateTime` 和 `LocalDate` 两个。

```java
// 当前时间
LocalDate localDate = LocalDate.now();
LocalDateTime localDateTime = LocalDateTime.now();
// 指定时间
LocalDateTime localDateTime = LocalDateTime.of(2015, 9, 10, 11, 38, 2);
// 时间计算
LocalDateTime localDateTime1 = localDateTime.plusDays(4);
// 是否为闰年
boolean isLeapYear = localDateTime1.toLocalDate().isLeapYear();  
```

### 时间格式化与解析

#### Joda Time

```java
DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd hh:mm:ss");
// 格式化
DateTime dateTime = new DateTime(2015, 9, 10, 11, 38, 2);
String formatTime = dateTime.toString("yyyy/MM/dd hh:mm:ss"));
formatTime = dateTime.format(formatter);
// 解析
DateTime dateTime = DateTime.parse("2013-11-21 18:11:03");
DateTime dateTime1 = format.parseDateTime("2013-11-21 18:11:03");
```

#### Java 8

```java
DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss");
// 格式化
LocalDateTime localDateTime = LocalDateTime.of(2015, 9, 10, 11, 38, 2);
String formatTime = localDateTime.format(formatter);
// 解析
LocalDateTime localDateTime = (LocalDateTime) formatter.parse("2013-11-21 18:11:03");
```

### 时间的计算

#### Joda Time

提供了 `Period` 用于加上指定的一段时间以及 `Duration` 用于加上指定时间间隔。

```java
DateTime dt = new DateTime(2005, 3, 26, 12, 0, 0, 0);
DateTime plusPeriod = dt.plus(Period.days(1));
DateTime plusDuration = dt.plus(new Duration(24L * 60L * 60L * 1000L));
Days days = Days.daysBetween(dt, plusDuration);
System.out.println(days.getDays());
```

#### Java 8

localDateTime.plus(Duration.ofMillis(24L * 60L * 60L * 1000L));

```java
LocalDateTime localDateTime = LocalDateTime.of(2015, 9, 10, 11, 38, 2);
LocalDateTime plusPeriod = localDateTime.plus(Period.ofDays(1));
LocalDateTime plusDuration = localDateTime.plus(Duration.ofMillis(24L * 60L * 60L * 1000L));
Period period = Period.between(localDateTime.toLocalDate(), plusPeriod.toLocalDate());
System.out.println(period.getDays());
```

### 获得时间戳

#### Joda Time

`Instant` 代表瞬时时间

```java
Instant instant = Instant.now();
//1453160981457
//1453160981457
System.out.println(instant.getMillis());
System.out.println(System.currentTimeMillis());
```

#### Java 8

`Instant` 代表瞬时时间

```java
Instant instant = Instant.now();
//1453161001
//1453161001854
System.out.println(instant.getEpochSecond());
System.out.println(System.currentTimeMillis());
```

### 时区支持

#### Joda Time

```java
DateTime tokyo = new LocalDateTime().toDateTime(DateTimeZone.forID("Asia/Tokyo"));
DateTime shanghai = tokyo.withZone(DateTimeZone.forID("Asia/Shanghai"));
```

#### Java 8

```java
ZonedDateTime tokyo = ZonedDateTime.of(LocalDateTime.now(), ZoneId.of("Asia/Tokyo"));
ZonedDateTime shanghai = tokyo.withZoneSameInstant(ZoneId.of("Asia/Shanghai"));
```



## 参考资料

- [日付操作(Joda Time)](https://terasolunaorg.github.io/guideline/public_review/ArchitectureInDetail/Utilities/JodaTime.html)
