[TOC]

# 时间工具

Java 原生的 Calendar 非常难用
- "月" 从0开始的设计非常反人类
- 创建时间时必须先 `getInstance()` 再调用 `set()` 设置各个字段而无法在构建阶段就指定
- 变更时间必须调用 `add()` 并传入对应的属性名。

解决这一难题的方法就是使用第三方的扩展包 Joda-Time 或者集成了 Joda-Time api 的 Java 8.
但是 Java 8 和 Joda-Time 本身还是有不少区别的。

## 实现

- [Joda-Time](http://www.joda.org/joda-time/)





------

可运行实例代码位于 `spring/first_jetty/src/test` 的 `joda` 目录下