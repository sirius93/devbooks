
# Gradle

目录

[TOC]

摘要

>定义远程仓库，定义本地仓库，发布到远程仓库，发布到本地仓库

##  仓库

###  远程仓库

#### Maven 中央仓库

```groovy
repositories {
    mavenCentral()
}
```

#### JCenter 仓库

```groovy
repositories {
    jcenter()
}
```

#### ivy 仓库

```groovy
repositories {
    ivy {
    	url "../repo"
	}
}
```

#### 自定义 Maven 仓库

```groovy
repositories {
    maven {
    	url "http://repo.mycompany.com/maven2"
	}
}
```

###  本地仓库

#### 本地 FlatDir

```groovy
repositories {
   flatDir {
        dirs 'libs'
   }
}
```

### 发布

#### 发布到 ivy 仓库

```groovy
uploadArchives {
    repositories {
        ivy {
            credentials {
                username "username"
                password "pw"
            }
            url "http://repo.mycompany.com"
        }
    }
}
```

####  发布到 Maven 仓库

```groovy
apply plugin: 'maven'

uploadArchives {
    repositories {
        mavenDeployer {
            snapshotRepository(url: RESP_SNAPSHOT_URL) {
                authentication(userName: RESP_SNAPSHOT_USERNAME, password: RESP_SNAPSHOT_PASSWORD)
            }
            repository(url: RESP_RELEASE_URL) {
                authentication(userName: RESP_RELEASE_USERNAME, password: RESP_RELEASE_PASSWORD)
            }

            pom.artifactId = POM_ARTIFACT_ID
            pom.groupId = POM_GROUP_ID
            pom.version = POM_VERSION

            pom.project {
                name POM_NAME
                description POM_DESCRIPTION
                packaging POM_PACKAGING
            }
        }
    }
}
```

#### 发布到本地仓库


```groovy
uploadArchives{
    repositories {
        flatDir {
            dirs 'repos'
        }
    }
}
```



