
目录

[TOC]

摘要

>project api，建立 Task（2 种），获得 Task，动态创建 Task，依赖外部 Task，指定执行顺序

## Project 和 Task

### 概述

运行时 gradle 会先成 project 对象，然后再生成 task 对象，对于不属于任何 task 的代码则生成为统一的 script 对象。

### Project

#### 获得 Project 对象

```groovy
project(projectName)
```

#### Project API

```groovy
task projApi << {
    println name    //projApi
    println project.name    //core
    println path    //:core:projApi
    println buildDir    //Users/me/Documents/bitbucket/steps_java/learn_gradle/core/build
    println projectDir  //Users/me/Documents/bitbucket/steps_java/learn_gradle/core
}
```

运行

```shell
gradle -q projApi
```

>当方法和属性没有在 build 脚本中定义时，gradle 会试图访问对应的 project 的成员

### Task

#### 建立 Task

第一种方式

```groovy
task hello {
	println "hello world"
}
```

以上代码定义了一个 Task，并指定了配置该 Task 时执行的代码。

第二种方式

```groovy
task hello << {
	println "hello world"
}
```

以上代码定义了一个 Task，并指定了执行此 Task 时执行的代码。实际为以下代码的简写形式

```groovy
task hello {
	doLast {
    	println "hello world"
    }
}
```

>task 有 configuration 和 action，当使用 `<<` 实际是定义了一个 action 的快捷方法

#### 获得某个 Task 对象

```groovy
project(projectName).taskName
```

或

```groovy
project(projectName).property(taskName)
```

#### 默认 Task

不指定任何 Task 时，gradle 会执行默认 Task

```groovy
task d1 << {
    println "d1"
}
task d2 << {
    println "d2"
}
defaultTasks "d1", "d2"
```

运行

```shell
gradle -q
```

#### 动态创建 Task

```groovy
4.times { counter ->
    task "task$counter" << {
        println "I'm task number $counter"
    }
}
```

运行

```shell
gradle -q task1
```

#### 追加行为

```groovy
task hi << {
    println "hi world"
}
hi.doFirst {
    println "first"
}
hi.doLast {
    println "last"
}
hi << {
    println "HELLO WORLD"
}
```

运行
```shell
gradle -q hi
```

输出

```
first
hi world
last
HELLO WORLD
```

#### 依赖外部 Task

```groovy
task taskX(dependsOn: ':core:hello') << {
    println "taskX"
}
```

运行

```shell
gradle -q taskX
```

或者

```groovy
task taskY << {
    println "taskY"
}
taskY.dependsOn {
    tasks.findAll { task ->
        task.name.startsWith("hello")
    }
}
```

运行
```shell
gradle -q taskY
```

#### 指定 Task 执行顺序

```groovy
task taskTwo<<{
    println "taskTwo"
}
task taskOne<<{
    println "taskOne"
}
taskTwo.mustRunAfter taskOne
```

运行

```shell
gradle -q taskTwo taskOne
```

#### 禁用指定 Task

```groovy
task taskThree<<{
    println "taskThree"
}
taskThree.enabled=false
```

运行

```shell
gradle -q taskThree
```




