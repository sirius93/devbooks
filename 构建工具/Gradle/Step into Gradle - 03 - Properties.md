
目录

[TOC]

摘要

>System Properties，Project Properties，Extra Properties，普通变量，属性文件

## Properties

### System Properties

定义 System 属性

格式

```shell
gradle taskName -D属性名=xxxx
```

在脚本中通过 `System.properties[属性名]` 来获取。

例

```groovy
task echo << {
    println System.getProperty("abc")   //123
    println System.getProperty("def")   //null
    if (hasProperty("zxc")) {
        println System.getProperty("zxc")
    }
}
```

运行

```
gradle -q echo -Dabc=123
```

>获取不存在的属性时会返回 null

### Project Properties

定义 Project 属性

格式

```shell
gradle taskName -P属性名=xxxx
```

在脚本中通过 `属性名` 来获取。

例

```groovy
task echoProj << {
    println foo         //bar
//    println foo2    //exception
    if (hasProperty("foo3")) {
        println foo3
    }
}
```

运行

```
gradle -q echoProj -Pfoo=bar
```

>获取不存在的属性会抛出异常，所以需要先进行判断


### Extra Properties

动态属性，必须使用 `ext` 在脚本中指定

```groovy
ext.属性名=xxx
```

- 定义在 task 外的为 project 属性，task 内的为 task 属性
- task 内部访问 project 的 ext 属性时，不要加前缀 "ext"。
- task 内部访问自身的 ext 属性时，前缀 "ext"可加可不加。

例

```groovy
ext.extFoo = "extProjectBar"
task echoExt << {
	//println ext.extFoo    //exception
    println extFoo  //extProjectBar

    ext.extFoo2 = "extTaskBar"
    println extFoo2 //extTaskBar
    println ext.extFoo2 //extTaskBar
}
```

运行

```
gradle -q echoExt
```

结合 SourceSets 的例子

```groovy
ext {
    springVersion = "3.1.0.RELEASE"
    emailNotification = "build@master.org"
}
sourceSets.all { ext.purpose = null }
sourceSets {
    main {
        purpose = "production"
    }
    test {
        purpose = "test"
    }
    plugin {
        purpose = "production"
    }
}
task printProperties << {
    println springVersion       //3.1.0.RELEASE
    println emailNotification   //build@master.org
    sourceSets.matching { it.purpose == "production" }.each { println it.name }
}
```

运行

```
gradle -q printProperties
```

输出

```
main
plugin
```

### 普通变量

使用关键字 `def` 定义

```groovy
def x = 1
task echoVar << {
    def y = 2
    println x   //1
    println y   //2
}
```

运行

```
gradle -q echoVar
```

### 属性文件

属性文件与 `build.gradle` 位于同文件夹，名为 `gradle.properties`。在 build 脚本中可以直接访问此文件中的属性。

定义属性

```java
POM_GROUP_ID=com.example
POM_ARTIFACT_ID=hello
POM_VERSION=0.0.1-SNAPSHOT
```

访问

```groovy
task info << {
    println POM_GROUP_ID   //com.example
}
```




