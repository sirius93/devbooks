
目录

[TOC]

摘要

>GUI 界面，常用 Task，运行指定 Task，查看 Task 列表，跳过指定 Task

## 基本概念

### Gradle 应用的组成

- Gradle 应用由两部分组成：`project` 和 `task`。
- 一个应用可以包括一个或多个 `project`
- 一个 `project` 可以包括一个或多个 `task`

###  build 脚本

- gradle 命令会执行当前目录的 build 脚本，即 `build.gradle` 文件。
- 默认 `build.gradle` 所在的目录名即为 `project 名`。
- 一个 build 脚本中也可以引入外部其它的 build 脚本，代码如下
```groovy
apply from: '../core/build.gradle'
```


### 基本 Gradle 指令

#### 显示 GUI 界面

``` shell
gradle --gui
```

#### 运行指定 Task

``` shell
gradle [-q] taskName
```

- `-q` 表示不输出构建中的 log 信息

#### 常用 Task

- `build`   编译，测试并打包
- `clean`   删除 build 文件夹
- `assemble`    编译和打包
- `check`   编译和测试

#### 查看 Task 列表

``` shell
gradle -q tasks
```

如果想同时查看 Task 间的依赖关系，可以加上 "--all" 关键字

```shell
gradle -q tasks --all
```

#### 查看 Project 列表

```shell
gradle -q projects
```

#### 查看 Project 所有属性

```shell
gradle properties
```

#### 查看 Project 的依赖关系

```shell
gradle -q dependencies
gradle -q dependencies core:dependencies groovy:dependencies
gradle -q dependencies --configuration specifiedTaskName
```

#### 跳过指定 Task

```shell
gradle build -x test
```

#### Task 名缩写

gradle 执行时不用指定完整的 Task 名，只要足够区分就行了。

#### 显示 build 报告

```shell
gradle build --profile
```

生成的报告在 `build/reports` 下



