
目录

[TOC]

摘要

>File Api，FileCollection Api，Copy

## 文件管理

### 创建文件夹

```groovy
task optFile << {
    //根目录为当前 Project
    def classesDir = new File('build/classes')
    classesDir.mkdirs()
}
```

###  File API

```groovy
task localFile << {
	// Using a relative path
    File configFile = file('src/config.xml')

	// Using an absolute path
    configFile = file(configFile.absolutePath)

	// Using a File object with a relative path
    configFile = file(new File('src/config.xml'))

	// File collections
    FileCollection collection = files('src/file1.txt',
            new File('src/file2.txt'),
            ['src/file3.txt', 'src/file4.txt'])
	// Iterate over the files in the collection
    collection.each {File file ->
        println file.name
    }
}
```

运行

```
gradle -q localFile
```

输出

```
file1.txt
file2.txt
file3.txt
file4.txt
```

### FileCollection API

```groovy
task listFile << {
    File srcDir

    // Create a file collection using a closure
    FileCollection collection = files { srcDir.listFiles() }
    collection + files('src3')

    srcDir = file('src')
    println "Contents of $srcDir.name"  //Contents of src
    collection.collect { relativePath(it) }.sort().each { println it }
    println "============================"

    srcDir = file('src2')
    println "Contents of $srcDir.name"  //Contents of src2
    collection.collect { relativePath(it) }.sort().each { println it }
}

//运行
gradle -q listFile
```

### 复制文件

第一种

```groovy
Copy myCopy = task(myCopy, type: Copy)
myCopy.from 'resources'
myCopy.into 'target'
myCopy.include('**/*.txt', '**/*.xml', '**/*.properties')
```

运行

```
gradle -q myCopy
```

第二种

```groovy
task copy2(type: Copy) {
    from 'resources'
    into 'target'
    include('**/*.txt', '**/*.xml', '**/*.properties')
}
```

运行

```
gradle -q copy2
```