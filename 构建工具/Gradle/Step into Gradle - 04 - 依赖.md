
目录

[TOC]

摘要

> 外部依赖，内部依赖

##  依赖

### 外部依赖

外部依赖存在于 maven，ivy，jcenter 或者本地文件系统

格式

`命令 group:name:version`

- 命令

	- configuration
	- compile
	- runtime
	- testCompile
	- testRuntime

例

```groovy
compile "com.example:hello:0.0.1"
```

### 内部依赖

- 内部依赖指 Task 间的依赖
- 依赖使用关键字 `dependsOn` 指定，`dependsOn` 可以为字面量或者字符串
- gradle 可以实现 lazy depend on，即需要依赖的工程可以写在被依赖的工程前

```groovy
task hello << {
    println "Hello world!"
}
task intro(dependsOn: hello) << {
    println "I'm Gradle"
}
```

运行

```groovy
gradle -q intro
```

输出

```
Hello world!
I'm Gradle
```

### Snapshot 的依赖问题

由于缓存机制的问题，同一版本的 Snapshot 代码默认每天只会被重新下载一次，需要下列设置后才能重新下载新的代码

```groovy
dependencies {
	compile ('example:core0.0.1-SNAPSHOT') { changing=true }
}

configurations.all {
	// check for updates every build
	resolutionStrategy.cacheChangingModulesFor 0, 'seconds'
}
```

```java
class Person {
	private String name;
	private int age;

	public static void main(String [] args) {
		String str = "hello world";
		System.out.println(str);
	}
}
```

在下载完型的代码后，可以暂时注释掉上述代码以防止每次编译都下载新代码

还有一种方法是在执行各种命令时加入参数`—refresh-dependencies` 来强制刷新依赖。
