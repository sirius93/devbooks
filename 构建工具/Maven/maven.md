[TOC]

# Maven

## Overview

### 搜索资源

中央仓库 [http://search.maven.org/#browse](http://search.maven.org/#browse)

### 中央仓库镜像

ibiblio.org [http://mirrors.ibiblio.org/maven2/](http://mirrors.ibiblio.org/maven2/)

jboss [https://repository.jboss.org/nexus/content/groups/public/](https://repository.jboss.org/nexus/content/groups/public/)


## 建立项目

```
mvn archetype:generate
```

## Maven命令

#### 生命周期命令

- mvn compile
    编译项目，编译后的文件放在 target 文件夹下

- mvn test-compile
    编译代码，再编译测试代码

- mvn test
    编译代码，再编译测试代码，再执行测试

- mvn package
    打包

- mvn install
    安装到jar包到本地仓库

   - 指定安装的方式
```
mvn install:install-file -Dfile=/Users/guest/Documents/pretty-time.jar -DartifactId=prettytime -Dversion=1.0.0 -Dpackaging=jar -DgroupId=org.ocpsoft
```
- mvn deploy
  安装到远程仓库

- mvn clean
  清除target目录

#### 其余命令

- mvn process-resources
  只处理资源文件

- mvn dependency:tree
  查看依赖树

- mvn dependency:sources
  下载依赖的源码

- mvn javadoc:jar
  生成javadoc文件

## 资源文件

### 动态替换文件属性

### 打开filtering功能

```xml
<build>
   <resources>
     <resource>
        <directory>src/main/resources</directory>
        <filtering>true</filtering>
     </resource>
   </resources>
 </build>
```

### 使用

- 引用pom.xml中的elements
  pom是pom.xml的根，所以只要使用${pom.version}就可以直接引用

- 引用指定的资源文件
```xml
<build>
	<filters>
      <filter>src/main/filters/testFilter.properties</filter>
   </filters>
   <resources>
     <resource>
        <directory>src/main/resources</directory>
        <filtering>true</filtering>
     </resource>
   </resources>
</build>
```

- 引用系统属性
  如`${java.version}`

- 引用pom.xml的<properties>指定的属性

## Profile

### 作用

满足指定条件时才激活特定的配置文件。可以用于不同os或不同jdk版本时切换不同的配置。

### 语法

```xml
<profiles>
    <profile>
        <id>develop</id>
        <jdk>1.5</jdk>
        <activation>
            <activeByDefault>true</activeByDefault>
        </activation>
    </profile>
    <profile>
        <id>production</id>
        <activation>
            <activeByDefault>true</activeByDefault>
        </activation>
    </profile>
</profiles>
```

其中<activation>指定激活条件，<activeByDefault>为默认执行的配置文件，即此时执行mvn命令时会使用此配置。

### 激活指定配置

- 运行时指定配置文件
  `mvn compile -p production`

- 根据jdk激活配置
  `<jdk>1.5</jdk>`
  `<jdk>[1.4,1.7)</jdk>`

- 根据os激活配置
```xml
<os>
	<name>Windows 7</name>
</os>
```

## pom.xml

### 基本结构

```xml
<modelVersion>4.0.0</modelVersion>
<groupId>com.juvenxu.mvnbook</groupId>
<artifactId>hello-world</artifactId>
<version>1.0-SNAPSHOT</version>
<name>Maven Hello World Project</name>
```

### 继承

任何没有声明parent的pom文件都会继承maven的默认pom.xml

使用时在子项目中指定父项目

如果父项目与子项目不在同一文件夹下则需要指定路径

```xml
<parent>
	...
    <relativePath>../dir/pom.xml</relativePath>
</parent>
```

### 聚合

在父项目中指定聚合在一起的子模块

```xml
<modules>
   <module>projectA</module>
   <module>../projectB</module>
</modules>
```

这里的module名其实是==文件夹名==

### 依赖

#### 依赖的范围

- compile
  - provided编译和测试时有效，运行时由jdk或容器提供
- runtime
- test
  - system依赖手动指定，而不是从仓库中引用
```xml
<scope>system</scope>
<systemPath>${java.home}/../lib/tools.jar</systemPath>
```

#### 依赖的传递性

A依赖B时默认也会依赖B的所有依赖
依赖传递优先选择深度最短的
路径相同时，优先选择先申明的

#### 可选依赖

即A依赖B，B依赖C，但是A依赖B的代码中用不到C，可以在B中对这部分依赖设置可选

```xml
<dependency>
    <groupId>org.example</groupId>
    <artifactId>template-maven-service</artifactId>
    <version>1.0-SNAPSHOT</version>
	<optional>true</optional>
</dependency>
```

#### 排除依赖

即A依赖B，B依赖C，但是A不需要B依赖的C

```xml
<dependency>
    <groupId>org.example</groupId>
    <artifactId>template-maven-service</artifactId>
    <version>1.0-SNAPSHOT</version>
    <exclusions>
        <exclusion>
            <groupId>org.ocpsoft.prettytime</groupId>
            <artifactId>prettytime</artifactId>
        </exclusion>
    </exclusions>
</dependency>
```

#### 强制刷新依赖

执行命令时添加参数 `-U`

#### dependencyManagement

设置在父项目中，集中管理依赖。
子模块不会自动继承，使用时无也需声明版本号。

### 仓库

镜像仓库

```xml
<repositories>
    <repository>
        <id>OS China</id>
        <name>OSChina</name>
        <url>http://maven.oschina.net/content/groups/public</url>
        <releases>
            <enabled>true</enabled>
        </releases>
        <snapshots>
            <enabled>false</enabled>
        </snapshots>
    </repository>
</repositories>

<pluginRepositories>
    <pluginRepository>
        <id>OS China</id>
        <name>OSChina</name>
        <url>http://maven.oschina.net/content/groups/public</url>
        <releases>
            <enabled>true</enabled>
        </releases>
        <snapshots>
            <enabled>false</enabled>
        </snapshots>
    </pluginRepository>
</pluginRepositories>
```

### 版本

#### 命名方法

总版本号-分支版本-小版本号-里程碑版本

#### 里程碑分类

- snapshot	快照
  - alpha内部测试版
  - beta外部测试版
  - release正式版
  - ga稳定版

## Nexus

hosted 内部仓库，分为
snapshots
releases
3rd party  第三方仓库，用于手动下载放入

## 插件
maven-source-plugin
对source打包，原来只能对代码打包
不指定execution就只能用mvn命令手动执行
指定在特定生命周期之后执行jar-no-fork命令

```xml
<executions>
	<execution>
    	<goals>
        	<phase>package</phase>
        	<goal>jar-no-fork</goal>
        </goals>
    </execution>
</executions>
```

maven-help-plugin

web项目
package为war
servlet,jsp api为provided,容器自带
war插件

发布web项目
jetty maven插件
