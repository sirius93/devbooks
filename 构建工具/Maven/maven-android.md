[TOC]

# Maven on Android

## 搭建环境

1. 确保配置了 ANDROID_HOME
2. 下载 maven-android-sdk-deployer
   [https://github.com/simpligility/maven-android-sdk-deployer](https://github.com/simpligility/maven-android-sdk-deployer)
3. 进入下载的根目录，执行
   ```shell
   mvn install
   ```
   ​	或者安装指定版本的 SDK

   ```shell
   mvn install -P 4.2
   ```

## 建立工程

执行以下命令

```shell
 mvn archetype:generate \
  -DarchetypeArtifactId=android-quickstart \
  -DarchetypeGroupId=de.akquinet.android.archetypes \
  -DarchetypeVersion=1.0.6 \
  -DgroupId=your.company \
  -DartifactId=my-android-application
```

## Maven 命令

- mvn android:help -Ddetail=true
  显示 maven android 帮助信息，detail 表示详细信息

- mvn otherCmd -Dmaven.test.skip=true
  用于任何 maven 命令，跳过 test 程序

- mvn android:deploy
  部署 apk 到连接到的设备

- mvn android:undeploy
  从连接到的设备中删除 apk

- mvn android:redeploy
  执行 undeploy 和 deploy

- mvn android:run

## 依赖

### 依赖 Java 工程

同普通 Maven Java 项目一致

### 依赖其它 Android 项目

```xml
<dependency>
    <groupId>com.simpligility.android</groupId>
    <artifactId>intents</artifactId>
    <version>0.1</version>
    <type>apk</type>
</dependency>
```

### 依赖其它 Android Source

```xml
<dependency>
    <groupId>com.simpligility.android</groupId>
    <artifactId>tools</artifactId>
    <version>0.1</version>
    <type>apklib</type>
</dependency>
```

### 依赖 Support 包

```xml
<dependency>
  <groupId>com.android.support</groupId>
  <artifactId>support-v4</artifactId>
  <version>x.y.z</version>
  <type>aar</type>
</dependency>
```

## 其它问题

### 使用 Eclipse 会遇到的问题

1. Plugin execution not covered by lifecycle configuration: com.jayway.maven.plugins.android.generation2:android-maven-plugin:3.5.0:manifest-update (execution: manifestUpdate, phase: process-resources) pom.xml

   解决方法
   在 pom.xml 中添加如下代码
```xml
<pluginManagement>
    <plugins>
        <plugin>
            <groupId>org.eclipse.m2e</groupId>
            <artifactId>lifecycle-mapping</artifactId>
            <version>1.0.0</version>
        	<configuration>
                <lifecycleMappingMetadata>
                    <pluginExecutions>
                        <pluginExecution>
                            <pluginExecutionFilter>
                                <groupId>com.jayway.maven.plugins.android.generation2</groupId>
                                <artifactId>android-maven-plugin</artifactId>
                                <versionRange>3.5.0</versionRange>
                                <goals>
                                    <goal>manifest-update</goal>
                                </goals>
                            </pluginExecutionFilter>
                        </pluginExecution>
                    </pluginExecutions>
                </lifecycleMappingMetadata>
			</configuration>
		</plugin>
    </plugins>
</pluginExecutionFilter>
```



## 其它资料

教程
1. Building Android Projects with Maven
   http://spring.io/guides/gs/maven-android/


